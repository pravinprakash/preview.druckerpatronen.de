<?php
$tsId = 'X9B616552B35F3AAE4E92C7FF9DD83756';
$cacheFileName = $tsId.'.xml';
$cacheTimeOut = 43200; // half a day
$apiUrl = 'http://www.trustedshops.com/api/ratings/v1/'.$tsId.'.xml';
$xmlFound = false;
// check if cached version exists
if (!cachecheck($cacheFileName, $cacheTimeOut)) {
// load fresh from API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    $output = curl_exec($ch);
    curl_close($ch);
// Write the contents back to the file
    // Make sure you can write to file's destination
    file_put_contents($cacheFileName, $output);
}
if ($xml = simplexml_load_file($cacheFileName)) {
    $xPath = "/shop/ratings/result[@name='average']";
    $result = (float) $xml -> xpath($xPath)[0];
    $max = "5.00";
    $count = $xml->ratings["amount"];
    $shopName = $xml->name;
    $xmlFound = true;
}
if ($xmlFound) { ?>
<a href="http://www.trustedshops.de/shop-info/trusted-shops-kundenbewertungen/" rel="nofollow">Kundenbewertungen von Trustedshops</a>:<span xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate"> <span rel="v:rating"><span property="v:value"><?php echo $result;?> </span> </span> / <span property="v:best"><?php echo $max;?> </span> bei <span property="v:count"><?php echo $count;?> </span> <a href="https://www.trustedshops.de/bewertung/info_<?php echo $tsId?>.html" rel="nofollow" title="<?php echo $shopName;?> bewertungen">Bewertungen</a> </span>
<?php }
    function cachecheck($filename_cache, $timeout = 10800) {
    if (file_exists($filename_cache) && time() - filemtime($filename_cache) < $timeout) {
        return true;
    }
        return false;
    }