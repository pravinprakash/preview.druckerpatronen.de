<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Sofortueberweisung
  * @copyright  Copyright (c) 2008 [m]zentrale GbR 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Varien_Data_Form_Element_Linkpc extends Varien_Data_Form_Element_Abstract
{
	public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('label');
    }

    public function getElementHtml()
    {
    	
		#echo $this->getConfigDataWeb('base_url');
		#echo "<pre>";		
		#print_r($params = $this->getParams()->toArray());
		#echo "</pre>";
		$params = $this->getParams()->toArray();
		foreach($params AS $key => $val){
			switch($key){
				case 'backlink':
					$params[$key] = $this->getConfigDataWeb('base_url').$val;
				break;
				case 'projectssetting_interface_success_link':
					$params[$key] = $this->getConfigDataWeb('base_url').$val;
				break;
				case 'projectsnotification_http_url':
					$params[$key] = $this->getConfigDataWeb('base_url').$val;
				break;
				case 'projectssetting_interface_cancel_link':
					$params[$key] = $this->getConfigDataWeb('base_url').$val;
				break;
				case 'projectssetting_interface_timeout_link':
					$params[$key] = $this->getConfigDataWeb('base_url').$val;
				break;
				case 'projectssetting_project_password':
					$params[$key] = $this->getPassword();
				break;
				default:
					$params[$key] = $val;
				break;		
			}
		}
		
		$queryString = http_build_query($params);
		
		$html = $this->getBold() ? '<strong>' : '';
    	$html.= sprintf($this->getValue(),$this->getConfigDataPayment('url_new').'?'.$queryString);
    	$html.= $this->getBold() ? '</strong>' : '';
    	$html.= $this->getAfterElementHtml();
    	return $html;
    }
	
	public function getConfigDataPayment($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $path = 'payment/paycode/'.$field;
        return Mage::getStoreConfig($path, $storeId);
    }
	
	public function getConfigDataWeb($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $path = 'web/unsecure/'.$field;
        return Mage::getStoreConfig($path, $storeId);
    }
	
	public function getParams()
    {
        $_types = Mage::getConfig()->getNode('global/params_pc/types')->asArray();
		$params = Mage::getModel('sofortueberweisung/params');		
        foreach ($_types as $data) {            
			$params->setData($data["param"],$data["value"]);
        }		
		return $params;
    }
	
	/*
	public function getPassword(){
		return substr(md5(uniqid(rand(), true)),0,8);
	}*/
	
	function getPassword($length = 9)
	{
		$min_lenght = 9;
		$stack = array();

		// srand nur für PHP versionen < 4.2 erforderlich
		srand ( (float) microtime() * 1000000);

		$component = array(
				'aeiou',
				'bdghjlmnpqrstvwxyz',
				'AEIOU',
				'BDGHJLMNPQRSTVWXYZ',
				'123456789',				
				 );

		if ($length < $min_lenght) {
			$length = $min_lenght;
		}

		$n = count ($component);

		for ($i = 0; $i < $n; $i++) {
			$s = $component[$i];
				$stack[] = $s [mt_rand() % strlen($s)];
		}

		for ($i = $n; $i < $length; $i++) {
				$r = rand() % $n;
			$s = $component[$r];
				$stack[] = $s[mt_rand() % strlen($s)];
		}

		shuffle ($stack);
		$pwd = join('', $stack);
		return $pwd;
	}

}