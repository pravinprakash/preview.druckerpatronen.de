<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require 'app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$file = 'feeds/confianza_feed_2.csv';
$csv = new Varien_File_Csv();

$csvdata = array();

$csvdata[] = array('id','title','description','availability','google_product_category','condition','brand','color','image_link','link','Mpn','Price','Product_type','Shipping(Country:Price)','Adwords_labels','Sku');

$csv->setDelimiter(',');

//$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product').' cp , ' . $resource->getTableName('catalog_product_entity_varchar')." cpev where cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') and cp.product_id = cpev.entity_id ";
$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_varchar')." cpev where cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') ";
$products = $read->fetchAll($query); 

$pobj = Mage::getSingleton('catalog/product');

$i = 1;

foreach($products as $pe){

    $pid = $pe['entity_id'];

    $product = Mage::getModel('catalog/product')->load($pid); 
    
    if( $product->getStatus() != 1){
        continue;
    }

    $original = _getAttr($pid,'original');
    $color    = _getAttr($pid,'color');
    $ean      = _getAttr($pid,'ean');
    $img      = _getAttr($pid,'img_big');
    
    $printer    = _getAttrC($pid,'printer_manufacturer');
    $printer_all = implode(" ",array_unique(array_diff($printer, _getAttrC($pid,'printer_manufacturer',true) ) ));
    
    $serie      = _getAttrC($pid,'serie');
    
    $sku = _getSku($pid);

//defined1
    $user_defined = _getDisplay($sku);
    $defined1 = $user_defined['defined1'];
    $defined2 = $user_defined['defined2'];
    $defined3 = $user_defined['defined3'];
    $brand = $user_defined['Marke'];
    
    $pack = "";

    if( substr($sku, -2) == 'dp' ){
        
        $pack = "Doppelpack";
        
    }else if( substr($sku, -3) == 'sp4' ){
        
        $pack = "Sparset 4x";

     }else if( substr($sku, -3) == 'sp5' ){
        
        $pack = "Sparset 5x";

    }

    $pack = "";

    if( substr($sku, -2) == 'dp' ){
        
        $pack = "Doppelpack";
        
    }else if( substr($sku, -3) == 'sp4' ){
        
        $pack = "Sparset 4x";

     }else if( substr($sku, -3) == 'sp5' ){
        
        $pack = "Sparset 5x";

    }

    $original_t = $original_d = "";

    if( $original == '1' ){
    
        $original_t = $original_d = "Original";
        
    }else{

        $original_d = "Alternative";

    }
    
    $name_type = $desc_type = "";
    
    if( $pe['value'] == 'toner' ){

        $name_type = "Toner";
        $desc_type = "Günstig Toner";

    }else{

        $name_type = "Tinte Patronen";
        $desc_type = "Günstig Druckerpatronen";
        
    }
    
    if( $serie == NULL || $serie == 'NULL' ){
        $serie = "";
    }
    
    if( $defined1 == NULL && $defined1 == 'NULL' ){
        $defined1 = "";
    }

    $name = array($brand,$defined2,$name_type,$original_t,$defined3,$color,$pack);
    $name = array_diff( $name, array( '' ) );

    $name_str = implode(" ",$name);
    if(strlen($name_str) > 70 ){
        unset($name[0]);
        $name_str = implode(" ",$name);
    }
    $desc = array($desc_type,$color,$pack,"für",$brand,$serie,$printer.".","Kaufen Sie",$original_d,$brand,"Patronen",$defined1,"für",$brand,$serie,$printer,$printer_all);
    $desc = array_diff( $desc, array( '' ) );
    $desc_str = implode(" ",$desc);
    
    $product_data = array(
                            $i,
                            $name_str,
                            $desc_str,
                            'In stock',
                            'Elektronik > Drucker-, Kopierer-, Scanner- & Faxzubehör > Druckerzubehör > Drucker-Verbrauchsmaterial > Toner- & Inkjet-Kartuschen',
                            'new',
                            $brand,
                            $color,
                            $img,
                            $product->getUrlInStore(),
                            $ean,
                            round($product->getPrice() * 1.19, 2),
                            $product->getTypeID(),
                            'DE:3.90 EUR',
                            "Artikel [".ucfirst($pe['value'])."]",
                             $sku,
                         );
 
    $csvdata[] = $product_data;
    
    if( $i > 500   ){
        
        break;
        
    }
    
    $i++;
}
 
$csv->saveData($file, $csvdata);

function _getStock($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT qty FROM ' . $resource->getTableName('cataloginventory_stock_item')." where product_id  = '$pid' ";
    return $read->fetchOne($query); 

}
function _getSku($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT sku FROM ' . $resource->getTableName('catalog_product_entity')." where entity_id  = '$pid' ";

    return $read->fetchOne($query); 

}
function _getDisplay($sku){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = "SELECT * FROM user_defined_table where sku = '$sku' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    return $ad[0];

}
function _getAttrC($pid,$name,$all = false){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    
    if( $all ){
        
        $carray = array();
        
        $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $c_all = $read->fetchAll($query); 
        foreach($c_all as $ca){
            $carray[] = _getAttr($ca['category_id'],$name,'category');
        }
        return $carray;
    }else{
        
        $query = 'SELECT category_id FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $cid = $read->fetchOne($query); 
        return _getAttr($cid,$name,'category');
        
    }
}
$product_attributes = array();
function _getAttr($eid,$name,$type_s='product'){
    
    $ad = _getAttrId($name);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    
    if( $type_s == 'product' ){

        $table = "catalog_product_entity_";

    }else{
        $table = "catalog_category_entity_";
        
    }
    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}
$atts_ids = array();
function _getAttrId($name){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

/*
//Magento does not load all attributes by default
//Add as many as you like
$products->addAttributeToSelect('name');
foreach($products as $product) {
//do something
}*/
