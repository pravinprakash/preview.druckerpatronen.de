<?php
/**
 * User: Markus G.
 * Date: 05.09.2014
 * Wabsolute GmbH
 */

require_once 'app/Mage.php';
Mage::app(0);

$start = time();

// DB write Connection
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');

// relevant tables
$tableEavAttribute = $resource->getTableName('eav/attribute');
$tableCatalogProduct = $resource->getTableName('catalog/product');
$tableCatalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
$tableCatalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
$tableCatalogProductEntityDecimal = $resource->getTableName('catalog_product_entity_decimal');

// Queries for product update
$productVisibilityQuery = $writeConnection->query("SELECT attribute_id FROM $tableEavAttribute WHERE attribute_code = 'visibility'")->fetchColumn(0);
$productStatusQuery = $writeConnection->query("SELECT attribute_id FROM $tableEavAttribute WHERE attribute_code = 'status'")->fetchColumn(0);
$productSparpreisQuery = $writeConnection->query("SELECT attribute_id FROM $tableEavAttribute WHERE attribute_code = 'sparpreis'")->fetchColumn(0);
$productPriceQuery = $writeConnection->query("SELECT attribute_id FROM $tableEavAttribute WHERE attribute_code = 'price'")->fetchColumn(0);

// define relevant product collection
$productIds = Mage::getModel('catalog/product')->getCollection()
    ->addFieldToFilter(
        'type2',
        array(
            array('eq' => 'druckerpatrone'),
            array('eq' => 'toner'),
        )
    )
//    ->addFieldToFilter(
//        'original', array('eq' => 0)
//    )
    ->addFieldToFilter('sku', array('in' => array('33891')))
    ->getAllIds();

// Get all relevant products
$product = Mage::getModel('catalog/product');
$productResource = $product->getResource();

// counter
$productsCount = count($productIds);
echo $productsCount . "\n";
$output = "";

foreach ($productIds as $index => $singleProductId) {
    $output = "";

    // get price of current product
    $singlePrice = $productResource->getAttributeRawValue($singleProductId, 'price', 0);

    // load
    $singleProduct = $product->load($singleProductId);
    $singleProductSku = $singleProduct->getSku();

    // create skus for the product packs
    $packProducts = array(
        'dp' => $singleProductSku . 'dp',
        'sp4' => $singleProductSku . 'sp4',
        'sp5' => $singleProductSku . 'sp5',
        'sp10' => $singleProductSku . 'sp10',
    );

    // if $singleProductSku contains dp, sp4, sp5 or sp10 continue the foreach
    foreach ($packProducts as $pack => $skuWithPack) {
        if (strpos($singleProductSku, $pack) !== false) {
            continue 2;
        }
    }

    // reset sparPreis and update the value for the single product
    $sparPreis = '';
    $sparPreis .= "1:" . round(($singlePrice * 1.19), 2) . ";";

    // run through all packs for the single product
    foreach ($packProducts as $state => $productPackSku) {
        // reset variables
        $productPackSparPreis = null;
        $productPackPrice = null;
        $continue = false;
        $packProduct = null;

        // load current product pack
        $productPackId = Mage::getModel('catalog/product')->getIdBySku($productPackSku);
        $packProduct = Mage::getModel('catalog/product')->load($productPackId);

        // if product could not be loaded
        if ($packProduct && !$packProduct->getId()) {
            // set sparPreis accordingly ...
            switch ($state) {
                case 'dp':
                    $sparPreis .= "2:0;";
                    break;
                case 'sp4':
                    $sparPreis .= "4:0;";
                    break;
                case 'sp5':
                    $sparPreis .= "5:0;";
                    break;
                case 'sp10':
                    $sparPreis .= "10:0;";
                    break;
            }

            // ... and continue
            continue;
        }

        // get type of product pack
        $type = $packProduct->getType2();

        // set sparpreis accordingly to the singlePrice and state
        if ((float)$singlePrice > 5 && ($state == 'sp5' || $state == 'sp10')) {
            $sparPreis .= ($state == 'sp5' ? '5' : '10') . ":0;";
            $continue = true;;
        } elseif ((float)$singlePrice < 5 && ($state == 'dp' || $state == 'sp4')) {
            $sparPreis .= ($state == 'dp' ? '2' : '4') . ":0;";
            $continue = true;;
        }

        if ($continue) {
            $status = '2';
            $vis = '1';
            $sqlS = "UPDATE $tableCatalogProductEntityInt SET value = $status WHERE attribute_id = $productStatusQuery AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $tableCatalogProduct WHERE sku = '$productPackSku');";
            $sqlV = "UPDATE $tableCatalogProductEntityInt SET value = $vis WHERE attribute_id = $productVisibilityQuery AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $tableCatalogProduct WHERE sku = '$productPackSku');";
            try {
                $writeConnection->query($sqlS . $sqlV);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            continue;
        } else {
            $status = '1';
            $vis = '4';
        }

        $sqlS = "UPDATE $tableCatalogProductEntityInt SET value = $status WHERE attribute_id = $productStatusQuery AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $tableCatalogProduct WHERE sku = '$productPackSku');";
        $sqlV = "UPDATE $tableCatalogProductEntityInt SET value = $vis WHERE attribute_id = $productVisibilityQuery AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $tableCatalogProduct WHERE sku = '$productPackSku');";

        try {
            $writeConnection->query($sqlS . $sqlV);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $price = round(($singlePrice * 1.19), 2);
        if ($state == 'sp5') {
            $productPackSparPreis = ($price * 5) - 4;
            $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            $packProduct->setPrice($productPackPrice);
            $sparPreis .= "5:" . number_format(round($productPackSparPreis, 2), 2) . ";";
        } elseif ($state == 'sp10') {
            $productPackSparPreis = ($price * 10) - 10;
            $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            $packProduct->setPrice($productPackPrice);
            $sparPreis .= "10:" . number_format(round($productPackSparPreis, 2), 2) . ";";
        } elseif ($state == 'dp') {
            if ($type == 'toner') {
                $productPackSparPreis = ($price * 2) - 1.50;
                $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            } elseif ($type == 'druckerpatrone') {
                $productPackSparPreis = (($price / 100) * 98) * 2;
                $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            }
            if ($productPackPrice) {
                $packProduct->setPrice($productPackPrice);
            }
            $sparPreis .= "2:" . number_format(round($productPackSparPreis, 2), 2) . ";";
        } elseif ($state == 'sp4') {
            if ($type == 'toner') {
                $productPackSparPreis = ($price * 4) - 5;
                $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            } elseif ($type == 'druckerpatrone') {
                $productPackSparPreis = (($price / 100) * 98) * 4;
                $productPackPrice = round(($productPackSparPreis / 1.19), 4);
            }
            if ($productPackPrice) {
                $packProduct->setPrice($productPackPrice);
            }
            $sparPreis .= "4:" . number_format(round($productPackSparPreis, 2), 2) . ";";
        }
        try {
            if ($packProduct && $packProduct->getId()) {

                $productPrice = $packProduct->getPrice();
                $spProductId = $packProduct->getId();
                $priceSql = "UPDATE $tableCatalogProductEntityDecimal SET value = '$productPrice' WHERE attribute_id = $productPriceQuery AND entity_type_id = 4 AND entity_id = $spProductId;";

                try {
                    $writeConnection->query($priceSql);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                $output .= $productPackSku . ',' . $productPrice . "\n";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    try {
        $spSql = "UPDATE $tableCatalogProductEntityVarchar SET value = '$sparPreis' WHERE attribute_id = $productSparpreisQuery AND entity_type_id = 4 AND entity_id = $singleProductId;";

        echo $singleProductSku . "," . $sparPreis . "\n" . $output;

        $writeConnection->query($spSql);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

$end = time();
$laufzeit = $end-$start;

echo "Laufzeit: ".$laufzeit." Sekunden! \n";