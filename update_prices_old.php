<?php
/**
 * User: Markus G.
 * Date: 05.09.2014
 * Wabsolute GmbH
 */

require_once 'app/Mage.php';
Mage::app(0);
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');

$t = $resource->getTableName('catalog_product_entity_int');
$tv = $resource->getTableName('catalog_product_entity_varchar');
$at = $resource->getTableName('eav/attribute');
$pt = $resource->getTableName('catalog/product');
$v = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'visibility'")->fetchColumn(0);
$s = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'status'")->fetchColumn(0);
$sp = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'sparpreis'")->fetchColumn(0);

$productIds = Mage::getModel('catalog/product')->getCollection()
    ->addFieldToFilter('type2', 'druckerpatrone')
    ->getAllIds();

$product = Mage::getModel('catalog/product');
$resource = $product->getResource();

foreach ($productIds as $index => $productId) {
    $price = $resource->getAttributeRawValue($productId, 'price', 0);
    if ($price != '3.3529' && $price != '3.3500') {
        continue;
    }
    $product = $product->load($productId);
    $sku = $product->getSku();
    var_dump($sku);
    var_dump($product->getSparpreis());


    $packProducts = array(
        'dp' => $sku . 'dp',
        'sp4' => $sku . 'sp4',
        'sp5' => $sku . 'sp5',
        'sp10' => $sku . 'sp10',
    );
    //$product->setSparpreis('1:3.99;2:0;4:0;5:15.95;10:29.9');
    $sparPreis = '1:3.99;2:0;4:0;5:15.95;10:29.9';
    try {
        $spSql = "UPDATE $tv SET value = '$sparPreis' WHERE attribute_id = $sp AND entity_type_id = 4 AND entity_id = $productId;";
        //var_dump($spSql);
        //$writeConnection->query($spSql);
        //$product->save();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    foreach ($packProducts as $state => $sku) {
        $spProduct = null;
        if ($state == 'sp5' || $state == 'sp10') {
            $status = '1';
            $vis = '4';
            $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
            $spProduct = Mage::getModel('catalog/product')->load($productId);
        } else {
            $status = '2';
            $vis = '1';
        }
        $sqlS = "UPDATE $t SET value = $status WHERE attribute_id = $s AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $pt WHERE sku = '$sku');";
        $sqlV = "UPDATE $t SET value = $vis WHERE attribute_id = $v AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $pt WHERE sku = '$sku');";

        if ($state == 'sp5') {
            $spProduct->setPrice(13.4033);
        } elseif ($state == 'sp10') {
            $spProduct->setPrice(25.126);
        }
        try {
            if ($spProduct && $spProduct->getId()) {
                $spProduct->save();
                var_dump('saved ' . $sku);
            }

            //$writeConnection->query($sqlS . $sqlV);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    //$price = $product->getPrice();
    //
//    if ((string)$price != '3.35') {
//        continue;
//    }
    //var_dump($product->getFinalPrice());
    //var_dump($product->getData());

}
