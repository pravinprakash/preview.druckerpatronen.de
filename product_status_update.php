<?php
die(__FILE__);
require_once 'app/Mage.php';

try {
    Mage::app(0);

    $csv = new Varien_File_Csv();
    $csv->setDelimiter(';')
        ->setEnclosure('"');
    $data = $csv->getData(Mage::getBaseDir('var') . '/import/' . 'products_import.csv');


    $resource = Mage::getSingleton('core/resource');
    $writeConnection = $resource->getConnection('core_write');

    $skip = true;
    foreach ($data as $index => $row) {
        if ($skip == true) {
            $skip = false;
            continue;
        }

        $sku = $row[4];
        $status = $row[16];
        $vis = $row[17];

        if (strpos($sku, 'dp') === false && strpos($sku, 'sp') === false) {
            continue;
        }
        echo "$sku\n";
        $t = $resource->getTableName('catalog_product_entity_int');
        $at = $resource->getTableName('eav/attribute');
        $pt = $resource->getTableName('catalog/product');
        $v = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'visibility'")->fetchColumn(0);
        $s = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'status'")->fetchColumn(0);

        $sqlS = "UPDATE $t SET value = $status WHERE attribute_id = $s AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $pt WHERE sku = '$sku');";
        $sqlV = "UPDATE $t SET value = $vis WHERE attribute_id = $v AND entity_type_id = 4 AND entity_id = (SELECT entity_id FROM $pt WHERE sku = '$sku');";
//var_dump($sqlS, $sqlV);
//        var_dump($sqlS, $sqlV);
        $writeConnection->query($sqlS . $sqlV);

    }
} catch (Exception $e) {
    echo $e->getMessage();
}



