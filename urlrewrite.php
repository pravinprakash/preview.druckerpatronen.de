<?php

define('MAGENTO', realpath(dirname(__FILE__)));
set_time_limit (0);

set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
error_reporting (E_ALL|E_STRICT);

require 'app/Mage.php';

umask(0);
Mage::app();

try
{
	$model = new Mage_Catalog_Model_Category;
	$root = $model->setStoreId(1)->load(2);
	$result = $urlPath = array();
	if($root->hasChildren()){
		$categoryIds = $root->getChildren();
		$categories = Mage::getResourceModel('catalog/category_collection')->addAttributeToSelect('*')->addIdFilter($categoryIds);	
		foreach($categories as $category){
			if($category->hasChildren()){
				$children = $category->getChildren();
				$subcats = Mage::getResourceModel('catalog/category_collection')->addAttributeToSelect('serie')->addIdFilter($children);
				$series = array();
				foreach($subcats as $subcat) {
					array_push($series, $subcat->getSerie());
					array_push($urlPath, str_replace(Mage::getBaseUrl(), '', $subcat->getUrl()));
				}
				$series = array_unique($series);
				asort($series);
				$rewrite = Mage::getModel('core/url_rewrite');
				foreach($series as $serie){
					if(null == $serie) continue;
					$str = str_replace(" ", "_", $serie);
					$id = $rewrite->loadByRequestPath($category->getUrlKey() . "/" . strtolower($str))->getId();
					$url = $category->getUrl() . "/". strtolower($str) . "/";
					$result[] = str_replace(Mage::getBaseUrl(), '', $url);
				}
			}
		}
	}

	foreach($result as $key => $path){	
		$path = strtolower(trim($path, '/'));
		if(in_array($path, $urlPath)) {
			$rewrite = Mage::getModel('core/url_rewrite')->loadByRequestPath($path);
			if($rewrite->getId()) $rewrite->delete();
			unset($result[$key]);
		}
	}
	unset($urlPath);
	
	foreach($result as $path){	
		$rewrite = Mage::getModel('core/url_rewrite')->loadByRequestPath($path);
		if($rewrite->getId()) continue;
		Mage::getModel('core/url_rewrite')->setIsSystem(0)->setOptions('RP')->setIdPath('custom-'.$path)->setTargetPath('')->setRequestPath($path)->save();		
	}	
} catch (Mage_Core_Exception $e) {
	echo $e->getMessage();
}
echo 'end';
exit;