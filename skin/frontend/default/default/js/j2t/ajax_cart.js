/**
 * J2T-DESIGN.
 *
 * @category   J2t
 * @package    J2t_Ajaxcheckout
 * @copyright  Copyright (c) 2003-2009 J2T DESIGN. (http://www.j2t-design.com)
 * @license    GPL
 */
/*
var loadingW = 260;
var loadingH = 50;
var confirmW = 260;
var confirmH = 134;
*/
var inCart = false;

if (window.location.toString().search('/product_compare/') != -1){
	var win = window.opener;
}
else{
	var win = window;
}

if (window.location.toString().search('/checkout/cart/') != -1){
    inCart = true;
}

// Pixelmechanics FL 20.12.2012: Minus wird abgebildet als Remove + Add
function removeOne(removeUrl, addUrl, qty, removeId, productSku) {
	
	if(qty > 1) {
		addUrl += 'addqty/' + (qty - 1) + '/remove/' + removeId + '/';
		
		setLocation(addUrl, productSku);
	} else {
		setLocation(removeUrl, productSku);
		jQuery('#' + productSku).removeClass('product-shop-cart');
		jQuery('#' + 'add-to-box-' + productSku + ' button.cartbutton').removeClass('incart');
        //$('div.header-cart').removeClass('active');
	}
}

function setLocation(url, productSku){
	// Pixelmechanics FL 20.12.2012: remove durch delete ausgetauscht, da der parameter in der url delete heisst
    if(!inCart && ((url.search('/add') != -1 ) || (url.search('/delete') != -1 ) || url.search('checkout/cart/add') != -1) ){
        sendcart(url, 'url', productSku);
    }else{
        window.location.href = url;
    }
}


function sendcart(url, type, productSku){

    showLoading();
	
    if (type == 'form'){

		productSku = jQuery('#product-sku').val();
		qty = jQuery('#qty').val();
		
        url = ($('product_addtocart_form').action).replace('checkout', 'j2tajaxcheckout/index/cart') + "?jsoncallback=?";
		
        jQuery.getJSON(url, { "qty": qty}, function(json) {

			if(document.getElementById('onestepcheckout-form')) {
				var summary = jQuery('#onestepcheckout-summary');
			} else {
				var summary = jQuery('#warenkorb-content');
			}

			//var cart_html_url = "https://" + window.location.hostname + "/onestepcheckout/ajax/set_methods_separate/?jsoncallback=?";
			var cart_html_url = real_base_url + "onestepcheckout/ajax/set_methods_separate/?jsoncallback=?";
			hideJ2tOverlay(); // Loading-Overlay schlie�en, und zwar erst wenn der letzte ajax aufruf durch ist
			
			jQuery.getJSON(cart_html_url, function(data) {

				summary.html(data.summary); // update() ist prototype

				if(productSku) {
					ajaxcart_background_color_anim(productSku, 2000);
				}
				
				
			});


        });



    } else if (type == 'url'){
        url = url.replace('checkout', 'j2tajaxcheckout/index/cart') + "?jsoncallback=?";
		//alert(url);
		// Post oder get werden nicht gesendet, die daten stehen in der url ( add/remove ) und werden im Controller der Extension abgefragt
		// erster Request �ndert das produkt im warenkorb, zweiter request l�d das html vom cart neu ( mit der �nderung aus request 1 )
		jQuery.getJSON(url, function(json) {
		
				if(document.getElementById('onestepcheckout-form')) {
					var summary = jQuery('#onestepcheckout-summary');
				} else {
					var summary = jQuery('#warenkorb-content');
				}

				//var cart_html_url = "https://" + window.location.hostname + "/onestepcheckout/ajax/set_methods_separate/?jsoncallback=?";
				var cart_html_url = real_base_url + "onestepcheckout/ajax/set_methods_separate/?jsoncallback=?";
				hideJ2tOverlay(); // Loading-Overlay schlie�en, und zwar erst wenn der letzte ajax aufruf durch ist
				
				jQuery.getJSON(cart_html_url, function(data) {

						summary.html(data.summary); // update() ist prototype

						if(productSku) {
							ajaxcart_background_color_anim(productSku, 2000);
						}
						updateOriginalProductPopup();
				}); // end reload cart ajax request
				
		}); // end addtocart ajax request
		
    }

}

function ajaxcart_background_color_anim(productSku, duration) {

	var elem = jQuery('tr#mini-cart-' + productSku); // Das zuletzt hinzugef�gte Produkt

	// Zuerst eine andere Hintergrundfarbe setzen
	jQuery("td", elem).css("background-color", '#E3077A');
	jQuery("td a.checkout-artikel:link", elem).css("color", '#FFFFFF');

	// Die Hintergrundfarbe zu wei� faden lassen
	jQuery("td", elem).stop().animate({ backgroundColor: "#ffffff" }, duration);
	jQuery("td a.checkout-artikel:link", elem).stop().animate({ color: "#000000" }, duration);
}

/*
 Color animation jQuery-plugin
 http://www.bitstorm.org/jquery/color-animation/
 Copyright 2011 Edwin Martin <edwin@bitstorm.org>
 Released under the MIT and GPL licenses.
*/
(function(d){function i(){var b=d("script:first"),a=b.css("color"),c=false;if(/^rgba/.test(a))c=true;else try{c=a!=b.css("color","rgba(0, 0, 0, 0.5)").css("color");b.css("color",a)}catch(e){}return c}function g(b,a,c){var e="rgb"+(d.support.rgba?"a":"")+"("+parseInt(b[0]+c*(a[0]-b[0]),10)+","+parseInt(b[1]+c*(a[1]-b[1]),10)+","+parseInt(b[2]+c*(a[2]-b[2]),10);if(d.support.rgba)e+=","+(b&&a?parseFloat(b[3]+c*(a[3]-b[3])):1);e+=")";return e}function f(b){var a,c;if(a=/#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(b))c=
[parseInt(a[1],16),parseInt(a[2],16),parseInt(a[3],16),1];else if(a=/#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(b))c=[parseInt(a[1],16)*17,parseInt(a[2],16)*17,parseInt(a[3],16)*17,1];else if(a=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(b))c=[parseInt(a[1]),parseInt(a[2]),parseInt(a[3]),1];else if(a=/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(b))c=[parseInt(a[1],10),parseInt(a[2],10),parseInt(a[3],10),parseFloat(a[4])];return c}
d.extend(true,d,{support:{rgba:i()}});var h=["color","backgroundColor","borderBottomColor","borderLeftColor","borderRightColor","borderTopColor","outlineColor"];d.each(h,function(b,a){d.fx.step[a]=function(c){if(!c.init){c.a=f(d(c.elem).css(a));c.end=f(c.end);c.init=true}c.elem.style[a]=g(c.a,c.end,c.pos)}});d.fx.step.borderColor=function(b){if(!b.init)b.end=f(b.end);var a=h.slice(2,6);d.each(a,function(c,e){b.init||(b[e]={a:f(d(b.elem).css(e))});b.elem.style[e]=g(b[e].a,b.end,b.pos)});b.init=true}})(jQuery);

function deleteCoupon() {

	showLoading();
	
	if(document.getElementById('onestepcheckout-form')) {
		var summary = jQuery('#onestepcheckout-summary');
	} else {
		var summary = jQuery('#warenkorb-content');
	}

	var url = real_base_url + "onestepcheckout/ajax/add_coupon/?jsoncallback=?";
		
	jQuery.getJSON(url, { remove: 1 },  function(data) {
		summary.html(data.summary);
		jQuery('#discount-msg').html(data.message);
		
		hideJ2tOverlay(); // Loading-Overlay schlie�en		
	});
}



function addCoupon() {

	
	var coupon_code = jQuery('#coupon_code').val();
	coupon_code = jQuery.trim(coupon_code);

	if(coupon_code == "") {
		alert("Geben Sie bitte einen Gutscheincode ein!!"); 
		return; // leere codes ignorieren
	}
	
	showLoading();
	
	var url = real_base_url + "onestepcheckout/ajax/add_coupon/?jsoncallback=?";

	if(document.getElementById('onestepcheckout-form')) {
		var summary = jQuery('#onestepcheckout-summary');
	} else {
		var summary = jQuery('#warenkorb-content');
	}

	jQuery.getJSON(url, { code : coupon_code },  function(data) {
		summary.html(data.summary);
		jQuery('#discount-msg').html(data.message);
		
		hideJ2tOverlay(); // Loading-Overlay schlie�en
	});
	
}

function replaceDelUrls(){
    if (!inCart){
        $$('a').each(function(el){
            if(el.href.search('checkout/cart/delete') != -1){
                el.href = 'javascript:cartdelete(\'' + el.href +'\')';
            }
        });
    }
}

function replaceAddUrls(){
    $$('a').each(function(link){
        if(link.href.search('checkout/cart/add') != -1){
            link.href = 'javascript:setLocation(\''+link.href+'\'); void(0);';
        }
    });
}

function cartdelete(url){
    showLoading();
    url = url.replace('checkout', 'j2tajaxcheckout/index/cart');
    var myAjax = new Ajax.Request(
    url,
    {
        method: 'post',
        postBody: '',
        onException: function (xhr, e)
        {
            alert('Exception : ' + e);
        },
        onComplete: function (xhr)
        {


            var start2 = xhr.responseText.indexOf('<div id="cart_content">')+23;
            var end2= xhr.responseText.indexOf("</div>",start2);
            $$('.top-link-cart').each(function (el){
                el.innerHTML = xhr.responseText.substring(start2,end2);
            });

            var start4 = xhr.responseText.indexOf('<div class="cart_side_ajax">')+28;
            var end4 = xhr.responseText.indexOf("<span>j2t_ajax_auto_add</span></div>",start4);
            $$('.mini-cart').each(function (el){
                el.replace(xhr.responseText.substring(start4,end4));
                //new Effect.Opacity(el, { from: 0, to: 1, duration: 1.5 });
            });
            $$('.block-cart').each(function (el){
                el.replace(xhr.responseText.substring(start4,end4));
                //new Effect.Opacity(el, { from: 0, to: 1, duration: 1.5 });
            });

            replaceDelUrls();

            //$('loadingpage').hide();
            hideJ2tOverlay();
        }

    });


}

function showJ2tOverlay(){
    new Effect.Appear($('j2t-overlay'), { duration: 0.5,  to: 0.8 });
}

function hideJ2tOverlay(){
    $('j2t-overlay').hide();
    $('loadingpage').hide();
    $('j2t_ajax_confirm').hide();
}


function j2tCenterWindow(element) {
     if($(element) != null) {

          // retrieve required dimensions
            var el = $(element);
            var elDims = el.getDimensions();
            var browserName=navigator.appName;
            if(browserName==="Microsoft Internet Explorer") {

                if(document.documentElement.clientWidth==0) {
                    //IE8 Quirks
                    //alert('In Quirks Mode!');
                    var y=(document.viewport.getScrollOffsets().top + (document.body.clientHeight - elDims.height) / 2);
                    var x=(document.viewport.getScrollOffsets().left + (document.body.clientWidth - elDims.width) / 2);
                }
                else {
                    var y=(document.viewport.getScrollOffsets().top + (document.documentElement.clientHeight - elDims.height) / 2);
                    var x=(document.viewport.getScrollOffsets().left + (document.documentElement.clientWidth - elDims.width) / 2);
                }
            }
            else {
                // calculate the center of the page using the browser and element dimensions
                var y = Math.round(document.viewport.getScrollOffsets().top + ((window.innerHeight - $(element).getHeight()))/2);
                var x = Math.round(document.viewport.getScrollOffsets().left + ((window.innerWidth - $(element).getWidth()))/2);
            }
            // set the style of the element so it is centered
            var styles = {
                position: 'absolute',
                top: y + 'px',
                left : x + 'px'
            };
            el.setStyle(styles);




     }
}

function showLoading(){
    var progress_box = $('loadingpage');
    progress_box.show();
    progress_box.style.position = 'fixed';
    progress_box.style.top = 0;
    progress_box.style.left = 0; 
}


function showConfirm(){
    $('loadingpage').hide();
    var confirm_box = $('j2t_ajax_confirm');
    confirm_box.show();
    //confirm_box.style.width = confirmW + 'px';
   // confirm_box.style.height = confirmH + 'px';

    confirm_box.style.position = 'absolute';

    //j2tCenterWindow(confirm_box);

}

document.observe("dom:loaded", function() {
    replaceDelUrls();
    replaceAddUrls();
    //Event.observe($('j2t-overlay'), 'click', hideJ2tOverlay);

    var cartInt = setInterval(function(){
        if (typeof productAddToCartForm  != 'undefined'){
            if ($('j2t-overlay')){
                Event.observe($('j2t-overlay'), 'click', hideJ2tOverlay);
            }
            productAddToCartForm.submit = function(url){
                if(this.validator && this.validator.validate()){
                    sendcart('', 'form');
                    clearInterval(cartInt);
                }

                return false;
            }
        } else {
            clearInterval(cartInt);
        }
    },500);
});


