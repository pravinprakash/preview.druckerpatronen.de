<?php
/**
 * User: Markus G.
 * Date: 08.09.2014
 * Wabsolute GmbH
 */

require_once 'app/Mage.php';

try {
    Mage::app(0);

    $csv = new Varien_File_Csv();
    $csv->setDelimiter(';')
        ->setEnclosure('"');
    $data = $csv->getData(Mage::getBaseDir('var') . '/import/' . 'products_import_only_meta.csv');
    $skip = true;

    $resource = Mage::getSingleton('core/resource');
    $writeConnection = $resource->getConnection('core_write');

    $tv = $resource->getTableName('catalog_product_entity_varchar');
    $at = $resource->getTableName('eav/attribute');

    $n = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'name' and entity_type_id = 4")->fetchColumn(0);
    $mt = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'meta_title' and entity_type_id = 4")->fetchColumn(0);
    $md = $writeConnection->query("SELECT attribute_id FROM $at WHERE attribute_code = 'meta_description' and entity_type_id = 4")->fetchColumn(0);


    /** @var $productModel Mage_Catalog_Model_Product */


    foreach ($data as $index => $row) {
        if ($skip == true) {
            $skip = false;
            continue;
        }


        //ar_dump($row);
        $sku = $row[0];
        $name = $row[1];
        $name = str_replace('Hp', 'HP', $name);
        $metaTitle = $row[2];
        $metaTitle = str_replace('Hp', 'HP', $metaTitle);
        $metaDescription = $row[3];
        $metaDescription = str_replace('Hp', 'HP', $metaDescription);
        $pId = Mage::getModel('catalog/product')->getIdBySku($sku);

        if ($pId) {
            $nSql = "UPDATE $tv SET value = '$name' WHERE attribute_id = $n AND entity_type_id = 4 AND entity_id = $pId;";
            $mtSql = "UPDATE $tv SET value = '$metaTitle' WHERE attribute_id = $mt AND entity_type_id = 4 AND entity_id = $pId;";
            $mdSql = "UPDATE $tv SET value = '$metaDescription' WHERE attribute_id = $md AND entity_type_id = 4 AND entity_id = $pId;";
            //var_dump($nSql . $mtSql . $mdSql);
            $writeConnection->query($nSql . $mtSql . $mdSql);
            //break;
        }
        echo $pId . "\n";

    }


} catch (Exception $e) {
    echo $e->getMessage();
}



