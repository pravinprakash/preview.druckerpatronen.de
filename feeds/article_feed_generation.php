<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$file = '../feeds/article_feed.csv';
$csv = new Varien_File_Csv();
$base_url = Mage::getBaseUrl(); 

$csvdata = array();

$csvdata[] = array('id','title','Costum Label','type','description','availability','google_product_category','condition','brand','color','image_link','link','Mpn','EAN','Price','Product_type','Shipping(time)','Shipping(Country:Price)','Adwords_labels','Sku');

$csv->setDelimiter(',');

//$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product').' cp , ' . $resource->getTableName('catalog_product_entity_varchar')." cpev where cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') and cp.product_id = cpev.entity_id ";
//$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_varchar')." cpev where cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') ORDER BY value_id desc";
//$products = $read->fetchAll($query); 

$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_entity_text')." ccet where ccet.value in ('toner','druckerpatrone','farbband') and ccet.attribute_id = 531 group by ccet.entity_id ";
//$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_entity_text').' ccet, ' . $resource->getTableName('catalog_category_product')." ccp where ccet.value in ('toner','druckerpatrone','farbband') and ccet.attribute_id = 531 and ccet.entity_id = ccp.category_id ";
$categories = $read->fetchAll($query); 

$pobj = Mage::getSingleton('catalog/product');

$i = 1;

foreach($categories as $cate){


    $url_path_c = _getAttrC($cate['entity_id'],'url_path',false,true);
    $url_path_ca = explode("/",$url_path_c);

    if( !in_array($url_path_ca[0], array('druckerpatrone','toner','farbband') ) ){
        continue;
    }
    $url_complete = $base_url.$url_path_c."/";
    
    $headline = strtolower(_getAttrC($cate['entity_id'],'headline',false,true));


    $tpid = _getAttrC($cate['entity_id'],'default_product',false,true);

	if( strtolower(_getAttr($tpid,'bechlem_artnr')) !=  $headline ){

	    $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." ccp where ccp.category_id = '".$cate['entity_id']."'";
	    $tmp_products = $read->fetchAll($query); 

		$tpdis_array = array();

		foreach($tmp_products as $tps){
				
			$tpids = $tps['product_id'];

			if( strtolower(_getAttr($tpids,'bechlem_artnr')) ==  $headline ){
				$tpdis_array[] = $tpids;
				break;
			}
		}

		if( count($tpdis_array) == 0 ){
			continue;
		}

		if( count($tpdis_array) == 1 ){
			$tpid = $tpdis_array[0];
		}else{
			echo " duplcate for ".$cate['entity_id'];
			print_r($tpdis_array);
			continue;
		}

	}
		
    $products = array(
				array(
					'product_id' => $tpid,
					'category_id' => $cate['entity_id'],
				)
			);
    
    foreach($products as $pe){
    
        $pid = $pe['product_id'];
        
        $pe['value']    = _getAttr($pid,'type2');
    
        $product = Mage::getModel('catalog/product')->load($pid); 
        
        if( $product->getStatus() != 1){
            continue;
        }
        
        $original = _getAttr($pid,'original');

        $sku = _getSku($pid);
    
        $user_defined = _getDisplay($sku);
        $defined1 = $user_defined['defined1'];
        $defined2 = $user_defined['defined2'];
        $defined3 = $user_defined['defined3'];
        $brand 	  = $user_defined['Marke'];

    	$compatible = _getAttr($pid,'compatible');
    	//echo $original." ".$compatible." ";
        $color    = _getAttr($pid,'color');
    
        $ean    = _getAttr($pid,'ean');
        $mpn    = _getAttr($pid,'bechlem_artnr');
    
        $img    = _getAttr($pid,'img_big');
        
        $delivery_time    = _getAttr($pid,'delivery_time');
    	$delivery_time = substr_replace($delivery_time ,"",-1);
            
        $printer    = _getAttrC($pid,'name');
        $printer_all_array  = array_diff(_getAttrC($pid,'name',true),array($printer) );
    
        $imps = ", ";
        if( $brand != '' )
        $imps = $imps.$brand." ";
        
        $printer_all = implode($imps,array_unique($printer_all_array) );
        
        if( $printer_all != '' )
        $printer_all = $brand." ".$printer_all;
        
        $serie      = _getAttrC($pid,'serie');
    
    //defined1
        
        $repace_clr = false;
        $pack = "";
        $pack_type = substr($sku, 5, 2);
		
		$costum_label = "";
        
        if( $pack_type == 'dp' ){
            
            $pack = "Doppelpack";
            $repace_clr = true;
    		if($pe['value'] == 'toner'){
				$costum_label ="Artikel_Toner";
    			$img = "http://www.druckerpatronen.de/products_images/dummies/dummie_medium_toner_schwarz_doppelpack.png";
    		}else if($pe['value'] == 'druckerpatrone'){
    			$costum_label = "Artikel_Druckerpatrone";
				$img="http://www.druckerpatronen.de/products_images/dummies/dummie_medium_druckerpatrone_schwarz_doppelpack.png";
    		} 
    
        }else if( $pack_type == 'sp' ){
    
           $pack_post = substr($sku, 7, strlen($sku));
    
            $pack = "sparset".$pack_post;
            $repace_clr = true;
    		
    		if($pe['value'] == 'toner'){
				$costum_label ="Artikel_Toner";
    			$pack = "sparpack".$pack_post;
    			$img = "http://www.druckerpatronen.de/products_images/dummies/dummie_medium_toner_schwarz_".$pack.".png";
    		}else if($pe['value'] == 'druckerpatrone'){
    			$costum_label = "Artikel_Druckerpatrone";
    			$img="http://www.druckerpatronen.de/products_images/dummies/dummie_medium_druckerpatrone_schwarz_".$pack.".png";
    		} 
    
        }
        
        if( $repace_clr ){
            
            $color_array = explode(" ",$color);
            array_pop($color_array);
            $color = implode(" ", $color_array);
            
        }
        
        $original_t = $original_d = "";
    
        if( $original == '1' ){
        
    
     		if($compatible != '0' || (round($product->getPrice() * 0.25, 2) > 10) ){
    
        		continue;
    		}
    
            $original_t = $original_d = "Original";
			
        }else{
    
            $original_d = "Alternativ";
    
        }
        
        $name_type = $desc_type = "";
        
		$m_text = "Patronen";
		
        if( $pe['value'] == 'toner' ){
    
            $name_type = "Toner";
            $desc_type = "Günstig Toner";
    
        }else if( $pe['value'] == 'farbband' ){
    
            $name_type = "Farbband";
            $desc_type = "Günstig Farbband";
            $m_text = "Farbband";
            $defined1 = "";
    
        }else{
    
            $name_type = "Tinte Patronen";
            $desc_type = "Günstig Druckerpatronen";
            
        }
        
        if( $serie == NULL || $serie == 'NULL' ){
            $serie = "";
        }else{
            if( strtolower(substr($printer, 0, strlen($serie))) == strtolower($serie)) {
                $printer = trim(substr($printer, strlen($serie)));
            } 
        }
        
        if( $defined1 == NULL && $defined1 == 'NULL' ){
            $defined1 = "";
        }
    
        $name = array($brand,$defined2,$name_type,$original_t,$defined3,$color,$pack);
        $name = array_diff( $name, array( '' ) );
    
        $name_str = implode(" ",$name);
        if(strlen($name_str) > 70 ){
            unset($name[0]);
            $name_str = implode(" ",$name);
        }
    
        $desc = array($desc_type,$color,$pack.".",$serie,"Kaufen Sie",$original_d,$brand,$m_text,$defined1,"für",$brand,$serie,$printer.",",$printer_all);
        $desc = array_diff( $desc, array( '' ) );
        $desc_str = implode(" ",$desc);
    
        $product_data = array(
                                "ART_".$i,
                                $name_str,
                                $costum_label,
                                $original_d,
                                $desc_str,
                                'In stock',
                                'Elektronik > Drucker-, Kopierer-, Scanner- & Faxzubehör > Druckerzubehör > Drucker-Verbrauchsmaterial > Toner- & Inkjet-Kartuschen',
                                'new',
                                $brand,
                                strtolower($color),
                                $img,
                                $url_complete,
                                $mpn,
                                $ean,
                                round($product->getPrice() * 1.19, 2),
                                $product->getTypeID(),
                                $delivery_time,
                                'DE:3.90 EUR',
                                "Artikel ".ucfirst($pe['value'])."",
                                 $sku,
                             );
    
    $csvdata[] = $product_data;
	
        //if( $i > 1000 ){
            
            //break;
            
        //}
    
        
        echo $i."\n";
        
        $i++;
		
    }
	

    //if( $i > 1000 ){
        
        //break;
        
    //}

}
     
$csv->saveData($file, $csvdata);

function _getStock($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT qty FROM ' . $resource->getTableName('cataloginventory_stock_item')." where product_id  = '$pid' ";
    return $read->fetchOne($query); 

}
function _getSku($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT sku FROM ' . $resource->getTableName('catalog_product_entity')." where entity_id  = '$pid' ";

    return $read->fetchOne($query); 

}
function _getDisplay($sku){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = "SELECT * FROM user_defined_table where sku = '$sku' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    return $ad[0];

}
function _getAttrC($pid,$name,$all = false,$dc = false){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    
    if( $all && !$dc ){
        
        $carray = array();
        
        $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $c_all = $read->fetchAll($query); 
        foreach($c_all as $ca){
            $carray[] = _getAttr($ca['category_id'],$name,'category');
        }
        
        return $carray;
    }else{
        
        if( !$dc ){
            
            $query = 'SELECT category_id FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
            $cid = $read->fetchOne($query); 
            
        }else{
            $cid = $pid;
        }
        
        return _getAttr($cid,$name,'category');
        
    }
}
function _getAttr($eid,$name,$type_s='product'){
    
    $ad = _getAttrId($name,$type_s);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    
    if( $type_s == 'product' ){

        $table = "catalog_product_entity_";

    }else{
        $table = "catalog_category_entity_";
        
    }
    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 
	
	/*if( $name == 'url_path' ){
		echo $query;
		var_dump($ad);
	}*/

    return $ad;
    
}
$atts_ids = array();
function _getAttrId($name,$type_s){
    
	 if( $type_s == 'product' ){

        $entity_type_id = "4";

    }else{
        $entity_type_id = "3";
        
    }
	
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where attribute_code = '$name' and entity_type_id='$entity_type_id'";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

/*
//Magento does not load all attributes by default
//Add as many as you like
$products->addAttributeToSelect('name');
foreach($products as $product) {
//do something
}*/
