Die Anpassung der Felder für die Solr-Suche befindet sich in /solr/schema.xml

Das Model für die Indexierung ist in \app\code\local\Magentix\Solr\Model\Indexer.php
und wird über das "Magento-Backend -> Konfiguration -> Cache Verwaltung -> Suchindex Neuaufbau" angestoßen.

Es soll für jeden Artikel zusätzliche Infos geben, die über eine CSV-Datei dazu geladen werden. Diese liegt in /artikelIndexSolr.csv
Momentan werden diese Infos nicht verwendet, um die Suchergebnisse sauber zu halten. Das kann später dazu kommen, wenn die Anzeige der Ergebnisse passt

Das HTML-Formular selbst liegt in:
\app\design\frontend\default\blank\template\catalogsearch\form.mini_header.phtml