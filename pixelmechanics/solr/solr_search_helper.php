<?
/**
 * Solr-Such-Helper für Druckerpatronen.de
 * @author: Robert Heine, PIXELMECHANICS
 **/ 
	class solr_search_helper {

	
		public function __construct() {
			// Einstellungen für die Solr-API
			$this->host='localhost';
			$this->port = '8983';
			$this->domain = '/solr';
			
			// hier werden von den einzelnen Funktionen die Werte zwischengespeichert.
			$this->words = array();
			$this->search_fields = array();
			$this->hl_fields = array();
			$this->solr_params = array();
			$this->hersteller_boost = array();
			$this->fuzzy_value = "0.5"; // fuzzy (ähnlichkeitssuche). werte von 0 bis 1, wobei 1 wörter mit hoher ähnlichkeit sucht
			$this->relevant_score = ""; // "1"; // Hierfür muss das Feld score mitgeliefert werden. Werte mit einem score größer dem hier sind relevant.

			// Debugging- und Fehlermeldungen.
			$this->err = "";
			$this->msg = "";
			
			// Pagination (wird erstmal nicht benötigt)
			$this->offset = 0;
			$this->limit = 10;
		}
		
		
		
		/**
		 * holt sich aus POST oder GET die Wörter, die gesucht werden sollen.
		 **/
			function get_search_words() {
				$words = array();
				
				if (isset($_POST['q'])) {
					$q = $_POST['q'];
				}
				elseif (isset($_GET['q'])) {
					$q = $_GET['q'];
				} else {
					return array();
				}
				
				$words = strtolower(trim(strip_tags($q)));
				$words = explode(" ", $words); // Aus jedem einzelnen Wort (anhand Leerzeichen) das Array bilden.
				
				// Jedes Wort durchlaufen und noch ein paar Dinge drauf anwenden...
					$this->words = array();
					foreach ($words as $word) {
						if (strlen($word)<=1) continue; // Wörter mit nur 1 Buchstaben ignorieren.
						$this->words[] = $word;
					}
				
				return $this->words;

			}
		
		
		/**
		 * Zusätzliche Parameter für die Suche
		 **/
			function get_solr_params() {
				
				$this->solr_params = array();	
				$this->solr_params["fl"] = "*,score"; // liefert alle vorhandenen Felder zurück, ausserdem den Score-Wert
				$this->solr_params["sort"] = "score desc"; // liefert alle vorhandenen Felder zurück, ausserdem den Score-Wert
				
				// Highlighting der gefundenen Characters aktivieren.
				$this->solr_params["hl"] = "true";			
				if (is_array($this->hl_fields) && !empty($this->hl_fields)) {
					$this->solr_params["hl.fl"] = implode(",", $this->hl_fields);
				}
				
				// $this->solr_params["hl.fl"] = "name,manufacturer,bechlem_artnr,bechlem_partnr,search_text_exact";
				// $this->solr_params["hl.fragsize"] = "2";
				// $this->solr_params["hl.fragmenter"] = "regex";
				// $this->solr_params["hl.snippets"] = "2";
						
				return $this->solr_params;
			}
		
		
		/**
		 * In diesen Feldern (laut schema.xml) soll nach dem Begriff gesucht werden.
		 **/
			function get_search_fields() {
			
				$search_fields = array( "name", "manufacturer");
				$search_fields[] = "search_manufacturer_alt";
				$search_fields[] = "search_name_alt";
				$search_fields[] = "search_beides_alt";
				$search_fields[] = "search_text_exact";
				// $search_fields[] = "bechlem_artnr";
				// $search_fields[] = "bechlem_partnr";
				
				$this->search_fields = $search_fields;
				return $search_fields;

			}
		
		
		/**
		 * Gibt man den Namen eines Herstellers ein, sollen deren Ergebnisse geboostet werden. Hier wird festgelegt welcher Hrst mit welchem Boost
		 **/
			function get_hersteller_boost() {		
				$hersteller_boost = array(
						"hp" => 17,
						"canon" => 16,
						"brother" => 15,
						"epson" => 14,
						"samsung" => 13,
						"lexmark" => 12,
						"dell" => 11,
						"kyocera" => 10,
						"mita" => 10,
						"xerox" => 9,
						"sharp" => 8,
						"konica" => 7,
						"minolta" => 7,
						"ibm" => 6,
						"oki" => 5,
						"olivetti" => 4,
						"panasonic" => 3,
						"philips" => 2,
						"sagem" => 1
					);
				// Für ein besseres Boosting die Werte verzehnfachen
				foreach ($hersteller_boost as $key => &$value) {
					$value = $value*100;
				}
				$this->hersteller_boost = $hersteller_boost;
				return $hersteller_boost;

			}
		
		
		/**
		 * Diese Felder sollen gehighlighted werden.
		 **/
			function get_highlighted_fields() {		
				
				$hl_fields = array("search_text_exact");
				// $hl_fields[] = "name";
				// $hl_fields[] = "manufacturer";
				$hl_fields[] = "bechlem_artnr";
				$hl_fields[] = "bechlem_partnr";
				
				// $hl_fields = $this->search_fields;
				$this->hl_fields = $hl_fields;
				return $hl_fields;

			}
			
			
		/**
		 *
		 **/	
			function create_search_terms() {
				$this->terms = array();
				// Für jedes einzelne Wort...
				foreach ($this->words as $word) {
				
					// .. soll in jedem durchsuchbaren Feld...
					foreach($this->search_fields as $field) {
							
							$value = $word;
							$boost = "";
							$fuzzy = "";
							
						// Nur das Feld Hersteller oder hersteller_alt boosten. Ausserdem mit Fuzzy versehen.
							if ( ($field=="manufacturer" OR $field=="search_manufacturer_alt") && isset($this->hersteller_boost[$word])) {
								$boost = "^".$this->hersteller_boost[$word];
								$fuzzy = "~".$this->fuzzy_value; 
							}	
						// Name kann mit * versehen werden, um eine Ngram-Suche in diesem Feld durchzuführen. Ausserdem mit Fuzzy versehen.
							if ( /*$field=="name" OR $field=="search_name_alt" OR */ $field=="search_beides_alt") {
								$value = "*".$value."*";
								// $fuzzy = "~".$this->fuzzy_value; 
							}	
						
						
						
							
						$this->terms[] = $field.": ".$value.$boost; // .$fuzzy;
					}	
				}
				return $this->terms;
			}
			
			
			
		/**
		 * Setzt aus $this->terms (array) das Suchquery zusammen, welches dem Solr-Object zur Suche übergeben werden kann.
		 * @param $addon hier kann ein zusätzliches Statement, wie zB " AND suchtyp=artikel" angegeben werden.
		 **/
			function create_query($addon="") {
				$query = "(".implode(" OR ", $this->terms).")"; // Dursucht in allen Feldern (entweder .. oder)
				if (!empty($this->relevant_score)) $query.= " AND score>".$this->relevant_score;				
				if (!empty($addon)) $query.= " ".$addon;
				return array( $query ); // Die Query muss als Array zurückgegeben werden.
			}
					
				
		/**
		 * Zum Solr Server verbinden
		 **/
			function init_solr() {
				$this->solr = new Apache_Solr_Service($this->host, $this->port, $this->domain);
	  
				if (!$this->solr->ping()) {
					$this->err = "Server temporär nicht erreichbar";
					return false;
				} else {
					return true;
				}
			}
			
			
		/**
		 * Führt die Solr-Suche per HTTP-Request aus und erzeugt $this->items aus jedem gefundenen Ergebnis.
		 * liefert alle geparsten Ergebnisse anschliessend als Array zurück.
		 * $query wird mit this->create_query() erzeugt.
		 **/
			function parse_query($query) {
				if (!isset($this->solr)) $this->init_solr();
				
				$response = $this->solr->search( $query, $this->offset, $this->limit, $this->solr_params );
				$this->items = array();
				
				if ( $response->getHttpStatus() != 200 ) {
					$this->err = "Suchserver lieferte kein Ergebnis für Anfrage (Statuscode=".$response->getHttpStatus().")";
					return false;
				}
				if ( $response->response->numFound <= 0 ) {
					return array();
				}
					
				// Gefundene Items zusammenfassen, Link hinzufügen
				foreach ( $response->response->docs as $doc ) { 
					$item = array();
					
					// Link zur Kategorie zusammensetzen
						$item["link"] = str_replace(" ","-", strtolower("/".$doc->manufacturer."/".$doc->name));
                        $item["link"] = str_replace("kyocera/mita","kyocera-mita",$item["link"]);
						
					// Felder vom Ergebnis auslesen & zusammenfassen	
						foreach($doc as $fieldName => $fieldValue) {
							$item[$fieldName] = $fieldValue;
						}
			
					// Preis formatieren
						if(isset($item['preis_ab'])) {
							$item['preis_ab'] = " ab ".number_format($item['preis_ab'], 2, ",", ".")."€";
						}
						
						
						
						
					/**
					 * Highlighting noch checken. Nur wenn Solr in den entsprechenden HL-Feldern etwas erzeugt hat, ist die Relevanz hoch genug.
					 **/
						// preprint($doc); die()
						$item_has_highlight = false;
						$id = $doc->id; // die ID des Eintrags, für welches nach dem Highlighting in separaten Feldern geschaut wird.
						
						if (isset($response->highlighting->$id) && !empty($response->highlighting->$id)) {
							
							// preprint($response->highlighting->$id);
							/**
							 * Oben muss definiert werden, welche Felder mit highlighting ausgegeben werden sollen.
							 * Diese hier durchlaufen und prüfen, ob sie existieren, d.h. ob etwas gehighlighted wurde.
							 * Falls nicht, ist die Relevanz vermutlich zu niedrig.
							 **/
								foreach ($this->hl_fields as $hl_field) {
									
									// preprint($response->highlighting->$id." -> ".$hl_field);
									if (isset($response->highlighting->$id->$hl_field)) {
										$hl_field_content = $response->highlighting->$id->$hl_field;
										if (isset($hl_field_content[0]) && !empty($hl_field_content[0])) {
											$item[$hl_field] = $hl_field_content[0];
											$item_has_highlight=true;
										}
									}
								}
						} // if highighting->id
						
					
					// Nur dann das Item der Ergebnissliste hinzufügen, wenn etwas gehighlighted wurde. Wenn nicht, ist die Relevanz vermutlich zu niedrig.
						if ($item_has_highlight)  $this->items[] = $item;

				} // foreach docs
				
				return $this->items;
				
			} // function
			
	} // class