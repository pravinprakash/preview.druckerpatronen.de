<?
/**
 * Diese Datei aktualisiert den Suchindex.
 * Indexierung wird gestartet über das Backend unter System -> Cache -> Suchindex Button "Neuaufbau"
 * muss von hier inkludiert werden aus \app\code\local\Magentix\Solr\Model\Indexer.php
 * 
 * @copyright Copyright 2013 PIXELMECHANICS
 * 
 * @category Solr
 * @package Magentix_Solr
 * @author Felix Kowalowka und Robert Heine kontakt@pixelmechanics.de
 */

class Magentix_Solr_Model_Indexer {
    
    /**
    * Refresh Solr Index
    * 
    * @param Varien_Event_Observer $observer
    */
    public function refresh($observer) {
        if(!Mage::getStoreConfigFlag('solr/active/admin')) {
            return;
        }
        
	 //  $products = $this->_getConnection()->fetchAll('SELECT * FROM '.$this->_getTable('catalogsearch/fulltext'));
		$db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$documents = array();
		$additionalProductData = array();
		$hersteller_attribute_id = 31; // Attribut ID in dem die Hersteller gespeichert sind
		$csvFile = $_SERVER['DOCUMENT_ROOT']."/artikelIndexSolr.csv";

		
		/**
		 * Laden aller für den Solr-index benötigter Daten
		 *
		 * -> Hersteller
		 * -> Artikel-Nr. bzw. Modell-Nr.
		 * -> Typ ( Toner, Gerät, .. )
		 * -> zusätzliche Infos aus der CSV [root]/artikelIndexSolr.csv ( enthält ab-preise, Skus und Hersteller-PartNrn)
		 *
		 * direkt per Mysql, magento methoden benötigen zuviel Speicher und Zeit !
		 **/
		
		// Herstellerfeld enthält nicht nur Geräte. Zur Unterscheidung kann anhand folgendem Array geprüft werden
		 $types = array(	"Toner",
							"Druckerpatrone",
							"Farbband",
							"Extra",
							"Thermotransferfolie"
						);
						
		// Hersteller laden ( 1. Kategorieebene ), Array mit ID => Name(Gerätenamen) erstellen
			$sql = "	SELECT e.entity_id, evar.value 
						FROM dpm_catalog_category_entity as e
						INNER JOIN dpm_catalog_category_entity_varchar as evar ON e.entity_id = evar.entity_id
						WHERE evar.store_id = 0 AND evar.attribute_id = $hersteller_attribute_id
						ORDER BY evar.value ASC";
			$manufactures = $db->fetchAll($sql);
			$manufactures = create_select_array($manufactures, 'entity_id', true, 'value'); // erzeugt: array(entity_id => value, ....)

			
		/**
		 * die zusätzlichen Infos aus der CSV in ein Array parsen, um diese dann weiter unten easy den Produkten zuordnen zu können.
		 * gruppiert nach Art-Nr ( Ist dann beim Durchlaufen der zu indexierenden Artikel anhand der Art-Nr abrufbar )
		 **/
			if (($handle = fopen($csvFile, "r")) !== FALSE) {
				
				$line=0;				
				while (($data = fgetcsv($handle, 9000, ";", '"')) !== FALSE) {
				
					$item = array(); // Zeile der CSV: array(artnr, bechlem_partnr, skus, druckertyp, preis_ab)
				
					// Erste Zeile in der CSV sind die Feldnamen. Diese als $csv_fieldnames abspeichern.
						if($line == 0) {
							$csv_fieldnames = $data;
						}
					
					// Alle Einträge ab Zeile 2 durchlaufen und mit Feldnamen aus Zeile 1 zu Assoc-Array speichern
						else {
							$num = count($data); // Anzahl der Spalten
							for ($spalte_nr=0; $spalte_nr < $num; $spalte_nr++) { // array( Feldname => Wert )
								$value = $data[$spalte_nr];
								$spaltenname = $csv_fieldnames[$spalte_nr];
								$item[$spaltenname] = $value;
							}
							
							/**
							 * die erste Spalte in jeder Zeile ist per Definition die Artikelnummer. 
							 * Das Item dem Gesamt-Array anhand der Artnr zuordnen.
							 **/
								$artnr = $item[$csv_fieldnames[0]];
								$additionalProductData[$artnr] = $item;
						}
					
					$line++;
				}
				fclose($handle);
			} // CSV Datei öffnen
			
			
			
				
		/**
		 * Geräte & Artikel-nr, Array mit ID => Name erstellen		
		 **/
			$subselect = ""; /* hinfällig, da dieser typ nur für die landingpage gedacht ist, nicht zur unterscheidung
							,( SELECT value 
								FROM dpm_catalog_category_entity_text
								WHERE
										entity_id = e.entity_id
									AND attribute_id = 535
									AND store_id = 0
									) as type";
									*/
			
			$category_level = 3; // speziell für diese abfrage		
			$sql = "SELECT	e.entity_id, 
							e.parent_id, 
							evar.value
							$subselect

							FROM dpm_catalog_category_entity AS e
							INNER JOIN dpm_catalog_category_entity_varchar AS evar ON e.entity_id = evar.entity_id
								
							WHERE 
								evar.store_id =0
							AND e.level = $category_level
							AND evar.attribute_id = $hersteller_attribute_id
							AND evar.value != ''
						";
			$articles = $db->fetchAll($sql);		
			
			
		/**
		 * Gefundene Artikel durchlaufen und den Typ dazuladen ( kommt aus der Kategorie, wenn leer dann ist es ein Gerät )
		 **/
			$i=0;
			$debug_data = array();
			foreach($articles as $article) {
			
				// @todo: NUR ZUM TESTEN die ersten 100 Einträge indexieren, damit es schneller geht.
				//if ($i>100) continue;
				
				
				/**
				 * In der Kategorieebene gibt es sowohl Artikel als auch Geräte
				 * Typ ist ein Gerät, wenn die Artikel-Nr (der Gerätenamen) im Hersteller-Array vorkommt
				 **/
					$type = 'artikel';

				
				// Hersteller fehlt, darf nie vorkommen!
					if(!isset($manufactures[$article['parent_id']])) {
						// @todo: Muss hier wirklich komplett abgerochen werden, wenn nur 1 Artikel keinen Hersteller hat??
						die("Fehler: Hersteller mit der ID ".$article['parent_id']." nicht vorhanden.".__LINE__.__FILE__);
					}
					else {
						$manufacturer = $manufactures[$article['parent_id']];
					}
					
				// Type aus der abfrage ist reserviert für die ansicht der Landingpage richtig ist die betrachtung des Herstellers 
					if ( !in_array($manufacturer, $types) ) {
						$type = "Gerät"; 
					}			
				
				// Zusammenfassen der zu indexierenden Daten, anschließend werden die Dokumente ( Items ) gespeichert
					$doc = Mage::getModel('solr/document');
					
					$doc->id = $article['entity_id'];
					$doc->name = $article['value'];
					$doc->store_id = 0;
					$doc->suchtyp = $type;
					$doc->manufacturer = $manufacturer;
					$prod_name = $article['value'];
					
					// Zusätzliche Produktinfos aus der seperaten CSV dazuladen			
						$data = array();
						if(isset($additionalProductData[$article['value']])) {
							$data = $additionalProductData[$article['value']];
						}
					
					/**
					 * In seperate Felder indexieren, da die Felder mit unterschiedlichen Algorithmen durchsucht werden
					 **/
						// Suchfeld für exakte Angaben ohne Filter, enthält Artikelname/Gerät, Hersteller/Typ, Artikelnummern 
							$additionalSearchData = array();
							if(!empty($data)) {
								$additionalSearchData = array(
																$data['bechlem_artnr'],
																$data['bechlem_partnr'],
																// $data['skus']
															);
							}
							
							
							$searchTerm_EXACT = $manufacturer." ".$article['value'];
							if (!empty($data['bechlem_partnr'])) {
								$searchTerm_EXACT = $data['bechlem_partnr']." ".$searchTerm_EXACT;
							}
							elseif (!empty($data['bechlem_artnr'])) {
								$searchTerm_EXACT = $data['bechlem_artnr']." ".$searchTerm_EXACT;
							}
							
						// Suchfeld für Wortähnlichkeit & NGRAM, Nur Textfelder: Artikelname/Gerät, Hersteller/Typ
							$searchTerm_STEM = $manufacturer." ".$article['value'];
					 
					 
					// Zu durchsuchende Felder dem Item hinzufügen
						$doc->search_text_exact = $searchTerm_EXACT; 	// Exakte Suche, ohne Filter wie NGRAM oder STEM
						$doc->search_text_stem 	= $searchTerm_STEM; 	// Wortstamm bilden. Bsp: "walks, walking, walked" => "walk"
						
					 
						/**
						 * Zum Ausprobieren von diversen anderen Sucheinstellungen.
						 **/
							$search = array("-", "_", " ");
							$replace = "";
							$doc->search_manufacturer_alt 		=  str_replace($search, $replace, $manufacturer);
							$doc->search_name_alt 				=  str_replace($search, $replace, $article['value']);
							$doc->search_beides_alt =  str_replace($search, $replace, $searchTerm_EXACT);
						
						if(isset($data['bechlem_partnr']) && $data['bechlem_partnr'] !='') $doc->bechlem_partnr	= $data['bechlem_partnr'];
						if(isset($data['bechlem_artnr']) && $data['bechlem_artnr'] !='') $doc->bechlem_artnr	= $data['bechlem_artnr'];
						if(isset($data['farbe']) && $data['farbe'] !='') $doc->farbe	= $data['farbe'];
						
					// Falls die zusätzlichen Infos aus der CSV nicht zugeordnet werden konnten, in die solr.log schreiben
						if(!isset($additionalProductData[$article['value']])) {
							Mage::log("ArtikelIndexSolr: Art-Nr/Kategorie \"".$article['value']."\" nicht in CSV vorhanden! Zusätzliche Daten wurden nicht in den Index aufgenommen!", 1, "solr.log");
						}
					
					
					
					$documents[] = $doc;
					unset($doc);
				// $debug_data[] = $doc;
				
				unset($articles[$i]); 
				$i++;
			} // foreach 
		
		unset($manufactures);
		unset($additionalProductData);
				
		// preprint($debug_data); die();
		
		
		
		
        try {
            $search = Mage::getModel('solr/search');
            $search->deleteAllDocuments();
            $search->addDocuments($documents);
            $search->commit();
            $search->optimize();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('solr')->__('Can not index data in Solr. %s',$e->getMessage()));
        }

        return;
    }
    
    /**
    * Retrieve resource
    * 
    * @return Mage_Core_Model_Resource
    */
    public function _getResource() {
        return Mage::getSingleton('core/resource');
    }
    
    /**
    * Retrieve connection
    * 
    * @return Varien_Db_Adapter_Pdo_Mysql
    */
    public function _getConnection() {
        return $this->_getResource()->getConnection('core_read');
    }
    
    /**
    * Retrieve table name
    * 
    * @return string
    */
    public function _getTable($tableName){
        return $this->_getResource()->getTableName($tableName);
    }
    
}