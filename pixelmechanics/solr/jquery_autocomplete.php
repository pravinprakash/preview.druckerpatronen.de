<? 
/**
 * @todo: Das hier könnte in das Main-Template ausgelagert werden.
 **/ ?>
<? /*
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"> -->
		<link rel="stylesheet" href="/skin/frontend/default/blank/css/jquery-ui.css">
	
	<!--<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>-->
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
*/
?>

<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.min.css">

	
<script type="text/javascript">	
jQuery(function($) {
	/**
	 * Autocompleter soll mit Kategorien funktionieren machen.
	 **/
		 jQuery.widget( "custom.catcomplete", jQuery.ui.autocomplete, {
			_renderMenu: function( ul, items ) {
				var that = this,
				currentCategory = "";
				jQuery.each( items, function( index, item ) {
					if ( item.category != currentCategory ) {
						ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
						currentCategory = item.category;
					}
				that._renderItemData( ul, item );
				});
				jQuery("li.ui-menu-item", ul).first().select();
			}, // renderMenu=function
				
			// Rendert ein einzelnes List-Item. Siehe http://jqueryui.com/autocomplete/#custom-data
			_renderItem: function(ul, item) {
				var v = item.value;
			
			if(item.category=="Artikel" && item.bechlem_partnr != undefined ) {
				return $("<li></li>") // <a href=''>"+item.label+"</a>
					.data("item.autocomplete", item)
					.append("<a class='item' href='"+item.url+"'>"+item.bechlem_partnr+" "+item.farbe+" ("+item.bechlem_artnr+")</a>")
					.appendTo(ul);
				
				} else if(item.category=="Artikel" && item.bechlem_partnr ==undefined && item.bechlem_artnr != undefined) {
				return $("<li></li>") // <a href=''>"+item.label+"</a>
					.data("item.autocomplete", item)
					.append("<a class='item' href='"+item.url+"'>"+item.bechlem_artnr+" "+item.farbe+"</a>")
					.appendTo(ul);
				
				} else if(item.category=="Artikel" && item.bechlem_artnr !=undefined ) {
				return $("<li></li>") // <a href=''>"+item.label+"</a>
					.data("item.autocomplete", item)
					.append("<a class='item' href='"+item.url+"'>"+item.label+"</a>")
					.appendTo(ul);
				
				} else {
				return $("<li></li>") // <a href=''>"+item.label+"</a>
					.data("item.autocomplete", item)
					.append("<a class='item' href='"+item.url+"'>"+item.label+"</a>")
					.appendTo(ul);
				
				}
				
				
			} // renderItem=function
			
		});

		// $( "#search_mini, #search, #search_mini_form_header, #search_mini_form" ).catcomplete({
		$( ".search_mini" ).catcomplete({
			
			minLength: 1,
			delay: 200,
			autoFocus: false,
			source: function( request, response ) {
						$.ajax({
							type: "POST",
							dataType: "JSON",
							//dataType: "jsonp",
							url: "<?=$url_to_solrscript?>",
							data: {
								q: request.term
							},
							success: function( data ) {
										var ret = [];
											
										$.each(data.items_grouped, function(typ, items) {alert(items.length);
											if(items.length > 0) {
													has_data = true;
													$.each(items, function(i, item) {
														
														// Ein Item zusammenbauen. Standardmäßig Hersteller+Bezeichnung. Ausser wenn Bechlem-Angaben existieren.
														var name = "";
														name += item.search_text_exact+" ";
																												
														var thisitem = {	label: name, 
																			category: typ,
																			url: item.link,
																			bechlem_artnr : item.bechlem_artnr,
																			bechlem_partnr : item.bechlem_partnr,
																			farbe : item.farbe
																		
																		};
														ret.push(thisitem);
													});
											} // if
										});
										
										response(ret);
							}, // success
						}); // ajax
					}, // source
				
				
				// Beim Auswählen eines Items, soll dessen Link geöffnet werden.
					select: function(event, ui) {
						jQuery("#loadingpage").show();
						location.href= ui.item.url;
						return false;
					},
				
				// Verhindern, dass die Auswahl ins Textfeld übernommen wird.
					focus: function(event, ui) {
						console.log("focus triggered");
						return false;
				    }
					
		}); /*.focus(function(){
			if (this.value !== "")
				$(this).autocomplete('search', "");
				$(this).trigger('open.autocomplete');
				$(this).trigger('suggest.autocomplete');
				$(this).trigger('keydown.autocomplete');
		});
		*/
		
		
		/**
		 * Wenn im Feld ENTER gedrückt wird, soll nicht das Formular submitted werden, sondern die Ajax-Suche ausgeführt.
		 **/
			$( ".search_mini" ).on("keypress", function(e) {
				var code = e.keyCode ? e.keyCode : e.which;
				if (code==13) {
					$( this ).focus();
					return false;
				}
				if (code==27) {
					console.log("ESC pressed");
					return false;
				}
			});
		
		// Klick auf den SUCHEN Button muss noch den Autocompleter auslösen.
			$( ".header-suchbox-left-form-button" ).on("click", function(event) {
				$( ".search_mini", $(this).parent() ).focus();
				return false;			
			});
	});
</script>