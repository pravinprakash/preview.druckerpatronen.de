<?php
/**
 * Anzeige des Suchormulars im oberen Bereich der Seite.
 **/
 
 $base_url = Mage::getBaseUrl();
 if (ISSET($_SERVER['HTTPS'])) $base_url = Mage::getUrl('', array('_secure' => Mage::app()->getFrontController()->getRequest()->isSecure()));
 $url_to_solrscript = $base_url."pixelmechanics/solr/solr_search.php";

      include('jquery_autocomplete.php'); // Autocompleter Plugin von jQUery UI verwenden. Wegen Pfeilnavigation
        // include('pm_solrsearchscript.php'); // Altes PM-Script von Jeff     
?>
<link rel="stylesheet" type="text/css" href="/pixelmechanics/solr/solr_styles.css" />


<? /*<form id="search_mini_form_header" name="search_mini_form_header" action="#<?//=$this->helper('catalogSearch')->getResultUrl()?>" method="get">*/ ?>
<div id="search_mini_form_header" class="form" name="search_mini_form_header">
        <fieldset class="form-header-search">

                <input id="search_mini_top"
                                type="text"
                                class="search_mini header-suchbox-left-form-field"
                                name="<?=$this->helper('catalogSearch')->getQueryParamName()?>"
                                value="<?=$this->helper('catalogSearch')->getEscapedQueryText()?>"
                                onfocus="javascript:jQuery(this).catcomplete('search', this.value);"
                                autocomplete="off" placeholder="Bitte Druckermodell eingeben..." />
                               
                <img src="<?=$base_url?>skin/frontend/default/blank/images/media/header/header-search-button.png" class="header-suchbox-left-form-button" />
                       
                <script type="text/javascript">
                //<![CDATA[
                        //var searchForm = new Varien.searchForm('search_mini_form_header', 'search_mini', 'Druckername eingeben...');
                        //searchForm.initAutocomplete('<?=$this->helper('catalogSearch')->getSuggestUrl() ?>', 'search_autocomplete'); // auskommentieren wenn autocomplete-extension genutzt
                //]]>
                </script>
        </fieldset>
</div>

<? /* Wird nur von pm_solrsearchscript.php verwendet
<div class="search-autocomplete"></div>
*/ ?>