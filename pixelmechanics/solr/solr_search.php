<?
/**
 * Suche über den SOLR-Server
 * Diese Datei wird per Ajax aufgerufen und liefert die Suchergebnisse für den Autocompleter zurück.
 **/

 
	require_once('../../lib/Apache/Solr/Service.php');
	require_once('../pm_helper.php');
	require_once('solr_search_helper.php');


	$items_artikel = array(); // items_artikel = Array welches per JSON letztlich zurückgeliefert wird
	$items_drucker = array();
	$query_artikel = "";
	$query_drucker = "";
	$helper = new solr_search_helper();
	
	echo 'ok';exit;
	/**
	 * Aus dem Helper die verschiedenen Funktionen nacheinander ausführen.
	 **/
		$search_fields = $helper->get_search_fields(); // In diesen Feldern soll gesucht werden.
		$hersteller_boost = $helper->get_hersteller_boost(); // Manche Hersteller werden weiter oben angezeigt als andere
		$hl_fields = $helper->get_highlighted_fields(); // Diese Felder sollen von Solr gehighlighted werden.
		$params = $helper->get_solr_params(); // Zusätzliche Parameter für die Suche
		$terms = array(); // Hier stehen die Felder drin, die gesucht werden sollen. zB "name:e120"
		
		
	// Und los geht's!
		$words = $helper->get_search_words(); // Nach diesen Wörtern soll gesucht werden.
		
		// Wenn keine Suchwörter übergeben wurden, muss auch nichts weiter gemacht werden.
		if (!is_array($words) OR empty($words)) {
			return;
		}
		
			
		
	// Query zusammensetzen anhand der einzelnen Suchwörter und den einzelnen Suchfeldern.
		$terms = $helper->create_search_terms();
				
				
		/**
		 * Finale Suchqueries, seperat nach Artikeln (Patronen,..) und nach Druckern suchen
		 **/
			$query_artikel = $helper->create_query("AND suchtyp:artikel");
			$query_drucker = $helper->create_query("AND suchtyp:Gerät");
				
				
			$items_artikel = $helper->parse_query($query_artikel);
			$items_drucker = $helper->parse_query($query_drucker);
			
			
			
		/**
		 * Limit für die Suchergebnisse ist 10 ( siehe oben )
		 *
		 * Es sollen jeweils 5 Drucker und 5 Artikel angezeigt werden
		 * Bzw. wenn weniger Drucker/Artikel, dann entsprechend mehr Artikel/Drucker anzeigenf
		 *
		 **/
			$count_artikel = count($items_artikel);
			$count_drucker = count($items_drucker);
			
			$limit_je_typ = $helper->limit / 2;
									
			// Wenn Anzahl beider Typen größer als 5 ($limit / 2) dann beide kürzen
				if($count_artikel > $limit_je_typ && $count_drucker > $limit_je_typ) {
					
					// array_chunk teilt das array anhand der größe, erster key enthält dann das gewünschte ergebnis
						$parts_artikel = array_chunk($items_artikel, $limit_je_typ, true);
						$parts_drucker = array_chunk($items_drucker, $limit_je_typ, true);
					
					// Keine prüfung auf Index, da hier immer mehr als 5 vorliegen ( key 0 immer existiert )
						$items_artikel = $parts_artikel[0];
						$items_drucker = $parts_drucker[0];
					
					
				}
				
			// Wenn Artikel < 5 und Drucker > 5, dann soviele Drucker aus dem Array löschen, bis es 10 ingesamt Einträge sind
				if( $count_artikel <= $limit_je_typ && $count_drucker > $limit_je_typ ) {
		
					$items_to_delete = 	$helper->limit - $count_artikel; // anzahl der drucker die zuviel im Array sind
											
					// array_chunk teilt das array anhand der größe, erster key enthält dann das gewünschte ergebnis
						if($items_to_delete > 0) { // nur löschen wenn zuviele items vorhanden sind, negative zahl bedeutet weniger als $limit
							$parts_drucker = array_chunk($items_drucker, $items_to_delete, true);
							
							// Keine prüfung auf Index, da hier immer mehr als 5 vorliegen ( key 0 immer existiert )
								$items_drucker = $parts_drucker[0];	
						}
				}
				
			// Wenn Artikel > 5 und Drucker < 5, dann soviele Artikel aus dem Array löschen, bis es 10 ingesamt Einträge sind
				if( $count_artikel > $limit_je_typ && $count_drucker <= $limit_je_typ ) {
				
					$items_to_delete = 	$helper->limit - $count_drucker; // anzahl der drucker die zuviel im Array sind
					
					// array_chunk teilt das array anhand der größe, erster key enthält dann das gewünschte ergebnis
						if($items_to_delete > 0) { // nur löschen wenn zuviele items vorhanden sind, negative zahl bedeutet weniger als $limit
							$parts_artikel = array_chunk($items_artikel, $items_to_delete, true);
								
							// Keine prüfung auf Index, da hier immer mehr als 5 vorliegen ( key 0 immer existiert )
								$items_artikel = $parts_artikel[0];	
								
						}
						
				}
			
//if ( !isset($items_artikel[0]['score']) ) $items_artikel[0]['score'] = 'nix';
//if ( !isset($items_drucker[0]['score']) ) $items_druckewr[0]['score'] = 'nix'; 
if ( isset($items_artikel[0]['score']) && (isset($items_drucker[0]['score']) && ($items_artikel[0]['score'] >= $items_drucker[0]['score'] ))) { 
	$return = array(	'msg' => $helper->msg,
						'err' => $helper->err,
						'query_artikel' => $query_artikel,
						'query_drucker' => $query_drucker,
						'items_grouped' => array(
											"Artikel" => $items_artikel,
											"Drucker" => $items_drucker
										)
						);
} else {
$return = array(	'msg' => $helper->msg,
						'err' => $helper->err,
						'query_artikel' => $query_artikel,
						'query_drucker' => $query_drucker,
						'items_grouped' => array(
											"Drucker" => $items_drucker,
											"Artikel" => $items_artikel
											
										)
						);

}						
	// @file_put_contents("ausgabe.txt", $query_artikel);
	
	if (IS_AJAX) {
		// return_json($return);
		echo json_encode($return);
		die();
	}
	else {
		header("content-type: text/html; charset=utf-8");
		preprint($return);		
	}
	