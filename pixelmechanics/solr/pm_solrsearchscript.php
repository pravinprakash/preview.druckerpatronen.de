<script type="text/javascript">
	jQuery(function($) {
		 /**
		  * Erweiterte Suche über den SOLR-Server
		  *
		  * per Ajax wird das Suchskript aufgerufen, welches die Daten per JSON zurückliefert
		  *
		  **/
			var xhr;
			// jQuery('.page').on('keyup submit', 'input, form', function(e) {
			$('#search_mini, #search, #search_mini_form_header, #search_mini_form').on('keyup submit', function(e) {
				e.preventDefault();
				
				check_keypress(this, e);

				var search_value = $(this).val();
				search_value = $.trim(search_value);

				var $wrapper = $(this).parent().parent().parent().find('.search-autocomplete');

				// Ajax abfrage nur wenn etwas im Eingabeformular eingegeben wurde
				if(search_value != "") {

				// Vorhergehende Anfragen abbrechen, falls neue kommt
					if (xhr != null) xhr.abort();

					xhr = $.ajax({
						type: "POST",
						dataType: "JSON",
						url: "<?=$url_to_solrscript?>",
						data: "q=" + search_value,
						success: function(data, textStats) {
							var html = "<span class='close'>X</span>";
							
							var has_data = false;

							$.each(data.items_grouped, function(typ, items) {
								var inner_html = "";
								inner_html+="<div class='type_wrap'>";
									inner_html+="<h4>"+typ+"</h4>";
									
									if(items.length > 0) {
										has_data = true;
										$.each(items, function(i, item) {
										
											var additional = new Array();
											
											// var name = item.manufacturer+" "item.name;
											var name = item.search_text_exact;
											
											inner_html+= "<a class='item' href='"+ item.link +"' >";
											if(item.bechlem_partnr) {
												name+= " "+item.bechlem_partnr;
											} else if (item.bechlem_artnr) {
												name+= " "+item.bechlem_artnr;
											} 
											
											inner_html += name;
											
											/*
											if((item.bechlem_artnr || item.bechlem_partnr || item.preis_ab) && item.suchtyp == 'artikel') {
												
												//if(item.bechlem_artnr) {
												//	additional.push(item.bechlem_artnr);
												// }	
												if(item.bechlem_partnr) {
													additional.push(item.bechlem_artnr);
												}
												if(item.farbe) {
													additional.push(item.farbe);
												 }	
												//if(item.preis_ab) {
												//	additional.push(item.preis_ab);
												//}	
												
												if(item.bechlem_partnr || item.farbe) inner_html += " (" + additional.join(", ") + ")";
											}
											*/
											inner_html+= "</a>";
											
										});
									} 
									else {
										inner_html+="Zu diesem Suchbegriff wurden keine Artikel gefunden!";
									}
									
								inner_html+="</div>";
								
								if(	( data.items_grouped.Artikel.length <= 0 &&
									data.items_grouped.Drucker.length <= 0 ) ||
									items.length > 0 ) {
										html+= inner_html;
								}
								
							});
							
							
							// Dropdown nur anzeigen wenn Ergebnisse vorhanden
							if(has_data) {
								$wrapper.html(html);
								$wrapper.slideDown('fast');
							}
							
							// ist das Suchfeld leer, muss die Suchbox wieder ausgeblendet werden.
							
						},
						error: function(errorObj, textStatus, errorThrown){
							console.log(errorThrown);
							console.log(textStatus);
							console.log(errorObj);
						}
							  

					});
				} else {
					$wrapper.slideUp('fast');
				}
				
				return false;
			});
			
			// Klick auf Suchen ignorieren, da immer per Eingabe oder Enter gesucht wird
				$('#search_mini_form_header input[name=submit], #search_mini_form input[name=submit]').on('click', function(e) {
					e.preventDefault();
					return false;
				});
			
			// Schließen der Suchbox per Click auf das icon
				jQuery('.search-autocomplete').on('click', '.close', function(e) {
					$(this).parent().slideUp('fast');
				});
				
				
			// Keyup Keydown
				jQuery('.search-autocomplete a.item').live('keypress', function(event) {
					check_keypress(this, event);
				});
			
		
						
	});
	
	
	function check_keypress(elem, event) {
		var key = event.keyCode;
		// alert(key);
	}
</script>
