<?php

error_reporting(E_ALL ^ E_NOTICE);
ob_start();
session_start();
include ("_config/inc.config.php"); 

$imgname = $_GET['imagename'];
$type = $_GET['type'];

if($imgname==navigation_background){
  $offsety1 = 29;
  $offseth = 23;
}
$img = $imgname.'.'.$type;
$size = getimagesize(ROOT."customer/tpl/images/themes/".$img);

$img = ROOT."customer/tpl/images/themes/".$img;
$default = false;
$upper = Hex2RGB($settings->get('css_color_link'));
$lower = Hex2RGB($settings->get('css_color_link_hover'));

list ( $r1, $g1, $b1, $r2, $g2, $b2 ) = array ( $upper[0], $upper[1], $upper[2], $lower[0], $lower[1], $lower[3] );
$im = imagecreatefrompng($img);
$white = imagecolorallocate($im, 255, 255, 255);
$h = $size[1];
if($offseth)$h=$offseth;


$steps = $h;
$height = $h;
$step_width = $size[0];
$diff = array();
for ($i = 0; $i < 4; $i++) {
  $diff[$i] = ($upper[$i] - $lower[$i]) / $steps;
}
for ($step = 0; $step < $steps; $step++) {
  for ($i = 0; $i < 4; $i++) {
    $rgb[$i] = round($upper[$i] - $diff[$i] * $step);
    if ($i < 3) {
      if ($rgb[$i] > 255) {
        $rgb[$i] = 255;
      }
    }elseif ($rgb[3] > 127) {
      $rgb[3] = 127;
    }
  }
 # var_dump($rgb);
  $col = imagecolorallocatealpha($im, $rgb[0], $rgb[1], $rgb[2], $rgb[3]);
  $x = 0;
  $y = $offsety1 + $step;
  imageline($im, $x, $y, $size[0], $y, $col);
}



header("Content-Type: image/jpeg");
imagejpeg($im, '', 92);
imagedestroy($im);


ob_end_flush(); 
 ?>