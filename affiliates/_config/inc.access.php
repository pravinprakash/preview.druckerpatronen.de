<? $menu = new backend_menu;
$menu->set_path(AFFILIATES);

// allow -> erlauben eine seite zu betreten; title -> meta_title; root -> breadcrumb


# checks
if (!$account->id) {
        $col = "";
        $cat = "";
        $site = "";
        $file = "start";
}

if ($col == account) {
        if (!$cat) {
                if (!$site) {
                        if ($file == start) {
                                $allow = 1;
                        }
                }
        }
        $title[] = "Accountprofil bearbeiten";
        $root[] = "<a href=\"".AFFILIATES."account/\">Accountprofil bearbeiten</a>";
}




if ($col == "affiliates") {
        if ($cat == "ads") {
                if (!$site) {
                        if ($file == "form" OR $file == "delete") {
                                $allow = 1;
                                $ads = new AffiliatesAds();
                                if ($id) {
                                        $ads->load($id);
                                        if (!$ads->id) $error = 1;
                                        if ($ads->ads->ads_affiliates_id != $account->id) $error = 1;
                                }
                                $title[] = "Werbemittel anlegen / bearbeiten";
                                $root[] = "<a href=\"".AFFILIATES."affiliates/ads/form&id=".$ads->id.".html\">Werbemittel anlegen / bearbeiten</a>";
                        }

                        if ($file == "start") {
                                $allow = 1;
                        }
                }
                $title[] = "Werbemittel";
                $root[] = "<a href=\"".AFFILIATES."affiliates/ads/\">Werbemittel</a>";
        }






        if ($cat == "links") {
                if (!$site) {

                        if ($file == "form" OR $file == "delete") {
                                $allow = 1;
                                $ads = new AffiliatesAds();
                                if ($id) {
                                        $ads->load($id);
                                        if (!$ads->id) $error = 1;
                                        if ($ads->ads->ads_affiliates_id != $account->id) $error = 1;
                                }
                                $title[] = "Textlink anlegen / bearbeiten";
                                $root[] = "<a href=\"".AFFILIATES."affiliates/links/form&id=".$ads->id.".html\">Textlink anlegen / bearbeiten</a>";
                        }

                        if ($file == "start") {
                                $allow = 1;
                        }
                }
                $title[] = "Textlinks";
                $root[] = "<a href=\"".AFFILIATES."accaffiliates/links/\">Textlinks</a>";
        }

        if (!$cat) {
                if (!$site) {

                        if ($file == "start") {
                                $allow = 1;
                                $forward = AFFILIATES."affiliates/ads/";
                        }
                }
        }
}

if (!$col) {
        if (!$cat) {
                if (!$site) {
                        if ($file == rights) {
                                $allow = 1;
                                $title[] = $root[] = "Unbefugter Zugriff";
                        }
                        if ($file == 404) {
                                $allow = 1;
                                $title[] = $root[] = "404: Seite nicht gefunden";
                        }

                        if ($file == clicks) {
                                $allow = 1;
                                $title[] = "Klickstatistik";
                                $root[] = "<a href=\"".AFFILIATES."\">Klickstatistik</a>";
                        }

                        if ($file == sales) {
                                $allow = 1;
                                $title[] = "Salestatistik";
                                $root[] = "<a href=\"".AFFILIATES."\">Salestatistik</a>";
                        }

                        if ($file == start) {
                                $allow = 1;
                                $title[] = "Statistik";
                                $root[] = "<a href=\"".AFFILIATES."\">Statistik</a>";
                        }
                }
        }
}

# menu
if (!$account->id) {
        $menu->add("","","Login");
}


if ($account->id) {

        $aMenu['Statistik']['Url']	= "";

        $aMenu['Werbemittel']['Url'] = "affiliates/ads/";
        $aMenu['Werbemittel']['Slave']['Werbemittel']['Url'] = AFFILIATES."affiliates/ads/";
        $aMenu['Werbemittel']['Slave']['Werbemittel']['Slave']['Werbemittel']['Slave']['Werbemittel hinzufügen']['Url'] = AFFILIATES."affiliates/ads/form.html";
        $aMenu['Werbemittel']['Slave']['Textlinks']['Url'] = AFFILIATES."affiliates/links/";
        $aMenu['Werbemittel']['Slave']['Textlinks']['Slave']['Textlinks']['Slave']['Textlink hinzufügen']['Url'] = AFFILIATES."affiliates/links/form.html";
        $aMenu['Accountoptionen']['Url'] = "account/";
        $menu->GenerateNavigation($aMenu);

}




# stop
if ($root) {
        $root = array_reverse($root);
        $parts = count($root);
        $x = 0;
        foreach($root as $val) {

                $x++;
                if ($parts == $x) {
                        $bullet_color = "blue";
                        $font_weight = "bold";
                } else {
                        $bullet_color = "black";
                        $font_weight = "";
                }

                for($y=1;$y<$x;$y++) {
                        $bread .= "&nbsp;&nbsp;&nbsp;";
                }

                $bread .= "<img src=\"".AFFILIATES."tpl/images/bullet_".$bullet_color.".png\" class=\"icon\"> <span class=\"".$font_weight."\">".$val."</span><br>";
        }

}
if (!$title) $error = 1;

if (!$allow) $error = 1;
$title[] = "Druckerpatronen.de Partnerprogramm";
$title = implode(" &raquo; ",$title);
if ($forward) {
        header("Location:".$forward);
        exit;
}
#if ($error) { header("Location: ".AFFILIATES."404.html"); exit; } ?>