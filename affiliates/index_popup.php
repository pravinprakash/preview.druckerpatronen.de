<?php

error_reporting(E_ALL ^ E_NOTICE);
ob_start();
// load config data
include ("../inc/inc.config.php");
session_start();
if (!$validate) { $validate = new validateForm(); }
// END load config data


// extract superglobals
$vars = array("id","col","file","cat","action");
foreach($vars as $val)
{
	${$val} = $_REQUEST[$val];
}

// extract superglobals


if (!$file)
{
	$file = start;
}

// load template files
include ("inc/inc.popup.access.php");
include ("tpl/tpl.popup.header.php");

if (!$account->id)
{
	$cat = ""; $subcat = ""; $file = "login";
}

if ($col)
{
	if ($cat)
	{
		include("content/popup/".$col."/".$cat."/".$file.".php");
	} else {
		include("content/popup/".$col."/".$file.".php");		
	}
	
} else if ($cat) {
	include("content/popup/".$cat."/".$file.".php");
} else {
	include ("content/popup/".$file.".php");
}
include ("tpl/tpl.popup.footer.php");
// END load template files

if ($alert) { ?>
<script language="javaScript">
alert("<? echo $alert; ?>");
</script>
<? } 

if ($goto) { goto($goto); }
ob_end_flush(); 
 ?>

