<h1>Impressum</h1>
<?dotted();?>

<h2>Anschrift</h2>
  <table class="table">
   <tr>
    <th width="150">
	 Hauptsitz:
    </th>
    <td>
     novinet GmbH & Co. KG<br />
  	 Marie-Curie-Stra&szlig;e 6<br />
  	 D - 85055 Ingolstadt
    </td>
   </tr>
   <tr>
    <th>
	 B&uuml;ro Stuttgart:
    </th>
    <td>
     Heusteigstr. 13<br/>
  	 70182 Stuttgart
    </td>
   </tr>   
  </table>  
  
  <br>

<h2>Weitere Angaben</h2>
  <table class="table">
   <tr>
    <th width="150">
	 Gesch&auml;ftsf&uuml;hrer:<br>
	 Sitz:<br>
	 Amtsgericht:<br>
	 UST-ID:
    </th>
    <td>
     Daniel Steffen<br>
     Stegaurach<br>
     AG Bamberg, HRA 11041<br>
     DE815045234
    </td>
   </tr>
   <tr>
    <th>
	 p.h.G:<br>
	 Sitz:<br>
	 Amtsgericht:
    </th>
    <td>
     Daniel Steffen<br>
     Stegaurach<br>
     AG Bamberg, HRB 6180
    </td>
   </tr>   
  </table>   

<br>

<h2>Bankverbindung</h2> 
  <table class="table">
   <tr>
    <th width="150" >
     Kontonummer:<br>
     Bankleitzahl:<br>
     Kreditinstitut:<br>
     Bic- / Swift-Code:<br>
     IBAN:
    </th>
    <td>
     300619129
     <br>
     77050000
     <br>
     Sparkasse Bamberg
     <br>
     BYLADEM15KB
     <br>
     DE24770500000300619129
    </td>
   </tr>
  </table>  

