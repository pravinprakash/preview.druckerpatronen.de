<? $press->print_details(1); ?>

<? box_content_top(); ?>
 <h3>Weitere Informationen zur Pressemitteilung</h3>
 
<br />
Wir freuen uns, wenn Sie einen Link zu uns setzen oder diese Pressemitteilung weiterempfehlen.
 <br /><br />
 <div style="float:left;width:33%;text-align:center">
  <a href="<?=PATH?>modal/blog/request&id=<?=$content->id?>.html" <? modal() ?> title="Frage zum Artikel stellen"><img src="<?=PATH?>tpl/images/icons/fragezeichen.png" class="icon"></a>
 </div>
 
 <div style="float:left;width:33%;text-align:center">
  <a href="<?=PATH?>modal/blog/print&id=<?=$content->id?>.html" <? modal() ?> title="Artikel drucken"><img src="<?=PATH?>tpl/images/icons/drucken.png" class="icon"></a>
 </div> 
 
 <div style="float:left;width:33%;text-align:center">
<!-- Seitzeichen -->
<script type="text/javascript">var szu=encodeURIComponent(location.href); var szt=encodeURIComponent(document.title).replace(/\'/g,'`'); var szjsh=(window.location.protocol == 'https:'?'https://ssl.seitzeichen.de/':'http://w3.seitzeichen.de/'); document.write(unescape("%3Cscript src='" + szjsh + "w/3d/10/widget_3d10757155c44e8c3c7521b88efd9a51.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- Seitzeichen -->
 </div> 
  
<div class="clearer"></div>

<br />
 <b>Link zur Pressemitteilung</b>
 <br />
 <input type="text" class="input" style="width:100%" id="linkurl" readonly value="<?=$press->url?>">
 <br />
 <a href="javascript:void(0)" onclick="copytext('linkurl')">Link markieren</a>
 <br /><br />
 <b>Quellode zum Link</b>
 <br />
 <textarea class="textarea" style="width:100%;height:40px" id="linktext" readonly>&lt;a href=&quot;<?=$press->url?>&quot; title=&quot;<?=$press->press->press_title?>&quot;&gt;<?=$press->press->press_title?>&lt;/a&gt;</textarea>
<br />
<a href="javascript:void(0)" onclick="copytext('linktext')">Quellcode markieren</a>

<? box_content_bottom(); ?>

<br /><br />

<? $press->print_more_press(); ?> 

<div align="right">
 <a href="#top">nach oben</a>
</div> 
