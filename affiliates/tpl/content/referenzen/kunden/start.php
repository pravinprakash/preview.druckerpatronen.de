<h1>Unsere Kunden</h1>
<?dotted(); ?>

<? $nums = get_rows(references_clients,"");
$le = round($nums/2);
$ri = $nums-$le; ?>

<div style="width:45%;margin-right:5%;float:left;line-height:25px;">
 <? $dbQ = db_q(references_clients,"ORDER BY clients_name ASC Limit 0,$le");
 while($res = read($dbQ))
 { ?>
 	<? if ($res->clients_url)
 	{ ?>
 		<a href="<?=$res->clients_url?>" target="_blank">&bull; <?=$res->clients_name?></a><br />
 	<? } else { ?>
 		&bull; <?=$res->clients_name?><br />
 	<? }
 } ?>
</div>
<div style="width:45%;margin-right:5%;float:left;line-height:25px;">
 <? $dbQ = db_q(references_clients,"ORDER BY clients_name ASC Limit $le,$ri");
 while($res = read($dbQ))
 { ?>
 	<? if ($res->clients_url)
 	{ ?>
 		<a href="<?=$res->clients_url?>" target="_blank">&bull; <?=$res->clients_name?></a><br />
 	<? } else { ?>
 		&bull; <?=$res->clients_name?><br />
 	<? }
 } ?>
</div>
<br style="clear:both">