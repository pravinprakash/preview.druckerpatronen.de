<h1>Fragen? Kontaktieren Sie uns direkt &uuml;ber unser Formular</h1>
<?dotted();?>

<? if ($action == send_do)
  {
  	$error = $validate->checkEmpty("firstname,lastname,email,subject,text,captcha");
  	$error = $validate->checkEmail("email");
  	$error = $validate->checkCaptcha("captcha");

	$form = array(firstname,lastname,email,subject,text,company);
	foreach($form as $val)
	{
		${$val} = form2db($_POST[$val]);
		$_POST[$val] = db2form(${$val});	
	} 

  	if (!$error)
  	{
  		

  		$contact = new contact();
  		$contact->set(array(company=>$company_m));
  		$contact->mail($firstname_m,$lastname_m,$email_m,$subject_m,$text_m);
  		$alert = "Vielen Dank. Die Nachricht wurde erfolgreich gespeichert. Wir werden uns umgehend um Ihr Anliegen k�mmern.";
  		$goto = PATH;
  	} ?>
  
  	<? if ($error)
  	{ ?>
  		<div class="errorBox">
	 	 <h2>Es ist ein Fehler aufgetreten. Bitte �berpr�fen Sie Ihre Angaben</h2>   
		</div>
	<? }
  	
  } ?>
  
<form method="post"> 
<fieldset class="fieldset noborder">
 <ul class="listForm">
  <li>
   <label for="firstname">
    Vorname <? star(); ?>
   </label>
   <input type="text" id="firstname" name="firstname" class="input<?=$validate->printStyle("firstname")?>" value="<?=$_POST[firstname]?>" />
  </li>
  <li>
   <label for="lastname">
    Nachname <? star(); ?>
   </label>
   <input type="text" id="lastname" name="lastname" class="input<?=$validate->printStyle("lastname")?>" value="<?=$_POST[lastname]?>"/>
  </li>
  <li>
   <label for="company">
    Firma <? star(); ?>
   </label>
   <input type="text" id="company" name="company" class="input" value="<?=$_POST[company]?>">
  </li>
  <li>
   <label for="email">
    E-Mailadresse <? star(); ?>
   </label>
   <input type="text" id="email" name="email" class="input<?=$validate->printStyle("email")?>" value="<?=$_POST[email]?>">
  </li>    
  <li>
   <label for="subject">
    Betreff <? star(); ?>
   </label>
   <input type="text" id="subject" name="subject" class="input<?=$validate->printStyle("subject")?>" value="<?=$_POST[subject]?>">
  </li>
  <li>
   <label for="message">
   	Nachricht <? star(); ?>
   </label>
   <textarea id="message" name="text" class="textarea<?=$validate->printStyle("text")?>" style="width:360px;height:150px;"><?=$_POST[text]?></textarea>
  </li>
  <li>
  Bitte geben Sie den im rechten Bereich sichtbaren vierstelligen Code in das linke Feld <span class="almostblack">"Sicherheitscode"</span> ein.
  </li>
  <li>
   <label for="captcha">
   	Sicherheitscode <? star(); ?>
   </label>
   <input type="text" id="captcha" name="captcha" class="input<?=$validate->printStyle("captcha")?>">
  </li>
  <li>
   <label for="captcha">
   	&nbsp;
   </label>
   <img src="<?=PATH?>core/captcha/inc.captcha.php">
  </li>
 </ul>
 <input type="hidden" name="action" value="send_do">
 <input type="submit" name="submit" value="Senden &raquo;" class="submit"> 
</fieldset>
</form>
