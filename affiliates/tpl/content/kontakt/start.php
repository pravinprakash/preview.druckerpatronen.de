<h1>So finden Sie uns</h1>
<?dotted();?>

<h2>Unsere Anschrift</h2>
novinet GmbH & Co. KG<br />
Marie-Curie-Stra&szlig;e 6<br />
D - 85055 Ingolstadt

<br><br>

<img src="<?=PATH?>tpl/images/karte.png" alt="Anfaht zu novinet">

<br /><br />

<b><a href="http://maps.google.de/maps?daddr=Marie+Curie+Strasse+11,+85055+Ingolstadt" target="_blank"><img src="<?=PATH?>tpl/images/link.png" class="icon"> Route berechnen</a></b>


<br><br><br>

<b><img src="<?=PATH?>tpl/images/anfahrt_auto.png" class="icon"> Autobahn A9 M�nchen-N�rnberg</b>: 
<br><br>
<li>Anschlu�stelle Ingolstadt-Nord (61) in Richtung Vohburg/ Gro�mehring/ Ingolstadt-Ost</li> 
<br>
<li>auf der B16a f&uuml;r 1,0 km bleiben und anschlie&szlig;end nach rechts auf die L2231 (Abfahrt Gewerbepark Nord-Ost) abbiegen</li> 
<br>
<li>Stra�enverlauf f�r 1,2 km folgen und links in die Isaak-Newton-Stra�e abbiegen.</li> 
<br>
<li>nach 300 m links in die Marie-Curie-Stra�e einbiegen</li> 
<br>
<li>f&uuml;r 400 m dem Stra&szlig;enverlauf folgen und dann auf den Parkplatz des EGZ abbiegen</li>
