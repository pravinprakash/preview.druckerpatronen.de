<div class="BBox">
 <div class="BBox_head">
  <h2>Anfahrt zu novinet</h2>
 </div>
 	
 <div class="BBox_content">
  <div class="inner">
<b><img src="<?=PATH?>tpl/images/anfahrt_auto.png" class="icon"> A9 M�nchen-N�rnberg</b>: 
<br><br>
<li>Anschlu�stelle Ingolstadt-Nord (61) in Richtung Vohburg/ Gro�mehring/ Ingolstadt-Ost</li> 
<br>
<li>auf der B16a f&uuml;r 1,0 km bleiben und anschlie&szlig;end nach rechts auf die L2231 (Abfahrt Gewerbepark Nord-Ost) abbiegen</li> 
<br>
<li>Stra�enverlauf f�r 1,2 km folgen und links in die Isaak-Newton-Stra�e abbiegen.</li> 
<br>
<li>nach 300 m links in die Marie-Curie-Stra�e einbiegen</li> 
<br>
<li>f&uuml;r 400 m dem Stra&szlig;enverlauf folgen und dann auf den Parkplatz des EGZ abbiegen</li>
<br>
<b><a href="http://maps.google.de/maps?daddr=Marie+Curie+Strasse+11,+85055+Ingolstadt" target="_blank"><img src="<?=PATH?>tpl/images/link.png" class="icon"> Route berechnen</a></b>
  </div>
 </div>
</div>