<div class="BBox">
 <div class="BBox_head">
  <h2>Kontakt</h2>
 </div>
 	
 <div class="BBox_content">
  <div class="inner">
   <b>B&uuml;rozeiten Mo - Fr 9 - 17 Uhr</b>
   <table class="table">
    <tr>
     <th width="30">
      tel:<br>
      fax:<br>
      email:
     </th>
     <td>
      0841 881 79 73-0
      <br>
      0841 881 79 73-8
      <br>
      info[at]novinet.de
     </td>
    </tr>
   </table>
  </div>
 </div>
</div>