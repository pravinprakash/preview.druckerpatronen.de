<div class="BBox">
 <div class="BBox_head">
  <h2>Kostenloser PDF Reader</h2>
 </div>
 	
 <div class="BBox_content">
  <div class="inner">
   Um unsere Pressemitteilungen im PDF-Format &ouml;ffnen zu k&ouml;nnen, ben&ouml;tigen Sie den kostenlosen Adobe Reader.
   <br /><br />
   <a href="http://www.adobe.com/de/products/reader/" target="_blank" title="Adobe Reader kostenlos herunterladen" alt="Adobe Reader kostenlos herunterladen"><?icon(pdf)?> Kostenlos herunterladen</a>
  </div>
 </div>
</div>