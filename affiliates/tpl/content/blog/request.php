<h1>Frage zum Artikel <?=$content->content->content_title?> stellen</h1>

<? if ($action == send_do)
{
	$error = $validate->checkEmpty("firstname,lastname,email,text,captcha");
  	$error = $validate->checkEmail("email");
  	$error = $validate->checkCaptcha("captcha");
  	
	$form = array(firstname,lastname,email,text,newsletter,company);
	foreach($form as $val)
	{
		${$val} = form2db($_POST[$val]);
		$_POST[$val] = db2form(${$val});	
	}  	
  	
  	if (!$error)
  	{
  		if ($newsletter)
  		{
			$tmp = read(db_q(newsletter_user,"WHERE user_email = '$email' Limit 1"));		
			if (!$tmp)
			{
				$res = db_i(newsletter_user,"user_firstname='$firstname',user_lastname='$lastname',user_company='$company',user_email='$email'");
  			}
  		}
  		
  		$content->set(array(company=>$company));
  		$content->add_request($firstname,$lastname,$email,$text);
  		$alert = "Vielen Dank. Die Nachricht wurde erfolgreich gespeichert. Wir werden uns umgehend um Ihr Anliegen k�mmern.";
  		$goto_modal = $content->url;
  	}
} ?>

<a name="request"></a>
<div class="servicebox" style="background-color: #efefef">
 <h2>Haben Sie Fragen?</h2>
 Wenn Sie Fragen haben oder einen pers&ouml;nliches Gespr&auml;ch w�nschen, k�nnen Sie uns bequem kontaktieren.
 <br><br>
 Geben Sie Ihre Fragen direkt in das folgende Formular ein.
</div>
  
<? if ($error)
{ ?>
 <div class="errorBox">
  <h2>Es ist ein Fehler aufgetreten. Bitte �berpr�fen Sie Ihre Angaben.</h2>   
 </div>
<? } else {
	$_POST[newsletter] = 1;
} ?>
  
  
<form method="post" action="#request"> 
<fieldset class="fieldset noborder">
 <ul class="listForm">
  <li>
   <label for="firstname">
    Vorname <? star(); ?>
   </label>
   <input type="text" id="firstname" name="firstname" class="input<?=$validate->printStyle("firstname")?>" value="<?=$_POST[firstname]?>" />
  </li>
  <li>
   <label for="lastname">
    Nachname <? star(); ?>
   </label>
   <input type="text" id="lastname" name="lastname" class="input<?=$validate->printStyle("lastname")?>" value="<?=$_POST[lastname]?>"/>
  </li>
  <li>
   <label for="company">
    Firma
   </label>
   <input type="text" id="company" name="company" class="input" value="<?=$_POST[company]?>">
  </li>
  <li>
   <label for="email">
    E-Mailadresse <? star(); ?>
   </label>
   <input type="text" id="email" name="email" class="input<?=$validate->printStyle("email")?>" value="<?=$_POST[email]?>">
  </li>    
  <li>
   <label for="message">
   	Nachricht <? star(); ?>
   </label>
   <textarea id="message" name="text" class="textarea<?=$validate->printStyle("text")?>" style="width:360px;height:150px;"><?=$_POST[text]?></textarea>
  </li>
  <li>
   <label for="newsletter" style="width:100% !important;text-align:left">
   	<input type="checkbox" id="newsletter" name="newsletter" value="1" <? if ($_POST[newsletter]) echo checked; ?>> Ja, ich m&ouml;chte k&uuml;nftig Informationen per E-Mail erhalten.
   </label>
   <br/><br />
  </li>
  <li>
  Bitte geben Sie den im rechten Bereich sichtbaren vierstelligen Code in das linke Feld <span class="almostblack">"Sicherheitscode"</span> ein.
  </li>
  <li>
   <label for="captcha">
   	Sicherheitscode <? star(); ?>
   </label>
   <input type="text" id="captcha" name="captcha" class="input<?=$validate->printStyle("captcha")?>">
  </li>
  <li>
   <label for="captcha">
   	&nbsp;
   </label>
   <img src="<?=PATH?>core/captcha/inc.captcha.php">
  </li>
 </ul>
 <input type="hidden" name="action" value="send_do">
 <input type="submit" name="submit" value="Senden &raquo;" class="submit"> 
</fieldset>
</form>