<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><? echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Language" content="de">
<meta name="description" content="<? echo $description; ?>">
<meta name="keywords" content="<? echo $keywords; ?>">
<meta name="expires" file="never">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="7 days">
<link rel="stylesheet" type="text/css" href="<?=PATH?>css/backend/backend.css">
<!--[if IE]><link rel="stylesheet" type="text/css" href="<?=PATH?>css/backend/backend_ie.css"><![endif]-->
<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?=PATH?>css/backend/backend_ie6.css"><![endif]-->
<link rel="stylesheet" href="<?php echo PATH; ?>css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=PATH?>inc/js/jsload.js"></script>

<script type="text/javascript" src="<?php echo PATH; ?>inc/js/prototype.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/lightbox.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/popup.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/effects.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/motionpack.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>inc/js/ajax.js"></script>

<? /* ?>
<script type="text/javascript" src="<?=PATH?>inc/js/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});
</script>

<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "text",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,cut,copy,paste,pastetext,pasteword",
		theme_advanced_buttons2 : "search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,template",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",

		// Example content CSS (should be your site CSS)
		content_css : "<?=PATH?>css/frontend.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

	});
</script>
<? */ ?>

<? include("../inc/inc.functions_js.php"); ?>
</head>
<body>

<center>
<div id="Content">
 <div id="mainContent">
  
  <div id="headTopLeft">
   <img src="<?=PATH?>img/pixel.gif" border="0" />
  </div>
  
  <div id="headTopRight">
   <img src="<?=PATH?>img/pixel.gif" border="0" />
  </div>
  

  
