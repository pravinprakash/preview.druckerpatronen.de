<h1>Statistik</h1>


<? $form = array(month,year,type,export_clicks,export_views);
foreach($form as $val) {
        ${$val} = form2db($_REQUEST[$val]);
        $_REQUEST[$val] = db2form(${$val});
}

$where = " && affiliates_id = '$account->id' ";


if ($month == "") {
        $month = date(n);
        $_REQUEST[month] = $month;
}

if (!$year) {
        $year = date(Y);
        $_REQUEST[year] = $year;
}


if ($month) {
        $start = mktime(0,0,0,$month,1,$year);
        $end = mktime(0,0,0,$month+1,1,$year);
} else {
        $start = mktime(0,0,0,1,1,$year);
        $end = mktime(0,0,0,1,1,$year+1);
}

?>



<form method="post">
        <? fieldset("Filter"); ?>
        <table class="table_blank">
                <tr>
                        <th>
                                Monat
                        </th>
                        <td>
                                <select name="month" id="month" class="select">
                                        <option value="0" <? if (!$_REQUEST[month]) echo selected; ?>>Alle</option>
                                        <? for($x=1;$x<13;$x++) { ?>
                                        <option value="<?=$x?>" <? if($_REQUEST[month] == $x) echo selected; ?>><?=convert_months($x)?></option>
                                                <? } ?>
                                </select>
                        </td>
                </tr>
                <tr>
                        <th width="30%">
                                Jahr
                        </th>
                        <td>
                                <select name="year" class="select">
                                        <? for($x=2009;$x<date(Y)+1;$x++) { ?>
                                        <option value="<?=$x?>" <? if ($year == $x) echo selected; ?>><?=$x?></option>
                                                <? } ?>
                                </select>
                        </td>
                </tr>
                <tr class="blank">
                        <td></td>
                        <td>
                                <br>
                                <input type="submit" class="submit" value="Filtern"> <? /* ?> <input type="submit" name="export_clicks" class="submit" value="Klicks Exportieren"><? */ ?>
                        </td>
                </tr>
        </table>
</fieldset>
</form>
<br>

<? fieldset("Quick view"); ?>
<table class="table_blank">

        <tr class="blank" style="background-color:#cccccc !important">
                <th width="20%"></th>
                <th width="20%" align="center">Views</th>
                <th width="20%" align="center">Klicks</th>
                <th width="20%" align="center">Sales</th>


                <th width="20%" align="center">Provision</th>
        </tr>
        <tr>

                <th>Heute</th>
                <? $day_start = mktime(0,0,0,date(m),date(d),date(Y));
                $day_end = mktime(0,0,0,date(m),date(d)+1,date(Y)); ?>
                <td align="center">
                        <? $res = read(db_sum(affiliates_ads_views,"views","views","WHERE date = '$day_start' $where"));
                        if ($res->views != "") echo $res->views; else echo "0"; ?>
                </td>
                <td align="center">
                        <? $clicks = get_rows(affiliates_ads_clicks,"WHERE date >= '$day_start' && date < '$day_end' $where");
                        echo $clicks; ?>
                </td>
                <td align="center">
                        <? $sales = get_rows(affiliates_sales,"WHERE date >= '$day_start' && date < '$day_end' $where");
                        echo $sales; ?>
                </td>


                <td align="center">
                        <? $res = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date != '0' && date >= '$day_start' $where"));
                        if ($res->provision != "") echo format_number($res->provision); else echo format_number(0); ?> EUR
                </td>
        </tr>
        <tr>

                <th>Gestern</th>
                <? $day_start = mktime(0,0,0,date(m),date(d)-1,date(Y));
                $day_end = mktime(0,0,0,date(m),date(d),date(Y)); ?>
                <td align="center">
                        <? $res = read(db_sum(affiliates_ads_views,"views","views","WHERE date = '$day_start' $where"));
                        if ($res->views != "") echo $res->views; else echo "0"; ?>
                </td>
                <td align="center">
                        <? $clicks = get_rows(affiliates_ads_clicks,"WHERE date >= '$day_start' && date < '$day_end' $where");
                        echo $clicks; ?>
                </td>
                <td align="center">
                        <? $sales = get_rows(affiliates_sales,"WHERE date >= '$day_start' && date < '$day_end' $where");
                        echo $sales; ?>
                </td>


                <td align="center">
                        <? $res = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date != '0' && date >= '$day_start' && date < '$day_end' $where"));
                        if ($res->provision != "") echo format_number($res->provision); else echo format_number(0); ?> EUR
                </td>
        </tr>
        <tr>
                <th>Aktuelle Woche</th>
                <? $date = time()-((date("N")-1)*86400);
                $date = mktime(0,0,0,date(m,$date),date(d,$date),date(Y,$date));?>
                <td align="center">
                        <? $res = read(db_sum(affiliates_ads_views,"views","views","WHERE date >= '$date' $where"));
                        if ($res->views != "") echo $res->views; else echo "0"; ?>
                </td>
                <td align="center">
                        <? $clicks = get_rows(affiliates_ads_clicks,"WHERE date >= '$date' $where");
                        echo $clicks; ?>
                </td>
                <td align="center">
                        <? $sales = get_rows(affiliates_sales,"WHERE date >= '$date' $where");
                        echo $sales; ?>
                </td>


                <td align="center">
                        <? $res = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date != '0' && date >= '$date' $where"));
                        if ($res->provision != "") echo format_number($res->provision); else echo format_number(0); ?> EUR
                </td>
        </tr>
        <tr>

                <th>Aktueller Monat</th>
                <? $date = mktime(0,0,0,date(m),1,date(Y)); ?>
                <td align="center">
                        <? $res = read(db_sum(affiliates_ads_views,"views","views","WHERE date >= '$date' $where"));
                        if ($res->views != "") echo $res->views; else echo "0"; ?>
                </td>
                <td align="center">
                        <? $clicks = get_rows(affiliates_ads_clicks,"WHERE date >= '$date' $where");
                        echo $clicks; ?>
                </td>
                <td align="center">
                        <? $sales = get_rows(affiliates_sales,"WHERE date >= '$date' $where");
                        echo $sales; ?>
                </td>


                <td align="center">
                        <? $res = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date != '0' && date >= '$date' $where"));
                        if ($res->provision != "") echo format_number($res->provision); else echo format_number(0); ?> EUR
                </td>
        </tr>
</table>
</fieldset>

<br>


<? if ($month == "0") {
        $for_start = 1;
        $for_end = 13;
}
else {
        $for_start = $month;
        $for_end = $month+1;
}

for($x=$for_start;$x<$for_end;$x++) {
        $month_start = mktime(0,0,0,date($x),1,$year);
        $days = date(t,$month_start);
        $month_clicks = 0;
        $month_clicks = 0;
        $month_views = 0; ?>

<h2><?=convert_months($x); ?> <?=$year?></h2>
<br>
<table class="table">
        <tr>
                <th width="10%" align="center">
                        Datum
                </th>

                <th width="8%" align="center">
                        Views
                </th>
                <th width="8%" align="center">
                        Klicks
                </th>
                <th width="10%" align="center">
                        Sales
                </th>
                <th width="12%" align="center">
                        Sales (bestätigt)
                </th>
                <th width="12%" align="center">
                        Sales (offen)
                </th>
                <th width="12%" align="center">
                        Sales (storniert)
                </th>

                <th></th>
        </tr>

                <? for($y=1;$y<$days+1;$y++) {
                        $day_start = mktime(0,0,0,$x,$y,$year);
                        $day_end = mktime(0,0,0,$x,$y+1,$year); ?>
        <tr>
                <td align="center">
                                        <?=date("d.m.Y",$day_start); ?>
                </td>
                <td align="center">
                                        <? $views = read(db_sum(affiliates_ads_views,"views","views","WHERE date = '$day_start' $where"));
                                        if ($views->views != "") echo $views->views; else echo "0"; ?>
                </td>
                <td align="center">
                                        <? $clicks = get_rows(affiliates_ads_clicks,"WHERE date >= '$day_start' && date < '$day_end' $where");
                                        echo $clicks; ?>
                </td>
                <td align="center">
                                        <? $sales = get_rows(affiliates_sales,"WHERE date >= '$day_start' && date < '$day_end' $where");
                                        echo $sales; ?>
                </td>
                <td align="center">

                        <? $confirmed = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date != '0' && date >= '$day_start' && date < '$day_end' $where"));
                        if ($confirmed->provision != "") echo format_number($confirmed->provision); else echo format_number(0); ?> EUR
                </td>
                <td align="center">
                        <? $pending = read(db_sum(affiliates_sales,"provision","provision","WHERE confirmed_date = '0' && cancelled_date = '0' && date >= '$day_start' && date < '$day_end' $where"));
                        if ($pending->provision != "") echo format_number($pending->provision); else echo format_number(0); ?> EUR
                </td>
                <td align="center">
                        <? $cancelled = read(db_sum(affiliates_sales,"provision","provision","WHERE cancelled_date != '0' && date >= '$day_start' && date < '$day_end' $where"));
                        if ($cancelled->provision != "") echo format_number($cancelled->provision); else echo format_number(0); ?> EUR
                </td>

                <td align="right">
                        <a href="<?=AFFILIATES?>clicks&day=<?=date(d,$day_start)?>&month=<?=date(m,$day_start)?>&year=<?=date(Y,$day_start); ?>.html"><?icon(magnifier); ?> Klicks</a> <a href="<?=AFFILIATES?>sales&day=<?=date(d,$day_start)?>&month=<?=date(m,$day_start)?>&year=<?=date(Y,$day_start); ?>.html"><?icon(magnifier); ?>Sales </a>
                </td>

        </tr>



                        <? $month_sales += $sales;
                        $month_sales_confirmed += $confirmed->provision;
                        $month_sales_pending += $pending->provision;
                        $month_sales_cancelled += $cancelled->provision;
                        $month_clicks += $clicks;
                        $month_views += $views->views;
                } ?>



        <tr>
                <th colspan="1">
                        Summe:
                        </td>
                <th align="center">
                                <?=$month_views?>
                </th>   
                <th align="center">
                                <?=$month_clicks?>
                </th>

                <th align="center">
                                <?=$month_sales?>
                </th>
                <th align="center">
                                <?=format_number($month_sales_confirmed)?> EUR
                </th>
                <th align="center">
                                <?=format_number($month_sales_pending)?> EUR
                </th>
                <th align="center">
                                <?=format_number($month_sales_cancelled)?> EUR
                </th>

                <th></th>
        </tr>
</table>
<br>
        <? } ?>

