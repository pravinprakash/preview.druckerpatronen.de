<? if ($action == save_do) {
        $error = $validate->checkEmpty("firstname,lastname,email");
        $error = $validate->checkEmail("email");
        if ($_POST[pass1]) {
                $error = $validate->checkEqual(pass1,pass2);
        }


	$form = array(name,pass,pass1,pass2,firstname,lastname,email,mobile,company,gender,lastname,firstname,address,zip,city,country,tel,fax,mobile,email,payment,payment_kontonr,payment_blz,payment_owner,payment_bank,payment_paypal,taxnr,ustid);

	foreach($form as $val) {
		${$val} = form2db($_POST[$val]);
		$_POST[$val] = form2db($_POST[$val]);
	}

        $name = strtolower($name);

        if ($pass1) {
                if (strlen($pass1) < 4) {
                        $error = $validate->addErrorFields("pass1,pass2,pass_len");
                }
        }

        if (!$error) {

                $aData = array(
			address => $address,
			city => $city,
			country => $country,
			email => $email,
			email => $email,
			fax => $fax,
			firstname => $firstname,
			gender => $gender,
			lastname => $lastname,
			mobile => $mobile,
			payment_owner => $payment_owner,
			payment_bank => $payment_bank,
			payment_blz => $payment_blz,
			payment => $payment,
			payment_kontonr => $payment_kontonr,
			payment_paypal => $payment_paypal,
			taxnr => $taxnr,
			tel => $tel,
			ustid => $ustid,
			zip => $zip,
                );

                if ($pass) $aData[pass] = md5($pass);
                if ($pass1) $aData[pass] = md5($pass1);

                $account->save($aData);

                $alert = "Ihre Angaben wurden erfolgreich gespeichert.";
                $goto = AFFILIATES."account/";
        }

} ?>

<? if ($error) { ?>
<div class="errorBox">
        <h2><img src="<?=PATH?>img/icons/cross_16.png" class="icon"> Es ist ein Fehler aufgetreten - bitte korrigieren Sie Ihre Angaben.</h2>
                <? $validate->printError("exists","Ein Benutzer mit diesem Benutzernamen ist bereits vorhanden."); ?>
                <? $validate->printError("name_false","Der Benutzername hat ein falsches Format - richtig: vorname.nachname"); ?>
                <? $validate->printError("pass_len","Das Passwort ist zu kurz. Mindestl�nge: 4 Zeichen."); ?>
</div>
<br>
        <? } else if ($account->id) {

        $form = array(name,firstname,lastname,email,gender,address,zip,city,country,tel,fax,mobile,email,payment,payment_kontonr,payment_blz,payment_owner,payment_bank,payment_paypal,taxnr,comment,ustid,);
        foreach($form as $val) {
                $_POST[$val] = db2form($account->affiliates->{"affiliates_".$val});
        }

        $form = array(company,provision,provision_type);
        foreach($form as $val) {
                ${$val} = $account->affiliates->{"affiliates_".$val};
        }

} ?>

<form name="form" method="post">
        <fieldset>
                <legend>Accountprofil bearbeiten</legend>
                <table class="table_blank">
                        <colgroup>
                                <col width="30%"><col>
                        </colgroup>
                        <tr>
                                <th>
					Benutzername <?star()?>
                                        </td>
                                <td>
                                        <?=$_POST[name]?>
                                </td>
                        </tr>

                        <tr>
                                <th width="30%">
				Anrede
                                </th>
                                <td>
                                        <select name="gender" class="select">
                                                <option value="Herr">Herr</option>
                                                <option value="Frau" <? if ($_POST[gender] == "Frau") echo selected; ?>>Frau</option>
                                        </select>
                                </td>
                        </tr>
                        <tr>
                                <th width="30%">
				Firma
                                        </td>
                                <td>
                                        <?=$company?>
                                </td>
                        </tr>
                        <tr>
                                <th>
					Vorname <?star()?>
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(firstname); ?>" name="firstname" value="<?=$_POST[firstname]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
					Nachname <?star()?>
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(lastname); ?>" name="lastname" value="<?=$_POST[lastname]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
					E-Mail <?star()?>
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(email); ?>" name="email" value="<?=$_POST[email]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Adresse. <?star()?>
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(address); ?>" name="address" value="<?=$_POST[address]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				PLZ / Ort <?star()?>
                                        </td>
                                <td>
                                        <input type="text" id="zip" class="input<? $validate->printStyle(zip); ?>" name="zip" value="<?=$_POST[zip]?>" style="width:50px" onkeyup="ajax_load('GET','<?=BACKEND?>ajax/ziplist&zip='+this.value+'.html','div_city');"> / <span id="div_city"><input type="text" class="input<? $validate->printStyle(city); ?>" name="city" value="<?=$_POST[city]?>" id="city"></span>
                                </td>
                        </tr>
                        <tr>
                                <th>
				Land <?star(); ?>
                                </th>
                                <td>
                                        <select name="country" class="select">
                                                <? $arr = array('DE','AT','CH');
                                                foreach($arr as $val) {
                                                        $country = read(db_q(countrylist,"WHERE country_short='$val'")); ?>
                                                <option value="<?=$country->country_short?>" <? if ($_POST[country] == $country->country_short) echo selected; ?>><?=utf8_encode($country->country_title)?></option>
                                                        <? } ?>
                                                <option value="">----------------</option>
                                                <? $dbQ = db_q(countrylist,"WHERE country_short != 'DE' && country_short != 'AT' && country_short != 'CH' ORDER BY country_title ASC");
                                                while($country = read($dbQ)) { ?>
                                                <option value="<?=$country->country_short?>" <? if ($_POST[country] == $country->country_short) echo selected; ?>><?=utf8_encode($country->country_title)?></option>
                                                        <? } ?>
                                        </select>
                                </td>
                        </tr>
                        <tr>
                                <th>
				Telefonnr.
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(tel); ?>" name="tel" value="<?=$_POST[tel]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Faxnr.
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(fax); ?>" name="fax" value="<?=$_POST[fax]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Mobilfunknr.
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(mobile); ?>" name="mobile" value="<?=$_POST[mobile]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				UstId
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(ustid); ?>" name="ustid" value="<?=$_POST[ustid]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Steuernr.
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(taxnr); ?>" name="taxnr" value="<?=$_POST[taxnr]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Zahlungsart
                                </th>
                                <td>
                                        <select name="payment" class="select">
                                                <option value="rechnung">Rechnung</option>
                                                <option value="lastschrift" <? if ($_POST[payment] == "lastschrift") echo selected; ?>>Lastschrift</option>
                                                <option value="paypal" <? if ($_POST[payment] == "paypal") echo selected; ?>>Paypal</option>
                                        </select>
                                </td>
                        </tr>
                        <tr>
                                <th>
				Kontonr
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(payment_kontonr); ?>" name="payment_kontonr" value="<?=$_POST[payment_kontonr]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Kontoinhaber
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(payment_owner); ?>" name="payment_owner" value="<?=$_POST[payment_owner]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				BLZ
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(payment_blz); ?>" name="payment_blz" value="<?=$_POST[payment_blz]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Bank
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(payment_bank); ?>" name="payment_bank" value="<?=$_POST[payment_bank]?>">
                                </td>
                        </tr>
                        <tr>
                                <th>
				Paypal - Emailadresse
                                        </td>
                                <td>
                                        <input type="text" class="input<? $validate->printStyle(payment_paypal); ?>" name="payment_paypal" value="<?=$_POST[payment_paypal]?>">
                                </td>
                        </tr>




                        <tr>
                                <td colspan="2">&nbsp;</td>
                        </tr>

                        <tr>
                                <th>
				Gutscheine
                                        </td>
                                <td>
                                        <? $dbQ = db_q(affiliates_coupons,"WHERE coupons_affiliates_id = '$account->id' && coupons_status = '1' ORDER BY coupons_code ASC");
                                        while($res = read($dbQ)) echo $res->coupons_title.": ".$res->coupons_code."<br>"; ?>
                                </td>
                        </tr>

                        <tr>
                                <td colspan="2">&nbsp;</td>
                        </tr>

                        <tr>
                                <th>
					Neues Passwort
                                </th>
                                <td>
                                        <input type="password" class="input<? $validate->printStyle(pass1); ?>" name="pass1" autocomplete="off">
                                </td>
                        </tr>
                        <tr>
                                <th>
					Neues Passwort (Best&auml;tigung)
                                </th>
                                <td>
                                        <input type="password" class="input<? $validate->printStyle(pass2); ?>" name="pass2" autocomplete="off">
                                </td>
                        </tr>

                        <? submit("Speichern"); ?>
                </table>
                <input type="hidden" name="action" value="save_do">
        </fieldset>
</form>

