<? if ($action == save_do) {

        $error = $validate->checkEmpty("title,link_url,link_title");

        $form = array(title,link_url,link_title);
        foreach($form as $val) {
                ${$val} = form2db($_POST[$val]);
                $_POST[$val] = db2form($_POST[$val]);
        }

        if (!$error) {
                $ads = new Affiliatesads($ads->id);

                $aData = array(
                        title => $title,
                        affiliates_id => $account->id,
                        link_url => $link_url,
                        link_title => $link_title,
                        type => "links",
                );
                $ads->save($aData);

                $alert = "Der Textlink wurde erfolgreich gespeichert.";
                $goto = AFFILIATES."affiliates/links/form&id=".$ads->id.".html";
        }

} ?>

<form name="form" method="post" enctype="multipart/form-data" id="form_data">
        <? fieldset("Textlink anlegen / bearbeiten"); ?>
        <? if ($error) { ?>
        <div class="errorBox">
                <p><img src="<?=PATH?>img/icons/cross_16.png" class="icon"> Es ist ein Fehler aufgetreten - bitte korrigieren Sie Ihre Angaben.</p>
        </div>

                <? } else if ($ads->id) {

                $form = array(title,link_url,link_title);
                foreach($form as $val) {
                        $_POST[$val] = form2db($ads->ads->{"ads_".$val});
                }

        } else {
        } ?>


        <table class="table_blank">
                <colgroup>
                        <col width="30%">
                        <col>
                </colgroup>
                <tr>
                        <th>Titel <? star(); ?></th>
                        <td><input type="text" name="title" class="input<?$validate->printstyle(title); ?>" value="<?=$_POST[title]?>"></td>
                </tr>
                <tr>
                        <th>Link Url <? star(); ?></th>
                        <td><input name="link_url" type="text" class="input<?$validate->printstyle(link_url); ?>" value="<?=$_POST[link_url]?>"></td>
                </tr>
                <tr>
                        <th>Link Titel <? star(); ?></th>
                        <td><input name="link_title" type="text" class="input<?$validate->printstyle(link_title); ?>" value="<?=$_POST[link_title]?>"></td>
                </tr>
                <? submit("Speichern")?>
                <input name="action" value="save_do" type="hidden">
        </table>
        <?fieldset_close()?>
</form>

<? if ($ads->id) { ?>
<br>
        <? fieldset("Textlink - Quellcode"); ?>
        <? $code = "<a href=\"http://www.druckerpatronen.de/affiliates/ads/goto.php?adsId=".$ads->id."&adsCheck=".$ads->ads->ads_check."&adsGoto=".$ads->ads->ads_link_url."\" target=\"_blank\">".$ads->ads->ads_link_title."</a>";
        echo htmlentities(utf8_decode($code)); ?>
        <?fieldset_close()?>
        <? } ?>
