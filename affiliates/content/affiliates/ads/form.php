<? if ($action == save_do) {

        $error = $validate->checkEmpty("title,types_id");

        $form = array(title,color,titlecolor,subtitlecolor,bgcolor,bordercolor,font,logo,logourl,types_id);
        foreach($form as $val) {
                ${$val} = form2db($_POST[$val]);
                $_POST[$val] = db2form($_POST[$val]);
        }

        if (!$error) {
                $ads = new AffiliatesAds($ads->id);

                $types = read(db_q(affiliates_ads_types,"WHERE types_id='$types_id'"));
                $form = array(path,width,height,layout,border,logo_width,bgimage);
                foreach($form as $val) {
                        ${$val} = $types->{"types_".$val};
                }

                $size = $width."x".$height;

                if (!$account->affiliates->affiliates_may_change_logo) {
                        $logo = $ads->ads->ads_logo;
                        $logourl = $ads->ads->ads_logourl;
                }

                $aData = array(
                        title => $title,
                        affiliates_id => $account->id,
                        path => $path,
                        width => $width,
                        height => $height,
                        size => $size,
                        layout => $layout,
                        color => $color,
                        titlecolor => $titlecolor,
                        subtitlecolor => $subtitlecolor,
                        bgcolor => $bgcolor,
                        bordercolor => $bordercolor,
                        border => $border,
                        font => $font,
                        logo => $logo,
                        logourl => $logourl,
                        logo_width => $logo_width,
                        bgimage => $bgimage,
                        type => "ads",
                        types_id => $types_id,
                );
                $ads->save($aData);

                $alert = "Das Werbemittel wurde erfolgreich gespeichert.";
                $goto = AFFILIATES."affiliates/ads/form&id=".$ads->id.".html#source";
        }

} ?>

Hier können Sie neue Werbemittel anlegen. Wählen Sie bitte zuerst eine Werbemittelgröße aus, weiter unten können Sie dann Details wie Farben an die Optik Ihrer Webseite anpassen.
<br><br>

<form name="form" method="post" enctype="multipart/form-data" id="form_data">
        <? fieldset("Werbemittel anlegen / bearbeiten"); ?>
        <? if ($error) { ?>
        <div class="errorBox">
                <p><img src="<?=PATH?>img/icons/cross_16.png" class="icon"> Es ist ein Fehler aufgetreten - bitte korrigieren Sie Ihre Angaben.</p>
        </div>

                <? } else if ($ads->id) {

                $form = array(title,affiliates_id,types_id,color,titlecolor,subtitlecolor,bgcolor,bordercolor,font,logo,logourl);
                foreach($form as $val) {
                        $_POST[$val] = form2db($ads->ads->{"ads_".$val});
                }

        } else {
                $_POST[affiliates_id] = form2db($_REQUEST[affiliates_id]);

                $_POST[color] = "009ee0";
                $_POST[titlecolor] = "ff9900";
                $_POST[subtitlecolor] = "009ee0";
                $_POST[bordercolor] = "009ee0";
                $_POST[border] = "";
                $_POST[font] = "arial";
                $_POST[logo] = "http://www.druckerpatronen.de/affiliates/ads/choosebox/images/logo.png";
                $_POST[logourl] = "http://www.druckerpatronen.de/";
                $_POST[bgcolor] = "ddddff";
        } ?>


        <table class="table_blank">
                <colgroup>
                        <col width="30%">
                        <col>
                </colgroup>


                <tr>
                        <th>Typ <? star(); ?></th>
                        <td>
                                <? if ($ads->id) { ?><b><a href="javascript:void(0)" onclick="jQuery('#div_types').slideToggle('slow');">&bull; Typen & Werbemittel zeigen</a></b><br><br><? } ?>

                                <div id="div_types" <? if ($ads->id) echo 'style="display:none"'; ?>>

                                <? $dbQ = db_q(affiliates_ads_types,"ORDER BY types_title ASC");
                                while($res = read($dbQ)) { ?>
                                <input type="radio" name="types_id" value="<?=$res->types_id?>" <? if ($_POST[types_id] == $res->types_id) echo checked; ?> id="radio_type_<?=$res->types_id?>" onclick="jQuery.post('<?=AFFILIATES?>ajax/affiliates/ads/form.html',jQuery('#form_data').serialize()+'&ads_id=<?=$ads->id?>',function(data){jQuery('#form_div').html(data);})"> <label for="radio_type_<?=$res->types_id?>">
                                                <?=$res->types_title?><br>
                                        <img src="http://www.druckerpatronen.de/affiliates/ads/choosebox/images/types/<?=$res->types_width."x".$res->types_height?>.png">
                                </label>
                                <div style="border-bottom:3px dotted #808080;margin: 15px 0 15px 0"></div>
                                        <? } ?>
                                </div>
                        </td>
                </tr>

        </table>
        <div id="form_div">
                <? include(ROOT_AFF."content/ajax/affiliates/ads/form.php") ?>
        </div>
        <table class="table_blank">
                <colgroup>
                        <col width="30%">
                        <col width="70%">
                </colgroup>
                <? submit("Speichern")?>
                <input name="action" value="save_do" type="hidden">
        </table>
        <?fieldset_close()?>
</form>

<? if ($ads->id) { ?>
<br>
<a name="source"></a>
        <? fieldset("Werbemittel - Quellcode"); ?>
        <? $code = "<script type=\"text/javascript\" src=\"http://www.druckerpatronen.de/affiliates/ads/choosebox/index.php?id=".$ads->id."&check=".$ads->ads->ads_check."\"></script>";
        echo htmlentities($code); ?>
        <?fieldset_close()?>



<br>

        <? fieldset("Werbemittel - Vorschau"); ?>
<script type="text/javascript" src="http://www.druckerpatronen.de/affiliates/ads/choosebox/index.php?id=<?=$ads->id?>&check=<?=$ads->ads->ads_check?>"></script>
        <? fieldset_close(); ?>
        <? } ?>