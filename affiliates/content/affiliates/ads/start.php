<h1>Werbemittel</h1>

<? $where = " && ads_affiliates_id = '$account->id' ";

$query = "WHERE ads_type = 'ads' $where ORDER BY ads_id ASC"; ?>


<fieldset>
        <legend>Gutscheincodes</legend>
        Hier sehen Sie die für Sie freigeschalteten Gutscheincodes. Wenn ein Kunde über Ihre codes bestellt wird die Provision automatisch Ihrem Konto zugeordnet.
        <br><br>
        <table class="table_blank">

                <tr class="blank" style="background-color:#cccccc !important">
                        <th width="50%">Name</th>
                        <th width="50%">Gutscheincode</th>
                </tr>
                <? $dbQ = db_q(affiliates_coupons,"WHERE coupons_affiliates_id = '$account->id' && coupons_status = '1' ORDER BY coupons_code ASC");
                while($res = read($dbQ)) { ?>
                <tr>
                        <td>
                                        <?=$res->coupons_title?>
                        </td>
                        <td>
                                        <?=$res->coupons_code?>
                        </td>
                </tr>
                        <? } ?>
        </table>

</fieldset>
<br>

<b><a href="<?=AFFILIATES?>affiliates/ads/form.html"><img src="<?=AFFILIATES?>tpl/images/bullet_blue.png" class="icon"> Werbemittel anlegen</a></b>
<br><br>



<? $nums = nums(db_q(affiliates_ads,$query));
$paging = new pagination(AFFILIATES."ads&page={x}/",$nums,10,$_REQUEST[page]);
$paging->show_pages("Einträge"); ?>

<form name="form" method="post" id="data">
        <table class="table">
                <tr>
                        <th width="25%">
                                Titel
                        </th>
                        <th width="35%">
                                Affiliate
                        </th>
                        <th width="15%" align="center">
                                Datum
                        </th>
                        <th width="15%" align="center">
                                Aktualisiert
                        </th>
                        <th align="right">
                                Aktion
                        </th>
                </tr>
                <? $query = $query.$paging->limit;
                $dbQ = db_q(affiliates_ads,$query);
                while($res = read($dbQ)) {
                        $bgcolor = ($i % 2) ? "efefef" : "efefef";
                        $i++;
                        $ads = new AffiliatesAds($res->ads_id);
                        $affiliates = new Affiliates($ads->ads->ads_affiliates_id) ?>
                <tr bgcolor="#<?=$bgcolor?>">
                        <td>

                                <b><a href="<?=AFFILIATES?>affiliates/ads/form&id=<?=$res->ads_id?>.html"><?=$res->ads_title?></a></b>
                        </td>
                        <td align="left">
                                        <?=$affiliates->affiliates->affiliates_name?> (<?=$affiliates->affiliates->affiliates_firstname." ".$affiliates->affiliates->affiliates_lastname?>)
                        </td>
                        <td align="center">
                                        <?=date("d.m.Y, H:i",$res->ads_date); ?>
                        </td>
                        <td align="center">
                                        <?=date("d.m.Y, H:i",$res->ads_last_update); ?>
                        </td>
                        <td align="right">
                                <a href="<?=AFFILIATES?>affiliates/ads/form&id=<?=$res->ads_id?>.html" title="Bearbeiten"><? icon(pencil); ?></a> <a href="<?=AFFILIATES?>affiliates/ads/delete&id=<?=$res->ads_id?>.html" title="L&ouml;schen" onclick="return confirm('Wirklich unwiderruflich l&ouml;schen?')" title="L&ouml;schen"><? icon(cross); ?></a>
                        </td>
                </tr>
                        <? /* ?>
                <tr>
                        <td colspan="5">
                                <script type="text/javascript" src="http://www.druckerpatronen.de/affiliates/ads/choosebox/index.php?id=<?=$ads->id?>&check=<?=$ads->ads->ads_check?>"></script>
                        </td>
                </tr>
                 <? */ ?>

                <tr>
                        <td colspan="5"><script type="text/javascript" src="http://www.druckerpatronen.de/affiliates/ads/choosebox/index.php?id=<?=$ads->id?>&check=<?=$ads->ads->ads_check?>"></script>
                                        <? fieldset("Werbemittel - Quellcode"); ?>
                                <div style="text-align:left">
                                                <? $code = "<script type=\"text/javascript\" src=\"http://www.druckerpatronen.de/affiliates/ads/choosebox/index.php?id=".$ads->id."&check=".$ads->ads->ads_check."\"></script>";
                                                echo htmlentities($code); ?>
                                </div>
                                        <?fieldset_close()?>
                        </td>
                </tr>

                        <? } ?>

        </table>
</form>

<? $paging->show_pages("Einträge"); ?>

