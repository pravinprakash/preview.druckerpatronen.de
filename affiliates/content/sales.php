<h1>Salestatistik</h1>


<? $form = array(day,month,year);
foreach($form as $val) {
        ${$val} = form2db($_REQUEST[$val]);
        $_REQUEST[$val] = db2form(${$val});
}

$where = " && affiliates_id = '$account->id' ";

if ($month == "") {
        $month = date(n);
        $_REQUEST[month] = $month;
}

if (!$year) {
        $year = date(Y);
        $_REQUEST[year] = $year;
}


if ($month) {
        $start = mktime(0,0,0,$month,1,$year);
        $end = mktime(0,0,0,$month+1,1,$year);
} else {
        $start = mktime(0,0,0,1,1,$year);
        $end = mktime(0,0,0,1,1,$year+1);
}

if ($day == "0" && $month) {
        $start = mktime(0,0,0,$month,1,$year);
        $end = mktime(0,0,0,$month+1,1,$year);
} else if ($day != "" && $month) {
        $start = mktime(0,0,0,$month,$day,$year);
        $end = mktime(0,0,0,$month,$day+1,$year);
}

?>



<form method="post" action="">
        <? fieldset("Filter"); ?>
        <table class="table_blank">
                <tr>
                        <th>
                                Tag
                        </th>
                        <td>
                                <select name="day" id="day" class="select">
                                        <option value="0" <? if (!$_REQUEST[day]) echo selected; ?>>Alle</option>
                                        <? for($x=1;$x<32;$x++) { ?>
                                        <option value="<?=$x?>" <? if($_REQUEST[day] == $x) echo selected; ?>><?=$x?></option>
                                                <? } ?>
                                </select>
                        </td>
                </tr>
                <tr>
                        <th>
                                Monat
                        </th>
                        <td>
                                <select name="month" id="month" class="select">
                                        <option value="0" <? if (!$_REQUEST[month]) echo selected; ?>>Alle</option>
                                        <? for($x=1;$x<13;$x++) { ?>
                                        <option value="<?=$x?>" <? if($_REQUEST[month] == $x) echo selected; ?>><?=convert_months($x)?></option>
                                                <? } ?>
                                </select>
                        </td>
                </tr>
                <tr>
                        <th width="30%">
                                Jahr
                        </th>
                        <td>
                                <select name="year" class="select">
                                        <? for($x=2009;$x<date(Y)+1;$x++) { ?>
                                        <option value="<?=$x?>" <? if ($year == $x) echo selected; ?>><?=$x?></option>
                                                <? } ?>
                                </select>
                        </td>
                </tr>
                <tr class="blank">
                        <td></td>
                        <td>
                                <br>
                                <input type="submit" class="submit" value="Filtern"> <? /* ?> <input type="submit" name="export_sales" class="submit" value="Klicks Exportieren"><? */ ?>
                        </td>
                </tr>
        </table>
</fieldset>
</form>
<br>

<? $x = $start;
while($x<$end) {
        $day_end = mktime(0,0,0,date(m,$x),date(d,$x)+1,date(Y,$x));
        $nums = nums(db_q(affiliates_sales,"WHERE date >= '$x' && date < '$day_end' $where ORDER BY date ASC"));
        if ($nums) { ?>

<h2><?=date("d.m.Y",$x); ?></h2>
<table class="table">
        <tr>
                <th></th>
                <th width="20%">Affiliate</th>
                <th width="20%">WM / GS</th>

                <th width="25%" align="center">Uhrzeit</th>
                <th width="10%" align="right">Betrag</th>
                <th width="10%" align="right">Provision</th>
        </tr>

                        <? $dbQ = db_q(affiliates_sales,"WHERE date >= '$x' && date < '$day_end' $where ORDER BY date ASC");
                        while($res = read($dbQ)) { ?>

        <tr>
                <td>
                                                <? if ($res->confirmed_date) icon(success); else if ($res->cancelled_date) icon(cross); else icon(help); ?>

                </td>
                <td>
                                                <? $affiliates = new affiliates($res->affiliates_id);
                                                echo $affiliates->affiliates->affiliates_name." (".$affiliates->affiliates->affiliates_firstname." ".$affiliates->affiliates->affiliates_lastname.")"; ?>
                </td>
                <td>
                                                <? if ($res->ads_id) {
                                                        $ads = new AffiliatesAds($res->ads_id); ?>
                        WM: <a href="<?=AFFILIATES?>affiliates/<?=$ads->ads->ads_type?>/form&id=<?=$ads->id?>.html"><?=$ads->ads->ads_title?></a>
                                                        <? } ?>
                                                <? if ($res->coupons_id) {
                                                        $coupons = read(db_q(affiliates_coupons,"WHERE coupons_id = '$res->coupons_id'"));
                                                        echo "GS: ".$coupons->coupons_code;
                                                } ?>
                </td>
                <td align="center">
                                                <?=date("d.m.Y, H:i:s",$res->date); ?>
                </td>
                <td align="right">
                                                <?=format_number($res->total);?> EUR
                </td>
                <td align="right">
                                                <?=format_number($res->provision);?> EUR
                </td>
        </tr>

                                <? } ?>



        <tr>
                <th colspan="1">
                        Summe:
                        </td>
                <th align="center">
                                        <?=$nums?> Sales
                </th>

                <th colspan="4"></th>
        </tr>
</table>
<br><br>
                <? }
        $x = $x+86400;
} ?>

<br>

<table class="table_blank">
        <tr>
                <td width="33%" align="center">
                        <? icon(success); ?> Sale bestätigt
                </td>
                <td width="33%" align="center">
                        <? icon(help); ?> Sale ausstehend
                </td>
                <td width="33%" align="center">
                        <? icon(cross); ?> Sale storniert
                </td>
        </tr>
</table>