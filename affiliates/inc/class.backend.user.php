<? class backend_user {

function backend_user($id=null)
{
	if ($id)
	{
		$this->load($id);
	}
	$this->date = time();
}

function set($src)
{
	foreach($src as $val => $key)
	{
		$this->${val} = $key;
	}
}

function load($id)
{
	$this->user = read(db_q(backend_user,"WHERE user_id = '$id'"));
	if (!$this->user)
	{
		return;
	}
	
	$this->id = $this->user->user_id;
	
	$this->type = $this->user->user_types_id;
}

function save($name,$firstname,$lastname,$email,$types_id,$status=null)
{	
	if ($this->user->user_id)
	{
		if ($this->pass)
		{
			$res = db_u(backend_user,"user_pass='$this->pass_md5' WHERE user_id = '$this->id'");
		}
		$res = db_u(backend_user,"user_name='$name',user_firstname='$firstname',user_lastname='$lastname',user_email='$email',user_types_id='$types_id',user_status='$status' WHERE user_id = '".$this->user->user_id."'");
		$this->load($this->user->user_id);
		if ($this->sendmail)
		{
			$this->mail_new_password();
		}		
	} else {
		$res = db_i(backend_user,"user_name='$name',user_pass='$this->pass_md5',user_firstname='$firstname',user_lastname='$lastname',user_email='$email',user_types_id='$types_id',user_status='$status',user_date='$this->date'");
		$this->load($res);
		if ($this->sendmail)
		{
			$this->mail_new_user();
		}		
	}
	return 1;	
}

function mail_new_password()
{
	if (!$this->id OR !$this->pass) return;
	
	$message = "
Hallo ".$this->user->user_firstname." ".$this->user->user_lastname.",

Ihr Passwort zum  Benutzernamen ".$this->user->user_name." wurde ge�ndert. Das neue Passwort lautet:

$this->pass

Um sich im System einzuloggen, klicken Sie bitte hier:
".BACKEND."

Ihre Logindaten sollten Sie sorgf�ltig aufbewahren und nicht an Dritte weitergeben.

".SIGNATURE;
	mail($this->user->user_email, "Neues Passwort f�r das Drucker.de Backend", $message, "From: ".SYSMAIL);		

	return 1;
}


function mail_new_user()
{
	if (!$this->id OR !$this->pass) return;
	
	$message = "
Hallo ".$this->user->user_firstname." ".$this->user->user_lastname.",

Es wurde ein neuer Benutzer f�r Sie angelegt. Hier Ihre Zugangsdaten:

Benutzername: ".$this->user->user_name."
Passwort: ".$this->pass."

Um sich im System einzuloggen, klicken Sie bitte hier:
".BACKEND."

Ihre Logindaten sollten Sie sorgf�ltig aufbewahren und nicht an Dritte weitergeben.

".SIGNATURE;
	mail($this->user->user_email, "Herzlich willkommen im Drucker.de Backend", $message, "From: ".SYSMAIL);		

	return 1;
}

function delete()
{
	if (!$this->user->user_id)
	{
		return;
	}
	
	$res = db_d(backend_user,"WHERE user_id = '".$this->user->user_id."'");
	return 1;
}

function login($user,$pass)
{
	
	$this->user = read(db_q(backend_user,"WHERE user_name = '$user' && (user_pass = '$pass') Limit 1"));
	if (!$this->user->user_id)
	{
		header("Location: ".BACKEND."login&res=nouser.html");
		exit;
	}
	
	if (!$this->user->user_status)
	{
		header("Location: ".BACKEND."login&res=inactive.html");
		exit;
	}
	setcookie("backend_user_id",$this->user->user_id);
	setcookie("backend_user_urn",$this->user->user_date);
	$logs = new backend_logs($this->user->user_id);
	$logs->login();
}


function logout()
{
	if (!$this->id)
	{
		header("Location: ".BACKEND);
	}
	setcookie("backend_user_id",$this->user->user_id,time()-365*24*3600);
	setcookie("backend_user_urn",$this->user->user->user_date,time()-365*24*3600);
	$logs = new backend_logs($this->user->user_id);
	$logs->logout();
}


function rights($type,$goto)
{
	if (!$this->type)
	{
		header("Location: ".BACKEND);
	}
	$types = explode(",",$type);
	if (!in_array($this->type,$types))
	{
		echo "<script language='javascript'>alert('Unbefugter Zugriff. Sie besitzen nicht die Berechtigung diesen Bereich zu betreten.');</script>";
		return $goto;
	}
}

} ?>