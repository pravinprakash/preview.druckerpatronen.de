<? class backend_menu {

function backend_menu()
{
	$this->set_path(BACKEND);
}

function set_path($path)
{
	$this->set_path = $path;
}

	/**
	 * Generiert eine Navigation mit beliebig vielen Ebenen
	 * @param	array		$aItems			Array mit den Menüpunkten und -unterpunkten		Standard: ---;		Status: obligatorisch;
	 * @param	string		$sUrlPrefix		Prefix für den Url					Standard: NULL;		Status: optional;
	 * @return	string		$this->topmenu		ungeordnete Menüpunktliste
	 */
	function GenerateNavigation($aItems, $sUrlPrefix = NULL) {
		foreach($aItems AS $sTitle => $aItem) {
			$sRandomNumber				 = md5(rand(1, 9999));
			if(is_array($aItem) && array_key_exists('Url', $aItem)) {
				$sUrl				 = str_replace('/', NULL, $aItem['Url']);
				$sUrlPrefixAdjusted		 = str_replace('/', NULL, $sUrlPrefix);

				$this->topmenu			.= '<li';
				$this->topmenu			.= (empty($sUrlPrefix)) ? ' onMouseOver="ShowSubNavigationInIe6(\'' . $sRandomNumber . '\');" onMouseOut="HideSubNavigationInIe6(\'' . $sRandomNumber . '\');"' : NULL;
				$this->topmenu			.= '><a class="';
				$this->topmenu			.= ((!empty($sUrlPrefix) && !$_REQUEST['col'] && $_REQUEST['cat'] == $sUrlPrefixAdjusted) || (empty($sUrlPrefix) && $_REQUEST['col'] == $sUrl)) ? 'NavigationActive' : NULL;
				$this->topmenu			.= (array_key_exists('Slave', $aItem)) ? ' NavigationHasChildren' : NULL;
				$this->topmenu			.= (empty($sUrlPrefix)) ? ' NavigationLevelOne' : NULL;
				$this->topmenu			.= '" href="';
				$this->topmenu			.= (strpos($aItem['Url'], '/') === 0 || strpos($aItem['Url'], 'http://') === 0 || strpos($aItem['Url'], 'ftp://') === 0) ? $aItem['Url'] : $this->set_path . $sUrlPrefix . $aItem['Url'];
				$this->topmenu			.= '"><span>' . $sTitle . '</span></a>';
			}

			if(is_array($aItem) && array_key_exists('Slave', $aItem)) {
				$this->topmenu			.= '<ul id="' . $sRandomNumber . '">';
				$this->topmenu			.= (empty($sUrlPrefix) && array_key_exists('Slave', $aItem)) ? '<a href="#close" class="CloseButton"></a>' : NULL;
				$this->GenerateNavigation($aItem['Slave'], $sUrlPrefix . $aItem['Url']);
				$this->topmenu			.= '</ul>';
			}

			if(is_array($aItem) && array_key_exists('More', $aItem)) {
				$this->topmenu			.= '<ul class="MoreBox">';

				foreach($aItem['More'] AS $sMoreTitle => $aMoreDetails) {
					$this->topmenu		.= '<li><a href="' . $aMoreDetails['Url'] . '">' . $sMoreTitle . '</a></li>';
				}

				$this->topmenu			.= '</ul>';
			}

			if(is_array($aItem) && array_key_exists('Url', $aItem)) {
				$this->topmenu			.= '</li>';
			}
		}
	}

function add($url=null,$end=null,$title,$left=null)
{
	if((!$_REQUEST['col'] && $_REQUEST['cat'] == $Url) || ($_REQUEST['col'] && $_REQUEST['col'] == $Url))
	{ 
		$this->topmenu .= "<li id=\"naviTopActive\"><a href=\"".$this->set_path.$url.$end."\" title=\"$title\" style=\"color:#fff !important\">$title</a></li>";
	} else { 
		$this->topmenu .= "<li><a href=\"".$this->set_path.$url.$end."\" title=\"$title\">$title</a></li>";

	}
	/*
	if ($left && $_REQUEST[col] == $url && !$_REQUEST[cat])
	{ 
		$this->leftmenu .= "<li id=\"naviLeftCatActive\"><a href=\"".$this->set_path.$url.$end."\" title=\"$title\" style=\"color:#fff !important\"><img src=\"".BACKEND."tpl/images/arrow_white.png\"> $title</a></li>"; 
	} else if ($_REQUEST[col] == $url && $_REQUEST[cat]) { 
		$this->leftmenu .= "<li id=\"naviLeftCat\"><a href=\"".$this->set_path.$url.$end."\" title=\"$title\">$title</a></li>";
	}
	*/
}


function add_cat($col,$url=null,$end=null,$title)
{
	if ($_REQUEST[col] == $col)
	{
		if ($_REQUEST[cat] == $url)
		{ 
			$this->leftmenu .= "<li id=\"naviLeftCatActive\"><a href=\"".$this->set_path.$col."/".$url.$end."\" title=\"$title\" style=\"color:#fff !important\"><img src=\"".BACKEND."tpl/images/arrow_white.png\"> $title</a></li>"; 
		} else { 
			$this->leftmenu .= "<li id=\"naviLeftCat\"><a href=\"".$this->set_path.$col."/".$url.$end."\" title=\"$title\">$title</a></li>";
		}
	}
}

function add_site($col,$cat,$url=null,$end=null,$title)
{
	
	if (!$url)
	{
		$_REQUEST[file] = "";	
	}
	if ($_REQUEST[col] == $col && $_REQUEST[cat] == $cat)
	{
		if ($_REQUEST[site] == $url)
		{ 
			$this->leftmenu .= "<li id=\"naviLeftSubCatActive\"><a href=\"".$this->set_path.$col."/".$cat."/".$url.$end."\" title=\"$title\">$title</a></li>"; 
		} else { 
			$this->leftmenu .= "<li id=\"naviLeftSubCat\"><a href=\"".$this->set_path.$col."/".$cat."/".$url.$end."\" title=\"$title\">$title</a></li>";
		}
	}
}

function add_thirdmenu($col,$cat,$site,$url=null,$end=null,$title)
{
	$inactive = "";
	if ($_REQUEST[col])
	{
		if ($col != $_REQUEST[col])
		{
			$inactive = 1;
		}
	}
	
	if ($_REQUEST[cat])
	{
		if ($cat != $_REQUEST[cat])
		{
			$inactive = 1;
		}
	}
	
	if ($_REQUEST[file])
	{
		if ($url != $_REQUEST[file])
		{
			$inactive = 1;
		}
	}
	if (!$inactive)
	{
		$this->thirdmenu .= "<li id=\"naviThirdActive\"><a href=\"".$this->set_path.$col."/"; if ($cat) { $this->thirdmenu .= $cat."/"; } if ($site) { $this->thirdmenu .= $site."/"; }$this->thirdmenu .= $url.$end."\" title=\"$title\" style=\"color:#ffffff !important\">$title</a></li>"; 
	} else {
		$this->thirdmenu .= "<li><a href=\"".$this->set_path.$col."/"; if ($cat) { $this->thirdmenu .= $cat."/"; } if ($site) { $this->thirdmenu .= $site."/"; } $this->thirdmenu .= $url.$end."\" title=\"$title\">$title</a></li>"; 	
	}
}

function add_extern($url,$title)
{
	$this->topmenu .= "<li><a href=\"".$url."\" title=\"$title\" target=\"_blank\">$title</a></li>";
	$this->leftmenu .= "<li id=\"naviLeftCat\"><a href=\"".$url."\" title=\"$title\" target=\"_blank\">$title</a></li>";
}

function display_topmenu()
{
	echo $this->topmenu;
}

function display_leftmenu()
{
	echo $this->leftmenu;
}

function display_thirdmenu()
{
	echo $this->thirdmenu;
}

} ?>