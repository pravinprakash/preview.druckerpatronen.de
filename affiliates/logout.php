<?php

error_reporting(E_ALL ^ E_NOTICE);
ob_start();
session_start();
include ("_config/inc.config.php"); 
if (!$validate) $validate = new validateForm();

if (!$account->id)
{
	header("Location:".AFFILIATES);
} else {
	
	$logout = $account->logout();

	if ($_REQUEST[redirect])
	{
		header("Location: ".$_REQUEST[redirect]);
	} else {
		header("Location:".AFFILIATES);
	}
} ?>