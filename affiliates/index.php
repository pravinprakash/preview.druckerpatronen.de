<?php
error_reporting(0);
#if(! ini_set("memory_limit","2048M")) echo "changing memory_limit failed<br>\n";
#if(! ini_set("max_execution_time","3000000")) echo "changing max_execution_time failed<br>\n";
#error_reporting(E_ALL ^ E_NOTICE);

ob_start();
session_start();
include ("_config/inc.config.php"); 
if (!$validate) $validate = new validateForm();

// extract superglobals
$vars = array("id","col","file","cat","site","action","ajax","popup");
foreach($vars as $val)
{
	${$val} = $_REQUEST[$val];
}

// extract superglobals

if (!$file) $file = start;

// load template files


if ($ajax) include(ROOT_AFF."tpl/tpl.ajax.php");
else if ($popup) 
{
		include (ROOT_AFF."_config/inc.access.popup.php");
		include (ROOT_AFF."tpl/tpl.popup.php");
} else {
	include (ROOT_AFF."_config/inc.access.php");
	include (ROOT_AFF."tpl/tpl.header.php");
	include (ROOT_AFF."tpl/tpl.topmenu.php");
	include (ROOT_AFF."tpl/tpl.leftmenu.php");
	
	if (!$account->id)
	{
		$cat = ""; $subcat = ""; $file = "login";
	}
	
	include (ROOT_AFF."tpl/tpl.content.php");
	include (ROOT_AFF."tpl/tpl.footer.php");
	// END load template files
}
if ($alert) $system->msg($alert); 
if ($goto) header("Location:".$goto);
ob_end_flush(); 

 ?>
