<? error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

include("../../_tintenmarkt_api/backend/_config/inc.config.php");

$form = array(adsId,adsCheck,adsGoto);
foreach($form as $val) {
        ${$val} = form2db($_GET[$val]);
}

$ads = new AffiliatesAds($adsId);
if (!$ads->id OR $ads->ads->ads_check != $adsCheck OR !$adsGoto) $error = 1;

if (!preg_match("/druckerpatronen.de/",$adsGoto)) {
        $error = 1;
        $adsGoto = "http://www.druckerpatronen.de";
}

if (!$error)
        {
        $ads->setCookies($adsGoto);
}
header("Location:".$adsGoto);
?>