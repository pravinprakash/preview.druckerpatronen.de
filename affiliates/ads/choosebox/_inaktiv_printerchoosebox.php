<?php

//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
require_once 'classes/class-xml.php';

/*
 * Xml-Datei mit Daten
*/
$sXmlFile			 = 'manufacturer/' . $_GET['xml'];
$oDataXml			 = new Xml();
$oDataXml->ParseXml($sXmlFile);
$sManufacturerSelection		 = NULL;



/*
 * Xml-Datei mit Konfiguration
*/
$oConfigXml			 = new Xml;
$oConfigXml->ParseXml('config.xml');



/*
 * Generieren des JavaScripts und Ausgabe
*/
foreach($oDataXml->aXmlArray['manufacturer'] AS $iManufacturerKey => $aManufacturer) {
        if(file_exists('manufacturer/' . $aManufacturer['id'] . '.xml')) {
                $sManufacturerSelection		.= '<option value="' . $aManufacturer['id'] . '" ';
                $sManufacturerSelection		.= (!empty($_REQUEST['Manufacturer']) && $_REQUEST['Manufacturer'] == $aManufacturer['name']) ? 'selected' : NULL;
                $sManufacturerSelection		.= '>' . $aManufacturer['name'] . '</option>';
        }
}

$aConfig				 = $oConfigXml->aXmlArray['config'];
$sWidth					 = (!empty($aConfig['width']))						? 'width: ' . $aConfig['width'] . 'px;'									: NULL;
$sHeight				 = (!empty($aConfig['height']))						? 'height: ' . $aConfig['height'] . 'px;'								: NULL;
$sColor					 = (!empty($aConfig['color']))						? 'color: #' . $aConfig['color'] . ';'									: NULL;
$sBoxBorder				 = (!empty($aConfig['border']) && !empty($aConfig['bordercolor']))	? 'border: 1px solid #' . $aConfig['bordercolor'] . ';'							: 'border: 1px solid transparent;';
$sSelectionBorder			 = (!empty($aConfig['color']))						? 'border: 3px solid #' . $aConfig['color'] . ';'							: NULL;
$sFont					 = (!empty($aConfig['font']))						? 'font-family: "' . $aConfig['font'] . '";'								: NULL;
$sBackgroundColor			 = (!empty($aConfig['bgcolor']))					? 'background-color: #' . $aConfig['bgcolor'] . ';'							: NULL;
$sBackgroundImage			 = (!empty($aConfig['bgimage']))					? 'background: url("images/' . $aConfig['bgimage'] . '") no-repeat;'						: NULL;
$sLayoutSeperator			 = (!empty($aConfig['layout']) && $aConfig['layout'] == 'vertical')	? '<div class="LayoutSeperator"></div>'									: NULL;
$sStepStrongBackgroundColor		 = (!empty($aConfig['color']))						? 'background-color: #' . $aConfig['color'] . ';'							: NULL;
$sStepWeakBackgroundColor		 = (!empty($aConfig['color']))						? $oDataXml->CalculateColor($aConfig['color'])									: NULL;
$sLogo					 = NULL;
$sSelectionBackground			 = (!empty($aConfig['layout']) && $aConfig['layout'] == 'vertical')	? 'background: url("background-vertical.png") no-repeat;'						: 'background: url("background-horizontal.png") no-repeat;';

if(!empty($aConfig['logo'])) {
        $sLogo				 = (!empty($aConfig['logourl'])) ? '<a href="' . $aConfig['logourl'] . '">' : NULL;
        $sLogo				.= '<img class="PrinterChooseBoxLogo" src="images/' . $aConfig['logo'] . '">';
        $sLogo				.= (!empty($aConfig['logourl'])) ? '</a>' : NULL;
}



$sJavaScriptSnippet			 = '
	window.document.write(\'
		<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
                <script type="text/javascript">
                    jQuery.noConflict();
                </script>

		<style type="text/css">
		.PrinterChooseBoxLogo {
			border: none;
			position: absolute;
			bottom: 10px;
			right: 10px;
			}
		.PrinterChooseBox {
			' . $sWidth . '
			' . $sHeight . '
			' . $sColor . '
			' . $sBoxBorder . '
			' . $sBackgroundColor . '
			' . $sBackgroundImage . '
			position: relative;
			}
		#Step1,
		#Step2 {
			vertical-align: middle;
			margin: 0 10px 0 20px;
			' . $sStepStrongBackgroundColor . '
			}
		.PrinterChooseBox,
		.PrinterChooseBox select {
			' . $sFont . '
			}
		.PrinterBoxTitle {
			font-size: 22px;
			margin: 20px 0 0 20px;
			}
		.SelectionBox {
			margin: 15px 0 0 40px;
			' . $sSelectionBackground . '
			padding: 20px 0 20px 0;
			}
		.PrinterSelection,
		.ManufacturerSelection {
			' . $sSelectionBorder . '
			width: 156px;
			}
		.PrinterChooseBox legend {
			color: #' . $oConfigXml->aXmlArray['config']['color'] . ';
			}
		.LayoutSeperator {
			margin:	0 0 10px 0;
			}
		</style>
		<div class="PrinterChooseBox">
			<h2 class="PrinterBoxTitle">Druckerpatronen- & Tonersuche</h2>
			<div class="SelectionBox">
				<img id="Step1" src="images/step1_active.png">
				<select class="ManufacturerSelection" name="Manufacturer" id="ManufacturerSelection" onchange="FillPrinterSelection(); jQuery.get(\\\'ajax/printerselection.php\\\', \\\'manufacturer=\\\'+this.value, function(data){jQuery(\\\'#Printer\\\').html(data);})">
					<option>Bitte w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2" src="images/step2.png">
				<select class="PrinterSelection" id="Printer" disabled onchange="jQuery.get(\\\'ajax/tracking.php\\\', \\\'url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection\\\').val()+\\\'&id=' . $_GET['id'] . '\\\', function(data){jQuery(\\\'#Printer\\\').html(data);})">
				
				</select>
			</div>
			' . $sLogo . '
		</div>
	\');
';



$sPrinterChooseBox			 = '
function FillPrinterSelection() {
	window.document.getElementById(\'Step1\').src					 = "images/step1.png";
	window.document.getElementById(\'ManufacturerSelection\').style.borderColor	 = "#' . $sStepWeakBackgroundColor . '";
	window.document.getElementById(\'Step2\').src					 = "images/step2_active.png";
	window.document.getElementById(\'Printer\').disabled				 = false;
	window.document.getElementById(\'Printer\').focus();
}

' . preg_replace("/[\t\r\n]/", NULL, $sJavaScriptSnippet) . '

';

print $sPrinterChooseBox;

?>