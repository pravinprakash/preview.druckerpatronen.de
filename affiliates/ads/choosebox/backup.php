<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

include("../../../_tintenmarkt_api/backend/_config/inc.config.php");

$relPath = "../../../affiliates/ads/choosebox/";
$absPath = "http://www.druckerpatronen.de/affiliates/ads/choosebox/";
$targetPath = "http://www.druckerpatronen.de/affiliates/ads/goto.php";

$iId = form2db($_GET[id]);
$iCheck = form2db($_GET[check]);

$ads = new AffiliatesAds($iId);
if ($ads->ads->ads_check != $iCheck) exit;
$ads->countViews();

require_once 'classes/class-xml.php';

/*
 * Xml-Datei mit Daten
*/
$sXmlFile			 = $relPath.'manufacturers/manufacturers.xml';
$oDataXml			 = new Xml();
$oDataXml->ParseXml($sXmlFile);
$sManufacturerSelection		 = NULL;



/*
 * Xml-Datei mit Konfiguration
*/
#$oConfigXml			 = new Xml;
#$oConfigXml->ParseXml('config.xml');

$form = array(path,width,height,layout,color,titlecolor,subtitlecolor,bgcolor,border,font,logo,logourl,logo_width,bgimage);
foreach($form as $val) {
        $aConfig[$val] = $ads->ads->{"ads_".$val};
}


/*
 * Generieren des JavaScripts und Ausgabe
*/
foreach($oDataXml->aXmlArray['manufacturer'] AS $iManufacturerKey => $aManufacturer) {
        if(file_exists($relPath.'manufacturers/' . $aManufacturer['id'] . '.xml')) {
                $sManufacturerSelection		.= '<option value="' . $aManufacturer['id'] . '" ';
                $sManufacturerSelection		.= (!empty($_REQUEST['Manufacturer']) && $_REQUEST['Manufacturer'] == $aManufacturer['name']) ? 'selected' : NULL;
                $sManufacturerSelection		.= '>' . $aManufacturer['name'] . '</option>';
        }
}

#$aConfig['border'] = 1;

#$aConfig				 = $oConfigXml->aXmlArray['config'];
$sWidth					 = (!empty($aConfig['width']))						? 'width: ' . $aConfig['width'] . 'px;'									: NULL;
$sHeight				 = (!empty($aConfig['height']))						? 'height: ' . $aConfig['height'] . 'px;'								: NULL;
$sColor					 = (!empty($aConfig['color']))						? 'color: #' . $aConfig['color'] . ';'									: NULL;
$sTitleColor				 = (!empty($aConfig['titlecolor']))					? 'color: #' . $aConfig['titlecolor'] . ';'								: NULL;
$sSubtitleColor				 = (!empty($aConfig['subtitlecolor']))					? 'color: #' . $aConfig['subtitlecolor'] . ';'								: NULL;
$sBoxBorder				 = (!empty($aConfig['border']) && !empty($aConfig['color']))		? 'border: 1px solid #' . $aConfig['color'] . ';'							: 'border: 1px solid transparent;';
$sSelectionBorder			 = (!empty($aConfig['color']))						? 'border: 3px solid #' . $aConfig['color'] . ';'							: NULL;
$sFont					 = (!empty($aConfig['font']))						? 'font-family: "' . $aConfig['font'] . '";'								: NULL;
$sBackgroundColor			 = (!empty($aConfig['bgcolor']))					? 'background-color: #' . $aConfig['bgcolor'] . ';'							: NULL;
$sBackgroundImage			 = (!empty($aConfig['bgimage']))					? 'background: url("' . $aConfig['bgimage'] . '") no-repeat;'						: NULL;
$sLayoutSeperator			 = (!empty($aConfig['layout']) && $aConfig['layout'] == 'vertical')	? '<div class="LayoutSeperator"></div>'									: NULL;
$sStepStrongBackgroundColor		 = (!empty($aConfig['color']))						? 'background-color: #' . $aConfig['color'] . ';'							: NULL;
$sStepWeakBackgroundColor		 = (!empty($aConfig['color']))						? $oDataXml->CalculateColor($aConfig['color'])									: NULL;
$sLogo					 = NULL;
$sSelectionBackground			 = 'background: url("'.$aConfig[path].'images/selectboxes/'.$aConfig['width'].'x'.$aConfig['height'].'-'.$aConfig['layout'].'.png") no-repeat;';

if(!empty($aConfig['logo'])) {
        $sLogo				 = (!empty($aConfig['logourl'])) ? '<a href="'.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto='. $aConfig['logourl'] . '" target="_blank">' : NULL;
        $sLogo				.= '<img class="PrinterChooseBoxLogo_'.$ads->id.'" src="' . $aConfig['logo'] . '" width="'.$aConfig['logo_width'].'">';
        $sLogo				.= (!empty($aConfig['logourl'])) ? '</a>' : NULL;
}



$sJavaScriptSnippet			 = '
	window.document.write(\'
		<script type="text/javascript" src="'.$aConfig[path].'js/jquery-1.3.1.min.js"></script>
                <script type="text/javascript">
                    jQuery.noConflict();
                </script>


		<style type="text/css">
		.PrinterChooseBoxLogo_'.$ads->id.' {
			border: none;
			position: absolute;
			bottom: 10px;
			right: 10px;
			}
		.PrinterChooseBox_'.$ads->id.' {
			' . $sWidth . '
			' . $sHeight . '
			' . $sColor . '
			' . $sBoxBorder . '
			' . $sBackgroundColor . '
			' . $sBackgroundImage . '
			font-family: arial, sans-serif;
			position: relative;
			}
		.Title_'.$ads->id.' {
			' . $sTitleColor . '
			}
		.Subtitle_'.$ads->id.' {
			' . $sSubtitleColor . '
			}
		#Step1_'.$ads->id.',
		#Step2_'.$ads->id.' {
			vertical-align: middle;
			margin: 0 10px 0 20px;
			' . $sStepStrongBackgroundColor . '
			}
		.PrinterChooseBox_'.$ads->id.',
		.PrinterChooseBox_'.$ads->id.' select {
			' . $sFont . '
			}
		.PrinterBoxTitle_'.$ads->id.' {
			font-size: 22px;
			margin: 20px 0 0 20px;
			}
		.SelectionBox_'.$ads->id.' {
			margin: 15px 0 0 40px;
			' . $sSelectionBackground . '
			padding: 20px 0 20px 0;
			}
		.PrinterSelection_'.$ads->id.',
		.ManufacturerSelection_'.$ads->id.' {
			' . $sSelectionBorder . '
			width: 156px;
			vertical-align: middle;
			}
		.PrinterChooseBox_'.$ads->id.' legend {
			color: #' . $aConfig['color'] . ';
			}
		.LayoutSeperator_'.$ads->id.' {
			margin:	0 0 10px 0;
			}
                        ';
if($aConfig['width'] == 728 && $aConfig['height'] == 90 && $aConfig['layout'] == 'horizontal') {
                $sJavaScriptSnippet .= '.PrinterChooseBoxLogo_'.$ads->id.' {
                border: none;
                position: absolute;
                bottom: 5px;
                right: 5px;
        }
        .PrinterChooseBox_'.$ads->id.' {
                position: relative;
        }
        #Step1_'.$ads->id.',
        #Step2_'.$ads->id.' {
                vertical-align: middle;
                margin: 0 10px 0 20px;
        }
        .PrinterChooseBox_'.$ads->id.',
        .PrinterChooseBox_'.$ads->id.' select {
        }
        .SelectionBox_'.$ads->id.' {
                margin: 10px 0 0 80px;
                padding: 10px 0 20px 0;
        }
        .PrinterSelection_'.$ads->id.',
        .ManufacturerSelection_'.$ads->id.' {
                width: 136px;
        }
        .PrinterChooseBox_'.$ads->id.' legend {
        }
        .LayoutSeperator_'.$ads->id.' {
                margin:	0 0 10px 0;
        }
        .Title_'.$ads->id.' {
                font-size: 24px;
                font-weight: bold;
                margin: 15px 0 0 99px;
        }
        .Subtitle_'.$ads->id.' {
                text-align: center;
                font-weight: bold;
                font-size: 16px;
                margin: 40px 0 0 0;
        }';
}

#@include($relPath.'css/'.$aConfig['width'].'x'.$aConfig['height'].'-'.$aConfig['layout'].'.php');
$sJavaScriptSnippet .= '</style>

<div class="PrinterChooseBox_'.$ads->id.'">';

if($aConfig['width'] == 160 && $aConfig['height'] == 600 && $aConfig['layout'] == 'vertical') {
        $sJavaScriptSnippet .= '<div class="Title_'.$ads->id.'">Druckerpatronen<br>&amp;<br>Toner Suche</div>
			<div class="SelectionBox_'.$ads->id.'">
				<img id="Step1_'.$ads->id.'" src="'.$aConfig[path].'images/step1_active.png">
				<select class="ManufacturerSelection_'.$ads->id.'" name="Manufacturer" id="ManufacturerSelection_'.$ads->id.'" onchange="FillPrinterSelection_'.$ads->id.'(); jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/printerselection.php?manufacturer=\\\'+this.value,success: function(data){jQuery(\\\'#Printer_'.$ads->id.'\\\').html(data.response);}});">
					<option>Hersteller w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2_'.$ads->id.'" src="'.$aConfig[path].'images/step2.png">
				<select class="PrinterSelection_'.$ads->id.'" id="Printer_'.$ads->id.'" disabled onchange="jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/tracking.php?url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection_'.$ads->id.'\\\').val()+\\\'&id=' . $ads->id . '&check=' . $ads->ads->ads_date . '\\\'});window.open(\\\''.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto=\\\'+this.value);">
					<option>Drucker w&auml;hlen</option>
				</select>
			</div>
			<div class="Subtitle_'.$ads->id.'">Bis zu 80%<br>g&uuml;nstiger drucken</div>
			' . $sLogo . '
		</div>';
} else if($aConfig['width'] == 250 && $aConfig['height'] == 250 && $aConfig['layout'] == 'vertical') {
        $sJavaScriptSnippet .= '<div class="Title_'.$ads->id.'">Druckerpatronen<br>&amp;<br>Toner Suche</div>
			<div class="SelectionBox_'.$ads->id.'">
				<img id="Step1_'.$ads->id.'" src="'.$aConfig[path].'images/step1_active.png">
				<select class="ManufacturerSelection_'.$ads->id.'" name="Manufacturer" id="ManufacturerSelection_'.$ads->id.'" onchange="FillPrinterSelection_'.$ads->id.'(); jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/printerselection.php?manufacturer=\\\'+this.value,success: function(data){jQuery(\\\'#Printer_'.$ads->id.'\\\').html(data.response);}});">
					<option>Hersteller w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2_'.$ads->id.'" src="'.$aConfig[path].'images/step2.png">
				<select class="PrinterSelection_'.$ads->id.'" id="Printer_'.$ads->id.'" disabled onchange="jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/tracking.php?url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection_'.$ads->id.'\\\').val()+\\\'&id=' . $ads->id . '&check=' . $ads->ads->ads_date . '\\\'});window.open(\\\''.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto=\\\'+this.value);">
					<option>Drucker w&auml;hlen</option>
				</select>
			</div>
			<div class="LogoContainer_'.$ads->id.'">' . $sLogo . '</div>
		</div>';
} else if($aConfig['width'] == 300 && $aConfig['height'] == 300 && $aConfig['layout'] == 'vertical') {
        $sJavaScriptSnippet .= '<div class="Title_'.$ads->id.'">Druckerpatronen<br>&amp;<br>Toner Suche</div>
			<img src="'.$aConfig[path].'images/pfeil.png" style="' . $sStepStrongBackgroundColor . ' float: left; margin: 36px 0 0 0;">
			<div class="SelectionBox_'.$ads->id.'">
				<img id="Step1_'.$ads->id.'" src="'.$aConfig[path].'images/step1_active.png">
				<select class="ManufacturerSelection_'.$ads->id.'" name="Manufacturer" id="ManufacturerSelection_'.$ads->id.'" onchange="FillPrinterSelection_'.$ads->id.'(); jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/printerselection.php?manufacturer=\\\'+this.value,success: function(data){jQuery(\\\'#Printer_'.$ads->id.'\\\').html(data.response);}});">
					<option>Hersteller w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2_'.$ads->id.'" src="'.$aConfig[path].'images/step2.png">
				<select class="PrinterSelection_'.$ads->id.'" id="Printer_'.$ads->id.'" disabled onchange="jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/tracking.php?url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection_'.$ads->id.'\\\').val()+\\\'&id=' . $ads->id . '&check=' . $ads->ads->ads_date . '\\\'});window.open(\\\''.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto=\\\'+this.value);">
					<option>Drucker w&auml;hlen</option>
				</select>
			</div>
			<div class="LogoContainer_'.$ads->id.'">' . $sLogo . '</div>
		</div>';
} else if($aConfig['width'] == 468 && $aConfig['height'] == 60 && $aConfig['layout'] == 'horizontal') {
        $sJavaScriptSnippet .= '<img src="'.$aConfig[path].'images/pfeil50.png" style="' . $sStepStrongBackgroundColor . ' float: left; margin: 5px 0 0 0;">
		<div class="Title_'.$ads->id.'">Druckerpatronen &amp; Toner Suche</div>
			<div class="SelectionBox_'.$ads->id.'">
				<img id="Step1_'.$ads->id.'" src="'.$aConfig[path].'images/step1_active.png">
				<select class="ManufacturerSelection_'.$ads->id.'" name="Manufacturer" id="ManufacturerSelection_'.$ads->id.'" onchange="FillPrinterSelection_'.$ads->id.'(); jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/printerselection.php?manufacturer=\\\'+this.value,success: function(data){jQuery(\\\'#Printer_'.$ads->id.'\\\').html(data.response);}});">
					<option>Hersteller w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2_'.$ads->id.'" src="'.$aConfig[path].'images/step2.png">
				<select class="PrinterSelection_'.$ads->id.'" id="Printer_'.$ads->id.'" disabled onchange="jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/tracking.php?url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection_'.$ads->id.'\\\').val()+\\\'&id=' . $ads->id . '&check=' . $ads->ads->ads_date . '\\\'});window.open(\\\''.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto=\\\'+this.value);">
					<option>Drucker w&auml;hlen</option>
				</select>
			</div>
		</div>';
} else if($aConfig['width'] == 728 && $aConfig['height'] == 90 && $aConfig['layout'] == 'horizontal') {
        $sJavaScriptSnippet .= '<img src="'.$aConfig[path].'images/pfeil.png" style="' . $sStepStrongBackgroundColor . ' float: left; margin: 10px 0 0 0;">
		<div class="Title_'.$ads->id.'">Druckerpatronen &amp; Toner Suche</div>
			<div class="SelectionBox_'.$ads->id.'">
				<img id="Step1_'.$ads->id.'" src="'.$aConfig[path].'images/step1_active.png">
				<select class="ManufacturerSelection_'.$ads->id.'" name="Manufacturer" id="ManufacturerSelection_'.$ads->id.'" onchange="FillPrinterSelection_'.$ads->id.'(); jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/printerselection.php?manufacturer=\\\'+this.value,success: function(data){jQuery(\\\'#Printer_'.$ads->id.'\\\').html(data.response);}});">
					<option>Hersteller w&auml;hlen</option>
					' . $sManufacturerSelection . '
				</select>
				' . $sLayoutSeperator . '
				<img id="Step2_'.$ads->id.'" src="'.$aConfig[path].'images/step2.png">
				<select class="PrinterSelection_'.$ads->id.'" id="Printer_'.$ads->id.'" disabled onchange="jQuery.ajax({dataType: \\\'jsonp\\\',jsonp: \\\'jsonp_callback\\\',url: \\\''.$aConfig[path].'ajax/tracking.php?url=\\\'+this.value+\\\'&printer=\\\'+this.options[this.options.selectedIndex].text+\\\'&manufacturer=\\\'+jQuery(\\\'#ManufacturerSelection_'.$ads->id.'\\\').val()+\\\'&id=' . $ads->id . '&check=' . $ads->ads->ads_date . '\\\'});window.open(\\\''.$targetPath.'?adsId='.$ads->id.'&adsCheck='.$ads->ads->ads_check.'&adsGoto=\\\'+this.value);">
					<option>Drucker w&auml;hlen</option>
				</select>
			</div>
			<div class="LogoContainer_'.$ads->id.'">' . $sLogo . '</div>
		</div>';
} else {
        $sJavaScriptSnippet .= '';
}
$sJavaScriptSnippet .= '\');
';



$sPrinterChooseBox			 = '
function FillPrinterSelection_'.$ads->id.'() {
	window.document.getElementById(\'Step1_'.$ads->id.'\').src					 = "'.$aConfig[path].'images/step1.png";
	window.document.getElementById(\'ManufacturerSelection_'.$ads->id.'\').style.borderColor	 = "#' . $sStepWeakBackgroundColor . '";
	window.document.getElementById(\'Step2_'.$ads->id.'\').src					 = "'.$aConfig[path].'images/step2_active.png";
	window.document.getElementById(\'Printer_'.$ads->id.'\').disabled				 = false;
	window.document.getElementById(\'Printer_'.$ads->id.'\').focus();
}

' . preg_replace("/[\t\r\n]/", NULL, $sJavaScriptSnippet) . '

';

print $sPrinterChooseBox;

?>