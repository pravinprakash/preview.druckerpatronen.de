<?php

$relPath = "../../../../affiliates/ads/choosebox/";
$absPath = "http://www.druckerpatronen.de/affiliates/ads/choosebox/";

require_once $relPath.'classes/class-xml.php';

$sPrinterXmlFile		 = $relPath.'manufacturers/' . $_REQUEST['manufacturer'] . '.xml';
$oPrinterXml			 = new Xml();
$oPrinterXml->ParseXml($sPrinterXmlFile);
$sPrinterSelection		 = NULL;
$sPrinterSelection = '<option value="">Bitte wählen</option>';
foreach($oPrinterXml->aXmlArray['printer'] AS $iPrinterKey => $aPrinter) {
	$sPrinterSelection	.= '<option value="' . $aPrinter['url'] . '">' . $aPrinter['name'] . '</option>';
}

$data = json_encode(Array("response" => $sPrinterSelection));
echo $_GET['jsonp_callback'] ."(". $data .");";

?>