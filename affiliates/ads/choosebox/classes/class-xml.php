<?php

class Xml {
	/*
	 * Parst Xml-Dateien
	*/
	function ParseXml($sXmlFile) {
		$sXmlContent		 = file_get_contents($sXmlFile);
		$oXmlObject		 = simplexml_load_string($sXmlContent, NULL, LIBXML_NOCDATA);
		$this->aXmlArray	 = $this->objectsIntoArray($oXmlObject);
	}

	/*
	 * Konvertiert Objekte in Arrays
	 * @param	array		$arrObjData
	 * @param	array		$arrSkipIndices
	 * @return	array		$arrData
	*/
	function objectsIntoArray($arrObjData, $arrSkipIndices = array()) {
		$arrData = array();

		if (is_object($arrObjData)) {
			$arrObjData = get_object_vars($arrObjData);
		}

		if (is_array($arrObjData)) {
			foreach ($arrObjData as $index => $value) {
				if (is_object($value) || is_array($value)) {
					$value = $this->objectsIntoArray($value, $arrSkipIndices); // recursive call
				}
				if (in_array($index, $arrSkipIndices)) {
					continue;
				}
				$arrData[$index] = $value;
			}
		}

		return $arrData;
	}

	/*
	 * Funktion Generiert eine Farbe aus einem bisherigen Hexcode;
	 */
	function CalculateColor($sHex, $iFactor = 6) {
		$aHexValues		 = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
		$sNewHex		 = NULL;

		for($iCounter1 = 0; $iCounter1 < strlen($sHex); $iCounter1++) {
			$iOldPosition	 = array_search(strtolower($sHex[$iCounter1]), $aHexValues);
			$iNewPosition	 = $iOldPosition + $iFactor;
			if($iNewPosition < 0) $iNewPosition = 0;
			if($iNewPosition > 15) $iNewPosition = 15;
			$sNewHex	.= $aHexValues[$iNewPosition];
		}

		return $sNewHex;
	}

}

?>