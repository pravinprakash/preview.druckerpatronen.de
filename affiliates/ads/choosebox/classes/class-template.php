<?php
/**
 * Enth�lt die Klasse Template
 * @author  $Author: SEBER-RIDER $
 * @version $Revision: 104 $
 * @date    $Date: 2010-03-19 19:48:26 +0100 (Fr, 19. Mrz 2010) $
 */

/**
 * Die Klasse ist f�r alles zust�ndig, was Templates betrifft.
 */
class Template {

	/**
	 * Constructor
	 */
	function Template() {
		
	}

	/**
	 * Holt den Inhalt eines Templates in eine Variable und filtert gleich Wiederhol- und Optionalbereiche heraus
	 * @param string $sTemplate Der Dateiname des Templates ohne Pfad; Standard: ---; Status: notwendig;
	 * @param boolean $bReturnDirect Wenn TRUE, wird das Template direkt zur�ck gegeben. Standard: FALSE; Status: optional
	 * @return string Inhalt des Templates
	 */
	function GetTemplate($sTemplate, $bReturnDirect = FALSE) {
		$sTemplateContent			 = file_get_contents($sTemplate, 1);

		if($bReturnDirect) {
			return $sTemplateContent;
		} else {
			$this->sTemplateWithoutAreas	 = preg_replace('/<!-- Open (Repeat|Optional)Area (.+) -->\s*?(.+)\s*?<!-- Close (Repeat|Optional)Area \\2 -->/isU', '$2_$1', $sTemplateContent);
			preg_match_all('/<!-- Open RepeatArea (.+) -->\s*?(.+)\s*?<!-- Close RepeatArea \\1 -->/isU', $sTemplateContent, $this->aTemplateRepeatAreas, PREG_SET_ORDER);
			preg_match_all('/<!-- Open OptionalArea (.+) -->\s*?(.+)\s*?<!-- Close OptionalArea \\1 -->/isU', $sTemplateContent, $this->aTemplateOptionalAreas, PREG_SET_ORDER);
		}
	}

	/**
	 * Gibt - wenn vorhanden - den Wiederholbereich eines Templates zur�ck
	 * @param string $sArea Die Bezeichnung des Wiederholbereiches
	 * @param string $sType "Optional" f�r Optionalbereiche, NULL f�r Wiederholbereiche
	 * @return string Inhalt des Wiederholbereichs
	 */
	function GetArea($sArea, $sType = NULL) {
		$aAreas		 = ($sType === 'Optional') ? $this->aTemplateOptionalAreas : $this->aTemplateRepeatAreas;

		foreach($aAreas AS $iKey => $aArea) {
			if($aArea[1] == $sArea) {
				return $aArea[2];
			}
		}
	}

	/**
	 * F�llt einen Wiederholbereich mit Inhalt
	 * @param array $aVariables Array mit den zu f�llenden Werten
	 * @param string $sArea Die Bezeichnung des Wiederholbereiches
	 */
	function FillRepeatArea($aVariables, $sArea, $sEmptyValueReplaceString = '&#x200B;') {
		$sEmptyRepeatArea		 = $this->GetArea($sArea);
		$sFullRepeatArea		 = NULL;

		foreach($aVariables AS $mKey => $aValue) {
			$sRepeatItem		 = $sEmptyRepeatArea;

			foreach($aValue AS $sKey => $mValue) {
				$mValue		 = (!isset($mValue)) ? $sEmptyValueReplaceString : $mValue;
				$sKey		 = (substr($sKey, 0, 1) != '$') ? '$' . $sKey : $sKey;
				$sRepeatItem	 = preg_replace('/\\' . $sKey . '\b/', $mValue, $sRepeatItem);
			}

			$sFullRepeatArea	.= $sRepeatItem . "\r\n";
		}
		$this->SetTemplateVariable(array($sArea . '_Repeat' => $sFullRepeatArea));
	}

	/**
	 * Zeigt Optionalbereiche an
	 * @param array $aAreas Die Bezeichnungen der Wiederholbereiche
	 * @param array $aVariables Array mit den zu f�llenden Werten; Standard = NULL; Status: optional
	 */
	function ShowOptionalAreas($mAreas, $aVariables = NULL) {
		if(!is_array($mAreas)) {
			$aAreas[]			 = $mAreas;
		}

		foreach($aAreas AS $iKey => $sArea) {
			$sOptionalArea			 = $this->GetArea($sArea, $sType = 'Optional');

			if(!empty($aVariables)) {
				foreach($aVariables AS $sKey => $mValue) {
					$mValue		 = (empty($mValue)) ? '&nbsp;' : $mValue;
					$sOptionalArea	 = preg_replace('/\\' . $sKey . '\b/', $mValue, $sOptionalArea);
				}
			}

			$this->SetTemplateVariable(array($sArea . '_Optional' => $sOptionalArea));
		}
	}

	/**
	 * Speichert Templatevariablen in eine Klassenvariable
	 * @param array $aVariables Array mit den zu f�llenden Werten
	 */
	function SetTemplateVariable($aVariables) {
		foreach($aVariables AS $sKey => $mValue) {
			$this->aTemplateVariables[$sKey]	 = $mValue;
		}
	}

	/**
	 * L�scht nicht ersetzte Templatevariablen
	 */
	function DeleteNotUsedAreas() {
		foreach($this->aTemplateRepeatAreas AS $iKey => $aTemplateRepeatArea) {
			$this->sFinishedTemplate	 = str_replace($aTemplateRepeatArea[1] . '_Repeat', NULL, $this->sFinishedTemplate);
		}
		foreach($this->aTemplateOptionalAreas AS $iKey => $aTemplateOptionalArea) {
			$this->sFinishedTemplate	 = str_replace($aTemplateOptionalArea[1] . '_Optional', NULL, $this->sFinishedTemplate);
		}
		$this->sFinishedTemplate		 = preg_replace('/\$[a-z][A-Za-z0-9]+/', NULL, $this->sFinishedTemplate);
	}

	/**
	 * F�llt die Templatevariablen in das Template
	 * @return boolean Gibt FALSE zur�ck, wenn die Funktion Template::GetTemplate($sTemplate) nicht zuvor aufgerufen wurde
	 */
	function MergeVariablesIntoTemplate() {
		if(!isset($this->sTemplateWithoutAreas)) {
			trigger_error('Funktion Template::GetTemplate($sTemplate) wurde nicht aufgerufen.');
			return FALSE;
		}

		$this->sFinishedTemplate		 = $this->sTemplateWithoutAreas;

		$aStandardVariables['$sRequestUri']	 = $_SERVER['REQUEST_URI'];
		$aStandardVariables['$sServerName']	 = $_SERVER['SERVER_NAME'];
		$aStandardVariables['$sPhpSelf']	 = $_SERVER['PHP_SELF'];
		$aStandardVariables['$sPath']		 = dirname($_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
		$aStandardVariables['$sReferer']	 = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : NULL;

		$this->SetTemplateVariable($aStandardVariables);

		foreach($this->aTemplateVariables AS $sKey => $mValue) {
			$this->sFinishedTemplate	 = preg_replace('/\\' . $sKey . '\b/', $mValue, $this->sFinishedTemplate);
		}
	}

	/**
	 * Entfernt die Kommentare im Dateikopf von *.htm-Dateien
	 */
	function RemoveTemplateHeaderComment() {
		$this->sFinishedTemplate	 = preg_replace('/\s*<!--.+?\/\*.+?\*\/.+?-->\s*/s', NULL, $this->sFinishedTemplate);
	}

	/**
	 * Gibt das fertige Template zur�ck
	 * @param	boolean		$bRemoveAllWhiteSpace		Wenn wahr, wird jeglicher �berfl�ssige Whitespace aus dem Template entfernt;		Standard: FALSE;		Status: optional;
	 * @return	string						Der fertige Inhalt des Templates;
	 */
	function GetFinishedTemplate($bRemoveAllWhiteSpace = FALSE) {
		$this->MergeVariablesIntoTemplate();
		$this->DeleteNotUsedAreas();
		$this->RemoveTemplateHeaderComment();

		if($bRemoveAllWhiteSpace) {
			$this->sFinishedTemplate	 = preg_replace('/\t+/', ' ', $this->sFinishedTemplate);
			$this->sFinishedTemplate	 = preg_replace('/ {2,}/', ' ', $this->sFinishedTemplate);
			$this->sFinishedTemplate	 = preg_replace('/([\r\n]| {2,})+/', NULL, $this->sFinishedTemplate);
			$this->sFinishedTemplate	 = preg_replace('/<!--\s/', NULL, $this->sFinishedTemplate);
			$this->sFinishedTemplate	 = preg_replace('/\s-->/', NULL, $this->sFinishedTemplate);
		}

		return $this->sFinishedTemplate;
	}
}

?>