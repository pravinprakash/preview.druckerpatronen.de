<?php
//require_once 'app/Mage.php';
//Mage::app();
//
error_reporting(E_ALL);
ini_set('display_errors', '1');

const DS = DIRECTORY_SEPARATOR;

class ProductReport {

    const DIR_CACHE = 'cache/dropdown';
    const FILE_CATS = '0';

    private $dirBase;
    private $dirCache;
    private $returnCode;

    public function __construct() {

        $this->dirBase = dirname(realpath(__FILE__));
        $this->dirCache = $this->dirBase . DS . '..' . DS . '..' . DS . self::DIR_CACHE;
    }

    private function _getCatIds() {

        $contents = file_get_contents($this->dirCache . DS . self::FILE_CATS);
        $matches = array();
        preg_match_all('/option value="(.+?)"/', $contents, $matches);
        return $matches[1];
    }

    private function _getFileContents($catId) {

        $contents = file_get_contents($this->dirCache . DS . $catId);
        return $contents;
    }

    private function _parseProductUrls($contents) {

        $matches = array();
        preg_match_all('/option value="(http.*?)"/', $contents, $matches);
        if (isset($matches[1]))
            return $matches[1];
        return array();
    }

    private function _isReachable($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->returnCode = $httpcode;
        if ($httpcode != 200)
            return false;
        return true;
    }

    public function run() {

        $catIds = $this->_getCatIds();

        foreach ($catIds as $catId) {

            try {
                $contents = $this->_getFileContents($catId);
                if (!$contents) {
                    echo 'File ' . $catId . ' does not exist. Abort.' . "\n";
                    continue;
                }

                $productUrls = $this->_parseProductUrls($contents);

                foreach ($productUrls as $productUrl) {

                    if (!$this->_isReachable($productUrl))
                        echo 'Not reachable ' . $productUrl . ' ' . $this->returnCode . "\n";
                }
            }
            catch (Exception $e) {
                echo 'Could not read file ' . $catId . "\n" . $e->getMessage() . "\n";
            }
        }


    }
}
$report = new ProductReport();
$report->run();

