<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Mage
 * @package    Mage_CashOnDelivery
 * @copyright  Copyright (c) 2008 Andrej Sinicyn, Mik3e
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Mage_CashOnDelivery_Model_CashOnDelivery extends Mage_Payment_Model_Method_Abstract
{

    /**
    * unique internal payment method identifier
    * 
    * @var string [a-z0-9_]
    */
    protected $_code = 'cashondelivery';

    protected $_formBlockType = 'cashOnDelivery/form';
    protected $_infoBlockType = 'cashOnDelivery/info';

    public function getCODTitle()
    {
        return $this->getConfigData('title');
    }

    public function getInlandCosts()
    {
        return floatval($this->getConfigData('inlandcosts'));
    }

    public function getForeignCountryCosts()
    {
        return floatval($this->getConfigData('foreigncountrycosts'));
    }

    public function getCustomText()
    {
        return $this->getConfigData('customtext');
    }

}
