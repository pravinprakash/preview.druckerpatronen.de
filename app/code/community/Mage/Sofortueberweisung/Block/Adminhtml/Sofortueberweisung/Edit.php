<?php

class Mage_Sofortueberweisung_Block_Adminhtml_Sofortueberweisung_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'sofortueberweisung';
        $this->_controller = 'adminhtml_sofortueberweisung';
        
        $this->_updateButton('save', 'label', Mage::helper('sofortueberweisung')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('sofortueberweisung')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('sofortueberweisung_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'sofortueberweisung_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'sofortueberweisung_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('sofortueberweisung_data') && Mage::registry('sofortueberweisung_data')->getId() ) {
            return Mage::helper('sofortueberweisung')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('sofortueberweisung_data')->getTitle()));
        } else {
            return Mage::helper('sofortueberweisung')->__('Add Item');
        }
    }
}