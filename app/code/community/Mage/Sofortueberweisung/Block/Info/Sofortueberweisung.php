<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Sofortueberweisung
  * @copyright  Copyright (c) 2008 [m]zentrale GbR 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_Sofortueberweisung_Block_Info_Sofortueberweisung extends Mage_Payment_Block_Info
{
    /**
     * Init default template for block
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('sofortueberweisung/info/sofortueberweisung.phtml');
    }
	
	 /**
     * Retrieve info model
     *
     * @return Mage_Sofortueberweisung_Model_Info
     */
    public function getInfo()
    {
        $info = $this->getData('info');
		
        if (!($info instanceof Mage_Payment_Model_Info)) {
            Mage::throwException($this->__('Can not retrieve payment info model object.'));
        }
        return $info;
    }
	
	 /**
     * Retrieve payment method model
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function getMethod()
    {
        return $this->getInfo()->getMethodInstance();
    }
	
	public function toPdf()
    {
        $this->setTemplate('sofortueberweisung/info/pdf/sofortueberweisung.phtml');
        return $this->toHtml();
    }
}