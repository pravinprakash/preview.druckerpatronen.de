<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Sofortueberweisung
  * @copyright  Copyright (c) 2008 [m]zentrale GbR 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_Sofortueberweisung_Model_Sofortueberweisung extends Mage_Payment_Model_Method_Abstract
{
    
	/**
	* Availability options
	*/
	protected $_code = 'sofortueberweisung';   
	protected $_paymentMethod = 'sofortueberweisung';
	
	protected $_formBlockType = 'sofortueberweisung/form_sofortueberweisung';
    protected $_infoBlockType = 'sofortueberweisung/info_sofortueberweisung';
	
    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = false;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;	
	
	
	public function _construct()
    {
        parent::_construct();
        $this->_init('sofortueberweisung/sofortueberweisung');
    }	 
	
	public function getUrl(){
		return $this->getConfigData('url');
	}
	
	 /**
     * Return redirect block type
     *
     * @return string
     */
    public function getRedirectBlockType()
    {
        return $this->_redirectBlockType;
    }

    /**
     * Return payment method type string
     *
     * @return string
     */
    public function getPaymentMethodType()
    {
        return $this->_paymentMethod;
    }
	
	/**
	* Get redirect URL
	*
	* @return Mage_Payment_Helper_Data
	*/
	public function getOrderPlaceRedirectUrl()
    {
          return Mage::getUrl('sofortueberweisung/sofortueberweisung/redirect');
    }
	
	public function assignData($data)
    {
       	if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setSuAccountNumber($data->getSuAccountNumber())
				->setSuBankCode($data->getSuBankCode())
				->setSuNlBankCode($data->getSuNlBankCode())
				->setSuHolder($data->getSuHolder());   
                  
        return $this;
    }
	
	public function getSecurityKey(){
		return uniqid(rand(), true);
	}
	
	public function validate()
    {
        parent::validate();
		
		if (!$this->getQuote()->getPayment()->getSuHolder()) {
            Mage::throwException(Mage::helper('sofortueberweisung')->__('Please fill out the account holder'));
        }
		if (!$this->getQuote()->getPayment()->getSuAccountNumber()) {
            Mage::throwException(Mage::helper('sofortueberweisung')->__('Please fill out the account number'));
        }
		if($this->getConfigData('netherlands')){
			if (!$this->getQuote()->getPayment()->getSuNlBankCode()) {
				Mage::throwException(Mage::helper('sofortueberweisung')->__('Please fill out the bank code'));
			}
		}else{
			if (!$this->getQuote()->getPayment()->getSuBankCode()) {
				Mage::throwException(Mage::helper('sofortueberweisung')->__('Please fill out the bank code'));
			}
		}
		
        return $this;
    }
	
	public function getFormFields()
    {
    	$amount		= number_format($this->getOrder()->getGrandTotal(),2,'.','');
		$billing	= $this->getOrder()->getBillingAddress();
		$security 	= $this->getSecurityKey();
		
		$this->getOrder()->getPayment()->setSuSecurity($security)->save();
		
		$hashStr	= '';		
    	$params = 	array(
	    				'user_id'				=> $this->getConfigData('customer'),
	    				'project_id' 			=> $this->getConfigData('project'),
						'sender_holder' 		=> $this->getOrder()->getPayment()->getSuHolder(),
						'sender_account_number' => $this->getOrder()->getPayment()->getSuAccountNumber(),
						'sender_bank_code' 		=> ($this->getConfigData('netherlands')) ? $this->getOrder()->getPayment()->getSuNlBankCode() : $this->getOrder()->getPayment()->getSuBankCode(),
						'sender_country_id'  	=> $billing->getCountry(),
						'amount' 				=> $amount,
						'currency_id' 			=> $this->getOrder()->getOrderCurrencyCode(),
						'reason_1' 				=> Mage::helper('sofortueberweisung')->__('Order No.: ').$this->getOrder()->getRealOrderId(),
						'reason_2' 				=> '',
						'user_variable_0' 		=> $this->getOrder()->getRealOrderId(),
						'user_variable_1' 		=> $this->getOrder()->getPayment()->getSuSecurity(),
						'user_variable_2' 		=> '',
						'user_variable_3' 		=> '',
						'user_variable_4' 		=> '',
						'user_variable_5' 		=> '',									
					);

			
		
		// create secure hash if needed		
		if($this->getConfigData('check_input_yesno') == 1)
			$params['hash'] = md5(implode('|',$params).'|'.$this->getConfigData('project_pswd'));

    	return $params;    	
    }
	
	/**
     * Get quote
     *
     * @return Mage_Sales_Model_Order
     */
	public function getQuote()
    {
        if (empty($this->_quote)) {            
            $this->_quote = $this->getCheckout()->getQuote();
        }
        return $this->_quote;
    }
	
	/**
     * Get checkout
     *
     * @return Mage_Sales_Model_Order
     */
	 public function getCheckout()
    {
        if (empty($this->_checkout)) {
            $this->_checkout = Mage::getSingleton('checkout/session');
        }
        return $this->_checkout;
    }	
	
	/**
     * Get order model
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->_order) {
            $paymentInfo = $this->getInfoInstance();
            $this->_order = Mage::getModel('sales/order')
                            ->loadByIncrementId($paymentInfo->getOrder()->getRealOrderId());
        }
        return $this->_order;
    }
	
}