<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Sofortueberweisung
  * @copyright  Copyright (c) 2008 [m]zentrale GbR 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
?>
<?php
$installer = $this;

$installer->startSetup();
$installer->addAttribute('order_payment', 'su_security', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_bank_code', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_account_number', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_holder', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_transaction_id', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_paycode', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_iban', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_bic', array('type'=>'varchar'));

$installer->addAttribute('quote_payment', 'su_security', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_bank_code', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_account_number', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_holder', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_paycode', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_iban', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_bic', array('type'=>'varchar'));
$installer->endSetup();

if (Mage::getVersion() >= 1.1) {
    $installer->startSetup();    
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_bank_code', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_account_number', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_holder', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_paycode', 'VARCHAR(255) NOT NULL'); 
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_iban', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_bic', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_security', 'VARCHAR(255) NOT NULL'); 
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_nl_bank_code', 'VARCHAR(255) NOT NULL'); 
    $installer->endSetup();
} 

$installer->startSetup();
$installer->run("
	DELETE FROM {$this->getTable('cms/page_store')} WHERE page_id = (SELECT `page_id` FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisung');
	DELETE FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisung' OR identifier = 'sofortueberweisunginfo';	
	
	INSERT INTO {$this->getTable('cms_page')}(`title`,`root_template`,`meta_keywords`,`meta_description`,`identifier`,`content`,`creation_time`,`update_time`,`is_active`,`sort_order`) VALUES ('Sofortüberweisung','two_columns_right','Sofortüberweisung','Informationen zur Bezahlung über Sofortüberweisung','sofortueberweisunginfo','<h3  style=\"color: rgb(241, 142, 0);\">sofortüberweisung.de</h3>\n\r<p>Die neue innovative Zahlungsart mit TÜV-Zertifikat</p>\n\r<br>\n\r<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\r	<tr>\n\r		<td align=\"left\" valign=\"top\" width=\"180\"><img src=\"{{skin url=\"images/sofortueberweisung/logo_tuev_144px.gif\" }}\" width=\"180\" alt=\"\"></td>\n\r		<td align=\"left\" valign=\"top\" style=\"padding-left:10px;\">\n\rsofortüberweisung.de ist ein neues und TÜV-zertifiziertes Online-Zahlungssystem auf Basis des bewährten Online-Bankings mit PIN-/TAN-Eingabe zur sicheren und schnellen Abwicklung Ihrer Online-Käufe.\n\r</td>\n\r	</tr>\n\r</table>\n\r<p>&#160;</p>\n\r<h3>So funktioniert \"sofortüberweisung.de\"</h3>\n\r<p><b>Die einzelnen Schritte im Überblick</b></p>\n\r<p><b>1. Schritt:</b>\"Überweisungsformular\"<br><br>\n\rBitte geben Sie zuerst Ihre Bankverbindung (Kontoinhaber, Kontonummer und Bankleitzahl) ein, damit wir eine Verbindung mit Ihrer Bank herstellen können. Der Name der Bank wird automatisch ausgefüllt.<br>\n\rAuf dem Überweisungsformular sind bereits folgende Angaben ausgefüllt:\n\r<ul>\n\r	<li>Überweisungsbetrag</li>\n\r	<li>Verwendungszweck (z.B. Überweisungsnummer, Kundennummer)</li>\n\r</ul>\n\r</p>\n\r<p><b>2. Schritt:</b>\"Legitimation\"<br><br>\n\rBitte geben Sie, wie beim Online-Banking bei Ihrer Bank gewohnt, Folgendes ein:\n\r<ul>\n\r	<li>Anmeldename zum Login bei Ihrer Bank (Directbanking-Nr. oder Konto-Nr., bankabhängig)</li>\n\r	<li>PIN, die Sie auch bei Ihrer Bank verwenden</li>\n\r</ul>\n\r</p>\n\r<p><b>3. Schritt:</b>\"Überweisung übermitteln\"<br><br>\n\rBitte überprüfen Sie an dieser Stelle nochmals alle Angaben der Überweisung.<br><br>\n\rUm die Überweisung abzuschließen, müssen Sie eine gültige TAN, die Ihnen von Ihrer Bank zugeteilt wurde, eingeben und anschließend den Button \"Überweisung bestätigen und Bestellung abschließen\" drücken.\n\r</p>\n\r<p>&#160;</p>\n\r<h3>Was ist \"sofortüberweisung.de\"?</h3>\n\r<p>\"sofortüberweisung.de\" ist eine neue, innovative Zahlungsart mit TÜV-Zertifikat und TÜV geprüfter Transaktionssicherheit. Sofortüberweisung wurde von der Payment Network AG entwickelt und bereits in zahlreichen Online-Shops in Deutschland eingesetzt.</p>\n\r<p>Über das gesicherte, für Händler nicht zugängliche Zahlformular der Payment Network AG stellt sofortüberweisung.de automatisiert und in Echtzeit eine Überweisung in Ihrem Online-Bankkonto ein. Der Kaufbetrag wird dabei sofort und direkt an das Bankkonto des Händlers überwiesen.</p>\n\r<p>&#160;</p>\n\r<h3>*Hinweis zur Haftung bei Missbrauchsfällen</h3>\n\r<p>Bei dem Dienst \"sofortüberweisung.de\" ist es bisher zu keinen Missbräuchen gekommen (TÜV- zertifiziertes-Online-Zahlungssystem). Vorsorglich weisen wir dennoch darauf hin, dass es viele Banken und Sparkassen gibt, die davon ausgehen, dass die Nutzung des Dienstes \"sofortüberweisung.de\" wegen der Verwendung Ihrer PIN und TAN zu einer Haftungsverlagerung bei etwaigen Missbrauchsfällen durch Dritte führt. Dies kann dazu führen, dass im Missbrauchsfall Ihre Bank sich weigert, den Schaden zu übernehmen und im Ergebnis Sie den Schaden zu tragen haben. Vorsorglich hat daher der Betreiber des Dienstes \"Sofortüberweisung\", die Payment Network AG, zu Ihren Gunsten eine Versicherung abgeschlossen, die Schäden bei Missbrauch nach Maßgabe der unter diesem Link wiedergegebenen Versicherungsbedingungen ersetzt. Hierdurch sollen Sie im Rahmen des Versicherungsumfanges vor etwaigen Haftungsrisiken geschützt werden.</p>\n\r<p>&#160;</p>\n\r<h3>Ihre Vorteile bei sofortüberweisung.de</h3>\n\r<p><b>Schnell</b><br>\n\rSofortiger Versand von Lagerware.\n\r</p><br>\n\r<p><b>Bequem</b><br>\n\rBereits unmittelbar nach Abschluss Ihrer Bestellung können Sie die Überweisung direkt durchführen. Ein Wechsel zur Internetseite Ihrer Bank ist damit nicht mehr erforderlich. Das spart Zeit!\n\r</p><br>\n\r<p><b>Praktisch</b><br>\n\rDie Daten für Ihre Überweisung sind bereits vorausgefüllt (Rechnungsbetrag, Zahlungsempfänger, Auftragsnummer, etc.). Das mühsames Eintippen und Ausfüllen des Überweisungsformulars entfällt.\n\r</p><br>\n\r<p><b>Sicher wie Online-Banking</b><br>\n\rDas System akzeptiert die gleiche PIN und erkennt die gültigen TANs Ihrer Bank, da Sie sich mit diesen Daten bei Ihrer Bank anmelden.\n\r</p><br>','2008-09-24 00:00:00','2008-09-24 00:00:00',1,0);
	INSERT INTO {$this->getTable('cms/page_store')} (`page_id`) SELECT `page_id` FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisunginfo';
");
$installer->endSetup();