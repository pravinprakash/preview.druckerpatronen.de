<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Sofortueberweisung
  * @copyright  Copyright (c) 2008 [m]zentrale GbR 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute('order_payment', 'su_security', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_bank_code', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_account_number', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_holder', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_transaction_id', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_paycode', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_iban', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'su_bic', array('type'=>'varchar'));

$installer->addAttribute('quote_payment', 'su_security', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_bank_code', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_account_number', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_holder', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_paycode', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_iban', array('type'=>'varchar'));
$installer->addAttribute('quote_payment', 'su_bic', array('type'=>'varchar'));
$installer->endSetup();

if (Mage::getVersion() >= 1.1) {
    $installer->startSetup();    
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_bank_code', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_account_number', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_holder', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_paycode', 'VARCHAR(255) NOT NULL'); 
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_iban', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_bic', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_security', 'VARCHAR(255) NOT NULL');
	$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_payment'), 'su_nl_bank_code', 'VARCHAR(255) NOT NULL');	
    $installer->endSetup();
} 

$installer->startSetup();
$installer->run("
	DELETE FROM {$this->getTable('cms/page_store')} WHERE page_id = (SELECT `page_id` FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisung');
	DELETE FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisung' OR identifier = 'sofortueberweisunginfo';	
	
	INSERT INTO {$this->getTable('cms_page')}(`title`,`root_template`,`meta_keywords`,`meta_description`,`identifier`,`content`,`creation_time`,`update_time`,`is_active`,`sort_order`) VALUES ('Sofort�berweisung','two_columns_right','Sofort�berweisung','Informationen zur Bezahlung �ber Sofort�berweisung','sofortueberweisunginfo','<h3  style=\"color: rgb(241, 142, 0);\">sofort�berweisung.de</h3>\n\r<p>Die neue innovative Zahlungsart mit T�V-Zertifikat</p>\n\r<br>\n\r<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\r	<tr>\n\r		<td align=\"left\" valign=\"top\" width=\"180\"><img src=\"{{skin url=\"images/sofortueberweisung/logo_tuev_144px.gif\" }}\" width=\"180\" alt=\"\"></td>\n\r		<td align=\"left\" valign=\"top\" style=\"padding-left:10px;\">\n\rsofort�berweisung.de ist ein neues und T�V-zertifiziertes Online-Zahlungssystem auf Basis des bew�hrten Online-Bankings mit PIN-/TAN-Eingabe zur sicheren und schnellen Abwicklung Ihrer Online-K�ufe.\n\r</td>\n\r	</tr>\n\r</table>\n\r<p>&#160;</p>\n\r<h3>So funktioniert \"sofort�berweisung.de\"</h3>\n\r<p><b>Die einzelnen Schritte im �berblick</b></p>\n\r<p><b>1. Schritt:</b>\"�berweisungsformular\"<br><br>\n\rBitte geben Sie zuerst Ihre Bankverbindung (Kontoinhaber, Kontonummer und Bankleitzahl) ein, damit wir eine Verbindung mit Ihrer Bank herstellen k�nnen. Der Name der Bank wird automatisch ausgef�llt.<br>\n\rAuf dem �berweisungsformular sind bereits folgende Angaben ausgef�llt:\n\r<ul>\n\r	<li>�berweisungsbetrag</li>\n\r	<li>Verwendungszweck (z.B. �berweisungsnummer, Kundennummer)</li>\n\r</ul>\n\r</p>\n\r<p><b>2. Schritt:</b>\"Legitimation\"<br><br>\n\rBitte geben Sie, wie beim Online-Banking bei Ihrer Bank gewohnt, Folgendes ein:\n\r<ul>\n\r	<li>Anmeldename zum Login bei Ihrer Bank (Directbanking-Nr. oder Konto-Nr., bankabh�ngig)</li>\n\r	<li>PIN, die Sie auch bei Ihrer Bank verwenden</li>\n\r</ul>\n\r</p>\n\r<p><b>3. Schritt:</b>\"�berweisung �bermitteln\"<br><br>\n\rBitte �berpr�fen Sie an dieser Stelle nochmals alle Angaben der �berweisung.<br><br>\n\rUm die �berweisung abzuschlie�en, m�ssen Sie eine g�ltige TAN, die Ihnen von Ihrer Bank zugeteilt wurde, eingeben und anschlie�end den Button \"�berweisung best�tigen und Bestellung abschlie�en\" dr�cken.\n\r</p>\n\r<p>&#160;</p>\n\r<h3>Was ist \"sofort�berweisung.de\"?</h3>\n\r<p>\"sofort�berweisung.de\" ist eine neue, innovative Zahlungsart mit T�V-Zertifikat und T�V gepr�fter Transaktionssicherheit. Sofort�berweisung wurde von der Payment Network AG entwickelt und bereits in zahlreichen Online-Shops in Deutschland eingesetzt.</p>\n\r<p>�ber das gesicherte, f�r H�ndler nicht zug�ngliche Zahlformular der Payment Network AG stellt sofort�berweisung.de automatisiert und in Echtzeit eine �berweisung in Ihrem Online-Bankkonto ein. Der Kaufbetrag wird dabei sofort und direkt an das Bankkonto des H�ndlers �berwiesen.</p>\n\r<p>&#160;</p>\n\r<h3>*Hinweis zur Haftung bei Missbrauchsf�llen</h3>\n\r<p>Bei dem Dienst \"sofort�berweisung.de\" ist es bisher zu keinen Missbr�uchen gekommen (T�V- zertifiziertes-Online-Zahlungssystem). Vorsorglich weisen wir dennoch darauf hin, dass es viele Banken und Sparkassen gibt, die davon ausgehen, dass die Nutzung des Dienstes \"sofort�berweisung.de\" wegen der Verwendung Ihrer PIN und TAN zu einer Haftungsverlagerung bei etwaigen Missbrauchsf�llen durch Dritte f�hrt. Dies kann dazu f�hren, dass im Missbrauchsfall Ihre Bank sich weigert, den Schaden zu �bernehmen und im Ergebnis Sie den Schaden zu tragen haben. Vorsorglich hat daher der Betreiber des Dienstes \"Sofort�berweisung\", die Payment Network AG, zu Ihren Gunsten eine Versicherung abgeschlossen, die Sch�den bei Missbrauch nach Ma�gabe der unter diesem Link wiedergegebenen Versicherungsbedingungen ersetzt. Hierdurch sollen Sie im Rahmen des Versicherungsumfanges vor etwaigen Haftungsrisiken gesch�tzt werden.</p>\n\r<p>&#160;</p>\n\r<h3>Ihre Vorteile bei sofort�berweisung.de</h3>\n\r<p><b>Schnell</b><br>\n\rSofortiger Versand von Lagerware.\n\r</p><br>\n\r<p><b>Bequem</b><br>\n\rBereits unmittelbar nach Abschluss Ihrer Bestellung k�nnen Sie die �berweisung direkt durchf�hren. Ein Wechsel zur Internetseite Ihrer Bank ist damit nicht mehr erforderlich. Das spart Zeit!\n\r</p><br>\n\r<p><b>Praktisch</b><br>\n\rDie Daten f�r Ihre �berweisung sind bereits vorausgef�llt (Rechnungsbetrag, Zahlungsempf�nger, Auftragsnummer, etc.). Das m�hsames Eintippen und Ausf�llen des �berweisungsformulars entf�llt.\n\r</p><br>\n\r<p><b>Sicher wie Online-Banking</b><br>\n\rDas System akzeptiert die gleiche PIN und erkennt die g�ltigen TANs Ihrer Bank, da Sie sich mit diesen Daten bei Ihrer Bank anmelden.\n\r</p><br>','2008-09-24 00:00:00','2008-09-24 00:00:00',1,0);
	INSERT INTO {$this->getTable('cms/page_store')} (`page_id`) SELECT `page_id` FROM {$this->getTable('cms/page')} WHERE identifier = 'sofortueberweisunginfo';
");
$installer->endSetup();
?>