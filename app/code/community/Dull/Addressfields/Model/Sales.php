<?php
 
class Dull_Addressfields_Model_Sales extends Mage_Sales_Model_Quote_Address
{
    
    public function validate()
    {
        $errors = array();
        $helper = Mage::helper('customer');
        $this->implodeStreetAddress();
        if (!Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter first name.');
        }

        if (!Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter last name.');
        }

        if (Mage::helper('addressfields/data')->getCompany() == 'req')
        {
            
            if (!Zend_Validate::is($this->getCompany(), 'NotEmpty')) {
                $errors[] = $helper->__('Please enter company name.');
            }
            
        }

        if (!Zend_Validate::is($this->getStreet(1), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter street.');
        }

        if (!Zend_Validate::is($this->getCity(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter city.');
        }

        if (Mage::helper('addressfields/data')->getTelephone() == 'req')
        {

            if (!Zend_Validate::is($this->getTelephone(), 'NotEmpty')) {
                $errors[] = $helper->__('Please enter telephone.');
            }
        
        }

        if (Mage::helper('addressfields/data')->getFax() == 'req')
        {

            if (!Zend_Validate::is($this->getFax(), 'NotEmpty')) {
                $errors[] = $helper->__('Please enter fax.');
            }
            
        }

        if (!Zend_Validate::is($this->getPostcode(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter zip/postal code.');
        }

        if (!Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter country.');
        }

        if (Mage::helper('addressfields/data')->getRegion() == 'req')
        {

            if ($this->getCountryModel()->getRegionCollection()->getSize()
                && !Zend_Validate::is($this->getRegionId(), 'NotEmpty')) {
                $errors[] = $helper->__('Please enter state/province.');
            }
        
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }
    
    public function getSameAsBilling()
    {
        $checkoutfields = Mage::getModel('adminhtml/config_data')->setSection('checkout/options')->load();
        $billing = 0;
    
        if (array_key_exists('checkout/options/same_as_billing', $checkoutfields))
            $billing = 1;
    
        return $billing;
    }
    
}