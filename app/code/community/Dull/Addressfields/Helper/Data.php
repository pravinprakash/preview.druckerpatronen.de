<?php
 
class Dull_Addressfields_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    public function getCompany()
    {
        $addressfields = Mage::getModel('adminhtml/config_data')->setSection('customer/address')->load();
        $company = 'opt';

        if (array_key_exists('customer/address/company_show', $addressfields))
            $company = $addressfields['customer/address/company_show'];
        
        return $company;
    }
    
    public function getFax()
    {
        $addressfields = Mage::getModel('adminhtml/config_data')->setSection('customer/address')->load();
        $fax = 'opt';
    
        if (array_key_exists('customer/address/fax_show', $addressfields))
            $fax = $addressfields['customer/address/fax_show'];

        return $fax;
    }
    
    public function getTelephone()
    {
        $addressfields = Mage::getModel('adminhtml/config_data')->setSection('customer/address')->load();
        $telephone = 'req';
    
        if (array_key_exists('customer/address/telephone_show', $addressfields))
            $telephone = $addressfields['customer/address/telephone_show'];

        return $telephone;
    }
    
    public function getRegion()
    {
        $addressfields = Mage::getModel('adminhtml/config_data')->setSection('customer/address')->load();
        $region = 'req';
    
        if (array_key_exists('customer/address/region_show', $addressfields))
            $region = $addressfields['customer/address/region_show'];

        return $region;
    }
    
}