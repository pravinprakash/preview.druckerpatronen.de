<?php
/**
 *  A wrapper object around an event
 */
class Copernica_MarketingSoftware_Model_QueueEvent_SubscriptionAdd extends Copernica_MarketingSoftware_Model_QueueEvent_SubscriptionModify
{
     /**
     *  Process this item in the queue the behaviour is the same is for a modification
     */
}