<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Phoenix
 * @package    Phoenix_Ipayment
 * @copyright  Copyright (c) 2008 Phoenix Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 */

class Phoenix_Ipayment_Block_Jsinclude extends Mage_Core_Block_Template
{
    public function getAccountId() {
        $storeId = Mage::app()->getStore()->getId();
        if (Mage::getStoreConfig('payment/ipayment_cc/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_cc/account_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_cc/account_id', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_elv/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_elv/account_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_elv/account_id', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_3ds/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_3ds/account_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_3ds/account_id', $storeId);
        }
        return 99999;
    }
    public function getTrxUserId() {
        $storeId = Mage::app()->getStore()->getId();
        if (Mage::getStoreConfig('payment/ipayment_cc/active', $storeId) &&  !is_null(Mage::getStoreConfig('payment/ipayment_cc/trxuser_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_cc/trxuser_id', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_elv/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_elv/trxuser_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_elv/trxuser_id', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_3ds/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_3ds/trxuser_id', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_3ds/trxuser_id', $storeId);
        }

        return 99999;
    }
    public function getTrxUserPassword() {
        $storeId = Mage::app()->getStore()->getId();
        if (Mage::getStoreConfig('payment/ipayment_cc/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_cc/trxuser_password', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_cc/trxuser_password', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_elv/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_elv/trxuser_password', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_elv/trxuser_password', $storeId);
        }
        if (Mage::getStoreConfig('payment/ipayment_3ds/active', $storeId) && !is_null(Mage::getStoreConfig('payment/ipayment_3ds/trxuser_password', $storeId))) {
            return Mage::getStoreConfig('payment/ipayment_3ds/trxuser_password', $storeId);
        }
        return 0;
    }

}
