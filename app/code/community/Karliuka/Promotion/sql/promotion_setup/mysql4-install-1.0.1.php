<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
$installer = $this;

$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_eav_attribute')}` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' ,
  `time` smallint(5) unsigned NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion EAV Attribute Table';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_entity')}` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Promotion Entity ID',
  `promotion_type_id` int(10) unsigned NOT NULL COMMENT 'Promotion Entity ID',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Promotion Creation Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Promotion Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `promotion_type_id` (`promotion_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Table';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_entity_datetime')}` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_PROMOTION_ENT_DTIME_ENT_TYPE_ID_ENT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_PRM_ENT_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `IDX_PRM_ENT_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_PRM_ENT_DATETIME_STORE_ID` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Datetime Attribute Backend Table';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_entity_int')}` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_PRM_ENT_INT_ENTITY_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_PRM_ENT_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_PRM_ENT_INT_STORE_ID` (`store_id`),
  KEY `IDX_PRM_ENT_INT_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Entity Int';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_entity_text')}` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_PRM_ENT_TEXT_ENTITY_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_PRM_ENT_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_PRM_ENT_TEXT_STORE_ID` (`store_id`),
  KEY `IDX_PRM_ENT_TEXT_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Entity Text';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_entity_varchar')}` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_PRM_ENT_VARCHAR_ENTITY_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_PRM_ENT_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_PRM_ENT_VARCHAR_STORE_ID` (`store_id`),
  KEY `IDX_PRM_ENT_VARCHAR_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Entity Varchar';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_product_link')}` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` int(10) unsigned NOT NULL,
  `qty` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`link_id`),
  KEY `IDX_PROMOTION_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `IDX_PROMOTION_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Product To Product Linkage Table';

ALTER TABLE `{$this->getTable('promotion_entity_datetime')}`
  ADD CONSTRAINT `FK_PRM_ENT_DTM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_DTM_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `{$this->getTable('eav_attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_DTM_ENT_ID_PRM_ENT_ENT_ID` FOREIGN KEY (`entity_id`) REFERENCES `{$this->getTable('promotion_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_entity_int')}`
  ADD CONSTRAINT `FK_PRM_ENT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `{$this->getTable('eav_attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_INT_PRM_ENT_ID` FOREIGN KEY (`entity_id`) REFERENCES `{$this->getTable('promotion_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_INT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_entity_text')}`
  ADD CONSTRAINT `FK_PRM_ENT_TXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `{$this->getTable('eav_attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_TXT_PRM_ENT_ID` FOREIGN KEY (`entity_id`) REFERENCES `{$this->getTable('promotion_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_TXT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_entity_varchar')}`
  ADD CONSTRAINT `FK_PRM_ENT_VRC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `{$this->getTable('eav_attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_VRC_PRM_ENT_ID` FOREIGN KEY (`entity_id`) REFERENCES `{$this->getTable('promotion_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_ENT_VRC_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
ALTER TABLE `{$this->getTable('sales_flat_quote_item')}` 
  ADD `gift_locked` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  ADD `gift_qty` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  ADD `gift_product_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  ADD `is_gift` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  ADD `rule_id` INT(10) UNSIGNED NOT NULL DEFAULT '0';

");

$installer->endSetup();

$installer->installEntities();