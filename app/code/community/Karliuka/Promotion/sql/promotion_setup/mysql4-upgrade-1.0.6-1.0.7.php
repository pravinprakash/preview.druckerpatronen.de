<?php

$installer = $this;
$installer->startSetup();

$sql = "
DROP TABLE IF EXISTS `{$this->getTable('promotion_log_summary')}`;
CREATE TABLE IF NOT EXISTS `dpm_promotion_log_summary` (
	`rule_id` int(10) unsigned NOT NULL COMMENT 'Rule ID',
	`total_suggested` int(10) unsigned NOT NULL DEFAULT '0',
	`total_exchanged` int(10) unsigned NOT NULL DEFAULT '0',
	`total_completed_orders` int(10) unsigned NOT NULL DEFAULT '0',
	`total_completed_orders_fully` int(10) unsigned NOT NULL DEFAULT '0',
	`total_completed_orders_partially` int(10) unsigned NOT NULL DEFAULT '0',
	`total_completed_orders_not` int(10) unsigned NOT NULL DEFAULT '0',
	`average_completed_orders` decimal(4,2) unsigned NOT NULL DEFAULT '0.00',
	`average_completed_orders_not` decimal(4,2) unsigned NOT NULL DEFAULT '0.00',
	`total_products` int(10) unsigned NOT NULL DEFAULT '0',
	`average_exchange_product` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
	`average_exchange_customer` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
	`total_customer` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
	PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Log Summary Table';

ALTER TABLE `{$this->getTable('promotion_log')}` 
	ADD `full_exchanged` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `{$this->getTable('sales_flat_quote_item')}` 
	ADD `exchange_count` INT(11) UNSIGNED NOT NULL DEFAULT '0';
";

$installer->run($sql);
$installer->endSetup();
