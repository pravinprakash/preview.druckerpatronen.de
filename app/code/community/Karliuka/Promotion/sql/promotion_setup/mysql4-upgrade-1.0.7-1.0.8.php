<?php

$installer = $this;
$installer->startSetup();

$sql = "
ALTER TABLE `{$this->getTable('promotion_rule')}` 
  ADD `price_control` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0';
";

$installer->run($sql);
$installer->endSetup();
