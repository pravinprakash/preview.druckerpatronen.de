<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$sql = "
CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule')}` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From Date',
  `to_date` date DEFAULT NULL COMMENT 'To Date',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_id`),
  KEY `IDX_PRM_RULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Rule';


CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule_customer_group')}` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `IDX_PRM_RULE_CUSTOMER_GROUP_RULE_ID` (`rule_id`),
  KEY `IDX_PRM_RULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Rules To Customer Groups Relations';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule_label')}` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `UNQ_PRM_RULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `IDX_PRM_RULE_LABEL_STORE_ID` (`store_id`),
  KEY `IDX_PRM_RULE_LABEL_RULE_ID` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Label' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule_product')}` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  PRIMARY KEY (`rule_id`,`product_id`),
  KEY `IDX_PRM_RULE_PRODUCT_RULE_ID` (`rule_id`),
  KEY `IDX_PRM_RULE_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Rules To Product Relations';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule_product_attribute')}` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `IDX_PRM_RULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `IDX_PRM_RULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_PRM_RULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Product Attribute';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_rule_website')}` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `IDX_PRM_RULE_WEBSITE_RULE_ID` (`rule_id`),
  KEY `IDX_PRM_RULE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Rules To Websites Relations';

ALTER TABLE {$this->getTable('promotion_rule_customer_group')}
  ADD CONSTRAINT `FK_PRM_RULE_CUSTOMER_GROUP_RULE_ID_PRM_RULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES {$this->getTable('promotion_rule')} (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_RULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `{$this->getTable('customer_group')}` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_rule_label')}`
  ADD CONSTRAINT `FK_PRM_RULE_LABEL_RULE_ID_PRM_RULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES {$this->getTable('promotion_rule')} (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_RULE_LABEL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_rule_product')}`
  ADD CONSTRAINT `FK_PRM_RULE_PRODUCT_RULE_ID_PRM_RULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES {$this->getTable('promotion_rule')} (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_RULE_PRD_PRD_ID_CAT_PRD_ENTT_PRD_ID` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{$this->getTable('promotion_rule_product_attribute')}`
  ADD CONSTRAINT `FK_PRM_RULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `{$this->getTable('eav_attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PRM_RULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `{$this->getTable('customer_group')}` (`customer_group_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PRM_RULE_PRODUCT_ATTRIBUTE_RULE_ID_PRM_RULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES {$this->getTable('promotion_rule')} (`rule_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PRM_RULE_PRD_ATTR_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `{$this->getTable('core_website')}` (`website_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `{$this->getTable('promotion_rule_website')}`
  ADD CONSTRAINT `FK_PRM_RULE_WEBSITE_RULE_ID_PRM_RULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES {$this->getTable('promotion_rule')} (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRM_RULE_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `{$this->getTable('core_website')}` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE;

";
$installer->run($sql);
$installer->endSetup();


$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();

/**
 * Add 'promotion_rule_ids' attribute for entities
 */
$entities = array(
    'quote',
    'quote_address',
    'order'
);
$options = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible' => false,
    'required' => false
);
foreach ($entities as $entity) {
    $installer->addAttribute($entity, 'promotion_rule_ids', $options);
}

$installer->endSetup();
