<?php

$installer = $this;
$installer->startSetup();

$sql = "
ALTER TABLE `{$this->getTable('promotion_rule')}` 
  ADD `actions_serialized` mediumtext COMMENT 'Conditions Serialized';
";

$installer->run($sql);
$installer->endSetup();
