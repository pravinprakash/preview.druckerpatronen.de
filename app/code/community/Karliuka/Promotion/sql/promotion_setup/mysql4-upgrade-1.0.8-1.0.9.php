<?php

$installer = $this;
$installer->startSetup();

$sql = "
DROP TABLE IF EXISTS `{$this->getTable('promotion_log')}`;
DROP TABLE IF EXISTS `{$this->getTable('promotion_log_exchanged')}`;
DROP TABLE IF EXISTS `{$this->getTable('promotion_log_product')}`;
DROP TABLE IF EXISTS `{$this->getTable('promotion_log_summary')}`;

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_log')}` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `exchange_product_id` int(10) unsigned NOT NULL COMMENT 'Promotion Product ID',
  `gift_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Gift Product ID',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Creation Time',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `unique_exchange` (`session_id`,`product_id`,`exchange_product_id`,`rule_id`),
  KEY `created_at` (`created_at`),
  KEY `product_id` (`product_id`),
  KEY `exchange_product_id` (`exchange_product_id`),
  KEY `rule_id` (`rule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Promotion Log Suggested Table';

CREATE TABLE IF NOT EXISTS `{$this->getTable('promotion_log_exchanged')}` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `product_qty` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `full_exchanged` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entity_id`),
  KEY `order_id` (`order_id`),
  KEY `created_at` (`created_at`),
  KEY `full_exchanged` (`full_exchanged`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Promotion Log Exchanged Table';

ALTER TABLE `{$this->getTable('promotion_log_exchanged')}` 
	ADD FOREIGN KEY (`entity_id`) REFERENCES `{$this->getTable('promotion_log')}` (`entity_id`) 
	ON DELETE CASCADE ON UPDATE RESTRICT;

";

$installer->run($sql);
$installer->endSetup();
