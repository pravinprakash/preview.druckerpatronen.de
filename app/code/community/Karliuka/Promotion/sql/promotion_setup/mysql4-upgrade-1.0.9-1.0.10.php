<?php

$installer = $this;
$installer->startSetup();

$sql = "
ALTER TABLE `{$this->getTable('promotion_log_exchanged')}` 
  ADD `session_id` varchar(255) NOT NULL;
  
UPDATE `{$this->getTable('promotion_log_exchanged')}` 
	SET session_id=(SELECT l.session_id FROM `{$this->getTable('promotion_log')}` AS l WHERE entity_id=l.entity_id LIMIT 1);
";

$installer->run($sql);
$installer->endSetup();
