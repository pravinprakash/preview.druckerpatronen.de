<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_CartController extends Mage_Core_Controller_Front_Action
{
	/**
     * Predispatch: should set layout area
     * @return Mage_Core_Controller_Front_Action
    */
    public function preDispatch()
    {
		parent::preDispatch();
    }
	
    /**
     * index action
    */		
    public function indexAction()
    { 
        $this->loadLayout();
        $this->renderLayout();
    }
	
    public function addExtraProductAction()
    {
        $helper = Mage::helper('onestepcheckout/extraproducts');
        $productId = $this->getRequest()->getPost('product_id');
        $remove = $this->getRequest()->getPost('remove', false);
		$result = array('id' => 0, 'remove' => (int)$remove);

        if($helper->isValidExtraProduct($productId)){
            if(!$remove){
                /* Add product to cart if it doesn't exist */
                $product = Mage::getModel('catalog/product')->load($productId);
                $cart = Mage::getSingleton('checkout/cart');
                $cart->addProduct($product);
                $cart->save();
            } else {
                $items = Mage::helper('checkout/cart')->getCart()->getItems();
                foreach($items as $item){
                    if($item->getProduct()->getId() == $productId) {
                        Mage::helper('checkout/cart')->getCart()->removeItem($item->getId())->save();
                    }
                }
            }
        }
		$this->getResponse()->setBody(json_encode($result));
    }
	
    /**
     * hasOriginal Action
    */		
	public function hasOriginalAction()
	{
		$this->loadLayout();
        $this->renderLayout();
	}
	
    /**
     * setGift Action
    */	
    public function setGiftAction()
    {
		$id = $this->getRequest()->getPost('id');
		$code = $this->getRequest()->getPost('code');
		$productId = $this->getRequest()->getPost('product');
		
		$collections = $this->_getPromoCollection();
		
		$cart = Mage::getModel('checkout/cart');
		$quote = $cart->getQuote();
				
		$error = true;
		//$cartProductIds = array($productId);
		foreach($collections as $rule)
		{
			if ($code != $rule->getId()) continue;
			$product = Mage::getModel('catalog/product')->load($productId);
			$exchange = $this->_getExchangeProduct($rule, $product);

			if(!$product->getId() || !$exchange->getId()) continue;
			
			$gift = $rule->getProductIds();
			$giftId = isset($gift[0]) ? $gift[0] : 0;
			$gift = Mage::getModel('catalog/product')->load($giftId);

			try {
				$logId = null;
				$qty = 1;

				foreach($quote->getAllVisibleItems() as $item){
					$productId = $item->getProductId();
					if($product->getId() == $productId){
						$qty = floor($item->getQty()/$rule->getConditionQty());
						$logId = $item->getLogId();
						break;
					}
				}

				$new = $quote->addProduct(
					Mage::getModel('catalog/product')->load($exchange->getId()), 
					$this->_getProductRequest($qty)
				);
				
				$new->setRuleId($rule->getId());
				
				if($logId){
					$logExchange = Mage::getModel('promotion/log_exchange');
					$log = Mage::getModel('promotion/log')->load($logId);
					$logExchange->setEntityId($logId);
					$logExchange->setProductQty($qty);
					$logExchange->setSessionId($log->getSessionId());
					$logExchange->save();
					$new->setLogId($logId);
				}
				
				if($gift->getId()){
				
					foreach($quote->getAllVisibleItems() as $item){
						if($gift->getId() == $item->getProductId()){
							$item->getProduct()->setIsSuperMode(true);
							break;
						}
					}				

					$gift->addCustomOption('gift_product', '1');
					$giftItem = $quote->addProduct($gift, $this->_getProductRequest($rule->getQty()*$qty));
					$giftItem->setCustomPrice(0);
					$giftItem->setOriginalCustomPrice(0);
					$giftItem->setRuleId($rule->getId());
					$giftItem->setIsGift(1);
					
					$giftQty = (int)$giftItem->getGiftQty() + (int)$rule->getQty()*$qty;
					
					//$giftItem->setGiftQty($giftQty);
					$giftItem->getProduct()->setIsSuperMode(true);

					$giftLockedQty = (int)$item->getGiftLocked() + $qty;
					$new->setGiftLocked($giftLockedQty);
					$new->setGiftQty($giftQty);
					$new->setGiftProductId($gift->getId());	
				}
				
				foreach($quote->getAllVisibleItems() as $item){
					$productId = $item->getProductId();
					if($product->getId() == $productId){
						$new->setExchangeCount($item->getExchangeCount());
						$new->setCategoryId($item->getCategoryId());
						$item->isDeleted(true);
						continue;
					}
				}

				$cart->save();				
				$error = false;
				Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			} 
			catch (Exception $e) {
				print_r($e->getMessage());exit;
				Mage::getSingleton('checkout/session')->addError($e->getMessage());
				Mage::getSingleton('checkout/session')->addError('Cannot add item');
			}
		}
		$result = array('id' => $id, 'hasError' => (int)$error, 'product' => (string)$product->getSku(), 'exchange' => (string)$exchange->getSku());
		$this->getResponse()->setBody(json_encode($result));
    }
	
    /**
     * Retrieve array Promotion Collections
     * @param bool $checkQty 
     * @return array
    */		
	protected function _getPromoCollection()
	{
		$quote = Mage::getModel('checkout/cart')->getQuote();
		$address = null;
		foreach($quote->getAllAddresses() as $addr){
			if($addr) {
				$address = $addr;
				break;
			}
		}
		$store = Mage::app()->getStore($quote->getStoreId());
        $validator = Mage::getModel('promotion/rule_validator')
            ->init($store->getWebsiteId(), $quote->getCustomerGroupId());
        
        $validator->process($address);
		$rules = $validator->getExchange();
		return $rules;
	}
	
	public function _getExchangeProduct($rule, $product)
	{
		$exchange = array();
		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection->addAttributeToFilter('bechlem_id', $product->getBechlemId());
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
		foreach ($collection as $_product){
			$res = (int)$rule->getActions()->validate($_product);
			if($res == 1) $exchange[] = $_product;
		}
		if(!isset($exchange[0])) $exchange[0] = null;
		return $exchange[0];
	}
	
	/**
     * Get request for product add to cart procedure
     * @see Mage_Checkout_Model_Cart::_getProductRequest()
     * @param mixed $requestInfo
     * @return Varien_Object
    */
    protected function _getProductRequest($info)
    {
        if ($info instanceof Varien_Object) {
            $request = $info;
        } else if (is_numeric($info)) {
            $request = new Varien_Object(array(
                'qty' => $info 
            ));
        } else $request = new Varien_Object($info);
        
        if (!$request->hasQty()) {
            $request->setQty(1);
        }
        return $request;
    }
}