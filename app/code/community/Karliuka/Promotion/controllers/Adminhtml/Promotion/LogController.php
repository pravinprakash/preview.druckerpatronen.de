<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Adminhtml_Promotion_LogController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Init Action
	*/
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('promotion/log')->_addBreadcrumb(
						Mage::helper('adminhtml')->__('Promotion Log Manager'),
						Mage::helper('adminhtml')->__('Promotion Log Manager')
		);
		return $this;
	}

	/**
	 * Index Action
	*/	
	public function indexAction() 
	{
		$this->_title($this->__('Promotion Log Manager'));
		$this->_initAction();
		$this->renderLayout();
	}

	/**
	 * Remove Action
	*/	
	public function deleteAction()
	{
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('promotion/log');
				$model->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} 
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Mass Remove Action
	*/		
	public function massRemoveAction()
	{
		try {
			$ids = $this->getRequest()->getPost('entity_ids', array());
			foreach ($ids as $id) {
				  $model = Mage::getModel('promotion/log');
				  $model->setId($id)->delete();
			}
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
		}
		catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}
		$this->_redirect('*/*/');
	}
			
	/**
	 * Export order grid to CSV format
	*/
	public function exportCsvAction()
	{
		$file = 'promotion-log.csv';
		$grid = $this->getLayout()->createBlock('promotion/adminhtml_log_grid');
		$this->_prepareDownloadResponse($file, $grid->getCsvFile());
	} 
	
	/**
	 *  Export order grid to Excel XML format
	*/
	public function exportExcelAction()
	{
		$file = 'promotion-log.xml';
		$grid = $this->getLayout()->createBlock('promotion/adminhtml_log_grid');
		$this->_prepareDownloadResponse($file, $grid->getExcelFile($file));
	}
}