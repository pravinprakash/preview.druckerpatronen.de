<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Adminhtml_Promotion_RuleController 
	extends Mage_Adminhtml_Controller_Action
{
    protected function _initRule()
    {
        $this->_title(Mage::helper('salesrule')->__('Promotions'))
            ->_title(Mage::helper('promotion')->__('Shopping Cart Promotion Rules'));
        
        Mage::register('promotion_current_rule', Mage::getModel('promotion/rule'));
        $id = (int)$this->getRequest()->getParam('id');
        
        if (!$id && $this->getRequest()->getParam('rule_id')) {
            $id = (int)$this->getRequest()->getParam('rule_id');
        }
        
        if ($id) {
            Mage::registry('promotion_current_rule')->load($id);
        }
    }
    
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('promotion/rule')->_addBreadcrumb(
			Mage::helper('adminhtml')->__('Shopping Cart Promotion Rules'),
			Mage::helper('adminhtml')->__('Shopping Cart Promotion Rules')
		);
		return $this;
    }
    
    public function indexAction()
    {
        $this->_title(Mage::helper('promotion')->__('Shopping Cart Promotion Rules'));
        $this->_initAction();
		$this->renderLayout();
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('promotion/rule');
        
        if ($id) {
            $model->load($id);
            if (!$model->getRuleId()) {
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('salesrule')
                            ->__('This rule no longer exists.'));
                $this->_redirect('*/*');
                return;
            }
        }
        
        $this
            ->_title($model->getRuleId() ? $model->getName()
                    : Mage::helper('salesrule')->__('New Rule'));
        
        // set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        
        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');
        $model->getActions()->setJsFormObject('rule_actions_fieldset');
        
        Mage::register('promotion_current_rule', $model);
        
        $this->_initAction()->getLayout()->getBlock('rule_edit')
            ->setData('action', $this->getUrl('*/*/save'));
        
        $this->_addBreadcrumb($id ? Mage::helper('salesrule')->__('Edit Rule')
                    : Mage::helper('salesrule')->__('New Rule'),
                $id ? Mage::helper('salesrule')->__('Edit Rule')
                    : Mage::helper('salesrule')->__('New Rule'))->renderLayout();
        
    }
    
    /**
     * Promotion rule save action
     *
     */
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                /** @var $model Mage_Promotion_Model_Rule */
                $model = Mage::getModel('promotion/rule');
                Mage::dispatchEvent('adminhtml_controller_promotion_prepare_save',
                    array(
                        'request' => $this->getRequest()
                    ));
                $data = $this->getRequest()->getPost('rule');
                $data = $this
                    ->_filterDates($data, array(
                        'from_date',
                        'to_date'
                    ));
                
                if (isset($data['id'])) {
                    $model->load($data['id']);
                    if ($data['id'] != $model->getId()) {
                        Mage::throwException(Mage::helper('salesrule')
                                ->__('Wrong rule specified.'));
                    }
                }
                
                $session = Mage::getSingleton('adminhtml/session');
                
                $validateResult = $model
                    ->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach ($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                    $session->setPageData($data);
                    $this->_redirect('*/*/edit', array(
                            'id' => $model->getId()
                        ));
                    return;
                }
                
                if (isset($data['products'])) {
                    $data['product_ids'] = Mage::helper('adminhtml/js')
                        ->decodeGridSerializedInput($data['products']);
                    unset($data['products']);
                } else if ($model->getId()) {
                    $data['product_ids'] = $model->getProductIds();
                }
                
                $model->loadPost($data);
                
                $session->setPageData($model->getData());
                
                $model->save();
                $session
                    ->addSuccess(Mage::helper('salesrule')
                            ->__('The rule has been saved.'));
                $session->setPageData(false);
				
                if ($this->getRequest()->getParam('back')) {
					$tabId = $this->getRequest()->getParam('tab', null);
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'active_tab' => $tabId));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $id = (int) $this->getRequest()->getParam('rule_id');
                if (!empty($id)) {
                    $this->_redirect('*/*/edit', array(
                            'id' => $id
                        ));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;
                
            } catch (Exception $e) {
                $this->_getSession()
                    ->addError(Mage::helper('salesrule')
                            ->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this
                    ->_redirect('*/*/edit',
                        array(
                            'id' => $this->getRequest()->getParam('rule_id')
                        ));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('promotion/rule');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('salesrule')
                            ->__('The rule has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()
                    ->addError(Mage::helper('salesrule')
                            ->__('An error occurred while deleting the rule. Please review the log and try again.'));
                Mage::logException($e);
                $this
                    ->_redirect('*/*/edit',
                        array(
                            'id' => $this->getRequest()->getParam('id')
                        ));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')
            ->addError(Mage::helper('salesrule')
                    ->__('Unable to find a rule to delete.'));
        $this->_redirect('*/*/');
    }
    
    public function newRuleHtmlAction()
    {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|',
            str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];
        
		$model = Mage::getModel($type)->setId($id)->setType($type)
            ->setRule(Mage::getModel('promotion/rule'))->setPrefix('conditions');
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }
        
        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }
	
    public function newActionHtmlAction()
    {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|',
            str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];
        
        $model = Mage::getModel($type)->setId($id)->setType($type)
            ->setRule(Mage::getModel('promotion/rule'))->setPrefix('actions');
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }
        
        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }   
	
    public function applyRulesAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }
    
    public function productsAction()
    {
        $products = $this->getRequest()->getPost('products', null);
        
        $this->_initRule();
        $this->loadLayout()->getLayout()
            ->getBlock('rule_edit_tab_products_grid')
            ->setProducts($products);
        $this->renderLayout();
    }
    
    public function productsGridAction()
    {
        $products = $this->getRequest()->getPost('products', null);
        
        $this->_initRule();
        $this->loadLayout()->getLayout()
            ->getBlock('rule_edit_tab_products_grid')
            ->setProducts($products);
        $this->renderLayout();
    }
    
    /**
     * Chooser source action
    */
    public function chooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $chooserBlock = $this->getLayout()
            ->createBlock('adminhtml/promo_widget_chooser',
                '',
                array(
                    'id' => $uniqId
                ));
        $this->getResponse()->setBody($chooserBlock->toHtml());
    }
    
    /**
     * Returns result of current user permission check on resource and privilege
     * @return boolean
    */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promotion/rule');
    }
}
