<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Adminhtml_Promotion_SummaryController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Init Action
	*/
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('promotion/log')->_addBreadcrumb(
						Mage::helper('adminhtml')->__('Promotion Log Manager'),
						Mage::helper('adminhtml')->__('Promotion Log Manager')
		);
		return $this;
	}

	/**
	 * Index Action
	*/	
	public function indexAction() 
	{
		$this->_title($this->__('Promotion Log Summary Manager'));
		$this->_initAction();
		$this->renderLayout();
	}
			
	/**
	 * Export order grid to CSV format
	*/
	public function exportCsvAction()
	{
		$file = 'promotion-log.csv';
		$grid = $this->getLayout()->createBlock('promotion/adminhtml_log_summary_grid');
		$this->_prepareDownloadResponse($file, $grid->getCsvFile());
	} 
	
	/**
	 *  Export order grid to Excel XML format
	*/
	public function exportExcelAction()
	{
		$file = 'promotion-log.xml';
		$grid = $this->getLayout()->createBlock('promotion/adminhtml_log_summary_grid');
		$this->_prepareDownloadResponse($file, $grid->getExcelFile($file));
	}
}