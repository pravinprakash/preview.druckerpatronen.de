<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Rule_Condition_Product_Found 
	extends Karliuka_Promotion_Model_Rule_Condition_Product_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('promotion/rule_condition_product_found');
    }

    /**
     * Load value options
     * @return Mage_SalesRule_Model_Rule_Condition_Product_Found
    */
    public function loadValueOptions()
    {
        $this->setValueOption(array(
            1 => Mage::helper('salesrule')->__('FOUND'),
            0 => Mage::helper('salesrule')->__('NOT FOUND')
        ));
        return $this;
    }

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml() 
            . Mage::helper('salesrule')->__("If an item is %s in the cart with %s of these conditions true:", 
                $this->getValueElement()->getHtml(), 
                $this->getAggregatorElement()->getHtml()
            );
        
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        
        return $html;
    }

    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if ($object instanceof Mage_Catalog_Model_Product) return parent::validate($object);
		
		$all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $found = false;
        
        foreach ($object->getAllItems() as $item) {
            $found = $all;
            foreach ($this->getConditions() as $cond) {
                $validated = $cond->validate($item);
                if (($all && !$validated) || (!$all && $validated)) {
                    $found = $validated;
                    break;
                }
            }
            if (($found && $true) || (!$true && $found)) {
                break;
            }
        }
        
        if ($found && $true) {
            // found an item and we're looking for existing one
            return true;
        } else if (!$found && !$true) {
            // not found and we're making sure it doesn't exist
            return true;
        }
        return false;
    }
}
