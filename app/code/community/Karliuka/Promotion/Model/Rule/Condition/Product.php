<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Rule_Condition_Product 
	extends Mage_Rule_Model_Condition_Product_Abstract
{
    /**
     * Add special attributes
     * @param array $attributes
    */
    protected function _addSpecialAttributes(array &$attributes)
    {
        parent::_addSpecialAttributes($attributes);
        $attributes['quote_item_qty'] = Mage::helper('salesrule')->__('Quantity in cart');
        $attributes['quote_item_price'] = Mage::helper('salesrule')->__('Price in cart');
        $attributes['quote_item_row_total'] = Mage::helper('salesrule')->__('Row total in cart');
    }
    
    /**
     * Validate Product Rule Condition
     * @param Varien_Object $object
     * @return bool
    */
    public function validate(Varien_Object $object)
    {
        $product = false;
        
        if ($object->getProduct() instanceof Mage_Catalog_Model_Product &&
             $object->getData($this->getAttribute()) !== null) {
            $product = $object->getProduct();
        } elseif ($object instanceof Mage_Catalog_Model_Product){
			$product = $object;
		} else {
            $product = Mage::getModel('catalog/product')
                ->load($object->getProductId());
        }
        
        $product->setQuoteItemQty($object->getQty())
            ->setQuoteItemPrice($object->getPrice())
            ->setQuoteItemRowTotal($object->getBaseRowTotal());
        
        return parent::validate($product);
    }
	
    /**
     * Case and type insensitive comparison of values
     * @param  string|int|float $validatedValue
     * @param  string|int|float $value
     * @return bool
    */
    protected function _compareValues($validatedValue, $value, $strict = true)
    {
        if ($strict && is_numeric($validatedValue) && is_numeric($value)) {
            return $validatedValue == $value;
        } else {
			return (bool)preg_match('~' . preg_quote($value, '~') . '~iu', $validatedValue);
        }
    }
	
    /**
     * Collect validated attributes
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $productCollection
     * @return Mage_CatalogRule_Model_Rule_Condition_Product
    */
    public function collectValidatedAttributes($productCollection)
    {
        $attribute = $this->getAttribute();
        if ('category_ids' != $attribute) {
			//echo $attribute;
            if ($this->getAttributeObject()->isScopeGlobal()) {
				//echo $attribute;
                $attributes = $this->getRule()->getCollectedAttributes();
                $attributes[$attribute] = true;
                $this->getRule()->setCollectedAttributes($attributes);
                $productCollection->addAttributeToSelect($attribute, 'left');
            } else {
                $this->_entityAttributeValues = $productCollection->getAllAttributeValues($attribute);
            }
        }

        return $this;
    }	
}