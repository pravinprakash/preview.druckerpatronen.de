<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Log_Summary
	extends Mage_Core_Model_Abstract
{
    /**
     * Prefix of model events names
     * @var string
    */
    protected $_eventPrefix = 'promotion_log_summary';

    /**
     * Parameter name in event
     * In observe method you can use $observer->getEvent()->getObject() in this case
     * @var string
    */
    protected $_eventObject = 'promotion_log_summary';

    /**
     * Construct
    */	
    protected function _construct()
	{
		$this->_init("promotion/log_summary");
    }
	
    /**
     * Standard model initialization
     * @param string $resourceModel
     * @param string $idFieldName
     * @return Mage_Core_Model_Abstract
    */
    protected function _init($resourceModel)
    {
		$this->_setResourceModel($resourceModel);
    }	
    /**
     * Update order info
     * @param int $ruleId
     * @return Karliuka_Content_Model_Resource_Post
    */	
	public function updateOrderInfo($ruleId)
	{
		return $this;
	}	
}