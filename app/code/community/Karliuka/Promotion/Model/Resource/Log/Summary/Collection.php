<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Resource_Log_Summary_Collection 
	extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Collection constructor
     * @param Mage_Core_Model_Resource_Db_Abstract $resource
    */
	public function _construct(){
		$this->_init('promotion/rule');
	}
	
    /**
     * Init collection select
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
    */
    protected function _initSelect()
    {
        parent::_initSelect();
		$select = $this->getSelect();
		
		$select->joinLeft(
			array('l' => $this->getTable('promotion/log')), 
			'(main_table.rule_id=l.rule_id)', 
			array(
				'total_suggested' => new Zend_Db_Expr('COUNT(l.entity_id)'),
				'average_exchange_customer' => new Zend_Db_Expr('IF(COUNT(DISTINCT l.session_id) > 0, COUNT(l.entity_id)/COUNT(DISTINCT l.session_id), 0)')
			)
		);
		
		$select->joinLeft(
			array('e' => $this->getTable('promotion/log_exchanged')), 
			'(e.entity_id=l.entity_id)', 
			array(
				'total_exchanged' => new Zend_Db_Expr('COUNT(e.entity_id)'),
				'total_products' => new Zend_Db_Expr('SUM(e.product_qty)'),
				'average_exchange_product' => new Zend_Db_Expr('IF(COUNT(tco.entity_id) > 0, SUM(tco.product_qty)/COUNT(tco.entity_id), 0)'),
			)
		);		
		
		$select->joinLeft(
			array('tco' => $this->getTable('promotion/log_exchanged')), 
			'(tco.entity_id=l.entity_id AND tco.order_id > 0)', 
			array(
				'total_completed_orders' => new Zend_Db_Expr('COUNT(tco.entity_id)'),
				'average_completed_orders' => new Zend_Db_Expr('IF(COUNT(e.entity_id) > 0, (COUNT(tco.entity_id)*100)/COUNT(e.entity_id), 0)')
			)
		);	
		
		$select->joinLeft(
			array('tcon' => $this->getTable('promotion/log_exchanged')), 
			'(tcon.entity_id=l.entity_id AND tcon.order_id = 0)', 
			array(
				'total_completed_orders_not' => new Zend_Db_Expr('COUNT(tcon.entity_id)'),
				'average_completed_orders_not' => new Zend_Db_Expr('IF(COUNT(e.entity_id) > 0, (COUNT(tcon.entity_id)*100)/COUNT(e.entity_id), 0)')
			)
		);	
		
		$select->joinLeft(
			array('tcof' => $this->getTable('promotion/log_exchanged')), 
			'(tcof.entity_id=l.entity_id AND tcof.order_id > 0 AND tcof.full_exchanged = 1)', 
			array('total_completed_orders_fully' => new Zend_Db_Expr('COUNT(tcof.entity_id)'))
		);	
		
		$select->joinLeft(
			array('tcop' => $this->getTable('promotion/log_exchanged')), 
			'(tcop.entity_id=l.entity_id AND tcop.order_id > 0 AND tcop.full_exchanged = 0)', 
			array('total_completed_orders_partially' => new Zend_Db_Expr('COUNT(tcop.entity_id)'))
		);	
		
		$select->group(array('main_table.rule_id'));
		//Mage::Log((string)$select, null, 'select.log');
		//echo $select;exit;
        return $this;
    }	
}