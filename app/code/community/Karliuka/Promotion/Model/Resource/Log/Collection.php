<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Resource_Log_Collection 
	extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Collection constructor
     * @param Mage_Core_Model_Resource_Db_Abstract $resource
    */	
	public function _construct()
	{
		$this->_init('promotion/log');
	}
	
    /**
     * Init collection select
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
    */
    protected function _initSelect()
    {
        parent::_initSelect();
		$select = $this->getSelect();
		
		$select->joinLeft(
			array('e' => $this->getTable('promotion/log_exchanged')), 
			'(e.entity_id=main_table.entity_id)', 
			array('product_qty', 'order_id', 'full_exchanged')
		);	
		
		$this->getSelect()
			->joinLeft(array('order' => $this->getTable('sales/order')), 
						'(order.entity_id = e.order_id)', 
						array('order_status' => 'order.status')
		);	
		//echo $select;exit;
        return $this;
    }		
}