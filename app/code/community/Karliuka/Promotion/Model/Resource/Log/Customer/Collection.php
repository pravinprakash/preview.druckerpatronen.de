<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Resource_Log_Customer_Collection 
	extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Collection constructor
     * @param Mage_Core_Model_Resource_Db_Abstract $resource
    */
	public function _construct(){
		$this->_init('promotion/rule');
	}
	
    /**
     * Init collection select
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
    */
    protected function _initSelect()
    {
        parent::_initSelect();
		$select = $this->getSelect();
		
		$select->joinLeft(
			array('l' => $this->getTable('promotion/log')), 
			'(main_table.rule_id=l.rule_id)', 
			array(
				'total_suggested' => new Zend_Db_Expr('COUNT(DISTINCT l.session_id)'),
			)
		);
		
		$select->joinLeft(
			array('e' => $this->getTable('promotion/log_exchanged')), 
			'(e.entity_id=l.entity_id)', 
			array(
				'total_exchanged' => new Zend_Db_Expr('COUNT(DISTINCT e.session_id)'),
			)
		);

		$select->joinLeft(
			array('tco' => $this->getTable('promotion/log_exchanged')), 
			'(tco.entity_id=l.entity_id AND tco.order_id > 0)', 
			array(
				'total_completed_orders' => new Zend_Db_Expr('COUNT(DISTINCT tco.session_id)'),
				'average_exchange_customer' => new Zend_Db_Expr('IF(COUNT(DISTINCT l.session_id) > 0, (COUNT(DISTINCT tco.session_id)*100)/COUNT(DISTINCT l.session_id), 0)'),
			)
		);

		$select->joinLeft(
			array('tcop' => $this->getTable('promotion/log_exchanged')), 
			'(tcop.entity_id=l.entity_id AND tcop.order_id > 0 AND tcop.full_exchanged = 0)', 
			array(
				'total_completed_orders_partially' => new Zend_Db_Expr('COUNT(DISTINCT tcop.session_id)'),
				'total_completed_orders_fully' => new Zend_Db_Expr('(COUNT(DISTINCT tco.session_id) - COUNT(DISTINCT tcop.session_id))'),
			)
		);

		$select->joinLeft(
			array('o' => $this->getTable('promotion/log_order')), 
			'(e.order_id=o.order_id AND l.exchange_product_id=o.product_id)', 
			array(
				'sum_under' => new Zend_Db_Expr('SUM(o.price*e.product_qty)'),
			)
		);
		
		$select->group(array('main_table.rule_id'));
		//echo $select;exit;
        return $this;
    }	
}