<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Observer
{
    /**
     * Called after message is added to abstract session
     * @param unknown_type $observer
     * @return Karliuka_Promotion_Model_Observer
    */
    public function onAbstractSessionMessageAdded($observer) 
    {
        $session = Mage::getSingleton('checkout/session');
        $this->_reorderSuccessMessages($session);
        
        return $this;
    }
    
    /**
     * Called after quote address totals are collected
     * @param unknown_type $observer
     * @return Karliuka_Promotion_Model_Observer
    */
    public function onQuoteAddressTotalsCollected($observer)
    {
        $address = $observer->getQuoteAddress();
        $this->_processPromotionRules($address);
        
        return $this;
    }
    
    /**
     * Process the gift rules
     * @param Mage_Sales_Model_Quote_Address $address
    */
    protected function _processPromotionRules($address)
    {
        $quote = $address->getQuote();
        $store = Mage::app()->getStore($quote->getStoreId());
        $validator = Mage::getSingleton('promotion/rule_validator')
            ->init($store->getWebsiteId(), $quote->getCustomerGroupId());
        
        $validator->process($address);
    }
    
    /**
     * Reorder success messages
     * @param Mage_Checkout_Model_Session $session
    */
    public function _reorderSuccessMessages($session)
    {
        $messages = Mage::registry('promotion_added_success_messages');
        if (!$messages || !($session instanceof Mage_Checkout_Model_Session)) {
            return;
        }
        foreach ($messages as $message) {
            /* @var $message Mage_Core_Model_Message_Abstract */
            $session->getMessages()->deleteMessageByIdentifier($message->getIdentifier());
            $session->getMessages()->add($message);
        }
    }
	
	/**
	 * sales order save
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function orderSave($observer)
    {	//return $this;
		$session = Mage::getSingleton('checkout/session');
		$order = $observer->getEvent()->getOrder();
		$full = true;
		$logs = array();
        foreach ($session->getQuote()->getAllItems() as $item) {
			if($item->getExchangeCount() > 0 && !$item->getLogId()) $full = false;
            if($item->getLogId() < 1) continue;
			$customerId = $order->getCustomerId();
			$logExchange = Mage::getModel('promotion/log_exchange')->load($item->getLogId());
			
			if(!$logExchange->getId()) continue;
			
			$logExchange->setOrderId($order->getId());
			$logExchange->setCustomerId($customerId);
			$logExchange->setOrderUpdate(true);
			$logs[] = $logExchange;
        }
		foreach ($logs as $logExchange) {
			if($full) $logExchange->setFullExchanged(1);
			$logExchange->save();
		}
		return $this;
    }
	
	/**
	 * sales Quote item Set Qty
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function itemSetQty($observer)
    {	
		$current = $observer->getEvent()->getItem();
		if(!$current->getIsGift())
		{	
			if($current->getRuleId() && $current->getQty() < $current->getGiftLocked()){
				$count = $current->getGiftLocked()/$current->getGiftQty();
				$giftQty = floor($current->getQty()/$count);
				$ruleId = $current->getRuleId();
				$giftProductId = $current->getGiftProductId();
				try {
					$cart = Mage::getModel('checkout/cart');
					$quote = $cart->getQuote();
					foreach ($quote->getAllItems() as $item){
						// if item is gift
						if($ruleId == $item->getRuleId() && $item->getIsGift() && 
							$giftProductId == $item->getProduct()->getId()){
							$item->setData('qty', $giftQty);
						// if item is current item
						} elseif($ruleId == $item->getRuleId() && !$item->getIsGift() &&
								$current->getProduct()->getId() == $item->getProduct()->getId()){
							$count = $item->getGiftLocked()/$item->getGiftQty();
							$item->setGiftLocked($giftQty * $count);
							$item->setGiftQty($giftQty);					
						}
					}
					$cart->save();
				} catch (Exception $e) {
					Mage::Log($e->getMessage());
				}
			}
		}// else $current->setData('qty', $current->getGiftQty());
		return $this;
    }
		
	/**
	 * sales Quote remove Item
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function removeItem($observer)
    {
		$current = $observer->getEvent()->getQuoteItem();
		if($current->getRuleId() && !$current->getIsGift()){
			try {
				$cart = Mage::getModel('checkout/cart');
				$quote = $cart->getQuote();
				foreach ($quote->getAllVisibleItems() as $item){
					// if item is gift
					if($current->getRuleId() == $item->getRuleId() && $item->getIsGift() && 
						$current->getGiftProductId() == $item->getProduct()->getId()){
						$item->getProduct()->setIsSuperMode(false);
						$item->isDeleted(true);
					}
				}
				$cart->save();
			} catch (Exception $e) {
				Mage::Log($e->getMessage());
			}	
		}
		return $this;
    }
}
