<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Model_Rule 
	extends Mage_Rule_Model_Abstract
{
    /**
     * Prefix of model events names
     * @var string
    */
    protected $_eventPrefix = 'promotion_rule';
    
    /**
     * Parameter name in event
     * In observe method you can use $observer->getEvent()->getRule() in this case
     * @var string
    */
    protected $_eventObject = 'rule';
    
    /**
     * Store already validated addresses and validation results
     * @var array
    */
    protected $_validatedAddresses = array();
	
    /**
     * Limitation for products collection
     * @var int|array|null
    */
	
    protected $_productsFilter = null;   
    /**
     * Set resource model and Id field name
    */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('promotion/rule');
        $this->setIdFieldName('rule_id');
    }
	
    /**
     * 
     * @param int $storeId
     * @return Varien_Db_Select
    */
    public function getActionsProductIds($storeId)
    {
		/** @var $resource Mage_Rule_Model_Resource_Abstract */
        $resource = $this->getResource();
        return $resource->getProductCollection($storeId, $this->getActions());
    }
	
    public function getItemProductIds($storeId)
    {
		/** @var $resource Mage_Rule_Model_Resource_Abstract */
        $resource = $this->getResource();
        return $resource->getProductCollection($storeId, $this->getConditions());
    }
    
    /**
     * Set coupon code and uses per coupon
     * @return Mage_SalesRule_Model_Rule
    */
    protected function _afterLoad()
    {
        return parent::_afterLoad();
    }
    
    /**
     * Save/delete coupon
     * @return Mage_SalesRule_Model_Rule
    */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }
    
    /**
     * Initialize rule model data from array.
     * Set store labels if applicable.
     * @param array $data
     * @return Mage_SalesRule_Model_Rule
    */
    public function loadPost(array $data)
    {
        parent::loadPost($data);
        if (isset($data['store_labels'])) {
            $this->setStoreLabels($data['store_labels']);
        }
        return $this;
    }
    
    /**
     * Get rule condition combine model instance
     * @return Mage_SalesRule_Model_Rule_Condition_Combine
    */
    public function getConditionsInstance()
    {
        return Mage::getModel('promotion/rule_condition_combine');
    }
    
    /**
     * Get rule condition product combine model instance
     * @return Mage_SalesRule_Model_Rule_Condition_Product_Combine
    */
    public function getActionsInstance()
    {
        return Mage::getModel('promotion/rule_condition_product_combine');
    }
    
    /**
     * Get promotion rule customer group Ids
     * @return array
    */
    public function getCustomerGroupIds()
    {
        if (!$this->hasCustomerGroupIds()) {
            $customerGroupIds = $this->_getResource()
                ->getCustomerGroupIds($this->getId());
            $this->setData('customer_group_ids', (array) $customerGroupIds);
        }
        return $this->_getData('customer_group_ids');
    }
    
    /**
     * Get promotion rule products group Ids
     * @return array
    */
    public function getProductIds()
    {
        if (!$this->hasProductIds()) {
            $productIds = $this->_getResource()
                ->getProductIds($this->getId());
            $this->setData('product_ids', (array) $productIds);
        }
        return $this->_getData('product_ids');
    }
	
    /**
     * Get Rule label by specified store
     * @param Mage_Core_Model_Store|int|bool|null $store
     * @return string|bool
    */
    public function getStoreLabel($store=null)
    {
        $storeId = Mage::app()->getStore($store)
            ->getId();
        $labels = (array) $this->getStoreLabels();
        
        if (isset($labels[$storeId])) {
            return $labels[$storeId];
        } elseif (isset($labels[0]) && $labels[0]) {
            return $labels[0];
        }
        return false;
    }
    
    /**
     * Set if not yet and retrieve rule store labels
     * @return array
    */
    public function getStoreLabels()
    {
        if (!$this->hasStoreLabels()) {
            $labels = $this->_getResource()
                ->getStoreLabels($this->getId());
            $this->setStoreLabels($labels);
        }
        return $this->_getData('store_labels');
    }
    
    /**
     * Check cached validation result for specific address
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  bool
    */
    public function hasIsValidForAddress($address)
    {
        $addressId = $this->_getAddressId($address);
        return isset($this->_validatedAddresses[$addressId]) ? true : false;
    }
    
    /**
     * Set validation result for specific address to results cache
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   bool $validationResult
     * @return  Mage_SalesRule_Model_Rule
    */
    public function setIsValidForAddress($address, $validationResult)
    {
        $addressId = $this->_getAddressId($address);
        $this->_validatedAddresses[$addressId] = $validationResult;
        return $this;
    }
    
    /**
     * Get cached validation result for specific address
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  bool
    */
    public function getIsValidForAddress($address)
    {
        $addressId = $this->_getAddressId($address);
        return isset($this->_validatedAddresses[$addressId])
            ? $this->_validatedAddresses[$addressId] : false;
    }
    
    /**
     * Return id for address
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  string
    */
    private function _getAddressId($address)
    {
        if ($address instanceof Mage_Sales_Model_Quote_Address) {
            return $address->getId();
        }
        return $address;
    }
	
	public function getMatchingProductIds($productIds=array())
    {	//Mage::Log($productIds, null, 'has.log');	
        if (is_null($this->_productIds)) {
            $this->_productIds = array();
            $this->setCollectedAttributes(array());

            if ($this->getWebsiteIds()) {
                /** @var $productCollection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
                $productCollection = Mage::getResourceModel('catalog/product_collection');
                $productCollection->addWebsiteFilter($this->getWebsiteIds());
                if ($this->_productsFilter) {
                    $productCollection->addIdFilter($this->_productsFilter);
                }
				$productCollection->addAttributeToSelect('*');
				$productCollection->addFieldToFilter('entity_id', array('in' => $productIds));
                $this->getConditions()->collectValidatedAttributes($productCollection);

				$catalogPriceRule = Mage::getModel('catalogrule/rule');
				//Mage::Log((string)$productCollection->getSelect(), null, 'has.log');	
				
                Mage::getSingleton('core/resource_iterator')->walk(
                    $productCollection->getSelect(),
                    array(array($this, 'callbackValidateProduct')),
                    array(
                        'attributes' => $this->getCollectedAttributes(),
                        'product'    => Mage::getModel('catalog/product'),
                    )
                );
            }
        }
		
        return $this->_productIds;
    }
	
    /**
     * Callback function for product matching
     *
     * @param $args
     * @return void
     */
    public function callbackValidateProduct($args)
    {
        $product = clone $args['product'];
        $product->setData($args['row']);
		if($this->getConditions()->validate($product) == 1){
			$this->_productIds[$product->getId()] = true;
		}
		return;
    }
	
    /**
     * Store matched product Ids
     *
     * @var array
     */
    protected $_productIds;
	
    /**
     * Prepare website to default assigned store map
     *
     * @return array
     */
    protected function _getWebsitesMap()
    {
        $map = array();
        foreach (Mage::app()->getWebsites(true) as $website) {
            if ($website->getDefaultStore()) {
                $map[$website->getId()] = $website->getDefaultStore()->getId();
            }
        }
        return $map;
    }
	
    /**
     * Filtering products that must be checked for matching with rule
     * @param  int|array $productIds
    */
    public function setProductsFilter($productIds)
    {
        $this->_productsFilter = $productIds;
    }

    /**
     * Returns products filter
     * @return array|int|null
    */
    public function getProductsFilter()
    {
        return $this->_productsFilter;
    }	
}
