<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Block_Adminhtml_Rule_Edit_Tabs 
	extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('promotion_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('promotion')->__('Shopping Cart Promotion Rule'));
    }
    
    protected function _prepareLayout()
    {
        $this->addTab('main_section',
            array(
                'label' => Mage::helper('salesrule')->__('General Information'),
                'title' => Mage::helper('salesrule')->__('General Information'),
                'content' => $this->_translateHtml($this->getLayout()
                        ->createBlock('promotion/adminhtml_rule_edit_tab_main')
                        ->toHtml())
            ));
			
        $this->addTab('conditions',
            array(
                'label' => Mage::helper('salesrule')->__('Conditions'),
                'title' => Mage::helper('salesrule')->__('Conditions'),
                'content' => $this->_translateHtml($this->getLayout()
                        ->createBlock('promotion/adminhtml_rule_edit_tab_conditions')
                        ->toHtml())
            ));
		/*	
        $this->addTab('actions',
            array(
                'label' => Mage::helper('salesrule')->__('Actions'),
                'title' => Mage::helper('salesrule')->__('Actions'),
                'content' => $this->_translateHtml($this->getLayout()
                        ->createBlock('promotion/adminhtml_rule_edit_tab_actions')
                        ->toHtml())
            ));	*/
			
        $this->addTab('products',
            array(
                'label' => Mage::helper('promotion')->__('Gift Product'),
                'title' => Mage::helper('promotion')->__('Gift Product'),
                'url' => $this->getUrl('*/*/products',
                    array(
                        '_current' => true
                    )),
                'class' => 'ajax',
            ));
    }
	
    /**
     * Translate html content
     * @param string $html
     * @return string
    */
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }
}
