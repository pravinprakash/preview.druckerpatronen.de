<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Block_Adminhtml_Rule_Edit_Tab_Products 
	extends Mage_Adminhtml_Block_Widget_Form 
		implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare content for tab
     * @return string
    */
    public function getTabLabel()
    {
        return Mage::helper('promotion')->__('Promotions');
    }
    
    /**
     * Prepare title for tab
     * @return string
    */
    public function getTabTitle()
    {
        return Mage::helper('promotion')->__('Promotions');
    }
    
    /**
     * Returns status flag about this tab can be showen or not
     * @return true
    */
    public function canShowTab()
    {
        return true;
    }
    
    /**
     * Returns status flag about this tab hidden or not
     * @return true
    */
    public function isHidden()
    {
        return false;
    }
    
    protected function _prepareForm()
    {
        $model = Mage::registry('promotion_current_rule');
        
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        
        $fieldset = $form->addFieldset('products_fieldset',
            array(
                'legend' => Mage::helper('promotion')->__('Update shopping cart using the following settings')
            ));
        
        $fieldset->addField('stop_rules_processing',
            'select',
            array(
                'label' => Mage::helper('salesrule')->__('Stop Further Rules Processing'),
                'title' => Mage::helper('salesrule')->__('Stop Further Rules Processing'),
                'name' => 'rule[stop_rules_processing]',
                'options' => array(
                    '1' => Mage::helper('salesrule')->__('Yes'),
                    '0' => Mage::helper('salesrule')->__('No'),
                ),
            ));
        
        $fieldset->addField('qty',
            'text',
            array(
                'name' => 'rule[qty]',
                'label' => Mage::helper('catalog')->__('Qty'),
            ));
        
        $form->setValues($model->getData());
        
        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }
        
        $this->setForm($form);
        
        Mage::dispatchEvent('adminhtml_promotion_rule_edit_tab_products_prepare_form',
            array(
                'form' => $form
            ));
        
        return parent::_prepareForm();
    }
}
