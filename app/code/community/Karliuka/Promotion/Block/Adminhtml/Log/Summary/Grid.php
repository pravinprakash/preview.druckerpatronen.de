<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Block_Adminhtml_Log_Summary_Grid 
	extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('logSummaryGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('promotion/log_summary')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
			$value = $column->getFilter()->getValue();
			$select = $this->getCollection()->getSelect();
            if($column->getId() == 'created_at') {
				$where = array('date' => true);
				if(isset($value['from'])) $where['from'] = $value['from'];
				if(isset($value['to'])) $where['to'] = $value['to'];
				if(isset($where['from']) || isset($where['to'])){
					$this->getCollection()->addFieldToFilter('l.created_at', $where);
				}
			} else parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
	
	protected function _prepareColumns()
	{
		$this->addColumn('rule_id', array(
			'header' => Mage::helper('promotion')->__('Rule'),
			'index' => 'rule_id',
			'frame_callback' => array($this, 'getRuleName')
		));

		$this->addColumn('total_suggested', array(
			'header' => Mage::helper('promotion')->__('Total Suggested'),
			'index'  => 'total_suggested',
			'width'  => '120px',
			'type'   => 'number',
		));	

		$this->addColumn('total_exchanged', array(
			'header' => Mage::helper('promotion')->__('Total Exchanged'),
			'index'  => 'total_exchanged',
			'width'  => '120px',
			'type'   => 'number',
		));	

		$this->addColumn('total_completed_orders_not', array(
			'header' => Mage::helper('promotion')->__('Total Not Orders'),
			'index'  => 'total_completed_orders_not',
			'width'  => '120px',
			'type'   => 'number',
		));	
		
		$this->addColumn('total_completed_orders', array(
			'header' => Mage::helper('promotion')->__('Total Orders'),
			'index'  => 'total_completed_orders',
			'width'  => '120px',
			'type'   => 'number',
		));	
		
		$this->addColumn('total_completed_orders_fully', array(
			'header' => Mage::helper('promotion')->__('Total Orders Fully'),
			'index'  => 'total_completed_orders_fully',
			'width'  => '120px',
			'type'   => 'number',
		));	

		$this->addColumn('total_completed_orders_partially', array(
			'header' => Mage::helper('promotion')->__('Total Orders Partially'),
			'index'  => 'total_completed_orders_partially',
			'width'  => '120px',
			'type'   => 'number',
		));			
		
		$this->addColumn('average_completed_orders', array(
			'header' => Mage::helper('promotion')->__('Average Orders (%)'),
			'index'  => 'average_completed_orders',
			'width'  => '120px',
			'type'   => 'number',
		));	
		
		$this->addColumn('average_completed_orders_not', array(
			'header' => Mage::helper('promotion')->__('Average Orders Not (%)'),
			'index'  => 'average_completed_orders_not',
			'width'  => '140px',
			'type'   => 'number',
		));	
		
		$this->addColumn('total_products', array(
			'header' => Mage::helper('promotion')->__('Total Products'),
			'index'  => 'total_products',
			'width'  => '120px',
			'type'   => 'number',
			'frame_callback' => array($this, 'getTotalProducts')
		));	
		
		$this->addColumn('average_exchange_product', array(
			'header' => Mage::helper('promotion')->__('Average Exchange Product'),
			'index'  => 'average_exchange_product',
			'width'  => '140px',
			'type'   => 'number',
		));	
		
		$this->addColumn('average_exchange_customer', array(
			'header' => Mage::helper('promotion')->__('Average Exchange Customer'),
			'index'  => 'average_exchange_customer',
			'width'  => '140px',
			'type'   => 'number',
		));	
		
		$this->addColumn('created_at', array(
			'header' => Mage::helper('promotion')->__('Date'),
			'align' =>'right',
			'index' => 'created_at',
			'type'  => 'datetime',
			'width' => '150px',
		));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		//return '#';
		return $this->getUrl("adminhtml/promotion_log", array("id" => $row->getId()));
	}
	
    /**
     * Decorate Total Products column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getTotalProducts($value, $row, $column, $isExport)
    {
        return (0 < $value) ? $value : '0';
    }	

    /**
     * Decorate product column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getRuleName($value, $row, $column, $isExport)
    {
        return $row->getName();
    }	
}