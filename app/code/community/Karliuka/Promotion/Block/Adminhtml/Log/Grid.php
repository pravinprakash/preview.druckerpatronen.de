<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Block_Adminhtml_Log_Grid 
	extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('logGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{//echo 1;exit;
		$ruleId  = (int) Mage::app()->getRequest()->getParam('id', null);
		$collection = Mage::getModel('promotion/log')->getCollection();//->addOrderStatus();
		if (null !== $ruleId) $collection->addFilter('main_table.rule_id', $ruleId);
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
			$value = $column->getFilter()->getValue();
			$select = $this->getCollection()->getSelect();
            if ($column->getId() == 'order_status') {
				if ($value) {
					$this->getCollection()->addFieldToFilter('order.status', array('eq' => $value));
				} 
            } elseif($column->getId() == 'created_at') {
				$where = array('date' => true);
				if(isset($value['from'])) $where['from'] = $value['from'];
				if(isset($value['to'])) $where['to'] = $value['to'];
				if(isset($where['from']) || isset($where['to'])){
					$this->getCollection()->addFieldToFilter('main_table.created_at', $where);
					//echo '<pre>';
					//print_r($value);
					//echo $select;
					//exit;
				}
			} else parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
	
	protected function _prepareColumns()
	{
		$this->addColumn('entity_id', array(
			'header' => Mage::helper('promotion')->__('ID'),
			'align' =>'right',
			'width' => '50px',
			'type' => 'number',
			'index' => 'entity_id',
		));
		
		$this->addColumn('product_id', array(
			'header' => Mage::helper('promotion')->__('Product'),
			'index'  => 'product_id',
			'frame_callback' => array($this, 'getProductName')
		));	
		
		$this->addColumn('exchange_product_id', array(
			'header' => Mage::helper('promotion')->__('Exchange Product'),
			'index' => 'exchange_product_id',
			'frame_callback' => array($this, 'getProductName')
		));
		
		$this->addColumn('gift_product_id', array(
			'header' => Mage::helper('promotion')->__('Gift Product'),
			'index' => 'gift_product_id',
			'frame_callback' => array($this, 'getProductName')
		));
		
		$this->addColumn('product_qty', array(
			'header' => Mage::helper('promotion')->__('Qty'),
			'align' =>'right',
			'index' => 'product_qty',
			'width' => '50px',
		));
		
		$this->addColumn('rule_id', array(
			'header' => Mage::helper('promotion')->__('Rule'),
			'index' => 'rule_id',
			'frame_callback' => array($this, 'getRuleName')
		));
			
		$this->addColumn('customer_id', array(
			'header' => Mage::helper('promotion')->__('Customer'),
			'align' =>'right',
			'index' => 'customer_id',
			'frame_callback' => array($this, 'getCustomerName')
		));
		
		$this->addColumn('order_id', array(
			'header' => Mage::helper('promotion')->__('Order Id'),
			'index' => 'order_id',
			'frame_callback' => array($this, 'getOrder'),
			'width' => '150px',
		));
		
		$this->addColumn('order_status', array(
			'header' => Mage::helper('promotion')->__('Order Status'),
			'index' => 'order_status',
			'width' => '150px',
			'type'  => 'options',
            'options' => Mage::getSingleton('promotion/sales_order_type')->getOptionArray(),
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('promotion')->__('Date'),
			'align' =>'right',
			'index' => 'created_at',
			'type'  => 'datetime',
			'width' => '150px',
		));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return '#';
	}
	
    /**
     * Decorate product column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getProductName($value, $row, $column, $isExport)
    {
        if(empty($value)) return '';
		$product = Mage::getModel('catalog/product')->load($value);
		$href = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id'=>$value));
        return "<a href='{$href}' target='_blank'>{$product->getName()} ({$product->getSku()})</a>";
    }
	
    /**
     * Decorate product column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getRuleName($value, $row, $column, $isExport)
    {
        $rule = Mage::getModel('promotion/rule')->load($value);
		$href = Mage::helper('adminhtml')->getUrl('adminhtml/promotion/edit', array('id'=>$value));
        return "<a href='{$href}' target='_blank'>{$rule->getName()}</a>";
    }
	
    /**
     * Decorate customer column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getCustomerName($value, $row, $column, $isExport)
    {
        if (0 == $value) return 'Guest';
		$customer = Mage::getModel('customer/customer')->load($value);
		$href = Mage::helper('adminhtml')->getUrl('adminhtml/customer/edit', array('id'=>$value));
        return "<a href='{$href}' target='_blank'>{$customer->getName()}</a>";
    }
	
    /**
     * Decorate order column values
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
    */
    public function getOrder($value, $row, $column, $isExport)
    {
        if (0 == $value) return '';
		$href = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id'=>$value));
        return "<a href='{$href}' target='_blank'>{$value}</a>";
    }	
}