<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Promotion_Block_Promotion
	extends Mage_Checkout_Block_Cart_Abstract
{
    /**
     * Retrieve array Promotion Collections
     * @param bool $checkQty 
     * @return array
    */		
	public function getPromoCollection()
	{	//Mage::Log('start', null, 'has.log');
		$quote = $this->getQuote();
		$address = null;
		$cartProductIds = array();
		
		
		foreach($quote->getAllAddresses() as $addr){
			if($addr) {
				$address = $addr;
				break;
			}
		}
		
		foreach($quote->getAllVisibleItems() as $item){
			$cartProductIds[] = $item->getProductId();
		}
		
		$store = Mage::app()->getStore($quote->getStoreId());
        $validator = Mage::getModel('promotion/rule_validator')
            ->init($store->getWebsiteId(), $quote->getCustomerGroupId());
        
        $validator->process($address);
		$rules = $validator->getExchange();
		$session = Mage::getSingleton('core/session');
		$visitor = $session->getVisitorData();
		$customerId = (Mage::getSingleton('customer/session')->isLoggedIn()) 
			? Mage::getSingleton('customer/session')->getCustomer()->getId() : 0;	
		$sessionId = $session->getSessionId();
		
		if(!$sessionId) return false;
		
		foreach($rules as $rule){
		
			$productIds = $rule->getMatchingProductIds($cartProductIds);
			foreach($productIds as $productId => $value)
			{
				$product = Mage::getModel('catalog/product')->load($productId);
				$exchange = $this->getExchangeProduct($rule, $product);
				if(!$product->getId() || !$exchange) continue;
				if($rule->getPriceControl() == '1' && $product->getPrice() <= $exchange->getPrice()) continue;			
				
				$key = $rule->getId() . '.' .  $product->getId() . '.' . $exchange->getId();
				$exchangeViews = @unserialize($session->getExchangeView());
				
				if(false === $exchangeViews || !isset($exchangeViews[$key]))
				{
					$gifts = $rule->getProductIds();
					$log = Mage::getModel('promotion/log');
					$log->addData(array(
						'rule_id'             => $rule->getId(),
						'product_id'          => $product->getId(),
						'exchange_product_id' => $exchange->getId(),
						'gift_product_id'     => isset($gift[0]) ? $gifts[0] : 0,
						'customer_id'         => $customerId,
						'session_id'          => $sessionId,
					));	
					try {
						$log->save();
					} catch (Exception $e) {}
					$exchangeViews[$key] = true;
					$session->setExchangeView(serialize($exchangeViews));
				}

				foreach($quote->getAllVisibleItems() as $item){
					if($productId == $item->getProductId()){
						$qty = floor($item->getQty()/$rule->getConditionQty());
						$item->setExchangeCount($qty);
						if(!empty($log))$item->setLogId($log->getId());
						$rule->setItemQty($item->getQty());
					}
				}				
			}
		}
		$quote->save();	
		return $rules;
	}
	
	public function getExchangeProduct($rule, $product)
	{
		$exchange = array();
		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection->addAttributeToFilter('bechlem_id', $product->getBechlemId());
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
		foreach ($collection as $_product){
			$res = (int)$rule->getActions()->validate($_product);
			if($res == 1) $exchange[] = $_product;
		}
		if(!isset($exchange[0])) $exchange[0] = null;
		return $exchange[0];
	}
	
    /**
     * get Image Src
    */		
	public function getImageSrc($product)
	{
		$isSecure = Mage::app()->getFrontController()->getRequest()->isSecure(); 
		$src = $this->helper('catalog/image')->init($product, 'small_image')->resize(50);
		$ext = $product->getImgSmall();
		$ext = str_replace('product_images/', $this->getUrl('product_images/'), $ext);
		if(strpos($src, '/placeholder/') && $ext) $src = $ext;
		if($isSecure) $src = str_replace('http://', 'https://', $src);
		return $src;
	}
	
	public function getContainerShow()
	{	
		return (bool)Mage::app()->getRequest()->getParam('container', true);
	}		
}