<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_discountlink
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (karliuka.vitalii@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_DiscountLink_Model_Observer 
	extends Varien_Event_Observer
{
    /**
     * apply Discount code
     * @param object $observer Varien_Event_Observer
     * @return Karliuka_DiscountLink_Model_Observer
    */
	public function apply($observer)
	{
		if (!Mage::helper('karliuka_discountlink')->isEnabled()) {
            return $this;
		}
		
		$code = trim(
			Mage::getSingleton('checkout/session')
				->getData('coupon_code')
		);
		
		if (!empty($code)){
			Mage::getSingleton('checkout/cart')
				->getQuote()
				->setCouponCode($code)
				->save();
		}
		return $this;
	}
}