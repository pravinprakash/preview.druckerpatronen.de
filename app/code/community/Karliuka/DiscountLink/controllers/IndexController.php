<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_discountlink
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (karliuka.vitalii@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_DiscountLink_IndexController 
	extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
    */
	public function indexAction() 
	{
		if (!Mage::helper('karliuka_discountlink')->isEnabled()) {
            $this->_redirect('/');
			return;
		}
		
		$code = $this->getRequest()->getParam('code');
		$url = $this->getRequest()->getParam('return');
		
		if (!empty($code)){
			Mage::getSingleton('checkout/session')
				->setData('coupon_code', $code);
				
			Mage::getSingleton('checkout/cart')
				->getQuote()
				->setCouponCode($code)
				->save();
			if(Mage::helper('checkout/cart')->getItemsCount() <= 0)
			{
			
				Mage::getSingleton('core/session')
					->addSuccess($this->__('<script type="text/javascript">setTimeout("initDiscountLinkPopup(\"'.$code.'\")", 1000);setTimeout("closeDiscountLinkPopup()", 15000);</script>'));
			}
		}else{
			Mage::getSingleton('checkout/session')
				->setData('coupon_code', '');
				
			$cart = Mage::getSingleton('checkout/cart');
			
			$items = Mage::getSingleton('checkout/session')
				->getQuote()
				->getItemsCollection();
				
			foreach($items as $item){
				$cart->removeItem($item->getId());
			}
		    $cart->save();
		}
		 
		if(empty($url)) $this->_redirect('/');
		else{
		    header('Location: '.$url);
			exit();
		}
	}	
}