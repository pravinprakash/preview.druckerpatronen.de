<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_AddToCart_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
    */		
    public function indexAction()
    { 
		//http://zeta.druckerpatronen.de/addtocart/?code=vorkasse&product[32067sp5]=3&product[32067]=2
		$quote = $this->_getOnepage()->getQuote();

		$products = $this->getRequest()->getParam('product');
		if(!is_array($products)) $products = array();
		
		if(0 < count($products)) $response['product'] =  $this->_addProduct($quote, $products);
        $couponCode = (string)$this->getRequest()->getParam('code');

		$response['coupon'] = $this->_addCoupon($quote, $couponCode);
		
		$redirect = $this->getRequest()->getParam('redirect');
		
		if(!empty($redirect)) Mage::app()->getFrontController()->getResponse()->setRedirect($redirect)->sendResponse();
		elseif(isset($_GET['jsoncallback'])) $this->getResponse()->setBody($_GET['jsoncallback'] . '(' . json_encode($response) . ')');
		else $this->getResponse()->setBody(Zend_Json::encode($response));		
    }
	
    protected function _addCoupon($quote, $couponCode)
    {
        $response = array(
            'success' => false,
            'error'   => false,
            'message' => false,
        );
		
        try 
		{
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->setCouponCode(strlen($couponCode) ? $couponCode : '')
            ->collectTotals()
            ->save();

            if ($couponCode) {
                if ($couponCode == $quote->getCouponCode()) {
                    $response['success'] = true;
                    $response['message'] = $this->__('Coupon code "%s" was applied successfully.', Mage::helper('core')->htmlEscape($couponCode));
                } else {
                    $response['error'] = true;
                    $response['message'] = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode));
                }
            } else {
                $response['success'] = true;
                $response['message'] = $this->__('Coupon code was canceled successfully.');
            }
        } catch (Mage_Core_Exception $e) {
            $response['success'] = false;
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['success'] = false;
            $response['error'] = true;
            $response['message'] = $this->__('Can not apply coupon code.');
        }
		return $response;
    }

    protected function _addProduct($quote, $products)
    {
        $responses = array();
		
		foreach($products as $sku => $qty)
		{
			$response = array(
				'success' => false,
				'error'   => false,
				'message' => '',
			);		
			$product = Mage::getModel('catalog/product');
			$product->load($product->getIdBySku($sku));
			if(!$product->getId()) {
				$response['error'] = true;
				$response['message'] = $this->__('Product sku "%s" is not valid.', Mage::helper('core')->htmlEscape($sku));
				continue;
			}
			$quote->addProduct($product, $this->_getProductRequest($qty));
			try {
				$quote->save();	
				$response['success'] = true;
				Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			} catch (Exception $e) {
				$response['success'] = false;
				$response['message'] = $this->__('Cannot add item.');
			}
			$responses[$sku] = $response;
		}
		return $responses;
    }
	
	/**
     * Get request for product add to cart procedure
     * @see Mage_Checkout_Model_Cart::_getProductRequest()
     * @param mixed $requestInfo
     * @return Varien_Object
    */
    protected function _getProductRequest($info)
    {
        if ($info instanceof Varien_Object) {
            $request = $info;
        } else if (is_numeric($info)) {
            $request = new Varien_Object(array(
                'qty' => $info 
            ));
        } else $request = new Varien_Object($info);
        
        if (!$request->hasQty()) {
            $request->setQty(1);
        }
        return $request;
    }
	
    protected function _getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }	
}