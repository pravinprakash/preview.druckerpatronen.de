<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    AddToCart
 * @package     Karliuka_AddToCart
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_AddToCart_Model_Observer
{
    public function addToCart($observer) 
    {	//echo Mage::app()->getRequest()->getControllerName();exit;
		if(Mage::app()->getRequest()->getControllerName() != 'product') return $this;
		
		//http://zeta.druckerpatronen.de/addtocart/?code=vorkasse&product[32067sp5]=3&product[32067]=2
		$redirect = false;
		$quote = $this->_getOnepage()->getQuote();

		
		$products = Mage::app()->getRequest()->getParam('product');
		if(!is_array($products)) {
			$products = array();
		} else $redirect = true;
		
		if(0 < count($products)) $response['product'] =  $this->_addProduct($quote, $products);
		
        $couponCode = (string)Mage::app()->getRequest()->getParam('code');
		if(!empty($couponCode)){
				
		if (!empty($code)){
			Mage::getSingleton('checkout/cart')
				->getQuote()
				->setCouponCode($couponCode)
				->save();
		}
			$redirect = true;
		}

		$response['coupon'] = $this->_addCoupon($quote, $couponCode);
		Mage::Log($response, null, 'AddToCart.log');
		//echo '<pre>';print_r($response);exit;
		$url = Mage::helper('core/url')->getCurrentUrl();
		$url = preg_replace("#(\?.+)$#is", '', $url);

		if($redirect){ 
			Mage::register('addtocart_redirect_url', $url);
			//Mage::app()->getFrontController()->getResponse()->setRedirect($url)->sendResponse();
			//exit;
		}
		return $this;	
    }
	
    protected function _addCoupon($quote, $couponCode)
    {
        $response = array(
            'success' => false,
            'error'   => false,
            'message' => false,
        );
		
        try 
		{
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->setCouponCode(strlen($couponCode) ? $couponCode : '')
            ->collectTotals()
            ->save();

            if ($couponCode) {
                if ($couponCode == $quote->getCouponCode()) {
                    $response['success'] = true;
                    $response['message'] = Mage::helper('addtocart')->__('Coupon code "%s" was applied successfully.', Mage::helper('core')->htmlEscape($couponCode));
                } else {
                    $response['error'] = true;
                    $response['message'] =  Mage::helper('addtocart')->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode));
                }
            } else {
                $response['success'] = true;
                $response['message'] =  Mage::helper('addtocart')->__('Coupon code was canceled successfully.');
            }
        } catch (Mage_Core_Exception $e) {
            $response['success'] = false;
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['success'] = false;
            $response['error'] = true;
            $response['message'] =  Mage::helper('addtocart')->__('Can not apply coupon code.');
        }
		return $response;
    }

    protected function _addProduct($quote, $products)
    {
        $responses = array();
		
		foreach($products as $sku => $qty)
		{
			$response = array(
				'success' => false,
				'error'   => false,
				'message' => '',
			);		
			$product = Mage::getModel('catalog/product');
			$product->load($product->getIdBySku($sku));
			if(!$product->getId()) {
				$response['error'] = true;
				$response['message'] = $this->__('Product sku "%s" is not valid.', Mage::helper('core')->htmlEscape($sku));
				continue;
			}
			$quote->addProduct($product, $this->_getProductRequest($qty));
			try {
				$quote->save();	
				$response['success'] = true;
				Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			} catch (Exception $e) {
				$response['success'] = false;
				$response['message'] = $this->__('Cannot add item.');
			}
			$responses[$sku] = $response;
		}
		return $responses;
    }
	
	/**
     * Get request for product add to cart procedure
     * @see Mage_Checkout_Model_Cart::_getProductRequest()
     * @param mixed $requestInfo
     * @return Varien_Object
    */
    protected function _getProductRequest($info)
    {
        if ($info instanceof Varien_Object) {
            $request = $info;
        } else if (is_numeric($info)) {
            $request = new Varien_Object(array(
                'qty' => $info 
            ));
        } else $request = new Varien_Object($info);
        
        if (!$request->hasQty()) {
            $request->setQty(1);
        }
        return $request;
    }
	
    protected function _getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }	
}