<?php
/**
 * Reminder rules data helper
 */
class Karliuka_Reminder_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'promo/karliuka_reminder/enabled';
    const XML_PATH_SEND_LIMIT = 'promo/karliuka_reminder/limit';
    const XML_PATH_EMAIL_IDENTITY = 'promo/karliuka_reminder/identity';
    const XML_PATH_EMAIL_THRESHOLD = 'promo/karliuka_reminder/threshold';

    /**
     * Check whether reminder rules should be enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    /**
     * Return maximum emails that can be send per one run
     *
     * @return int
     */
    public function getOneRunLimit()
    {
        return (int)Mage::getStoreConfig(self::XML_PATH_SEND_LIMIT);
    }

    /**
     * Return email sender information
     *
     * @return string
     */
    public function getEmailIdentity()
    {
        return (string)Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY);
    }

    /**
     * Return email send failure threshold
     *
     * @return int
     */
    public function getSendFailureThreshold()
    {
        return (int)Mage::getStoreConfig(self::XML_PATH_EMAIL_THRESHOLD);
    }
}
