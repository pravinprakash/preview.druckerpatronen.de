<?php

$installer = $this;
/** @var $installer Karliuka_Reminder_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table 'karliuka_reminder/rule'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('karliuka_reminder/rule'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Rule Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Name')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Description')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        'nullable'  => false,
        ), 'Conditions Serialized')
    ->addColumn('condition_sql', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'Condition Sql')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        ), 'Is Active')
    ->addColumn('salesrule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        ), 'Salesrule Id')
    ->addColumn('schedule', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Schedule')
    ->addColumn('default_label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Default Label')
    ->addColumn('default_description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Default Description')
    ->addColumn('active_from', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Active From')
    ->addColumn('active_to', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Active To')
    ->addIndex($installer->getIdxName('karliuka_reminder/rule', array('salesrule_id')),
        array('salesrule_id'))
    ->addForeignKey($installer->getFkName('karliuka_reminder/rule', 'salesrule_id', 'salesrule/rule', 'rule_id'),
        'salesrule_id', $installer->getTable('salesrule/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Karliuka Reminder Rule');
$installer->getConnection()->createTable($table);

/**
 * Create table 'karliuka_reminder/website'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('karliuka_reminder/website'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Rule Id')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Website Id')
    ->addIndex($installer->getIdxName('karliuka_reminder/website', array('website_id')),
        array('website_id'))
    ->addForeignKey(
        $installer->getFkName('karliuka_reminder/website', 'rule_id', 'karliuka_reminder/rule', 'rule_id'),
        'rule_id', $installer->getTable('karliuka_reminder/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Karliuka Reminder Rule Website');
$installer->getConnection()->createTable($table);

/**
 * Create table 'karliuka_reminder/template'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('karliuka_reminder/template'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Rule Id')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Store Id')
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        ), 'Template Id')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Label')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Description')
    ->addIndex($installer->getIdxName('karliuka_reminder/template', array('rule_id')),
        array('rule_id'))
    ->addIndex($installer->getIdxName('karliuka_reminder/template', array('template_id')),
        array('template_id'))
    ->addForeignKey(
        $installer->getFkName('karliuka_reminder/template', 'template_id', 'core/email_template', 'template_id'),
        'template_id', $installer->getTable('core/email_template'), 'template_id',
        Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        $installer->getFkName('karliuka_reminder/template', 'rule_id', 'karliuka_reminder/rule', 'rule_id'),
        'rule_id', $installer->getTable('karliuka_reminder/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Karliuka Reminder Template');
$installer->getConnection()->createTable($table);

/**
 * Create table 'karliuka_reminder/coupon'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('karliuka_reminder/coupon'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Rule Id')
    ->addColumn('coupon_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        ), 'Coupon Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Customer Id')
    ->addColumn('associated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        ), 'Associated At')
    ->addColumn('emails_failed', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        ), 'Emails Failed')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '1',
        ), 'Is Active')
    ->addIndex($installer->getIdxName('karliuka_reminder/coupon', array('rule_id')),
        array('rule_id'))
    ->addForeignKey(
        $installer->getFkName('karliuka_reminder/coupon', 'rule_id', 'karliuka_reminder/rule', 'rule_id'),
        'rule_id', $installer->getTable('karliuka_reminder/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Karliuka Reminder Rule Coupon');
$installer->getConnection()->createTable($table);

/**
 * Create table 'karliuka_reminder/log'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('karliuka_reminder/log'))
    ->addColumn('log_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Log Id')
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Rule Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Customer Id')
    ->addColumn('sent_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        ), 'Sent At')
    ->addIndex($installer->getIdxName('karliuka_reminder/log', array('rule_id')),
        array('rule_id'))
    ->addIndex($installer->getIdxName('karliuka_reminder/log', array('customer_id')),
        array('customer_id'))
    ->addForeignKey($installer->getFkName('karliuka_reminder/log', 'rule_id', 'karliuka_reminder/rule', 'rule_id'),
        'rule_id', $installer->getTable('karliuka_reminder/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Karliuka Reminder Rule Log');
$installer->getConnection()->createTable($table);


$installer->endSetup();
