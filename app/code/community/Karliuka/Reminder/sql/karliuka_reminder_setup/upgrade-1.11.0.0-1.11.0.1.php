<?php

/** @var $installer Karliuka_Reminder_Model_Resource_Setup */
$installer = $this;

$ruleTable  = $installer->getTable('karliuka_reminder/rule');
$ruleWebsiteTable = $installer->getTable('karliuka_reminder/website');
$coreWebsiteTable = $installer->getTable('core/website');
$connection = $installer->getConnection();

$installer->startSetup();

$connection->changeColumn(
    $ruleTable,
    'active_from',
    'from_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => true,
        'default'   => null
    )
);

$connection->changeColumn(
    $ruleTable,
    'active_to',
    'to_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => true,
        'default'   => null
    )
);


/**
 * Clean relations with not existing websites
 */
$selectWebsiteIds = $connection->select()
    ->from($coreWebsiteTable, 'website_id');
$websiteIds = $connection->fetchCol($selectWebsiteIds);
if (!empty($websiteIds)) {
    $connection->delete($ruleWebsiteTable, $connection->quoteInto('website_id NOT IN (?)', $websiteIds));
}

/**
 * Add foreign key for rule website table onto core website table
 */
$connection->addForeignKey(
    $installer->getFkName('karliuka_reminder/website', 'website_id', 'core/website', 'website_id'),
    $ruleWebsiteTable,
    'website_id',
    $coreWebsiteTable,
    'website_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();
