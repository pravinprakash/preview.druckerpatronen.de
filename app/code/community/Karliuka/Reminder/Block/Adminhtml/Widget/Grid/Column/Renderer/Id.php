<?php

class Karliuka_Reminder_Block_Adminhtml_Widget_Grid_Column_Renderer_Id
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Render customer id linked to its account edit page
     *
     * @param   Varien_Object $row
     * @return  string
     */
    protected function _getValue(Varien_Object $row)
    {
        $customerId = $this->htmlEscape($row->getData($this->getColumn()->getIndex()));
        return '<a href="' . Mage::getSingleton('adminhtml/url')->getUrl('*/customer/edit',
            array('id' => $customerId)) . '">' . $customerId . '</a>';
    }
}
