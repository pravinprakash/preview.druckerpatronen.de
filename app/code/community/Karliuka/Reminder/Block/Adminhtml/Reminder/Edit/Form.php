<?php

class Karliuka_Reminder_Block_Adminhtml_Reminder_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Intialize form
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('karliuka_reminder_rule_form');
        $this->setTitle(Mage::helper('karliuka_reminder')->__('Email Reminder Rule'));
    }

    /**
     * Prepare edit form
     *
     * @return Karliuka_Reminder_Block_Adminhtml_Reminder_Edit_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
