<?php

class Karliuka_Reminder_Block_Adminhtml_Reminder_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Intialize form
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('karliuka_reminder_rule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('karliuka_reminder')->__('Email Reminder Rule'));
    }

    /**
     * Add tab sections
     *
     * @return Karliuka_Reminder_Block_Adminhtml_Reminder_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label'   => Mage::helper('karliuka_reminder')->__('Rule Information'),
            'content' => $this->getLayout()->createBlock('karliuka_reminder/adminhtml_reminder_edit_tab_general',
                'adminhtml_reminder_edit_tab_general')->toHtml(),
        ));

        $this->addTab('conditions_section', array(
            'label'   => Mage::helper('karliuka_reminder')->__('Conditions'),
            'content' => $this->getLayout()->createBlock('karliuka_reminder/adminhtml_reminder_edit_tab_conditions',
                'adminhtml_reminder_edit_tab_conditions')->toHtml()
        ));

        $this->addTab('template_section', array(
            'label'   => Mage::helper('karliuka_reminder')->__('Emails and Labels'),
            'content' => $this->getLayout()->createBlock('karliuka_reminder/adminhtml_reminder_edit_tab_templates',
                'adminhtml_reminder_edit_tab_templates')->toHtml()
        ));

        $rule = Mage::registry('current_reminder_rule');
        if ($rule && $rule->getId()) {
            $this->addTab('matched_customers', array(
                'label' => Mage::helper('karliuka_reminder')->__('Matched Customers'),
                'url'   => $this->getUrl('*/*/customerGrid', array('rule_id' => $rule->getId())),
                'class' => 'ajax'
            ));
        }

        return parent::_beforeToHtml();
    }
}
