<?php

class Karliuka_Reminder_Block_Adminhtml_Reminder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize invitation manage page
     *
     * @return void
     */
    public function __construct()
    {
        $this->_blockGroup = 'karliuka_reminder';
        $this->_controller = 'adminhtml_reminder';
        $this->_headerText = Mage::helper('karliuka_reminder')->__('Automated Email Marketing Reminder Rules');
        $this->_addButtonLabel = Mage::helper('karliuka_reminder')->__('Add New Rule');
        parent::__construct();
    }
}
