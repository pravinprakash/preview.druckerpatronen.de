<?php

class Karliuka_Reminder_Model_Resource_Helper_Mysql4 extends Mage_Core_Model_Resource_Helper_Mysql4
{
    /**
     * Sets limit for rules specific select
     *
     * @param Varien_Db_Select $select
     * @param int $limit
     * @return void
     */
    public function setRuleLimit(Varien_Db_Select $select, $limit)
    {
        $select->limit($limit);
    }
}
