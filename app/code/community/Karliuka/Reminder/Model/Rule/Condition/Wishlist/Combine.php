<?php

class Karliuka_Reminder_Model_Rule_Condition_Wishlist_Combine
    extends Karliuka_Reminder_Model_Condition_Combine_Abstract
{
    /**
     * Intialize model
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setType('karliuka_reminder/rule_condition_wishlist_combine');
    }

    /**
     * Get inherited conditions selectors
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $prefix = 'karliuka_reminder/rule_condition_wishlist_';

        return array_merge_recursive(
            parent::getNewChildSelectOptions(), array(
                $this->_getRecursiveChildSelectOption(),
                Mage::getModel("{$prefix}sharing")->getNewChildSelectOptions(),
                Mage::getModel("{$prefix}quantity")->getNewChildSelectOptions(),
                array( // subselection combo
                    'value' => 'karliuka_reminder/rule_condition_wishlist_subselection',
                    'label' => Mage::helper('karliuka_reminder')->__('Items Subselection')
                )
            )
        );
    }
}
