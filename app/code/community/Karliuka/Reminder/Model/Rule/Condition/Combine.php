<?php

class Karliuka_Reminder_Model_Rule_Condition_Combine
    extends Karliuka_Reminder_Model_Condition_Combine_Abstract
{
    /**
     * Intialize model
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setType('karliuka_reminder/rule_condition_combine');
    }

    /**
     * Get inherited conditions selectors
     *
     * @return array
     */
        public function getNewChildSelectOptions()
    {
        $conditions = array(
            array( // customer wishlist combo
                'value' => 'karliuka_reminder/rule_condition_wishlist',
                'label' => Mage::helper('karliuka_reminder')->__('Wishlist')),

            array( // customer shopping cart combo
                'value' => 'karliuka_reminder/rule_condition_cart',
                'label' => Mage::helper('karliuka_reminder')->__('Shopping Cart')),

        );

        $conditions = array_merge_recursive(parent::getNewChildSelectOptions(), $conditions);
        return $conditions;
    }
}
