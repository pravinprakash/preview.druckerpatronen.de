<?php
class Karliuka_Reminder_Model_Rule extends Mage_Rule_Model_Abstract
{
    const XML_PATH_EMAIL_TEMPLATE  = 'karliuka_reminder_email_template';

    /**
     * Store template data defined per store view, will be used in email templates as variables
     */
    protected $_storeData = array();

    /**
     * Init resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('karliuka_reminder/rule');
    }

    /**
     * Set template, label and description data per store
     *
     * @return Karliuka_Reminder_Model_Rule
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        $storeData = $this->_getResource()->getStoreData($this->getId());
        $defaultTemplate = self::XML_PATH_EMAIL_TEMPLATE;

        foreach ($storeData as $data) {
            $template = (empty($data['template_id'])) ? $defaultTemplate : $data['template_id'];
            $this->setData('store_template_' . $data['store_id'], $template);
            $this->setData('store_label_' . $data['store_id'], $data['label']);
            $this->setData('store_description_' . $data['store_id'], $data['description']);
        }

        return $this;
    }

    /**
     * Set aggregated conditions SQL and reset sales rule Id if applicable
     *
     * @return Karliuka_Reminder_Model_Rule
     */
    protected function _beforeSave()
    {
        $this->setConditionSql(
            $this->getConditions()->getConditionsSql(null, new Zend_Db_Expr(':website_id'))
        );

        if (!$this->getSalesruleId()) {
            $this->setSalesruleId(null);
        }

        parent::_beforeSave();
        return $this;
    }

    /**
     * Getter for rule combine conditions instance
     *
     * @return Karliuka_Reminder_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('karliuka_reminder/rule_condition_combine_root');
    }

    /**
     * Getter for rule actions collection instance
     *
     * @return Mage_Rule_Model_Action_Collection
     */
    public function getActionsInstance()
    {
        return Mage::getModel('rule/action_collection');
    }

    /**
     * Send reminder emails
     *
     * @return Karliuka_Reminder_Model_Rule
     */
    public function sendReminderEmails()
    {	
        /* @var $translate Mage_Core_Model_Translate */
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $identity = Mage::helper('karliuka_reminder')->getEmailIdentity();

        $this->_matchCustomers();
        $limit = Mage::helper('karliuka_reminder')->getOneRunLimit();

        $recipients = $this->_getResource()->getCustomersForNotification($limit, $this->getRuleId());
		
		$data = array(
			array('code','#profile_created','customerId','FirstName','LastName','Email','cartItem.code','cartItem.#subprofile_created','cartItem.ItemId','cartItem.Name','cartItem.Qty')
		);
		$path = Mage::getBaseDir('media') . DS . 'export' . DS;
		$file = $path . 'RecentAbandonedCarts.csv';
		$csv = new Varien_File_Csv();
		$csv->setDelimiter("\t")->setEnclosure('');
		
        foreach ($recipients as $recipient) {
            /* @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')->load($recipient['customer_id']);
            if (!$customer || !$customer->getId()) {
                continue;
            }
			
            if ($customer->getStoreId()) {
                $store = $customer->getStore();
            } else {
                $store = Mage::app()->getWebsite($customer->getWebsiteId())->getDefaultStore();
            }

            $storeData = $this->getStoreData($recipient['rule_id'], $store->getId());
            if (!$storeData) {
                continue;
            }

			$part = array(
				'code'       => md5($customer->getId()),
				'#profile_created' => date('Y-m-d H:i:s'),
				'customerId' => $customer->getId(),
				'FirstName'  => $customer->getFirstname(),
				'LastName'   => $customer->getLastname(),
				'Email'      => $customer->getEmail(),
			);
			
			$quote = Mage::getModel('sales/quote')->loadByCustomer($customer);
			foreach($quote->getAllVisibleItems() as $item){
				$data[] = array_merge($part, array(
					'cartItem.code' => md5($item->getId()),
					'cartItem.#subprofile_created' => date('Y-m-d H:i:s'),
					'cartItem.ItemId' => $item->getId(),
					'cartItem.Name' => $item->getProduct()->getName(),
					'cartItem.Qty' => $item->getQty(),
				));
			}			
            $this->_getResource()->addNotificationLog($recipient['rule_id'], $customer->getId());
        }
		
		$csv->saveData($file, $data);
		
		$account = null;
		$config = Mage::getStoreConfig('general/karliuka_copernica', Mage::app()->getStore());

		try {
			$soapclient = new Karliuka_Copernica_Model_Api(
				$config['email'], $config['password'], $account, $config['url'], $config['charset']
			);
			$database = $soapclient->Account_database(array(
				'identifier' => $config['database']
			));

			$soapclient->Database_clear(array(
				'id' => $database->id
			));	
			
			$import = $soapclient->Database_createImport(array(
				'id' => $database->id,
				'name' => 'RecentAbandonedCarts',
				'source' => Mage::getBaseUrl('media') . 'export/RecentAbandonedCarts.csv'
			));
			
			$result = $soapclient->Import_start(array(
				'id' => $import->id
			));			

		}
		catch (Exception $e){
			Mage::logException($e);
			return false;
		}
		
        $translate->setTranslateInline(true);
        return $this;
    }

    /**
     * Match customers for current rule and assign coupons
     *
     * @return Karliuka_Reminder_Model_Observer
     */
    protected function _matchCustomers()
    {
        $threshold   = Mage::helper('karliuka_reminder')->getSendFailureThreshold();
        $currentDate = Mage::getModel('core/date')->date('Y-m-d');
        $rules       = $this->getCollection()->addDateFilter($currentDate)->addIsActiveFilter(1);

        if ($this->getRuleId()) {
            $rules->addRuleFilter($this->getRuleId());
        }

        foreach ($rules as $rule) {
            $this->_getResource()->deactivateMatchedCustomers($rule->getId());

            if ($rule->getSalesruleId()) {
                /* @var $salesRule Mage_SalesRule_Model_Rule */
                $salesRule = Mage::getSingleton('salesrule/rule')->load($rule->getSalesruleId());
                $websiteIds = array_intersect($rule->getWebsiteIds(), $salesRule->getWebsiteIds());
            } else {
                $salesRule = null;
                $websiteIds = $rule->getWebsiteIds();
            }

            foreach ($websiteIds as $websiteId) {
                $this->_getResource()->saveMatchedCustomers($rule, $salesRule, $websiteId, $threshold);
            }
        }
        return $this;
    }

    /**
     * Retrieve store template data
     *
     * @param int $ruleId
     * @param int $storeId
     *
     * @return array|false
     */
    public function getStoreData($ruleId, $storeId)
    {
        if (!isset($this->_storeData[$ruleId][$storeId])) {
            if ($data = $this->_getResource()->getStoreTemplateData($ruleId, $storeId)) {
                if (empty($data['template_id'])) {
                    $data['template_id'] = self::XML_PATH_EMAIL_TEMPLATE;
                }
                $this->_storeData[$ruleId][$storeId] = $data;
            }
            else {
                return false;
            }
        }

        return $this->_storeData[$ruleId][$storeId];
    }

    /**
     * Detaches Sales Rule from all Email Remainder Rules that uses it
     *
     * @param int $salesRuleId
     * @return Karliuka_Reminder_Model_Rule
     */
    public function detachSalesRule($salesRuleId)
    {
        $this->getResource()->detachSalesRule($salesRuleId);
        return $this;
    }

    /**
     * Retrieve active from date.
     * Implemented for backwards compatibility with old property called "active_from"
     *
     * @return string
     */
    public function getActiveFrom()
    {
        return $this->getData('from_date');
    }

    /**
     * Retrieve active to date.
     * Implemented for backwards compatibility with old property called "active_to"
     *
     * @return string
     */
    public function getActiveTo()
    {
        return $this->getData('to_date');
    }
}
