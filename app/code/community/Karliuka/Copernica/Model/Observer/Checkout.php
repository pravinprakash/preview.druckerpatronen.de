<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Copernica_Model_Observer_Checkout
	extends Karliuka_Copernica_Model_Observer_Abstract
{
	/**
	 * sales order save
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function orderSave($observer)
    {
		if($this->log) Mage::Log('method orderSave:start', null, $this->logFile);
		/*$session = Mage::getSingleton('checkout/session');
		$order = $observer->getEvent()->getOrder();
        foreach ($session->getQuote()->getAllItems() as $item) {
            if(!$item->getLogId() || $item->getRuleId() > 0) continue;
			$customerId = $order->getCustomerId();
			$log = Mage::getModel('exchange/log')->load($item->getLogId());
			$log->setPaid(1);
			$log->setOrderId($order->getId());
			$log->setCustomerId($customerId);
			$log->save();
        }*/
				if($this->log) Mage::Log('method orderSave:stop', null, $this->logFile);
		return $this;
    }	
}