<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Copernica_Model_Observer_Abstract
	extends Varien_Event_Observer
{
	protected $database;
	
	protected $api;
	
	protected $collection;

	protected $log = true;
	protected $logFile = 'copernica.log';		

    protected function _construct()
    {echo 5;exit;
		$account = null;
		$config = Mage::getStoreConfig('general/karliuka_copernica', Mage::app()->getStore());
		try {
			$this->api = new Karliuka_Copernica_Model_Api(
				$config['email'], $config['password'], $account, $config['url'], $config['charset']
			);
			$this->database = $this->api->Account_database(array(
				'identifier' => $config['database']
			));
		}
		catch (Exception $e){
			Mage::logException($e);
			return false;
		}	
    }

    public function isLoggedIn()
    {
		return (bool)$this->getSession()->isLoggedIn();
    }

    public function getCustomer()
    {
		return $this->getSession()->getCustomer();
    }
	
    public function getSession()
    {
		return Mage::getSingleton('customer/session');
    }

    public function getProfile($customer)
    {
		$profiles = $this->api->Database_searchProfiles(array(
			'id' => $this->database->id,
			'requirements' => array(
				$this->api->toObject(array(
					'fieldname' => 'customerId',
					'value'     => $customer->getId(),
					'operator'  => '='
				)),
				$this->api->toObject(array(
					'fieldname' => 'Email',
					'value'     => $customer->getEmail(),
					'operator'  => '='
				)),						
			),
		));
		foreach ($profiles->items as $profile){
			return $profile;		
		}
		return $this->api->Database_createProfile(array(
			'id' => this->database->id,
			'fields' => array(
				'customerId' => $customer->getId(),
				'FirstName'  =>  $customer->getFirstname(),
				'LastName'   =>  $customer->getLastname(),
				'Email'      =>  $customer->getEmail(),
			)
		));		
    }
/*	
    public function getSubProfile($profile)
    {
		$sub = $soapclient->Profile_subProfiles(array(
            'id'         => $profile->id,
            'collection' => $soapclient->toObject(array(
                                'id' => $this->getCollection()->id
                            )),
			'allproperties' => true
        ));		
    }
	
    public function getCollection()
    {
		if(null === $this->collection){
			$this->collection = $this->api->Database_collection(array(
				'id' => this->database->id,
				'identifier' => 'cartItem'
			));	
		}
		return $this->collection;
    }*/	
}