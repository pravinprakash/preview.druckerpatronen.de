<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    promotion
 * @package     karliuka_promotion
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (wit.kharkov@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
class Karliuka_Copernica_Model_Observer_Cart
	extends Karliuka_Copernica_Model_Observer_Abstract
{
	/**
	 * sales Quote item Set Qty
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function itemSetQty($observer)
    {	echo 6;exit;
	/*	if($this->log) Mage::Log('method itemSetQty:start', null, $this->logFile);
		$item = $observer->getEvent()->getItem();
		$profile = $this->getProfile($this->getCustomer());
		$subProfileId = null;
		$subProfiles = $this->getSubProfile($profile);
		foreach ($subProfiles->items as $subProfile){
			foreach ($subProfile->fields->pair as $pair){
				if($pair['key'] == 'ItemId' && $pair['value'] == $item->getId()){
					$subProfileId = $subProfile->id;
					break;
				}
			}
		}
		if(null === $subProfileId){
			$this->api->Profile_createSubProfile(array(
				'id' => $profile->id,
				'collection' => $this->api->toObject(array('id' => $this->getCollection()->id)),
				'fields' => array(
						'Name'   => $item->getProduct()->getName(),
						'ItemId' => $item->getId(),
						'Qty'    => $item->getQty()
						
					)
			));		
		} else {
			$this->api->Profile_updateFields(array(
				'id' => $subProfileId,
				'fields' => array('Qty' => $item->getQty())
			));
		}
		
		
		if($this->log) Mage::Log('method itemSetQty:stop', null, $this->logFile);	*/	
		return $this;
    }
	
	/**
	 * sales Quote remove Item
	 * @param object $observer Varien_Event_Observer
	 * @return object Varien_Event_Observer
	*/
	public function removeItem($observer)
    {
		/*if($this->log) Mage::Log('method removeItem:start', null, $this->logFile);
		if($this->isLoggedIn()){
			$item = $observer->getEvent()->getQuoteItem();
			$profile = $this->getProfile($this->getCustomer());
		}
		if($this->log) Mage::Log('method removeItem:stop', null, $this->logFile);	*/	
		return $this;
    }	
}