<?php 

class Karliuka_BechlemImport_Model_Import
	extends Varien_Object
{
	protected $_resource;
	protected $_count;
	protected $_color = array();
	
	protected $_default = array(
		'sku' => null, 
		'_store' => null, 
		'_attribute_set' => null, 
		'_type' => 'simple',
		'_category' => null, 
		'description' => '', 
		'name' => '', 
		'price' => '0.00',
		'short_description' => '', 
		'status' => 1, 
		'tax_class_id' => 1, 
		'visibility' => 4,
		'weight' => null, 
		'qty' => '10000.0000', 
		'is_in_stock' => 1, 
		'manage_stock' => 0,
		'use_config_manage_stock' => 0
	);

    public function getResource() 
	{
		if(null == $this->_resource) $this->_resource = new Mage_Core_Model_Resource();
		return $this->_resource;
    }
	
    public function loadCategories() 
	{
		ignore_user_abort(true);
		set_time_limit(0);
		
		//return $this->loadProducts() ;
		
		Mage::Log('loadCategories', null, 'import.log');
		$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
		$parent = Mage::getModel('catalog/category')->load($rootCategoryId);
		
		$read = $this->getResource()->getConnection('core_read');
		$write = $this->getResource()->getConnection('core_write'); 
		
		$select = "
		SELECT x.name AS printer, s.name AS series, m.name AS model
		FROM bechlem_article2model AS am
			INNER JOIN bechlem_articles AS a ON(am.art_id=a.art_id)
			INNER JOIN bechlem_articles_new AS n ON (n.art_id = a.art_id)
			INNER JOIN bechlem_hq_products AS p ON(a.new_art_id=p.bechlem_id)
			INNER JOIN bechlem_models AS m ON(m.model_id=am.model_id)
			INNER JOIN bechlem_modelseries AS s ON(m.modelseries_id=s.modelseries_id)
			INNER JOIN bechlem_producer AS x ON(x.prod_id=s.prod_id)
			LEFT JOIN bechlem_categories_magento AS c ON(c.hersteller=x.name AND c.drucker=m.name)
		WHERE c.cat_id IS NULL GROUP BY m.name";	
			
		Mage::Log((string)$select, null, 'import.log');
		$result = $read->fetchAll($select);
		//Mage::Log($result, null, 'import.log');		
		foreach ($result as $row) {
			Mage::Log($row['printer'], null, 'import.log');
			$category = Mage::getModel('catalog/category')->loadByAttribute('name', $row['printer']);
			
			if($category && $category->getId()){
				Mage::Log($category->getId(), null, 'import.log');
			}else{
				Mage::Log('add category', null, 'import.log');
				$category = Mage::getModel('catalog/category');
				$category
					->setName($row['printer'])
					->setIsActive(1)
					->setIsAnchor(1)
					->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
					->setPrinterManufacturer($row['printer'])
					->setIncludeInMenu(1)
					->save();
				Mage::getResourceModel('catalog/category')->changeParent($category, $parent);		
			}
			Mage::Log('add model', null, 'import.log');

			$model = Mage::getModel('catalog/category');
			$model->setName($row['model'])
				->setIsActive(1)
				->setIsAnchor(1)
				->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
				->setPrinterManufacturer($row['printer'])
				->setIncludeInMenu(1)
				->save();
			
			Mage::getResourceModel('catalog/category')->changeParent($model, $category);
			
			Mage::Log('printer-'.$row['printer'], null, 'import.log');
			Mage::Log('model-'.$row['model'], null, 'import.log');	
			
			if($model && $model->getId()){
				Mage::Log('insert-start', null, 'import.log');
				$sql = "INSERT IGNORE INTO bechlem_categories_magento (`cat_id`, `hersteller`, `drucker`)
						VALUES ('".$model->getId()."', '{$row['printer']}', '{$row['model']}');";
				Mage::Log($sql, null, 'import.log');		
				$stmt = $write->query($sql);
				
				//$result = (int)$stmt->rowCount();
				Mage::Log('insert-end', null, 'import.log');
				//$write->insertMultiple('bechlem_categories_magento', $data);	// !! table name		
			}
			//break;
		}
		return $this->loadProducts();
    }
	
    public function loadProducts() 
	{
		Mage::Log('loadProducts', null, 'import.log');
		
		//$table = $this->getResource()->getTableName('bechlem_models');
		//for($i=0; $i<40; $i++){
		
			$products = array();
			//$limit = ($i == 0) ? 0 : $i*500;
			$read = $this->getResource()->getConnection('core_read'); 
			/*
			$select = "
			SELECT m.name AS model, s.name AS series, p.name AS producer, `a`.* 
				FROM `bechlem_article2model` AS `am` 
				INNER JOIN `bechlem_articles` AS `a` ON am.art_id = a.art_id 
				INNER JOIN `bechlem_models` AS `m` ON am.model_id = m.model_id
				INNER JOIN `bechlem_modelseries` AS `s` ON s.modelseries_id = m.modelseries_id
				INNER JOIN `bechlem_producer` AS `p` ON s.prod_id = p.prod_id 
				LIMIT {$limit}, 1000";*/
				
			$select = "
			SELECT a.syncartnr, a.art_id, a.new_art_id, a.ean, a.description, a.fulldescription, a.artnr, a.partnr, p.*
				FROM `bechlem_articles` AS a
				INNER JOIN `bechlem_articles_new` AS n ON (n.art_id = a.art_id)
				INNER JOIN `bechlem_hq_products` AS p ON (p.bechlem_id = a.new_art_id)";	
			
			Mage::Log((string)$select, null, 'import.log');
			
			$result = $read->fetchAll($select);
			//$_count = 42000;
			//Mage::Log($result, null, 'import.log');
			//$mage_product = Mage::getModel('catalog/product');
			foreach ($result as $row) {
				Mage::Log($row['art_id'], null, 'import.log');
				//$mage_productId = $mage_product->getIdBySku($row['new_art_id']);
				//if ($mage_productId) { // skip existing
                //    Mage::Log($row['art_id']." exists, skipping", null, 'import.log');
                //    continue;
				//}
				
				//$color = $row['color'];
				$price = $row['preis_netto'];// * 180 / 100;
		
                //$price *= 1.19;
                //$price = ceil($price)-0.01;
                //if ($price < 4) {
                //    $price = 3.99;
                //}
				//if(!isset($this->_color[$color])) {
				//	$this->_installOptionValue('color', $row['colorcode']);
				//	$this->_color[$color] = true;
				//}
				
				$product = $this->_default;
				
				//$product['original'] = 'Yes';
				$product['art_id'] = $row['art_id'];
                $product['sku'] = $row['id']; //$i;
				$product['name'] = $row['new_name'];//$row['bezeichnung'];
				$product['description'] = $row['fulldescription'];
				$product['short_description'] = $row['fulldescription'];
				$product['price'] = sprintf("%0.2f", $price);
				$product['weight'] = 1;
				$product['bechlem_id'] = $row['syncartnr'];
				$product['bechlem_artnr'] = $row['artnr'];
				$product['bechlem_partnr'] = $row['artnr'];
				//$product['free_shipping'] = 0;
				
				$product['ean'] = $row['ean'];
				$product['_attribute_set'] = 'Default';
				$product['_category'] = null;//$row['producer'].'/'.$row['series'].'/'.$row['model'];
				$product['color'] = $row['color'];
				$product['manufacturer'] = $row['marke'];
				//$product['original'] = ($row['istoriginal'] == 'True') ? '1' : '0';
				$product['type2'] = strtolower($row['type']);
				$product['url_key'] = strtolower($row['marke'].'-'.str_replace(' ', '-', $row['new_name']).'-'.$row['id']);
				$product['delivery_time'] = 'Sofort lieferbar';
				
				//$products[] = $product;
				$products[$row['artnr']] = $product;
				//$i++;
			}
			
			$data = new Mage_ImportExport_Model_Resource_Import_Data;
			$data->cleanBunches();
			$data->saveBunch('catalog_product', 'replace', $products);

			$importModel = Mage::getModel('importexport/import');
			$importModel->importSource();
			$importModel->invalidateIndex();
			if($importModel->getErrors()) Mage::Log($importModel->getErrors(), null, 'import.error.log');	
			Mage::Log($importModel->getFormatedLogTrace(), null, 'trace.log');
			//return true;		
		//}
			try {
				$this->loadProductsCategories($products);
			}catch (Exception $e) {
				Mage::Log($e->getMessage(), null, 'import.log');
			}
		
		return $this->loadBundles();
    }

    public function loadBundles() 
	{
		Mage::Log('loadBundles', null, 'import.log');
		$products = array();
		$read = $this->getResource()->getConnection('core_read'); 

			$select = "
			SELECT a.syncartnr, a.art_id, a.new_art_id, a.ean, a.description, a.fulldescription, a.artnr, a.partnr, p.*
			FROM `bechlem_hq_products_bundles` AS p
			INNER JOIN `bechlem_articles` AS a ON (p.bechlem_id = a.new_art_id)
			INNER JOIN `bechlem_articles_new` AS n ON (n.art_id = a.art_id)";	
			
			Mage::Log((string)$select, null, 'import.log');
			
			$result = $read->fetchAll($select);

			$mage_product = Mage::getModel('catalog/product');
			foreach ($result as $row) {

				$price = $row['price'];

				$product = $this->_default;

                $product['sku'] = $row['sku']; //$i;
				$product['art_id'] = $row['art_id'];
				$product['name'] = $row['new_name'];//$row['bezeichnung'];
				$product['description'] = $row['fulldescription'];
				$product['short_description'] = $row['fulldescription'];
				$product['price'] = sprintf("%0.2f", $price);
				$product['weight'] = 1;
				$product['bechlem_id'] = $row['syncartnr'];
				$product['bechlem_artnr'] = $row['artnr'];
				$product['bechlem_partnr'] = $row['artnr'];
				//$product['free_shipping'] = 0;
				
				$product['ean'] = $row['ean'];
				$product['_attribute_set'] = 'Default';
				$product['_category'] = null;//$row['producer'].'/'.$row['series'].'/'.$row['model'];
				$product['color'] = $row['color'];
				$product['manufacturer'] = $row['marke'];
				//$product['original'] = ($row['istoriginal'] == 'True') ? '1' : '0';
				$product['type2'] = strtolower($row['type']);
				$product['url_key'] = strtolower($row['marke'].'-'.str_replace(' ', '-', $row['new_name']).'-'.$row['sku']);
				$product['delivery_time'] = 'Sofort lieferbar';				
				
				$products[$row['artnr']] = $product;
				//$i++;
			}
			
			$data = new Mage_ImportExport_Model_Resource_Import_Data;
			$data->cleanBunches();
			$data->saveBunch('catalog_product', 'replace', $products);

			$importModel = Mage::getModel('importexport/import');
			$importModel->importSource();
			$importModel->invalidateIndex();
			if($importModel->getErrors()) Mage::Log($importModel->getErrors(), null, 'import.error.log');	
			Mage::Log($importModel->getFormatedLogTrace(), null, 'trace.log');
			$this->loadProductsCategories($products);
			return true;		
		//}
    }
	
	public function loadProductsCategories($products)
	{
		Mage::Log('loadProductsCategories', null, 'import.log');
		
		$read = $this->getResource()->getConnection('core_read'); 
		$write = $this->getResource()->getConnection('core_write'); 
			
		foreach($products as $product){
			Mage::Log('get product', null, 'import.log');
			$id = (int)Mage::getModel("catalog/product")->getIdBySku($product['sku']);
			Mage::Log($id, null, 'import.log');
			
			if(!$id) continue;
			
			$condition = array( 'product_id = ?' => $id );
			
			try {
				$write->delete('catalog_category_product', $condition);
			}catch (Exception $e) {}
			
			$select = "
			SELECT c.cat_id
			FROM bechlem_article2model AS am
				INNER JOIN bechlem_articles AS a ON(am.art_id=a.art_id)
				INNER JOIN bechlem_articles_new AS n ON (n.art_id = a.art_id)
				INNER JOIN bechlem_hq_products AS p ON(a.new_art_id=p.bechlem_id)
				INNER JOIN bechlem_models AS m ON(m.model_id=am.model_id)
				INNER JOIN bechlem_modelseries AS s ON(m.modelseries_id=s.modelseries_id)
				INNER JOIN bechlem_producer AS x ON(x.prod_id=s.prod_id)
				INNER JOIN bechlem_categories_magento AS c ON(c.hersteller=x.name AND c.drucker=m.name)
			WHERE a.art_id='{$product['art_id']}' GROUP BY m.name";	
			
			//Mage::Log($select, null, 'import.log');
			
			$result = null;
			try {
				$result = $read->fetchAll($select);
			}catch (Exception $e) {}
			
			if(!$result) continue;
			Mage::Log('insert category product', null, 'import.log');
			//$data = array();
			foreach ($result as $row) 
			{
				Mage::Log($row['cat_id'], null, 'import.log');
				try {
					$cat = null;
					Mage::Log('load', null, 'import.log');
					$cat = Mage::getModel('catalog/category')->load($row['cat_id']);
					Mage::Log('load-end', null, 'import.log');
					if (!empty($cat) && !empty($cat->getId())) {
						Mage::Log('insert', null, 'import.log');
						$sql = "INSERT IGNORE INTO dpm_catalog_category_product (`product_id`, `category_id`)
								VALUES ('{$id}', '{$row['cat_id']}');";
								
						Mage::Log($sql, null, 'import.log');
						try {
							$stmt = $write->query($sql);
						}catch (Exception $e) {}						
					}
				}catch (Exception $e) {
					Mage::Log($e->getMessage(), null, 'import.log');
					continue;
				}
			}
		}
	}
	
	protected function _installOptionValue($attribute, $label)
	{
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', $attribute);
		$id = $attribute->getAttributeId();
		$options = $attribute->getSource()->getAllOptions();

		foreach ($options as $option){
			if ($option['value']){
				if( strtolower($option['label']) == strtolower($label)) return $option['value'];
			}
		}
		$option = array();
		$option['attribute_id'] = $id;
		$option['value']['option'][0] = $label;

		$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
		$setup->addAttributeOption($option);
		
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($id);
		$options = $attribute->getSource()->getAllOptions();

		foreach ($options as $option){
			if ($option['value']){
				if( strtolower($option['label']) == strtolower($label)) return $option['value'];
			}
		}
	}	
}
