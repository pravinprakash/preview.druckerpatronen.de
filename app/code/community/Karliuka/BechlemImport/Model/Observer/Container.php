<?php 

class Karliuka_BechlemImport_Model_Observer_Container
	extends Varien_Event_Observer
{
    /**
     * add Button for Reference import.
     * @param object $observer The Observer object.
     * @return object Varien_Event_Observer.
    */	
    public function addButton($observer) 
	{
		$block = $observer->getEvent()->getBlock();
		if ($block instanceof Mage_Adminhtml_Block_Catalog_Product){
			$block->addButton('import_of_bechlemimport',  
				array(	'name'    => 'import_of_bechlemimport', 
						'title'   => 'Bechlem Import', 
						'type'    => 'button', 
						'label'   => Mage::helper('catalog')->__('Bechlem Import'),
						//‘onclick' => 'Bechlem.import(\''.$block->getUrl('adminhtml/bechlem_import’).’\’)’,
						'onclick'    => 'Bechlem.import(\''.Mage::getUrl('adminhtml/bechlem_import', array('_secure' => true)).'\');', 						
				));
			$block->getLayout()->getBlock('head')->addJs('karliuka/bechlem.js');
		}
		return $this;
    }
}