<?php

class Karliuka_BechlemImport_ProcessController 
	extends Mage_Core_Controller_Front_Action
{
    /**
     * Index action
    */
    public function indexAction()
    {
		Mage::Log('start', null, 'import.log');
		$import = Mage::getModel('bechlemimport/import');
		$import->loadCategories();
		//sleep(30);
		Mage::Log('end', null, 'import.log');
		$this->setProcessEndFlag();
		//
    }
	
	public function getLockDir()
	{
		return Mage::getBaseDir('var') . DS . 'locks' . DS;
	}

	public function getEndFile()
	{
		return $this->getLockDir() . 'end_import_bechlem.lock';
	}
	
	public function getLockFile()
	{
		return $this->getLockDir() . 'import_bechlem.lock';
	}
	
	protected function setProcessEndFlag()
	{
		@unlink($this->getLockFile());
		return fopen($this->getEndFile(), 'x');
	}
}