<?php

class Karliuka_BechlemImport_Adminhtml_Bechlem_ImportController 
	extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
		$import = Mage::getModel('bechlemimport/import');
		$import->loadCategories();
		
		//if (!$this->isActiveDaemon()) $this->runDaemon();
		//if($this->isActiveDaemon()) sleep(10);
		$this->getResponse()->setBody(json_encode(
			array(
				'error' => 0, 
				'action' => 'end'//($this->isStopDaemon() && !$this->isActiveDaemon()) ? 'end' : 'import'
		)));
    }
	
	public function getLockDir()
	{
		return Mage::getBaseDir('var') . DS . 'locks' . DS;
	}

	public function getLockFile()
	{
		return $this->getLockDir() . 'import_bechlem.lock';
	}
	
	public function getEndFile()
	{
		return $this->getLockDir() . 'end_import_bechlem.lock';
	}
	
	public function isStopDaemon()
	{
		return file_exists($this->getEndFile());
	}
	
	public function isActiveDaemon()
	{
		$file = new Varien_Io_File();
		$file->mkdir($this->getLockDir());
		return file_exists($this->getLockFile());
	}	

	public function runDaemon()
	{
		@unlink($this->getEndFile());
		$fp = fopen($this->getLockFile(), 'x');
		if ($fp != false){
			$code = Mage::getSingleton("core/session")->getEncryptedSessionId();
			fputs($fp, $code); 
			fclose($fp);
			$curl = curl_init(Mage::getUrl('bechlemimport/process'), array('_secure' => true));
			if ($curl){
				curl_setopt($curl, CURLOPT_TIMEOUT, 5);
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);   
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array('code' => $code), null, '&'));
				curl_exec($curl);
			}
		}
	}	
}