<?php

/**
 * Kit Hy
 * Enhanced Search module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   hyteckit 
 * @package    hyteckit_EnhancedSearch
 * @copyright  Copyright (c) 2008 Kit Hy
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class hyteckit_EnhancedSearch_Model_Query
    extends Mage_CatalogSearch_Model_Query
{
    protected function _construct()
    {
        $this->_init('catalogsearch/query');
    }

    /**
     * Retrieve collection of search results
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getResultCollection()
    {
    	#echo "ENHANCED QUERY\n";
        $collection = $this->getData('result_collection');
        if (is_null($collection)) {
            $collection = Mage::getResourceModel('catalogsearch/search_collection');

			/*
            $text = $this->getSynonimFor();
            if (!$text) {
                $text = $this->getQueryText();
            }
            */
			$text = $this->getQueryText();
			
            $collection->addSearchFilter($text)
                ->addStoreFilter()
                ->addMinimalPrice()
                ->addTaxPercents();
            $this->setData('result_collection', $collection);
        }
        return $collection;
    }
}

?>