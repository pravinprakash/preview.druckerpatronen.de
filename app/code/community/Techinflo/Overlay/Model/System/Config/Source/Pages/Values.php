<?php

class Techinflo_Overlay_Model_System_Config_Source_Pages_Values
{
    public function toOptionArray()
    {
		//Get current store id
		$storeId = Mage::app()->getStore()->getId();
		//Get cms page collection for current store
		$collection = Mage::getModel('cms/page')->getCollection()
              //->addStoreFilter($storeId)
              ->addFieldToFilter('is_active',1)
              ->addFieldToFilter('identifier',array(array('nin'=>array('enable-cookies'))));
		$cms_pages_list = array();
		if($collection->getSize()){
			$cms_pages_list = $collection->toOptionArray();
		}
		$extra_pages_list = array(
			array(
                'value' => 'category',
                'label' => 'Category Page',
            ),
			array(
                'value' => 'product',
                'label' => 'Product Page',
            ),
            array(
                'value' => 'checkout',
                'label' => 'Checkout',
            )
			);
		$allPages = array_merge($cms_pages_list,$extra_pages_list);
		if(count($allPages))
			return $allPages;
		else 
			return;
       
    }
}