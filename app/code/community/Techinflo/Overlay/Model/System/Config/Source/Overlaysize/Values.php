<?php

class Techinflo_Overlay_Model_System_Config_Source_Overlaysize_Values
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'percent',
                'label' => 'Percent',
            ),
            array(
                'value' => 'pixel',
                'label' => 'Pixel',
            ),
        );
    }
}