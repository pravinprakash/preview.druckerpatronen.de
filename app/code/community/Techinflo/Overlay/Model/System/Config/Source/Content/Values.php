<?php

class Techinflo_Overlay_Model_System_Config_Source_Content_Values
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'static',
                'label' => 'Static Block',
            ),
            array(
                'value' => 'cms',
                'label' => 'CMS Page',
            ),
        );
    }
}