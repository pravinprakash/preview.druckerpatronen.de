<?php
/**
 * @category     Techinflo
 * @package     Techinflo Overlay
 * @author      <Techinflo Team>
 */
class Techinflo_Overlay_Block_View extends Mage_Core_Block_Template
{	
	
	public function __construct()
	{	
		
	}
	public function getOverlayContent($t1,$oN){
		$enable = $this->getIsActive();
		$static_block_id =  $this->getStaticBlockId($oN);
		if(!$enable) return false;
		//if(!$t1)return false;
		return $this->getLayout()->createBlock('cms/block')->setBlockId($static_block_id)->toHtml();
		
	}
	public function getIsActive(){
		if(Mage::getStoreConfig("techinflo/general/active")) 
			return true;
		else 
			return false;
	}
	public function getContentMode(){
		if(Mage::getStoreConfig("techinflo/overlay/dropdown_overlay_content"))
			return Mage::getStoreConfig("techinflo/overlay/dropdown_overlay_content");
		else
			return "";
	}
	public function getStaticBlockId($oN){
		$field = 'techinflo/overlay_'.$oN.'/text_field_static_block_'.$oN;
		if(Mage::getStoreConfig($field))
			return Mage::getStoreConfig($field);
		else
			return '';
	}
	public function getCmsPageId(){
		if(Mage::getStoreConfig("techinflo/overlay/dependant_text_field_cmspage"))
			return Mage::getStoreConfig("techinflo/overlay/dependant_text_field_cmspage");
		else
			return '';
	}
	public function getOverlaySizeIn($oN){
		$field= 'techinflo/overlay_'.$oN.'/dropdown_'.$oN;
		return Mage::getStoreConfig($field);
	}
	public function getOverlayHeight($oN){
		if($this->getOverlaySizeIn($oN) == "pixel"){
			$height= 'techinflo/overlay_'.$oN.'/text_field_height_'.$oN;
			if(Mage::getStoreConfig($height))
				return Mage::getStoreConfig($height).'px';
			else
				return "auto";
		}
		elseif($this->getOverlaySizeIn($oN) == "percent"){
			$height= 'techinflo/overlay_'.$oN.'/text_field_height_'.$oN;
			if(Mage::getStoreConfig($height))
				return Mage::getStoreConfig($height).'%';
			else
				return "auto";
		}
	}
	public function getOverlayWeight($oN){
		if($this->getOverlaySizeIn($oN) == "pixel"){
			$width = 'techinflo/overlay_'.$oN.'/text_field_width_'.$oN;
			if(Mage::getStoreConfig($width))
				return Mage::getStoreConfig($width).'px';
			else
				return "auto";
		}
		elseif($this->getOverlaySizeIn($oN) == "percent"){
			$width = 'techinflo/overlay_'.$oN.'/text_field_width_'.$oN;
			if(Mage::getStoreConfig($width))
				return Mage::getStoreConfig($width).'%';
			else
				return "auto";
		}
	}
	public function getOverlayTitle($oN){
		$show_title = 'techinflo/overlay_'.$oN.'/boolean_'.$oN;
		if(Mage::getStoreConfig($show_title)) {
			$field = 'techinflo/overlay_'.$oN.'/dependant_text_field_'.$oN;
			return Mage::getStoreConfig($field);
		}
		else {
			return false;
		}
	}
	public function getOverlayPages($oN){
		/* $oN = Overlay Num. */
		if(!$oN){
			return array();
		}
		if(!$this->getStaticBlockId($oN)){
			return array();
		}
		$pages = array();
		$field = 'techinflo/overlay_'.$oN.'/multiple_dropdown_'.$oN;
		$pages = explode(',',Mage::getStoreConfig($field));
		return $pages;
		
	}
	public function getCategoryIdentifire(){
		if(Mage::getStoreConfig("techinflo/overlay/text_field_category")) {
			return Mage::getStoreConfig("techinflo/overlay/text_field_category");
		}
		else {
			return false;
		}
	}
	public function getProductIdentifire(){
		if(Mage::getStoreConfig("techinflo/overlay/text_field_product")) {
			return Mage::getStoreConfig("techinflo/overlay/text_field_product");
		}
		else {
			return false;
		}
	}
	public function getCookieExpireTime($oN){
		if($this->getCookieName($oN) == "techinfloOverlay"){
			if(Mage::getStoreConfig("techinflo/general/text_field_cookie_time")) {
				return Mage::getStoreConfig("techinflo/general/text_field_cookie_time");
			}
			else {
				return false;
			}
		}
		else{
			$field= 'techinflo/overlay_'.$oN.'/text_field_cookie_time_'.$oN;
			if(Mage::getStoreConfig($field)) {
				return Mage::getStoreConfig($field);
			}
			else {
				return false;
			}
		}
	}
	public function getCookieExpireTimeCommon(){
		if(Mage::getStoreConfig("techinflo/general/text_field_cookie_time")) {
			return Mage::getStoreConfig("techinflo/general/text_field_cookie_time");
		}
		else {
			return false;
		}
	}
	
	public function getCookieName($oN){
		$active_common_field = 'techinflo/general/active_common_cookie';
		$active_common = Mage::getStoreConfig($active_common_field);
		
		if($active_common){
			return 'techinfloOverlay';
		}
		$field = 'techinflo/overlay_'.$oN.'/text_field_cookie_name_'.$oN;
		if(Mage::getStoreConfig($field)) {	
			return Mage::getStoreConfig($field);
		}	
	}
}
?>