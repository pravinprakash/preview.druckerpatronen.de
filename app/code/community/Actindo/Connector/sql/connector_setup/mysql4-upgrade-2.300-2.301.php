<?php
    /**
     * Actindo Faktura/WWS Connector
     *
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     */
    $installer = $this;
    $installer->startSetup();
    $installer->run("
        DROP TABLE IF EXISTS {$this->getTable('connector/accustmap')};
        CREATE TABLE {$this->getTable('connector/accustmap')} (
            `id` int(11) NOT NULL,
          `deb_kred_id` int(11) NOT NULL,
          PRIMARY KEY `id` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
    $installer->endSetup();
