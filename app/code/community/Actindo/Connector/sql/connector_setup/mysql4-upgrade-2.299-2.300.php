<?php
    /**
     * Actindo Faktura/WWS Connector
     *
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     */
    $installer = $this;
    $installer->startSetup();
    $installer->run("
        DROP TABLE IF EXISTS {$this->getTable('connector/acmapper')};
        CREATE TABLE {$this->getTable('connector/acmapper')} (
            `id` int(11) NOT NULL auto_increment,
            `value` varchar(255) NOT NULL,
            `type` VARCHAR(32) NOT NULL,
            `site` int(11) NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`)
        )
        ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
    $installer->endSetup();