<?php
    /**
     * Actindo Faktura/WWS Connector
     *
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     */
    $installer = $this;
    $installer->startSetup();
    $installer->run("
        DROP TABLE IF EXISTS {$this->getTable('connector/ackeyvaluemap')};
        CREATE TABLE {$this->getTable('connector/ackeyvaluemap')} (
            `id` int(11) NOT NULL AUTO_INCREMENT,
          `type` VARCHAR(32) NOT NULL,
          `ident` VARCHAR(32) NOT NULL,
          `value` TEXT,
          PRIMARY KEY `id` (`id`),
          UNIQUE KEY `type` (`type`,`ident`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
    $installer->endSetup();
