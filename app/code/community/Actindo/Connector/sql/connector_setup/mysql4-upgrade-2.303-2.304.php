<?php
/**
 * Actindo Faktura/WWS Connector
 *
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 */
$installer = $this;
$installer->run("
            DROP TABLE IF EXISTS {$this->getTable('connector/actaxordermap')};
            CREATE TABLE {$this->getTable('connector/actaxordermap')} (
              `orderid` int(11) NOT NULL,
              `tax` int(11) NOT NULL DEFAULT 0,
              `shipping` int(11) NOT NULL DEFAULT 0,
              PRIMARY KEY (`orderid`)
            );
            ALTER TABLE  {$this->getTable('connector/acmapper')} ADD UNIQUE (`value` ,`type` ,`site`);
        ");
$installer->startSetup();
$installer->endSetup();