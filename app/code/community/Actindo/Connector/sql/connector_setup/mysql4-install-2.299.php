<?php
    /**
     * Actindo Faktura/WWS Connector
     *
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     */
    $installer = $this;
    $installer->startSetup();
    $installer->run('ALTER TABLE '.$this->getTable('api_user').' ADD COLUMN actindo_key VARCHAR(32) NOT NULL DEFAULT \'\';');
    $installer->endSetup();