<?php

    /**
     * Actindo Faktura/WWS Connector
     * admin url input field
     * extracts the current site from the magento shop
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Block_Adminhtml_System_Config_Url extends Mage_Adminhtml_Block_System_Config_Form_Field{
        /**
         * Prepare Layout related task (setting template etc
         * @return $this|Mage_Core_Block_Abstract
         */
        protected function _prepareLayout(){
            parent::_prepareLayout();
            if (!$this->getTemplate()) {
                $this->setTemplate('actindo/system/config/url.phtml');
            }
            return $this;
        }
        /**
         * render output
         * @param Varien_Data_Form_Element_Abstract $element
         *
         * @return string
         */
        public function render(Varien_Data_Form_Element_Abstract $element){
            $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
            return parent::render($element);
        }
        /**
         * returns the elements html code
         * @param Varien_Data_Form_Element_Abstract $element
         *
         * @return string
         */
        protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element){

            return $this->_toHtml();
        }
        /**
         * returns the current websites scope
         * @param null $website
         *
         * @return mixed|string
         */
        public function getCurrentWebsiteScope($website = null){
            if(is_null($website)){
                $website = $this->getRequest()->getParam('website');
            }
            $websiteModel = Mage::getModel('core/website')->getCollection()->addFieldToFilter('code',$website)->getFirstItem();
            $scopeModel = Mage::getModel('core/store_group')->load($websiteModel->getDefaultGroupId());
            $store = Mage::getModel('core/store')->load($scopeModel->getDefaultStoreId());
            $useAttachUrl = Mage::getStoreConfig('web/url/use_store',$store->getId());
            $useAttachUrl = (int)$useAttachUrl>0;
            $url = Mage::getStoreConfig('web/secure/base_url',$store->getId());
            $url.='index.php/'.(($useAttachUrl)?$store->getCode().'/':'').'actindo/connector/xmlrpcServerPhp/';
            return $url;
        }
    }