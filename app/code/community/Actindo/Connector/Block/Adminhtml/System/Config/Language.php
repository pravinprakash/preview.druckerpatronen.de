<?php

/**
 * Actindo Faktura/WWS Connector
 * admin url input field
 * extracts the current site from the magento shop
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @version 2.309
 */
    class Actindo_Connector_Block_Adminhtml_System_Config_Language extends Mage_Adminhtml_Block_System_Config_Form_Fieldset{
        protected $dummyElement;
        protected $fieldRenderer;
        protected $values;
        const XML_PATH = 'actindo/storemapping/';
        const XML_DEFAULT = 'actindo/general/defaultlang';

        public function render(Varien_Data_Form_Element_Abstract $element)
        {
            $html = $this->_getHeaderHtml($element);
            //here you cand loop through all the fields you want to add
            //for each element you neet to call $this->_getFieldHtml($element, $group);

            $website = $this->getRequest()->getParam('website');
            $websiteModel = Mage::getModel('core/website')->getCollection()->addFieldToFilter('code',$website)->getFirstItem();
            $stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('website_id',$websiteModel->getWebsiteId());
            foreach ($stores as $store) {
                $html.= $this->_getFieldHtml($element, $store);
            }
            $html .= $this->_getFooterHtml($element);

            return $html;
        }
        //this creates a dummy element so you can say if your config fields are available on default and website level - you can skip this and add the scope for each element in _getFieldHtml method
        protected function _getDummyElement()
        {
            if (empty($this->_dummyElement)) {
                $this->_dummyElement = new Varien_Object(array('show_in_default'=>0, 'show_in_website'=>1,'show_in_store'=>0));
            }
            return $this->_dummyElement;
        }
        //this sets the fields renderer. If you have a custom renderer tou can change this.
        protected function _getFieldRenderer()
        {
            if (empty($this->_fieldRenderer)) {
                $this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
            }
            return $this->_fieldRenderer;
        }
        //this is usefull in case you need to create a config field with type dropdown or multiselect. For text and texareaa you can skip it.
        protected function _getValues()
        {

            $keymaps = Mage::getModel('connector/ackeyvaluemap')
                ->getCollection()
                ->addFieldToFilter('type','actindoLang');
            $returnValue = array(
                array(
                    'value'=>'default',
                    'label'=>'Standard Übersetzungswert',
                )
            );
            if(count($keymaps)>0){
                foreach($keymaps as $keymap){
                    $returnValue[] = array(
                        'value'=>$keymap->getValue(),
                        'label'=>$keymap->getIdent(),
                    );
                }
            }
            return $returnValue;
        }
        //this actually gets the html for a field
        protected function _getFieldHtml($fieldset, $store)
        {
            $configData = $this->getConfigData();
            $path = self::XML_PATH.'language_'.$store->getCode();//this value is composed by the section name, group name and field name. The field name must not be numerical (that's why I added 'group_' in front of it)
            if (isset($configData[$path])) {
                $data = $configData[$path];
            } else {
                $data = 'default';
            }
            $group = Mage::app()->getGroup($store->getGroupId());
            $e = $this->_getDummyElement();//get the dummy element
            $site = Mage::app()->getWebsite($store->getWebsiteId());
            $field = $fieldset->addField($store->getId(), 'select',//this is the type of the element (can be text, textarea, select, multiselect, ...)
                array(
                    'name'          => 'groups[storemapping][fields][language_'.$store->getCode().'][value]',//this is groups[group name][fields][field name][value]
                    'label'         => $store->getName().' ('.$site->getName().' - ' .$group->getName().')',//this is the label of the element
                    'value'         => $data,//this is the current value
                    'values'        => $this->_getValues(),//this is necessary if the type is select or multiselect
                ))->setRenderer($this->_getFieldRenderer());

            return $field->toHtml();
        }
    }