<?php
    define('ACTINDO_CONNECTOR_OVERWRITE',true);
    /**
     * Actindo Faktura/WWS Connector
     * main action controller that the actindo erp accesses
     * ==> Main Entry Point
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Mage_Core_Controller_Front_Action
     * @version 2.309
     */
    class Actindo_Connector_ConnectorController extends Mage_Core_Controller_Front_Action{
        /**
         * Index Action keeps people out of the connector
         * so nobody can access any default actions
         */
        public function indexAction(){
            $this->_redirect('');
        }
        /**
         * developer Actinon controller to output developer specific informations
         */
        public function devAction(){
            echo 'empty!';
            die;
        }
        /**
         * Main Working Action and Main entry point for Actindo ERP
         * it's output is printed as valid xml document
         * @uses Actindo_Connector_Model_Components_XmlRpc_Server
         * @uses Actindo_Connector_Model_Components_XmlRpc_Request
         * @uses Actindo_Connector_Model_Auth_Util
         * @uses Actindo_Connector_Model_Exception_Error
         */
        public function xmlrpcServerPhpAction(){
            Actindo_Connector_Model_Data::getStoreConfig();
            Actindo_Connector_Model_System_Service::setStoreId(Mage::app()->getStore()->getStoreId());
            /**
             * check crypt mode
             */
            if(null !== $this->getRequest()->getParam('get_cryptmode')) {
                $url = 'cryptmode=MD5&connector_type=XMLRPCUTF8';
                if(Actindo_Connector_Model_System_Service::getMethodActive('pushpull'))
                {
                    $url .= '&extended=yes';
                }
                die($url);
            }
            /**
             * Initialize server and request
             */
            $server = new Actindo_Connector_Model_Components_XmlRpc_Server();
            $request = new Actindo_Connector_Model_Components_XmlRpc_Request(null, null, $this->getRequest()->getServer());
            /**
             * Do authentification
             */
            try {
                $request = Actindo_Connector_Model_Auth_Util::checkAuth($request);
            } catch(Actindo_Connector_Model_Exception_Error $e) {
                $response = $server->fault($e);
            }
            /**
             * if no problems appeared handle response
             */
            if(!isset($response)) {
                $response = $server->handle($request);
            }
            // return response
            echo $response;
        }
        /**
         * Method for checking current Site/Store/view
         */
        public function showStoreAndViewAction(){
            print 'Site: '.Mage::app()->getWebsite()->getName()."\n<br />";
            print 'Store: '.Mage::app()->getStore()->getName()."\n<br />";
            print 'View: '.Mage::app()->getStore()->getGroup()->getName()."\n<br>";
        }
    }