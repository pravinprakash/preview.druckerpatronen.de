<?php
    /**
     * Actindo Faktura/WWS Connector
     * class for handling xmlrpc requests
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Zend_XmlRpc_Request_Http
     * @version 2.309
     */
    class Actindo_Connector_Model_Components_XmlRpc_Request extends Zend_XmlRpc_Request_Http{
        protected $compression = null;
        /**
         * Create a new XML-RPC request
         * @param string $method (optional)
         * @param array $params  (optional)
         * @param array $server (optional) $_SERVER array (used to detect content compression)
         */
        public function __construct($method = null, $params = null, $server = null) {
            if($server !== null) {
                if(isset($server['HTTP_CONTENT_ENCODING'])
                    && strtolower($server['HTTP_CONTENT_ENCODING']) == 'gzip')
                {
                    $this->compression = 'gzip';
                }
            }

            parent::__construct($method, $params);
        }
        /**
         * load XML data into internal object
         * extends the parent function to handle compressed xml documents
         * @param string $request
         * @uses parent::loadXML
         * @return bool
         */
        public function loadXML($request) {
            switch($this->compression) {
                case 'gzip':
                    if(function_exists('gzinflate') && ($inflated = @gzinflate(substr($request, 10)))) {
                        $request = $inflated;
                    }
                    break;
            }

            return parent::loadXML($request);
        }
    }