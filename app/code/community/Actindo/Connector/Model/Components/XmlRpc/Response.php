<?php
    /**
    * Actindo Faktura/WWS Connector
    * import product
    * Imports Products from Actindo ERP to Shop
    * Extends the Product Export
    * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
    * @author Daniel Haimerl
    * @author Patrick Prasse
    * @author Christopher Westerfield
    * @uses Actindo_Connector_Model_Service_Product_Export
    * @version 2.309
    */

class Actindo_Connector_Model_Components_XmlRpc_Response extends Zend_XmlRpc_Response{
    /**
     * Override __toString() to send HTTP Content Type Header
     */
    public function __toString(){
        /**
         * removed according to customer notes about double headers with fcgi
         *if (!headers_sent()) {
         *   header('Content-Type: text/xml; charset=' . strtolower($this->getEncoding()));
         * }
         */
        return parent::__toString();
    }
}