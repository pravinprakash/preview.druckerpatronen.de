<?php
/**
 * Actindo Faktura/WWS Connector
 * Customer Export Helper
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 */
    class Actindo_Connector_Model_Customer_Exporter{
        /**
         * Var collection
         */
        protected $deb_kred_id=0,
                  $_customers_id='',
                  $anrede='Herr',
                  $kurzname='',
                  $firma='',
                  $vorname='',
                  $name='',
                  $adresse='',
                  $adresse2='',
                  $plz='',
                  $ort='',
                  $land='',
                  $tel='',
                  $fax='',
                  $ustid='',
                  $email='',
                  $print_brutto,
                  $currency=0,
                  $preisgruppe,
                  $gebdat='0000-00-00',
                  $delivery_anrede,
                  $delivery_addresses=array(),
                  $delivery_id='',
                  $delivery_kurzname='',
                  $delivery_firma='',
                  $delivery_name='',
                  $delivery_vorname='',
                  $delivery_adresse='',
                  $delivery_adresse2='',
                  $delivery_plz='',
                  $delivery_ort='',
                  $delivery_land='',
                  $delivery_tel='',
                  $delivery_fax='',
                  $delivery_ustid = '',
                  $mode=false,
                  $custid=-1,
                  $result=false;

        /**
         * Constructor
         */
        public function __construct(){

        }
        /**
         * If you need a complete Object Set call this method (else call set simple object
         * By default a simple object set is called
         */
        public function setCompleteObject(){
            $this->mode = true;
        }
        /**
         * If you need only a simple set for exporting customers call this function else call set complete object
         * By default a simple object set is called
         */
        public function setSimpleObject(){
            $this->mode = false;
        }
        /**
         * Set Customer Id
         * @param $id int Customer ID
         */
        public function setCustomerId($id){
            $this->custid = (int)$id;
        }
        /**
         * Set the Customer Currency
         * @param $currency mixed currency
         */
        public function setCurrency($currency){
            $this->currency = $currency;
        }
        /**
         * Method Populates the object with all customer data
         * if it succeeds it returns true, else false
         * @return bool
         */
        public function populate($order=null){
            $customer = Mage::getModel('customer/customer')->load((int)$this->custid);
            if($order!==null){
                $custid = $order->getCustomerId();
                if(!empty($custid)){
                    $this->_customers_id = $custid;
                }
                $this->email = $order->getCustomerEmail();
                $billing = $order->getBillingAddress()->toArray();
                $shipping = $order->getShippingAddress()->toArray();
                if(empty($shipping)){
                    $shipping = $billing;
                }
                $this->anrede = empty($billing['prefix'])?'':$billing['prefix'];
                $this->kurzname = (empty($billing['company']))?$billing['firstname'].' '.$billing['lastname']:$billing['company'];
                $this->firma = $billing['company'];
                $this->vorname = $billing['firstname'];
                $this->name = $billing['lastname'];
                $this->adresse = $billing['street'];
                $this->plz = $billing['postcode'];
                $this->ort = $billing['city'];
                $this->land = $billing['country_id'];
                $this->tel = ''.$billing['telephone'];
                $this->fax = ''.$billing['fax'];
                $this->ustid = ''.$billing['vat_id'];
                $this->print_brutto = 1;
                $this->delivery_anrede = empty($shipping['prefix'])?'':$shipping['prefix'];
                $this->delivery_kurzname = (empty($shipping['company']))?$shipping['firstname'].' '.$shipping['lastname']:$shipping['company'];
                $this->delivery_firma = $shipping['company'];
                $this->delivery_vorname = $shipping['firstname'];
                $this->delivery_name = $shipping['lastname'];
                $this->delivery_adresse = $shipping['street'];
                $this->delivery_plz = $shipping['postcode'];
                $this->delivery_ort = $shipping['city'];
                $this->delivery_land = $shipping['country_id'];
                $this->delivery_tel = $shipping['telephone'];
                $this->delivery_fax = $shipping['fax'];
                $this->delivery_ustid = $shipping['vat_id'];
                foreach($this as $key=>$value){
                    if(empty($value) && $value!==0){
                        $this->$key = (string)'';
                    }
                }
            }
            if($customer->getId()>0 && $order!==null){
                $custgroup = (int)$customer->getGroupId();
                //$custgroup += 250;
                $this->preisgruppe = $custgroup;
            }else if($customer->getId()>0){
                //get Primary Address
                $address = $customer->getPrimaryBillingAddress();
                //get Addition Addresses
                $addressRows = $customer->getAddressesCollection();
                //check if Pirmary Address is not false
                if($address===false){
                    //check if at least one entry exists
                    if(count($addressRows)>0){
                        foreach($addressRows as $key){
                            $address = $key;
                            break;
                        }
                    }else{
                        $this->result = false;
                        return false;
                    }
                }
                //get currency symbol
                //Process
                $company = $address->getCompany();
                $this->firma = (string)$company;
                $street = $address->getStreet();
                if(!isset($street[1]))
                    $street[1] = '';
                $this->adresse = $street[0];
                $this->adresse2 = $street[1];
                $this->print_brutto = 1;
                $custgroup = (int)$customer->getGroupId();
                //Build Main Data Block
                $anrede = $address->getPrefix();
                $this->anrede = (empty($anrede))?'':$anrede;
                $this->vorname = (string)$address->getFirstname();
                $this->kurzname = (string)((!empty($company))?$company:$address->getFirstname().' '.$address->getLastname());
                $this->name = (string)$address->getLastname();
                $this->plz = (string)$address->getPostcode();
                $this->ort = (string)$address->getCity();
                $this->land = (string)$address->getCountryId();
                $this->tel = (string)$address->getTelephone();
                $this->fax = (string)$address->getFax();
                $this->ustid = (string)$address->getVatId();
                $this->email = (string)$customer->getEmail();
                $this->gebdat = (string)$customer->getDob();
                $this->ustid = ''.(string)$address->getVatId();
                $this->preisgruppe = $custgroup;
                $debKredId = (int)$this->getCustomerKredId((int)$customer->getId());
                if($debKredId>0){
                    $this->deb_kred_id = (int)$debKredId;
                }
                $this->_customers_id = (int)$customer->getId();
                $company = $address->getCompany();
                $street = $address->getStreet();
                if(!isset($street[1]))
                    $street[1] = '';
                $firstname = $address->getFirstname();
                $lastname = $address->getLastname();
                $plz = $address->getPostcode();
                $city = $address->getCity();
                $land = $address->getCountryId();
                $tel = ''.$address->getTelephone();
                $fax = ''.$address->getFax();
                $this->delivery_id = $address->getId();
                $this->delivery_kurzname = (!empty($company)?$company:sprintf('%s %s', $firstname, $lastname));
                $this->delivery_name = ((!empty($lastname))?$lastname:'');
                $this->delivery_vorname = ((!empty($firstname))?$firstname:'');
                $this->delivery_adresse = $street[0];
                $this->delivery_adresse2 = $street[1];
                $this->delivery_plz = ((!empty($plz))?$plz:'');
                $this->delivery_ort = ((!empty($city))?$city:'');
                $this->delivery_land = ((!empty($land))?$land:'');
                foreach ($addressRows as $addressRow){
                    $company = $addressRow->getCompany();
                    $street = $addressRow->getStreet();
                    $firstname = $addressRow->getFirstname();
                    $lastname = $addressRow->getLastname();
                    $plz = $addressRow->getPostcode();
                    $city = $addressRow->getCity();
                    $land = $addressRow->getCountryId();
                    $tel = ''.$addressRow->getTelephone();
                    $fax = ''.$addressRow->getFax();
                    $ustid = $addressRow->getVarId();
                    if(!isset($street[1]))
                        $street[1] = '';
                    $addressArray = array(
                        'delivery_id'=>$addressRow->getId(),
                        'delivery_kurzname'=>(!empty($company)?$company:sprintf('%s %s', $firstname, $lastname)),
                        'delivery_firma'=>((!empty($company))?$company:''),
                        'delivery_name'=>((!empty($lastname))?$lastname:''),
                        'delivery_vorname'=>((!empty($firstname))?$firstname:''),
                        'delivery_adresse'=>$street[0],
                        'delivery_adresse2'=>$street[1],
                        'delivery_plz'=>((!empty($plz))?$plz:''),
                        'delivery_ort'=>((!empty($city))?$city:''),
                        'delivery_land'=>((!empty($land))?$land:''),
                        'delivery_tel'=>$tel,
                        'delivery_fax' => $fax,
                        'delivery_ustid'=>$ustid,
                    );
                    $this->delivery_addresses[] = $addressArray;
                }
                $this->result = true;
                return true;
            }else{
                $this->result = false;
                return false;
            }
        }
        /**
         * Returns the Customers deb_kred_id
         * @param int $id
         * @return int|bool
         */
        protected function getCustomerKredId($id){
            $model = Mage::getModel('connector/accustmap')
                ->getCollection()
                ->addFieldToFilter('id',(int)$id)
                ->getFirstItem();
            if((int)$model->getId()>0){
                return $model->getDebKredId();
            }else{
                return false;
            }
        }
        /**
         * Returns the Data Set as array or boolean false if the populate method was not called or something went wrong!
         * @return array|bool
         */
        public function toArray(){
            if($this->mode){
                $notneeded = array(
                    'mode',
                );
            }else{
                $notneeded = array(
                    'mode',
                    'tel',
                    'fax',
                    'ustid',
                    'gebdat',
                    'delivery_id',
                    'delivery_kurzname',
                    'delivery_firma',
                    'delivery_name',
                    'delivery_vorname',
                    'delivery_adresse',
                    'delivery_adresse2',
                    'delivery_plz',
                    'delivery_ort',
                    'delivery_land',
                    'delivery_addresses',
                    'print_brutto',
                    'currency',
                    'ustid',
                    'adresse2',
                    'preisgruppe',
                );
            }
            $result = array();
            foreach($this as $key=>$value){
                if(!in_array($key,$notneeded)){
                    $result[$key] = $value;
                }
            }
            return $result;
        }
    }