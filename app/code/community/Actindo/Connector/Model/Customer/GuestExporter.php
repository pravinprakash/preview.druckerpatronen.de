<?php
/**
 * Actindo Faktura/WWS Connector
 * Guest User Helper
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 * @abstract
 */
class Actindo_Connector_Model_Customer_GuestExporter{
    /**
     * Var collection
     */
    protected $deb_kred_id=0,
        $_customers_id=0,
        $anrede='Herr',
        $kurzname='',
        $firma='',
        $vorname='',
        $name='',
        $adresse='',
        $adresse2='',
        $plz='',
        $ort='',
        $land='',
        $tel='',
        $fax='',
        $ustid='',
        $email='',
        $print_brutto,
        $currency='EUR',
        $preisgruppe,
        $gebdat='0000-00-00',
        $delivery_anrede,
        $delivery_id='',
        $delivery_kurzname='',
        $delivery_firma='',
        $delivery_name='',
        $delivery_vorname='',
        $delivery_adresse='',
        $delivery_adresse2='',
        $delivery_plz='',
        $delivery_ort='',
        $delivery_land='',
        $delivery_tel='',
        $delivery_fax='',
        $delivery_ustid = '',
        $mode=false;

    /**
     * Constructor
     */
    public function __construct($order){
        $custid = $order->getCustomerId();
        if(!empty($custid)){
            $this->_customers_id = $custid;
        }
        $this->email = $order->getCustomerEmail();
        $billing = $order->getBillingAddress()->toArray();
        $shipping = $order->getShippingAddress()->toArray();
        if(empty($shipping)){
            $shipping = $billing;
        }
        $this->anrede = empty($billing['prefix'])?'':$billing['prefix'];
        $this->kurzname = (empty($billing['company']))?$billing['firstname'].' '.$billing['lastname']:$billing['company'];
        $this->firma = $billing['company'];
        $this->vorname = $billing['firstname'];
        $this->name = $billing['lastname'];
        $this->adresse = $billing['street'];
        $this->plz = $billing['postcode'];
        $this->ort = $billing['city'];
        $this->land = $billing['country_id'];
        $this->tel = ''.$billing['telephone'];
        $this->fax = ''.$billing['fax'];
        $this->ustid = ''.$billing['vat_id'];
        $this->print_brutto = 1;
        $this->preisgruppe =  250;
        $this->delivery_anrede = empty($shipping['prefix'])?'':$shipping['prefix'];
        $this->delivery_kurzname = (empty($shipping['company']))?$shipping['firstname'].' '.$shipping['lastname']:$shipping['company'];
        $this->delivery_firma = $shipping['company'];
        $this->delivery_vorname = $shipping['firstname'];
        $this->delivery_name = $shipping['lastname'];
        $this->delivery_adresse = $shipping['street'];
        $this->delivery_plz = $shipping['postcode'];
        $this->delivery_ort = $shipping['city'];
        $this->delivery_land = $shipping['country_id'];
        $this->delivery_tel = $shipping['telephone'];
        $this->delivery_fax = $shipping['fax'];
        $this->delivery_ustid = $shipping['vat_id'];
        foreach($this as $key=>$value){
            if(empty($value) && $value!==0){
                $this->$key = (string)'';
            }
        }
    }
    /**
     * If you need a complete Object Set call this method (else call set simple object
     * By default a simple object set is called
     */
    public function setCompleteObject(){
        $this->mode = true;
    }
    /**
     * If you need only a simple set for exporting customers call this function else call set complete object
     * By default a simple object set is called
     */
    public function setSimpleObject(){
        $this->mode = false;
    }
    /**
     * Returns the Data Set as array or boolean false if the populate method was not called or something went wrong!
     * @return array|bool
     */
    public function toArray(){
        if($this->mode){
            $notneeded = array(
                'mode',
            );
        }else{
            $notneeded = array(
                'mode',
                'tel',
                'fax',
                'ustid',
                'gebdat',
                'delivery_id',
                'delivery_kurzname',
                'delivery_firma',
                'delivery_name',
                'delivery_vorname',
                'delivery_adresse',
                'delivery_adresse2',
                'delivery_plz',
                'delivery_ort',
                'delivery_land',
                'delivery_addresses',
                'print_brutto',
                'currency',
                'ustid',
                'adresse2',
                'preisgruppe',
            );
        }
        $result = array();
        foreach($this as $key=>$value){
            if(!in_array($key,$notneeded)){
                $result[$key] = $value;
            }
        }
        return $result;
    }
}