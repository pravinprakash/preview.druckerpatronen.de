<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 28.02.14
 * Time: 14:58
 */

class Actindo_Connector_Model_Data extends Varien_Object{
    protected static $configData;
    public static function getStoreConfig(){
        if(self::$configData===null){
            self::$configData = Mage::getStoreConfig('actindo/mappingfields');
        }
        return self::$configData;
    }
    public static function getFieldValue($field){
        if(isset(self::$configData[$field])){
            return self::$configData[$field];
        }
    }
}