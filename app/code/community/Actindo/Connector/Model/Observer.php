<?php
    /**
     * Actindo Faktura/WWS Connector
     * observer to assign list fields to a special mapping table to get the
     * list elements into actindo erp
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.303
     */
    class Actindo_Connector_Model_Observer{
        /**
         * Paths in the main config file
         */
        const XML_PATH_MANUFATURER_ATTRIBUTE = "actindo/mappingfields/manufacturerid";
        const XML_PATH_DELIVERYTIME_ATTRIBUTE = "actindo/mappingfields/shippingstatus";
        const XML_PATH_VPE_ATTRIBUTE = "actindo/mappingfields/productvpe";
        /**
         * checks if the field exists in the database
         * if not it is created
         * @param $value
         * @param $type
         * @param $site
         * @return bool
         */
        private function runCheckAndCreate($value,$type,$site){
			Mage::log($value);
			Mage::log($type);
			Mage::log($site);
            if(empty($value)) return true;
            $mapperEntry = Mage::getModel('connector/acmapper')->getCollection()
                ->addFieldToFilter('type',$type)
                ->addFieldToFilter('value',$value)
                ->addFieldToFilter('site',$site)
                ->getFirstItem();
            try{
                if(!$mapperEntry->getId() ){
                    $mapperEntry->setValue($value)
                        ->setType($type)
                        ->setSite($site)
                        ->save();
                }
                return true;
            }catch(Exception $e){
                return false;
            }
        }
        /**
         * adds a Entry to the Mapper
         * @param Varien_Event_Observer $observer
         */
        public function addVaulueToMapper(Varien_Event_Observer $observer){
            $product = $observer->getEvent()->getProduct();
            Mage::log('observer');
            Mage::log($product);
            if((int)$product->getStoreId()>0){
                $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_DELIVERYTIME_ATTRIBUTE,$product->getStoreId())),'shipping',$product->getStoreId());
                $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_MANUFATURER_ATTRIBUTE)),'manufacturers',$product->getStoreId());
                $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_VPE_ATTRIBUTE)),'vpe',$product->getStoreId());
            }else{
                $storeviews = Mage::getModel('core/store')->getCollection();
                foreach($storeviews as $storeview){
                    $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_DELIVERYTIME_ATTRIBUTE,$storeview->getId())),'shipping',$storeview->getId());
                    $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_MANUFATURER_ATTRIBUTE,$storeview->getId())),'manufacturers',$storeview->getId());
                    $this->runCheckAndCreate($product->getData(Mage::getStoreConfig(self::XML_PATH_VPE_ATTRIBUTE,$storeview->getId())),'vpe',$storeview->getId());
                }
            }
            Mage::log('################################################################');
        }
        /**
         * Pre Dispatch
         */
        public function preDispatch(){
            $session = Mage::getSingleton('admin/session');
            if ($session->isLoggedIn()) {
                $feedModel = Mage::getModel( 'connector/feed' );
                $feedModel->checkUpdate();
            }

        }
    }
