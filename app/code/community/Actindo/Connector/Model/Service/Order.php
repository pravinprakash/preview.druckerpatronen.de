<?php
    /**
     * Actindo Faktura/WWS Connector
     * class to handle orders
     * this class supports
     * order import
     * order update
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Order{
        const XML_PATH_SHIPPING_TAX = "tax/classes/shipping_tax_class";
        /**
         * Singleton Helper Class Container
         * @var Mage_Core_Model_Abstract
         */
        protected $singleton;
        /**
         * Service class
         * @var
         */
        protected $service;
        /**
         * array with translations
         * @var array
         */
        protected $language;
        /**
         * ISO to Shop Mapping
         * @var array
         */
        protected $isomap = array();
        /**
         * Consturctor
         */
        public function __construct(){
            //Init Singleton class
            $this->singleton = Mage::getSingleton('connector/util_util');
            /**
             * load and init language and translation
             */
            $this->language = $this->singleton->getLanguages();
            foreach($this->language as $key){
                $this->isomap[$key['language_id']] = $key['language_code'];
            }
        }
        /**
         * Calculation Base for timestamp
         * Possible Values ar mktime (based on current timezone) or gmmktime (based on GMT+0)
         */
        const MKTIMECALL = 'gmmktime';
        /**
         * Order Status
         * @var array with different Order status (array after first get Order Status call)
         */
        protected $orderstatus=null;
        /**
         * gets the different order status either as array or as value
         * @param mixed $ident if ident is false returns all status or one searched by ident and status. Possible Values are: name (get ID based on name), id (get name based on id), value (get Value based on id)
         * @param mixed $status (field to search for, if all are required leave it as false
         *
         * @return array|mixed|bool either array of all values, searched value or false if it doesn't exist.
         */
        protected final function getOrderStatus($statusquery=false){
            if($this->orderstatus===null){
                $this->orderstatus = array();
                $status = Mage::getModel('connector/ackeyvaluemap')
                    ->getCollection()
                    ->addFieldToFilter('type','orderstatus')
                    ->getItems();
                foreach($status as $value){
                    $this->orderstatus[$value->getIdent()] = $value->getId();
                }
            }
            if($statusquery===false){
                return $this->orderstatus;
            }else{
                return (isset($this->orderstatus[$statusquery]))?$this->orderstatus[$statusquery]:false;
            }
        }
        /**
         * Shipping stats
         * @var array with different Order status (array after first get Order Status call)
         */
        protected $shipping=null;
        /**
         * gets the different shipping states either as array or as value
         * @param mixed $ident if ident is false returns all status or one searched by ident and status. Possible Values are: name (get ID based on name), id (get name based on id), value (get Value based on id)
         * @param mixed $status (field to search for, if all are required leave it as false
         *
         * @return array|mixed|bool either array of all values, searched value or false if it doesn't exist.
         */
        protected final function getShippingMethods($ident=false,$status=false){
            if($this->shipping === null){
                $singleton = Mage::getSingleton('connector/util_util');
                $siteid = Mage::app()->getStore()->getWebsiteId();
                $ship = $singleton->getShippingVendors($siteid);
                foreach($ship as $key=>$value){
                    $this->shipping['name'][$value['code']] = $key;
                    $this->shipping['id'][$key] = $value['code'];
                    $this->shipping['value'][$key] = $value['name'];
                    $this->shipping['active'][$key] = (bool)$value['active'];
                }
            }
            if($ident===false){
                return $this->shipping;
            }else{
                return (isset($this->shipping[$ident][$status]))?$this->shipping[$ident][$status]:false;
            }
        }
        /**
         * get Column List
         * @return array
         */
        protected final function getColumenList(){
            return array(
                'order_id'    => 'entity_id',
                'orders_status'     => 'status',
            );
        }
        /**
         * count all available orders
         * @return array with status ok, array containing total order count and max order id
         */
        public function count() {
            $totalOrders = 0;
            $sql = 'SELECT count(entity_id) as cnt, max(increment_id) as mx FROM dpm_sales_flat_order;';
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $result = $read->fetchAll($sql);
            return array(
                'ok'=>true,
                'counts'=>array(
                    'count'=>(int)$result[0]['cnt'],
                    'max_order_id'=>(int)$result[0]['mx'],
                ),
            );
            /**

            $salesModel = Mage::getModel('sales/order');
            $salesCollection = $salesModel->getCollection();
            $totalOrders = $salesCollection->count();
            //get Last Order Id
            $currentStore = Mage::app()->getStore()->getId();
            $sales = Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToFilter('store_id',$currentStore)
                ->addAttributeToSort('increment_id','desc')
                ->setPageSize(1)
                ->getFirstItem();
            $lastOrderId = $sales->getIncrementId();
            $result = array(
                'ok' => true,
                'counts' => array(
                    'count' => (int)$totalOrders,
                    'max_order_id' => (int)$lastOrderId,
                ),
            );
            return $result;
             */
        }
        /**
         * Actindo_Connector_Model_Service_Order::getList()
         * get's a list of all orders
         * limited by the $filters content
         * @param struct $filters array containing filter parameters
         * @return array of order
         */
        public function getList($filters) {
            if(!Actindo_Connector_Model_System_Service::getMethodActive('importorder')){
                throw new Actindo_Connector_Model_Exception_Error('Order Import has been disabled! Enable it in the Administration of youre shop Magento Shop!',800);
            }
            $helper = Mage::helper('connector');
            //Prepare Order Status
            $this->getOrderStatus();
            //Prepare Shipping Methods
            $this->getShippingMethods();
            //Get Data
            $currentWebsite = MAge::app()->getWebsite();
            $stores = $currentWebsite->getStores();
            $collection = Mage::getModel('sales/order')
                ->getCollection();
            $st = array();
            foreach($stores as $store){
                $st[] = $store->getId();
            }
            $collection->addAttributeToFilter('store_id',$st);
            //Set Query Filters and Limits
            $statusarray = $this->getOrderStatus();
            $query = $helper->buildSearchQuery($filters,$this->getColumenList(),array('orders_status'=>array_flip($statusarray)));
            if(count($query['where'])>0){
                //prepare where filter
                $filters = array();
                foreach($query['where'] as $where){
                    $filters[$where['field']][] = $where['value'];
                }
                //now build filters
                foreach($filters as $key=>$value){
                    if($key=='entity_id'){
                        $value = explode(',',$value[0]);
                    }
                    if(count($value)>1){
                        $queryset = array(
                            'in'=>$value
                        );
                        $collection->addAttributeToFilter(
                            $key,
                            $queryset
                        );
                    }else{
                        $collection->addFieldToFilter($key,$value[0]);
                    }
                }
            }
            $collection->addAttributeToSort($query['order']['field'],strtoupper($query['order']['direction']));
            $collection->setPageSize($query['limit']);
            $page = ($query['offset'] / $query['limit']) + 1;
            $collection->setCurPage($page);
            //Load Items
            $orders = $collection->getItems();
            //If count < 1 throw exception ==> no orders available
            if(count($orders)<1){
                throw new Actindo_Connector_Model_Exception_Error('Could not find any Orders!',801);
            }else{
                $resultSet = array();
                //run through orders and fill resultSet
                foreach($orders as $order){
                    try{
                        $orderSet = $this->prepareOrderExport($order);
                        $orderid = $order->getIncrementId();
                        $resultSet[$orderid] = $orderSet;
                        unset($orderid,$orderSet);
                    }catch(Actindo_Connector_Model_Exception_Error $ex){
                        Mage::log($ex->getMessage());die;
                        continue;
                    }
                }
                //return data
                $resultSet = Actindo_Connector_Model_Util_Util::getOutput($resultSet);
                $result = array('ok' => true, 'orders' => $resultSet);
                Mage::log($result);
                return $result;
            }
        }
        /**
         * build single order epxort line
         * @param $order Mage_Sales_Model_Order Contains one complete order
         * @return array Array of Order Record Set
         * @used getList
         */
        protected function prepareOrderExport($order){
            //makeing time generator replaceable!
            $gmmktime = self::MKTIMECALL;
            //create create time stamp
            $created = explode(' ',$order->getCreatedAt());
            $createDate = explode('-',$created[0]);
            $createTime = explode(':',$created[1]);
            $created = $gmmktime($createTime[0],$createTime[1],$createTime[2],$createDate[1],$createDate[2],$createDate[0]);
            //create update time stamp
            $updated = explode(' ',$order->getCreatedAt());
            $updateTime = explode('-',$updated[0]);
            $updateDate = explode(':',$updated[1]);
            $updated = $gmmktime($updateTime[0],$updateTime[1],$updateTime[2],$updateDate[1],$updateDate[2],$updateDate[0]);
            if($order->getCustomerId()>0){
                $customer = new Actindo_Connector_Model_Customer_Exporter();
                $customer->setCustomerId($order->getCustomerId());
                $customer->populate($order);
            }else{
                $customer  = new Actindo_Connector_Model_Customer_GuestExporter($order);
            }
            $customer->setCompleteObject();
            $customer = $customer->toArray();
            //currency converter
            $baseCode = Mage::app()->getBaseCurrencyCode();
            $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
            $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCode, array_values($allowedCurrencies));
            $netto = (float)$order->getGrandTotal() - (float)$order->getTaxAmmount();
            //get Order status
            $orderstatus = (int)$this->getOrderStatus($order->getStatus());
            //get currency
            $currency = $order->getOrderCurrencyCode();
            if(empty($currency)){
                $currency = $order->getBaseCurrencyCode();
            }
            //build basic return set
            $returnset = array(
                //Basic Global Order Data
                '_customers_id'=>(string)$order->getCustomerId(),
                '_payment_method'=>(string)$order->getPayment()->getMethodInstance()->getCode(),
                'beleg_status_text'=>(string)'', /** @todo currently not supported */
                'bill_date'=>(string)date('H:m:s',$created),
                'currency'=>(string)$currency,
                'currency_value'=>((((float)$rates)>0)?(float)$rates:1),
                'external_order_id'=>(string)$order->getIncrementId(),
                'language'=>$this->isomap[(int)$order->getStoreId()],
                'langcode'=>$this->isomap[(int)$order->getStoreId()],
                'netto'=>(float)$netto,
                'netto2'=>(float)$netto,
                'order_id'=>(int)$order->getEntityId(),
                'orders_status'=>$orderstatus,
                'project_id'=>(int)$order->getIncrementId(),
                'rabatt_type'=>'betrag',
                'rabatt_betrag'=>(float)0.0,
                'saldo'=>(float)$order->getGrandTotal(),
                'subshop_id'=>(int)$order->getStoreId(),
                'tstamp'=>(int)$updated,
                'bill_date'=>(string)date('Y-m-d H:i:s',$created),
                'webshop_order_date'=>(string)date('Y-m-d',$created),
                'webshop_order_time'=>(string)date('H:m:s',$created),
                'val_date'=>(string)date('Y-m-d H:i:s',$created),
                //Customer Record Set
                'customer'=>array(
                    'language'=>$this->isomap[(int)$order->getStoreId()],
                    'langcode'=>$this->isomap[(int)$order->getStoreId()],
                    'anrede'=>$customer['anrede'],
                    'kurzname'=>$customer['kurzname'],
                    'firma'=>$customer['firma'],
                    'name'=>$customer['name'],
                    'vorname'=>$customer['vorname'],
                    'adresse'=>$customer['adresse'],
                    'adresse2'=>$customer['adresse2'],
                    'plz'=>$customer['plz'],
                    'ort'=>$customer['ort'],
                    'land'=>$customer['land'],
                    'tel'=>$customer['tel'],
                    'fax'=>$customer['fax'],
                    'ustid'=>(string)$customer['ustid'],
                    'email'=>$customer['email'],
                    'preisgruppe'=>$customer['preisgruppe'],
                    'gebdat'=>$customer['gebdat'],
                    'print_brutto'=>'1', /** @todo Get Tax with help of  Tom */
                ),
                //delivery Record Set
                'delivery'=>array(
                    'anrede'=>$customer['delivery_anrede'],
                    'firma'=>$customer['delivery_firma'],
                    'name'=>$customer['delivery_name'],
                    'vorname'=>$customer['delivery_vorname'],
                    'adresse'=>$customer['delivery_adresse'],
                    'adresse2'=>$customer['delivery_adresse2'],
                    'plz'=>$customer['delivery_plz'],
                    'ort'=>$customer['delivery_ort'],
                    'land'=>$customer['delivery_land'],
                    'tel'=>$customer['delivery_tel'],
                    'fax'=>$customer['delivery_fax'],
                    'ustid'=>(string)$customer['delivery_ustid'],
                )
            );
            if($order->getData('payone_transaction_status')!=='' && Actindo_Connector_Model_System_Service::getMethodActive('payoneSupportEnabled'))
            {
                $factory = new Payone_Core_Model_Factory();
                $payoneObject = $factory->getModelTransaction()->getCollection();
                $payoneObject->addFieldToFilter('order_id',$order->getId());
                $payOneData = $payoneObject->getFirstItem();
                $returnset['payment_type'] = 'payone';
                $returnset['payment_order_id'] = sprintf("%d#%04d", $payOneData->getTxid(), $payOneData->getReference());
            }
            return $returnset;
        }
        /**
         * returns the Customers "Debitoren/Kreditoren" Id (Actindo Internal Customer Id)
         * @param $id Customer ID
         * @return bool or Actindo Internal Id
         */
        protected function getCustomerKredId($id){
            //Get the debkredid based on the customer id
            $model = Mage::getModel('connector/accustmap')
                ->getCollection()
                ->addFieldToFilter('id',(int)$id)
                ->getFirstItem();
            //if customer id exists, return it
            if((int)$model->getId()>0){
                return $model->getDebKredId();
            }else{
                //no deb cred id exists, return false
                return false;
            }
        }
        /**
         * Array of all defined Tax classes => rate
         * @var array of tax rates identified by their tax class
         */
        protected $taxclasses = null;
        /**
         * returns the current tax rate
         * @param $taxident
         *
         * @return mixed
         */
        protected function getTaxClassById($taxident){
            if($this->taxclasses===null){
                $helper = Mage::helper('core');
                $taxhelper = Mage::helper('tax');
                $this->taxclasses = $helper->jsonDecode($taxhelper->getAllRatesByProductClass());
                $newclass = array();
                foreach($this->taxclasses as $taxid => $taxrate){
                    $newclass[str_replace('value_','',$taxid)] = $taxrate;
                }
                $this->taxclasses = $newclass;
            }
            if(!((int)$taxident>0)){
                $taxident = 1;
            }
            return $this->taxclasses[$taxident];
        }
        /**
         * list_positions
         * List of all Order Positions
         * @param int $orderID
         */
        public function list_positions($orderID) {
            if(!Actindo_Connector_Model_System_Service::getMethodActive('importorder')){
                throw new Actindo_Connector_Model_Exception_Error('Order Import has been disabled! Enable it in the Administration of youre shop Magento Shop!',800);
            }
            $order = Mage::getModel('sales/order')->load($orderID);
            $taxmap = Mage::getModel('connector/actaxordermap')->load($orderID);
            if($taxmap->getId()<1){
                $taxmap = Mage::getModel('connector/actaxordermap');
                $taxmap->setOrderid($orderID);
                $taxmap->setTax((int)Actindo_Connector_Model_System_Service::priceIncludesTax());
                $taxmap->setShipping((int)Actindo_Connector_Model_System_Service::ShippingIncludesTax());
                $taxmap->save();
            }
            $taxdata = $taxmap->toArray();
            //set default everything to tax = true
            $shipTax = true;
            $productTax = true;
            if((int)$taxdata['tax']<1){
                $productTax = false;
            }
            if((int)$taxdata['shipping']<1){
                $shipTax = false;
            }
            unset($taxdata);
            $responseArray = array();
            $maxTaxRate = 0;
            $lastTax=0;
            $orderitems = $order->getAllItems();
            $orderItemsResult = array();
            foreach($orderitems as $orderitem){
                if((int)$orderitem->getParentItemId()>0){
                    continue;
                }
                $articleName = (string)$orderitem->getName();
                if(!empty($articleName)){
                    $articleName = $articleName;
                }
                $productPrice = $orderitem->getPriceInclTax();
                if($productTax===true){
                    $productIsBrutto = 1;
                }else{
                    $productIsBrutto = 0;
                }
                $item = array(
                    'art_nr'      => (string)$orderitem->getSku(),
                    'art_nr_base' => (string)$orderitem->getSku(),
                    'art_name'    => $articleName,
                    'preis'       => (float)$productPrice,
                    'is_brutto'   => $productIsBrutto,
                    'type'        => (string)'Lief',//in_array($position['mode'], arra y(0, 1), true) ? 'Lief' : 'NLeist',
                    'mwst'        => (float)$orderitem->getTaxPercent(),//(string) $position['taxRate'],
                    'menge'       => (float)$orderitem->getQtyOrdered(),//$position['quantity'],
                    'attributes'  => array(), // filled below array definition
                    'langtext'    => (string)'',      // filled below array definition
                );
                $lastTax = (float)$orderitem->getTaxPercent();
                $orderItemsResult[] = $item;
                $item = null;
            }
            //shopping Position
            $shippingTaxId = Mage::getStoreConfig(self::XML_PATH_SHIPPING_TAX,Actindo_Connector_Model_System_Service::getCurrentWebsiteId());
            $shippingTaxPercent = $this->getTaxClassById($shippingTaxId);
            $shipping = array(
                'art_nr'=> strtoupper($order->getShippingMethod()),
                'art_nr_base'=> strtoupper($order->getShippingMethod()),
                'art_name'=> $order->getShippingDescription(),
                'preis'=>(float)$order->getShippingInclTax(),
                'is_brutto'=>true,
                'type'=>'NLeist',
                'mwst'=>$shippingTaxPercent,
                'menge'=>1,
                'langtext'=>''
            );
            $orderItemsResult[] = $shipping;
            //discount position
            $discount = (float)$order->getBaseDiscountAmount();
            if($discount<(float)0){
                $total = (float)$order->getBaseSubtotalInclTax() + (float)$order->getShippingInclTax() - (float)$order->getGrandTotal();
                $total+= $discount;
                $total *= (float)-1;
                $discount += $total;
                
                //Prepare Coupon
                $coupon = array(
                      'art_nr'=> strtoupper($order->getCouponCode()),
                      'art_nr_base'=> strtoupper($order->getCouponCode()),
                      'art_name'=>'Gutschein',
                      'preis'=>$discount,
                      'is_brutto'=>1,
                      'type'=>'NLeist',
                      'mwst'=>$lastTax,
                      'menge'=>1,
                      'langtext'=>'',
                );
                $orderItemsResult[] = $coupon;
            }
            $orderItemsResult = Actindo_Connector_Model_Util_Util::getOutput($orderItemsResult,true,true);
            return $orderItemsResult;
        }
        /**
         * set order status
         * @param int $orderID
         * @param string $status
         * @param string $comment
         * @param int $notifyCustomer
         * @param int $sendComments
         * @return array
         */
        public function set_status($orderID, $status, $comment, $notifyCustomer, $sendComments) {
            $order = Mage::getModel('sales/order')->load((int)$orderID);
            if((int)$status>0){
                $order_status = $this->getOrderStatus();
                $order_status = array_flip($order_status);
                $status = $order_status[$status];
            }
            $order->setStatus($status);
            $order->save();
            return array('ok' => true);
        }
        /**
         * set invoice status
         * @param struct $params
         * @return mixed
         */
        public function set_status_invoice($params) {
            return call_user_func_array(array($this, 'set_status'), $params);
        }
        /**
         * set tracking code
         * Also sets order to shipped
         * @param int $orderID
         * @param string $trackingCode
         * @return array
         */
        public function set_trackingcode($orderID, $trackingCode) {
            $successState=false;
            $order = Mage::getModel('sales/order')->load((int)$orderID);
            if((int)$order->getId()>0){
                $orderStatus = $order->getStatus();
                $orderStatus = 'Complete';
                $convertor = Mage::getModel('sales/convert_order');
                $shipment = $convertor->toShipment($order);
                foreach($order->getAllItems() as $orderItem){
                    if(!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()){
                        continue;
                    }
                    $item = $convertor->itemToShipmentItem($orderItem);
                    $qty = $orderItem->getQtyToShip();
                    $item->setQty($qty);
                    $shipment->addItem($item);
                }
                $carriers = $this->getActiveCarriers();
                if(isset($carriers[$order->getShippingMethod()])){
                    $activeCarrier = $carriers[$order->getShippingMethod()];
                }else{
                    $activeCarrier = array(
                        'title'=>$order->getShippingDescription(),
                        'code'=>'custom'
                    );
                }
                $data = array(
                    'carrier_code'=>$activeCarrier['code'],
                    'title'=>$order->getShippingDescription(),
                    'number'=>$trackingCode
                );
                $track = Mage::getModel('sales/order_shipment_track')->addData($data);
                $shipment->addTrack($track);
                $shipment->register();
                $shipment->addComment('',true);
                $shipment->setEmailSent(true);
                $shipment->getOrder()->setIsInProcess(true);
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($shipment)
                    ->addObject($shipment->getOrder())
                    ->save();
                if(Actindo_Connector_Model_System_Service::getMethodActive('triggerSend')){
                    $shipment->sendEmail(true,'');
                }else{
                    $shipment->sendEmail(false,'');
                }
                $order->setStatus($orderStatus);
                $order->addStatusToHistory($order->getStatus(), 'Order Completed And Shipped Automatically via Automation Routines', false);
                $shipment->save();
                $order->save();
                $successState = true;
            }
            return array('ok' => $successState);
        }
        /**
         * get list of active carriers
         * @return array
         */
        private function getActiveCarriers(){
            $carrier = array();
            try{
                $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
                if(count($methods)>0){
                    foreach($methods as $methodName=>$method){
                        $entry = array(
                            'code'=>$methodName.'_'-$method->getId(),
                        );
                        $carrier[$methodName.'_'.$method->getId()];
                    }
                }
                return $carrier;
            }catch(Exception $e){
            }
        }
    }
