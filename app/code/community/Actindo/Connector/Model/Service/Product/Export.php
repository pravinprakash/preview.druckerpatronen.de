<?php
/**
 * Actindo Faktura/WWS Connector
 * Product Export Class
 * Exports Article from Shop into Actindo ERP
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @version 2.309
 * @abstract
 */
abstract class Actindo_Connector_Model_Service_Product_Export{
    const DISABLE_PARENT_ADDED_PICTURES = true;
    /**
     * @var Mage Product
     */
    protected $product;
    /** 
     * Products
     * @var
     */
    protected $products;
    /**
     * @var Actindo Data
     */
    protected $actindo;
    /**
     * Helper Object
     * @var
     */
    protected $helper;
    /**
     * @var Image Array Cache
     */
    protected $images;
    /**
     * Image Type Map
     * @var array
     */        protected $util;
    protected $typeMap = array(
        // 'extension' => 'mimetype'
        'bmp'  => 'image/bmp',
        'gif'  => 'image/gif',
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'png'  => 'image/png',
    );
    /**
     * Counter var
     * @var int
     */
    protected $counter = 0;
    /**
     * Singleton Helper
     * @var Actindo_Connector_Model_Util_Util
     */
    protected $singleton = null;
    /**
     * Customer Groups
     * @var array
     */
    protected $customerGroups = array();
    /**
     * Restricted Properties List
     * @var array
     */
    protected $allowedFields = array();
    /**
     * Default Language
     * @var int
     */
    protected $defaultLanguage = 0;
    /**
     * Default Product
     * @var null
     */
    protected $defaultProduct = null;
    /**
     * Parent Pictures
     * @var array
     */
    protected $parentPictures = array();
    /**
     * Store Id
     * @var int
     */
    protected $storeid;
    /**
     * contains the system languages
     * @var array
     */
    protected $languages = array();
    /**
     * contains the parents categories
     */
    protected $parentCategories;
    /** METHODS **/
    /**
     * Constructor
     * initializes the product export related data set
     * @uses Actindo_Connector_Model_Util_Util
     * @uses Actindo_Connector_Model_System_Config_Source_Mapping
     */
    /**
     * Service class
     * @var
     */
    protected $service;
    /**
     * Store Mapping
     * @var array
     */
    protected $mapping=array();
    public function __construct(){
        $this->singleton = Mage::getSingleton('connector/util_util');
        $groups = $this->singleton->getCustomerGroups();
        foreach($groups as $key=>$value){
            $this->customerGroups[] = $key;
        }
        $filter = new Actindo_Connector_Model_System_Config_Source_Mapping();
        $this->allowedFields = $filter->getFieldList(Mage::app()->getStore()->getWebsiteId());
        $this->storeid = Mage::app()->getStore()->getWebsiteId();
        $this->defaultLanguage = $this->singleton->getDefaultLanguage();
        $this->languages = $this->singleton->getLanguages();
        $this->mapping = Actindo_Connector_Model_System_Service::getStoreMapping();
    }
    /** METHODS **/
    /**
     * exports either the article list or all the details of one specific article
     * the 2nd param, $ordernumber, is actually the shops article id if article details are exported!
     *
     * @api
     * @param string $categoryID the category to export the list from
     * @param string $ordernumber the articles ordernumber if the list is requested, otherwise its the shops article id
     * @param string $language not supported
     * @param int $justList is 1 if an article listing is requested, otherwise its 0
     * @param int $offset only for list: offset to start with
     * @param int $limit  only for list: limits the number of articles
     * @param struct $filters only for list: an array of filters
     * @return array
     */
    public function get($categoryID=null, $ordernumber=null, $language=null, $justList=null, $offset=null, $limit=null, $filters=null) {
        if($justList>0){
            if(!empty($justList)) {
                // $ordernumber given but $justList requested, get "list" of just that product
                $filters = array_merge($filters, array(
                    'ordernumber' => $ordernumber
                ));
            }
            return $this->exportList($offset, $limit, $filters);
        }else{
            $ordernumber = $filters['filter'];
            $ordernumber = array_shift($ordernumber);
            if($ordernumber['field']=='products_id'){
                $ordernumber = $ordernumber['data']['value'];
                // export all details of a single product
                return $this->exportProduct($ordernumber, $language);
            }
        }
    }
    /**
     * returns an article listing
     *
     * @param int $offset offset to start the list with
     * @param int $limit maximum amount of items in the returned list
     * @param array $filters an array of filters to apply to the list query
     * @return array
     */
    protected function exportList($offset, $limit, $filters) {
        $offset = isset($filters['start']) ? (int) $filters['start'] : $offset;
        $limit  = isset($filters['limit']) ? (int) $filters['limit'] : $limit;
        $whereClause = '';
        if(isset($filters['filter'][0]) && !empty($filters['filter'][0]['field']) && $filters['filter'][0]['field']==='art_nr') {
            $sku = $filters['filter'][0]['data']['value'];
            $result = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
            if($result!==false && $result->getId()>0){
                $products = $this->prepareArticleListElement($result);
                $resSet = array(
                    'ok' => true,
                    'products' => array(
                        $result->getId() => $products
                    )
                );
                $resSet = Actindo_Connector_Model_Util_Service::ScanForNullAndCorrect($resSet);
                return $resSet;
            }
        }else{
            $offset = (int)round($offset/$limit);
            $productCollection = Mage::getModel('connector/export_product_collection')
                #->getCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('is_active')
                ->addAttributeToSelect('category_ids')
                ->setCurPage($offset)
                ->setPageSize($limit)
                ->load();
            if($productCollection->count()){
                $return = array();
                foreach($productCollection as $product){
                    $webSiteIds = $product->getWebsiteIds();
                    if(in_array(Actindo_Connector_Model_System_Service::getCurrentWebsiteId(),$webSiteIds)){
                        $productId = $product->getId();
                        $result = $this->prepareArticleListElement($product);
                        if($result){
                            $return[$productId] = $result;
                        }
                    }
                }
                $result =  array(
                    'ok' => true, 'products' => $return
                );
                $result = Actindo_Connector_Model_Util_Service::ScanForNullAndCorrect($result);
                return $result;
            }
        }
    }
    /**
     * prepare Article List Elements for Export of List
     * @param Mage_Catalog_Model_Product $data Product Data
     * @return array
     */
    protected function prepareArticleListElement(Mage_Catalog_Model_Product $data){
        $categories = $data->getCategoryIds();
        if(count($categories)<1)
            return false;
        $categorie = array_shift($categories);
        if(Actindo_Connector_Model_System_Service::getMethodActive('submitUpdateDate')){
           $updateDate =  Varien_Date::toTimestamp($data->getUpdatedAt());
        }else{
           $updateDate = '0000-01-01 00:00:00';
        }
        $return = array(
            'products_id'       =>  ''.$data->getId(),
            'art_nr'            =>  ''.$data->getSku(),
            'art_name'          =>  new Zend_XmlRpc_Value_Base64($data->getName()),
            'grundpreis'        =>  (float)$data->getPrice(),
            'categories_id'     =>  $categorie,
            'products_status'    =>  (int)(($data->getStatus())?true:false),
            'created'           =>  ''.Varien_Date::toTimestamp($data->getCreatedAt()),
            'last_modified'     =>  $updateDate
        );
        return $return;
    }
    /**
     * main entry point of single product export (with all details)
     * @param int $articleID the article id to export
     * @param string $language deprecated
     * @return array
     */
    protected function exportProduct($articleId,$language) {
        $this->helper = Mage::helper('connector');
        $langSets = $this->singleton->getLanguages();
        foreach($langSets as $langSet){
            $langid = $langSet['language_id'];
            $this->products[$langid] = Mage::getModel('catalog/product')->setStoreId($langid)->load((int)$articleId);
        }
        $this->product = $this->products[$this->defaultLanguage];
        $this->defaultProduct = Mage::getModel('catalog/product')->setStoreId(0)->load((int)$articleId);
        if($this->product->getId()>0){
            $this->exportCoreData();
            $this->exportAttributes();
            $this->exportCategories();
            $this->exportCustomerGroupPermissions();
            $this->exportCrossselings();
            $this->exportImages();
            $this->exportPrices();
            $this->exportProperties();
            $this->exportTranslations();
            $this->exportVariants($language);
            $this->actindo = Actindo_Connector_Model_Util_Util::getOutput($this->actindo,true,true);
            return array(
                'ok' => 'true',
                'products' => array(
                    $this->actindo
                )
            );
        }
    }
    /**
     * Export Versions
     * @param $language
     */
    protected function exportVariants($language){
        if($this->product->isConfigurable()){
            $this->actindo['attributes']['combination_advanced'] = array();
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this->product);
            if(count($childProducts)>0){
                //now the names, values
                $attributes = $this->product->getTypeInstance(true)->getConfigurableAttributesAsArray($this->product);
                $this->actindo['attributes']['names'] = array();
                $attributeCode = array();
                foreach($attributes as $attribute){
                    $attributeCode[$attribute['attribute_id']] = $attribute['code'] = $attribute['attribute_code'];
                    $this->actindo['attributes']['names'][$attribute['attribute_id']]['de'] = $attribute['frontend_label'];
                    $this->actindo['attributes']['values'][$attribute['attribute_id']] = array();
                    $this->actindo['attributes']['combination_simple'][$attribute['attribute_id']] = array();
                    foreach($attribute['values'] as $value){
                        $this->actindo['attributes']['values'][$attribute['attribute_id']][$value['value_index']]['de'] = $value['label'];
                        $this->actindo['attributes']['combination_simple'][$attribute['attribute_id']][$value['value_index']]=array(
                            'options_values_price'=>0,
                            'attributes_model'=>0,
                            'options_values_weight'=>0,
                            'sortorder'=>0
                        );
                    }
                }
                $langSets = $this->singleton->getLanguages();
                $mapped = $this->helper->getAllMappedFields();
                foreach($childProducts as $childProduct){
                    $products = array();
                    foreach($langSets as $langSet){
                        $langid = $langSet['language_id'];
                        $products[$langid] = Mage::getModel('catalog/product')->setStoreId($langid)->load((int)$childProduct->getId());
                    }
                    $product = $products[$this->defaultLanguage];
                    $defaultProduct = Mage::getModel('catalog/product')->setStoreId(0)->load((int)$childProduct->getId());
                    $row = array(
                        'attribute_name_id'=>array(),
                        'attribute_value_id'=>array(),
                        'data'=>array()
                    );
                    foreach($this->actindo['attributes']['names'] as $key=>$value){
                        $row['attribute_name_id'][]     = $key;
                        $entry = $attributeCode[$key];
                        $valueId = $product->getData($entry);
                        $row['attribute_value_id'][]    = $valueId;
                    }
                    $row['data'] = array(
                        'products_status'=>(bool)$childProduct->getStatus(),
                        'products_ean'=>$this->getFieldValue('productean',false,false,$products,$defaultProduct)
                    );
                    $prices                 =   $this->exportPrices($product) ;
                    $images                 =   $this->exportImages($products);
                    /**
                     * Get PArents Values first
                     */
                    $row['shop']['description'] = $this->actindo['description'];
                    //now parse parents data
                    foreach($this->mapping['code'] as $code){
                        $language = $products[$code];
                        $translations = $this->returnTranslationData($language,$code);
                        $ident = $translations['language_id'];
                        foreach($translations as $key=>$value){
                            if(!empty($value)){
                                $row['shop']['description'][$ident][$key] = $value;
                            }
                        }
                    }
                    $row = array_merge( $row , $prices );
                    $row['shop']['images'] = $images;
                    //get properties
                    $row['shop']['properties'] = $this->getVariantsProperties($products,$product,$mapped);
                    //bestand
                    $data = $product->getData();
                    $stockContainer = $data['stock_item'];
                    if($stockContainer->getManageStock()){
                        $stock = $stockContainer->getQty();
                        if($stockContainer->getIsQtyDecimal()){
                            $stock = (float)$stock;
                        }else{
                            $stock = (int)$stock;
                        }
                        $row['l_bestand'] =   $stock;
                    }
                    $this->actindo['attributes']['combination_advanced'][$product->getSku()] = $row;
                }
            }
        }
    }
    protected function getVariantsProperties($products,$product,$mapped){
        $resultArray = array();
        foreach($this->allowedFields as $value){
            $usedlanguages = array();
            if($value!==-1 && $value!=="-1" && !isset($mapped[$value])){
                $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$value);
                $attribute = Mage::getModel('eav/entity_attribute')->load($attributeId);
                $attributeValue = $this->defaultProduct->getData($value);
                $attributeLanguage = $this->languages[$this->defaultLanguage]['language_code'];
                $attributeCode = $attribute->getAttributeCode();
                if($attribute->getIsGlobal()){
                    $parent = false;
                    if(empty($attributeValue) && empty($products[$this->mapping['code'][$attributeLanguage]])){
                        $attributeValue = $products[$this->mapping['code'][$attributeLanguage]]->getData($value);
                    }
                    if(empty($attributeValue)){
                        $attributeValue = $products[$this->defaultLanguage]->getData($value);
                    }
                    if($attributeValue!==null){
                        $usedlanguages[] = $attributeLanguage;
                        $data = array(
                            'field_id'=>$attributeCode,
                            'language_code'=>(((int)$attribute->getIsGlobal()===1)?'':$attributeLanguage),
                            'field_value'=>(($this->product->getData($value)===null)?$attributeValue:$product->getData($value))
                        );
                        $resultArray[] = $data;
                    }
                }else{
                    foreach($this->mapping['code'] as $key){
                        $ident = $key;
                        $valueToIdent = $products[$key]->getData($value);
                        if(empty($valueToIdent)){
                            $valueToIdent = $this->products[$key]->getData($value);
                        }
                        $attributeLanguage = $this->languages[$ident]['language_code'];
                        if(!empty($valueToIdent) && !in_array($attributeLanguage,$usedlanguages) && (int)$ident!==(int)$this->defaultLanguage){
                            $usedlanguages[] = $attributeLanguage;
                            $data = array(
                                'field_id'=>$attributeCode,
                                'language_code'=>$attributeLanguage,
                                'field_value'=>$valueToIdent,
                            );
                            $resultArray[] = $data;
                        }
                    }
                }
            }
        }
        return $resultArray;
    }
    /**
     * Method Exports the Basic Information and Prepares Array Elements for later Call
     * @return void
     */
    protected function exportCoreData(){
        $this->actindo['articledetailsID']              =   $this->product->getId();                                        // get Id
        $this->actindo['products_id']                   =   $this->product->getId();                                        // get Id
        $this->actindo['art_nr']                        =   $this->product->getSku();                                       // get Article Number
        $this->actindo['all_categories']                =   array();                                                        // is set below array definition: $this->_exportCategories()
        $this->actindo['attributes']                    =   array();                                                        // is set below array definition: $this->_exportVariants()
        $this->actindo['categories_id']                 =   0;                                                              // is set below array definition: $this->_exportCategories()
        $this->actindo['content']                       =   array();                                                        // is set below array definition: $this->_exportContent()
        $this->actindo['description']                   =   array();                                                        // translations, exported below array definition: $this->_exportTranslations()
        $this->actindo['ek']                            =   0.0;                                                            // is set below array definition: $this->_exportPrices(),
        $this->actindo['group_permissions']             =   array();                                                        // is set below array definition: $this->_exportCustomerGroupPermissions()
        $this->actindo['grundpreis']                    =   0;                                                              // is set below array definition: $this->_exportPrices()
        $this->actindo['images']                        =   array();                                                        // is set below array definition: $this->_exportImages()
        //removed original setting because of changes in handling
        $this->actindo['is_brutto']                     =   (int)Actindo_Connector_Model_System_Service::priceIncludesTax();//0; // is set below array definition: $this->_exportPrices()
        $this->actindo['preisgruppen']                  =   array();                                                        // is set below array definition: $this->_exportPrices()
        $this->actindo['products_keywords']             =   '';                                                             // done in $this->_exportTranslations()
        $this->actindo['products_pseudoprices']         =   array();                                                        // is set below array definition: $this->_exportPrices()
        $this->actindo['products_short_description']    =   '';                                                             // done in $this->_exportTranslations()
        $this->actindo['properties']                    =   array();                                                        // is set below array definition: $this->_exportProperties()
        $this->actindo['xselling']                      =   array();                                                        // is set below array definition: $this->_exportCrossellings()
        //get Article Name
        $articleName = $this->defaultProduct->getName();
        if(empty($articleName)){
            $articleName = $this->product->getName();
        }
        $this->actindo['art_name']                      =   $articleName;                                                  // Article Name
        $this->actindo['filtergroup_id']                =   $this->product->getAttributeSetId();
        $data = $this->product->getData();
        $stockContainer = $data['stock_item'];
        if($stockContainer->getManageStock()){
            $stock = $stockContainer->getQty();
            if($stockContainer->getIsQtyDecimal()){
                $stock = (float)$stock;
            }else{
                $stock = (int)$stock;
            }
            $this->actindo['l_bestand'] =   $stock;
        }
    }
    /**
     * returns the Value of a field
     * @param $field
     * @param bool $defaultValue
     * @param bool $language
     * @return bool
     */
    protected function getFieldValue($field,$defaultValue = false,$language=false,$variant=false,$default=null){
        $product = ($variant===false)?$this->products:$variant;
        $defaultProduct = ($variant===false)?$this->defaultProduct:$default;
        $language = ($language==false)?$this->defaultLanguage:$language;
        $ident = $this->helper->getMappedFieldName($field);
        if($ident!==false && $ident!==-1 && $ident!="-1"){
            if(!is_array($product))
            {
                $value = $product->getData($ident);
            }
            else
            {
                $value = $product[$language]->getData($ident);
            }
            /**
             * If Value is null try it with the default Global Attribute Value
             */
            if($value===null){
                $value = $defaultProduct->getData($ident);
            }
            if($value===null){
                return $defaultValue;
            }
            if($field === 'ursprungsland'){
                return $value;
            }elseif($this->helper->getFieldType($field)=='select' && $field!=='manufacturerid'){
                return $this->helper->getSelectValue($field,$value);
            }else{
                return $value;
            }
        }
        return $defaultValue;
    }
    /**
     * Export Attributes of Artikel (exp. FSK18)
     */
    protected function exportAttributes(){
        //basic data
        $this->actindo['products_ean']                  =   $this->getFieldValue('products_ean','');
        $this->actindo['ursprungsland']                 =   $this->getFieldValue('ursprungsland','');
        $this->actindo['auslaufdatum']                  =   $this->getFieldValue('auslaufdatum');
        $this->actindo['einheit']                       =   $this->getFieldValue('einheit','');
        $this->actindo['width']                         =   $this->getFieldValue('size_b','');
        $this->actindo['height']                        =   $this->getFieldValue('height','');
        $this->actindo['length']                        =   $this->getFieldValue('length','');
        #$this->actindo['size_unit']                     =   $this->getFieldValue('size_unit','');
        //webshop data
        if((int)$this->product->getStatus()===1){
            $this->actindo['products_status'] = 1;
        }else{
            $this->actindo['products_status'] = 0;
        }
        if((int)$this->getFieldValue('email_notification')===1){
            $this->actindo['email_notification'] = 1;
        }else{
            $this->actindo['email_notification'] = 0;
        }
        $this->actindo['products_weight']               =   $this->getFieldValue('products_weight','');
        $this->actindo['topseller']                     =   $this->getFieldValue('topseller','');
        $this->actindo['products_date_available']       =   $this->getFieldValue('products_date_available','');
        $this->actindo['manufacturers_id']              =   $this->getFieldValue('manufacturerid');
        $this->actindo['suppliernumber']                =   $this->getFieldValue('suppliernumber','');
        $this->actindo['pseudosales']                   =   $this->getFieldValue('pseudosales','');
        if((int)$this->getFieldValue('products_vpe_status')===1){
            $this->actindo['products_vpe_status'] = 1;
        }else{
            $this->actindo['products_vpe_status'] = 0;
        }
        $this->actindo['products_vpe_value']            =   $this->getFieldValue('products_vpe_value','');
        $this->actindo['products_vpe']                  =   $this->getFieldValue('products_vpe','');
        $this->actindo['products_vpe_staffelung']       =   $this->getFieldValue('products_vpe_staffelung','');
        $this->actindo['products_vpe_referenzeinheit']  =   $this->getFieldValue('products_vpe_referenzeinheit','');
        $this->actindo['products_digital']              =   $this->getFieldValue('products_digital');
        $this->actindo['shipping_free']                 =   $this->getFieldValue('shipping_free','');
        $this->actindo['abverkauf']                     =   $this->getFieldValue('abverkauf');
        $taxClassId = $this->product->getTaxClassId();
        $helper = Mage::helper('core');
        $taxhelper = Mage::helper('tax');
        $taxclass = $helper->jsonDecode($taxhelper->getAllRatesByProductClass());
        if($taxClassId<1){
            $taxClassId = 1;
        }
        $taxrate = $taxclass['value_'.$taxClassId];
        $this->actindo['mwst']                          =       $taxrate;
        /**
          *get shipping status
          */
        $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
        $this->actindo['shipping_status'] = $this->product->getData($shippingField);
    }
    /**
     * Export Categories
     */
    protected function exportCategories(){
        $lowestLevel = null;
        foreach($this->product->getCategoryIds() AS $category) {
            $this->actindo['all_categories'][] = $category;

            if($lowestLevel === null || $category < $lowestLevel) {
                $lowestLevel = $category;
                $this->actindo['categories_id'] = $category;
            }
        }
        $this->parentCategories = array(
            'all_categories'    =>  $this->actindo['all_categories'],
            'categories_id'     =>  $this->actindo['categories_id'],
        );
    }
    /**
     * export product images
     * @param struct|bool $return parameter for either returning struct with product images ($return !== false but $return===product objects) or void out put if $return === false
     * @return bool|struct
     */
    protected function exportImages($return=false){
        if($return===false){
            $galleryData = array();
            foreach($this->products as $key=>$value){
                $galleryData[$key] = $value->getMediaGallery('images');
            }
            $this->actindo['images'] = array();
            $image = $galleryData[$this->defaultLanguage];
            for($i=0;$i<count($image);$i++){
                $id = $image[$i]['value_id'];
                if(!isset($this->images[$id])){
                    $this->images[$id] = $this->getImageEntry($galleryData,$i);
                }
                $this->parentPictures[$id] = true;
                $this->actindo['images'][] = $this->images[$id];
            }
        }else{
            $result = array();
            //run through images
            $galleryData = array();
            foreach($return as $key=>$value){
                $galleryData[$key] = $value->getMediaGallery('images');
            }
            $image = $galleryData[$this->defaultLanguage];
            for($i=0;$i<count($image);$i++){
                $id = $image[$i]['value_id'];
                if(!isset($this->images[$id])){
                    $this->images[$id] = $this->getImageEntry($galleryData,$i);
                }
                $result[] = $this->images[$id];
                unset($imageResult,$key,$value);
            }
            if(self::DISABLE_PARENT_ADDED_PICTURES===false){
                //add parent pictures
                if(count($this->parentPictures)>0){
                    foreach($this->parentPictures as $key=>$value){
                        $result[] = $this->images[$key];
                    }
                }
            }
            return $result;
        }
    }
    /**
     * Helper Method for Image Getter to Reduce reading Images from FS
     * @param $image Image Gallery Data Array
     * @param $count int Counter of current Element
     * @return mixed Image Array
     */
    protected function getImageEntry($gallery,$count){
        $image = $gallery[$this->defaultLanguage][$count];
        #first Prepare Basic Data
        $extension = explode('.',$image['file']);
        $extension = $extension[(count($extension)-1)];
        $path = Mage::getBaseDir('media').DIRECTORY_SEPARATOR.'catalog'.DIRECTORY_SEPARATOR.'product'.$image['file'];
        $entry = array(
            'image'=>((file_exists($path))?file_get_contents($path):''),
            'image_size'=>((file_exists($path))?filesize($path):0),
            'image_type'=>(isset($this->typeMap[$extension])?$this->typeMap[$extension]:'image/jpeg'),
            'image_name'=>$image['file'],
            'image_nr'=>$count,
            'image_title'=>array(),
        );
        foreach($this->mapping['id'] as $id=>$language){
            $entry['image_title'][$language] = $gallery[$id][$count]['label'];
        }
        return $entry;
    }
    /**
     * Currently Black List/White List Customer Groups are not Supported by Magento.
     * There is a plugin but within this connector not supported!
     */
    protected function exportCustomerGroupPermissions(){
        $this->actindo['group_permission'] = $this->customerGroups;
    }
    /**
     * exports the Crosselling Related Information
     */
    protected function exportCrossselings(){
        #Cross Selling Products
        $products = $this->product->getCrossSellProducts();
        $this->counter = 0;
        if(count($products)>0)
            foreach($products as $key){
                $this->actindo['xselling'][] = array(
                    'art_nr'    =>  $key->getSku(),
                    'group'     =>  1,
                    'sort_order' =>  $this->counter++
                );
            }
        #UpSelling Products
        $products = $this->product->getUpsellProducts();
        $this->counter = 0;
        if(count($products)>0)
            foreach($products as $key){
                $this->actindo['xselling'][] = array(
                    'art_nr'    =>  $key->getSku(),
                    'group'     =>  2,
                    'sort_order' =>  $this->counter++
                );
            }
        #Realted Products
        $products = $this->product->getRelatedProducts();
        $this->counter = 0;
        if(count($products)>0)
            foreach($products as $key){
                $this->actindo['xselling'][] = array(
                    'art_nr'    =>  $key->getSku(),
                    'group'     =>  3,
                    'sort_order' =>  $this->counter++
                );
            }
    }
    /**
     * Method used to export Article Properties
     */
    protected function exportProperties($return = false){
        $mapped = $this->helper->getAllMappedFields();
        $resultArray = array();
        foreach($this->allowedFields as $value){
            $usedlanguages = array();
            if($value!==-1 && $value!=="-1" && !isset($mapped[$value])){
                $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$value);
                $attribute = Mage::getModel('eav/entity_attribute')->load($attributeId);
                $attributeValue = $this->defaultProduct->getData($value);
                $attributeLanguage = $this->languages[$this->defaultLanguage]['language_code'];
                $attributeCode = $attribute->getAttributeCode();
                if($attribute->getIsGlobal()){
                    if(empty($attributeValue) && is_object($this->products[$this->mapping['code'][$attributeLanguage]])){
                        $attributeValue = $this->products[$this->mapping['code'][$attributeLanguage]]->getData($value);
                    }
                    if(empty($attributeValue)){
                        $attributeValue = $this->products[$this->defaultLanguage]->getData($value);
                    }
                    if($attributeValue!==null){
                        $usedlanguages[] = $attributeLanguage;
                        $data = array(
                            'field_id'=>$attributeCode,
                            'language_code'=>(((int)$attribute->getIsGlobal()>0)?'':$attributeLanguage),
                            'field_value'=>(string)(($this->product->getData($value)===null)?$attributeValue:$this->product->getData($value))
                        );
                        $resultArray[] = $data;
                    }
                }else{
                    foreach($this->mapping['code'] as $key){
                        $ident = $key;
                        $valueToIdent = $this->products[$key]->getData($value);
                        $attributeLanguage = 'de';//$this->languages[$ident]['language_code'];
                        # && (int)$ident!==(int)$this->defaultLanguage
                        if((!empty($valueToIdent)||$valueToIdent=="0") && !in_array($attributeLanguage,$usedlanguages)){
                            $usedlanguages[] = $attributeLanguage;
                            $data = array(
                                'field_id'=>$attributeCode,
                                'language_code'=>$attributeLanguage,
                                'field_value'=>(string)$valueToIdent,
                            );
                            $resultArray[] = $data;
                        }
                    }
                }
            }
        } 
        if($return){
            return $resultArray;
        }else{
            $this->actindo['properties'] = $resultArray;
        }
    }
    protected function returnTranslationData($value,$key){
        return array(
            'language_id'=>$key,
            'products_description'=>(($value->getData('description')!==null)?$value->getData('description'):''),
            'products_name'=>(($value->getData('name')!==null)?$value->getData('name'):''),
            'products_short_description'=>(($value->getData('short_description')!==null)?$value->getData('short_description'):''),
            'products_meta_keywords'=>(($value->getData('meta_keyword')!==null)?$value->getData('meta_keyword'):''),
            'products_meta_title'=>(($value->getData('meta_title')!==null)?$value->getData('meta_title'):''),
            'products_meta_description'=>(($value->getData('meta_description')!==null)?$value->getData('meta_description'):''),
            'seo_url'=>str_replace('.html','',(($this->product->getData('url_key')!==null)?$this->product->getData('url_key'):'')),
            'products_keywords'=>$this->getFieldValue('products_keywords',false,$key,($value->getId()>0?$value:false),$this->product),
            'products_url'=>$this->getFieldValue('products_url',false,$key,($value->getId()>0?$value:false),$this->product),
        );
    }
    /**
     * gets article translation
     * @param struct|bool $return parameter for telling method to either return information (containing variant data) or false to not return anything at all
     * @return array
     */
    protected function exportTranslations(){
        $data = array();
        foreach($this->mapping['code'] as $code){
            $data[$code] = $this->returnTranslationData($this->products[$code],$code);
        }
        $this->actindo['description'] = $data;
    }

    /**
     * method exports the price list of the current article
     * @param struct|bool $return attachs prices to $this->actindo if false, else this parameter needs to contain everything to calculate prices for variants
     * @return void|struct void if $return is false, else struct if parameter return contains variant specific informations
     */
    protected function exportPrices($return=false){
        $isBrutto = (int)Actindo_Connector_Model_System_Service::priceIncludesTax();
        if($return === false){
            $recordSet = $this->product;
        }else{
            $recordSet = $return;
        }
        $resultSet = array();
        $pricegroup = $recordSet->getData('group_price');
        $tierprice = $recordSet->getData('tier_price');
        $taxHelper = Mage::getSingleton('tax/calculation');
        //Pseudo Preis
        $resultSet['products_pseudoprices'] = array(
            -1=>(float)$recordSet->getMsrp()
        );
        // get Grundpreis
        $resultSet['grundpreis'] = (float)$recordSet->getPrice();
        //EK
        $resultSet['ek'] = (float)$recordSet->getCost();
        $defaultTax = $this->helper->getTaxRate($this->product,$taxHelper->getDefaultCustomerTaxClass($this->storeid));
        //removed original tax detection with the store settings
        $resultSet['is_brutto']=$isBrutto;//(int)((int)$defaultTax>0);
        //now get the group prices
        if(count($tierprice)>0){
            //pre sorting
            $tierPriceByGroup = array();
            foreach($tierprice as $tier){
                if((int)$tier['all_groups']>0){
                    $tierPriceByGroup[-1][] = $tier;
                }else{
                    $tierPriceByGroup[$tier['cust_group']][] = $tier;
                }
            }
            $tierprices = array();
            //create basic fields for complete set
            foreach($this->customerGroups as $group){
                $tierprices[$group] = array();
            }
            //if price is valid for all groups
            if(isset($tierPriceByGroup[-1])){
                foreach($tierPriceByGroup[-1] as $tier){
                    foreach($this->customerGroups as $group){
                        $tierprices[$group][(int)$tier['price_qty']] = $tier['price'];
                    }
                }
                //unset all groups tier price
                unset($tierPriceByGroup[-1]);
            }
            //do rest of the group prices
            if(count($tierPriceByGroup)>0){
                foreach($tierPriceByGroup as $ident=>$tierByGroup){
                    foreach($tierByGroup as $tier){
                        $tierprices[$ident][(int)$tier['price_qty']] = $tier['price'];
                    }
                }
            }
            //now fill the main data array
            if(count($tierprices)>0){
                foreach($tierprices as $groupid=>$priceranges){
                    //sort array
                    ksort($priceranges);
                    $defaultTax = $this->helper->getTaxRate($this->product,$groupid);
                    //removed original tax detection with the store settings
                    $resultSet['preisgruppen'][$groupid] = array(
                        'is_brutto'=>$isBrutto//(int)((int)$defaultTax>0)
                    );
                    //check grundpreis, if not product price is used
                    if(!isset($priceranges[1])){
                        $resultSet['preisgruppen'][$groupid]['grundpreis']=$recordSet->getPrice();
                    }else{
                        $resultSet['preisgruppen'][$groupid]['grundpreis']=$priceranges[1];
                        unset($priceranges[1]);
                    }
                    //run throuh remaining groups
                    $i = 1;
                    foreach($priceranges as $range=>$price){
                        $resultSet['preisgruppen'][$groupid]['preis_gruppe'.$i] = (float)$price;
                        $resultSet['preisgruppen'][$groupid]['preis_range'.$i] = (int)$range;
                        $i++;
                    }
                }
                foreach($pricegroup as $tier){
                    if(!isset($resultSet['preisgruppen'][$tier['cust_group']])){
                        $resultSet['preisgruppen'][$tier['cust_group']] = array();
                    }
                    $defaultTax = $this->helper->getTaxRate($this->product,$tier);
                    //removed original tax detection with the store settings
                    $resultSet['preisgruppen'][$tier['cust_group']]['is_brutto'] = $isBrutto;//(int)((int)$defaultTax>0);
                    $resultSet['preisgruppen'][$tier['cust_group']]['grundpreis']=$tier['price'];
                }
            }
        }elseif(count($pricegroup)>0){
            foreach($pricegroup as $tier){
                if(isset($resultSet['preisgruppen'][$tier['cust_group']]))
                    $resultSet['preisgruppen'][$tier['cust_group']] = array();
                $defaultTax = $this->helper->getTaxRate($this->product,$tier);
                //removed original tax detection with the store settings
                $resultSet['preisgruppen'][$tier['cust_group']]['is_brutto'] = $isBrutto;//(int)((int)$defaultTax>0);
                $resultSet['preisgruppen'][$tier['cust_group']]['grundpreis']=$tier['price'];
            }
        }
        if($return!==false){
            return $resultSet;
        }else{
            foreach($resultSet as $key=>$value){
                $this->actindo[$key] = $value;
            }
        }
    }
}
