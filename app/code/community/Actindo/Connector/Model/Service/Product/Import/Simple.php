<?php
/**
 * Actindo Faktura/WWS Connector
 * import product
 * Imports Products from Actindo ERP to Shop
 * Extends the Product Export
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 */
class Actindo_Connector_Model_Service_Product_Import_Simple{
    const XMLPATHLANG = 'actindo/storemapping/';
    /**
     * @var Actindo_Connector_Model_Service_Product Magento PRoduct
     */
    protected $actindo;
    /**
     * @var Actindo_Connector_Model_Service_Product Magento PRoduct
     */
    protected $product;
    /**
     * @var array containing the shop article data set of actindo propertie
     */
    protected $shoparticle;
    /**
     * @var bool displaying if product is new created or not
     */
    protected $newProduct = false;
    /**
     * @var Actindo_Connector_Model_Variants_Helper Variant Helper Object
     */
    protected $helper;
    /**
     * @var array map of possible tax classes
     */
    protected $taxmap;
    /**
     * @var string containing the master article number
     */
    protected $artnr = null;
    /**
     * @var Varien_Io_File helper
     */
    protected $filehandler;
    /**
     * Import Directory
     * @var string
     */
    protected $importDir;
    /**
     * allowed Customer Groups
     * @var array
     */
    protected $customgroup=array();
    /**
     * Attribute Collection
     * @var array
     */
    protected $attributeCollection = array(
        //basic data
        'products_ean',
        'ursprungsland',
        'auslaufdatum',
        'einheit',
        'size_b',
        'height',
        'length',
        #'size_unit',
        //webshop
        'email_notification',
        'products_weight',
        'topseller',
        'products_date_available',
        'manufacturerid',
        'suppliernumber',
        'pseudosales',
        'products_vpe_status',
        'products_vpe_value',
        'products_vpe',
        'products_vpe_staffelung',
        'products_vpe_referenzeinheit',
        'products_digital',
        'shipping_free',
        'abverkauf',
    );
    /**
     * Attribute Cross Mapping
     * @var array
     */
    protected $actindoMapping = array(
        'height'=>'size_h',
        'length'=>'size_l',
        'size_b'=>'size_b',
        'weightunit'=>'weight_unit',
        'einheit'=>'einheit',
        'ursprungsland'=>'ursprungsland',
        'manufacturerid'=>'manufacturers_id',
        'auslaufdatum'=>'auslaufdatum',
    );
    /**
     * contains the default Language Id
     * @var integer
     */
    protected $defaultLanguage;
    /**
     * Array that contains data that is prepared in the update translation method
     * @var array
     */
    protected $imageTranslation=array();
    /**
     * construction of class and preparing create data
     */
    public function __construct(){
        $this->filehandler = new Varien_Io_File();
        $this->importDir = Mage::getBaseDir('media') . DS . 'actindoimport' . DS;
        if($this->filehandler->fileExists($this->importDir,false)){
            $this->filehandler->rmdirRecursive($this->importDir);
        }
        $importReadyDirResult = $this->filehandler->mkdir($this->importDir);
        if(!$importReadyDirResult){
            throw new Actindo_Connector_Model_Exception_Error('Directory '.$this->importDir.' could not be created!',400);
        }
        $this->helper = Mage::helper('connector');
        $helper = Mage::helper('core');
        $taxhelper = Mage::helper('tax');
        $taxclass = $helper->jsonDecode($taxhelper->getAllRatesByProductClass());
        $allowedTax = Mage::getStoreConfig('actindo/taxmapping/taxsets',Mage::app()->getStore()->getId());
        $allowedTax = explode(',',$allowedTax);
        if(count($allowedTax)>0){
            foreach($taxclass as $key=>$value){
                $checkVal = explode('_',$key);
                if(in_array((int)$checkVal[1],$allowedTax)){
                    $this->taxmap[$value] = (int)$checkVal[1];
                }
            }
        }
        $singleton = Mage::getSingleton('connector/util_util');
        $siteid = Mage::app()->getStore()->getWebsiteId();
        $group = $singleton->getCustomerGroups($siteid);
        foreach($group as $key){
            $this->customgroup[] = $key['customers_status_id'];
        }
        $this->defaultLanguage = $singleton->getDefaultLanguage();
    }
    /**
     *
     * @param $product
     * @return array
     */
    public function doUpdate($product) {
        $this->actindo = $product;
        $newArticle = false;
        $this->checkFields();
        $this->shoparticle = $product['shop']['art'];
        $this->artnr = $this->actindo['art_nr'];
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        if($this->product!==false &&  $this->product->getTypeId()!==Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($this->product);
            $childProducts = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
            if(count($childProducts)>0){
                foreach($childProducts as $product){
                    $product->delete();
                }
            }
            $this->product->delete();
            $this->product = false;
        }
        if($this->product === false){
            $newArticle = true;
        }
        $this->product = Mage::getModel('catalog/product');
        if(isset($productId) && (int)$productId>0){
            $this->product->setId($productId);
        }
        $this->product->setSku($this->actindo['art_nr']);
        $this->newProduct = true;
        $this->product->setAttributeSetId($this->actindo['shop']['art']['filtergroup_id']);
        $this->product->setStoreId(Mage::app()->getStore()->getStoreId());//Mage::app()->getStore()->getWebsiteId()
        $this->product->setCreatedAt(strtotime('now'));
        $this->product->save();
        $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        $this->setWebSiteId();
        $this->product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        //Execute the core stuff
        $this->updateCoreData();
        //Edit Artikel for each Language and save it again!
        $this->updateTranslations();
        $this->updateAttributes($this->product);
        #$this->updateCategories();
        $this->updateContent();
        $this->updateCrosssellings();
        $this->updateCustomerGroupPermissions();
        Mage::register('product', $product);
        Mage::register('current_product', $product);
        //Add/Update Prices
        $this->updatePrices();
        $this->doUpdateStock($this->actindo['l_bestand']);
        //Add Images
        $this->updateImages();
        $this->product->save();
        $this->updateImageTranslation();
        return array('ok' => true, 'success' => 1);
    }
    /**
     * Check for Required Fields
     * Article Name
     * Description
     * Longtext
     * tax
     * price
     * @return void
     * @throws Actindo_Connector_Model_Exception_Error if a value is missing!
     */
    protected function checkFields(){
        if(empty($this->actindo['art_name'])){
            throw new Actindo_Connector_Model_Exception_Error('Article name musst contain a value!',401);
        }
        //description
        //shortdescription
        if(
            empty($this->actindo['shop']['desc'][0]['products_description'])
            &&
            Actindo_Connector_Model_System_Service::getRequiredField('description'))
        {
            throw new Actindo_Connector_Model_Exception_Error('Article description (long description) musst contain a value!',402);
        }
        if(
            empty($this->actindo['shop']['desc'][0]['products_short_description'])
            &&
            Actindo_Connector_Model_System_Service::getRequiredField('shortdescription'))
        {
            throw new Actindo_Connector_Model_Exception_Error('Article description (short description) musst contain a value!',403);
        }
        if(
            empty($this->actindo['ean'])
            &&
            Actindo_Connector_Model_System_Service::getRequiredField('eancode'))
        {
            throw new Actindo_Connector_Model_Exception_Error('Article musst have an EAN Code!',406);
        }
        //price
        if(empty($this->actindo['grundpreis']))
        {
            throw new Actindo_Connector_Model_Exception_Error('Price musst be set!',404);
        }
        //price greator then 0
        if(
            (float)$this->actindo['grundpreis'] == 0.00
            &&
            !(bool)Mage::getStoreConfig('actindo/features/zeroprice',Mage::app()->getStore()->getWebsiteId()))
        {
            throw new Actindo_Connector_Model_Exception_Error('Price musst be greater then 0 Euro!',405);
        }
    }
    /**
     * Setter for Setting special Options
     */
    protected function setProductSpecialOptions()
    {
        $type = $visibility = '';
        foreach($this->actindo['shop']['properties'] as $property)
        {
            if($property['field_id'] == 'product_type')
            {
                $type = $property['field_value'];
            }
            if($property['field_id']=='product_vsibility')
            {
                $visibility = $property['field_value'];
            }
        }
        //set type
        if(!empty($type) && $this->isNewProduct())
        {
            $this->product->setTypeId($type);
        }
        else
        {
            $this->product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        }
        if(!empty($visibility))
        {
            //set visibility
            $this->product->setVisibility($visibility);
        }
        else
        {
            $this->product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        }
    }
    /**
     * Setter for Website Id
     */
    protected function setWebSiteId()
    {
        $sites = $this->product->getWebsiteIds();
        if(!in_array(Actindo_Connector_Model_System_Service::getStoreID(),$sites))
        {
            $sites[] = Actindo_Connector_Model_System_Service::getStoreID();
        }
        $this->product->setWebsiteIds($sites);
    }
    /**
     * check if product has been just created
     * @return bool
     */
    protected function isNewProduct()
    {
        return $this->newProduct;
    }
    /**
     * method for updating core information (like status or article name)
     */
    protected function updateCoreData()
    {
        $status = '';
        if(isset($this->shoparticle['products_status']) && (bool)$this->shoparticle['products_status'])
        {
            $status = 1;
        }
        else
        {
            $status = 2;
        }
        $this->product->setStatus($status);
        $this->product->setName($this->actindo['art_name']);
        if(isset($this->actindo['shop']['art']['shipping_status']))
        {
            $shippingstatus = $this->actindo['shop']['art']['shipping_status'];
            if(Actindo_Connector_Model_System_Service::isShippingObserverEnabled())
            {
                if((int)$shippingstatus>0)
                {
                    $ship = Mage::getModel('connector/acmapper')->load($shippingstatus);
                    $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                    if($ship->getId()>0)
                    {
                        $this->product->setData($shippingField,$ship->getValue());
                    }
                }
            }
            else
            {
                $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                $this->product->setData($shippingField,$this->actindo['shop']['art']['shipping_status']);
            }
        }
        //Update Article URL
        $util = new Actindo_Connector_Model_Util_Util();
        //first check if default Langage has an URL set
        $default = $util->getDefaultLanguage();
        if(
            isset($this->actindo['shop']['desc'][$default])
            &&
            isset($this->actindo['shop']['desc'][$default]['seo_url'])
            &&
            !empty($this->actindo['shop']['desc'][$default]['seo_url']))
        {
            $url = $this->actindo['shop']['desc'][$default]['seo_url'];
        }
        else
        {
            foreach($this->actindo['shop']['desc'] as $desc)
            {
                if(!empty($desc['seo_url']))
                {
                    $url = $desc['seo_url'];
                    break;
                }
            }
        }
        if(!empty($url))
        {
            $url = str_replace(array('.html','.htm'),'',$url);
            $url = Mage::getModel('catalog/product_url')->formatUrlKey($url);
            $this->product->setUrlKey($url);
            $this->product->setUrlKeyCreateRedirect(1);
        }
        //Mapping According to the System Mapper
        $fieldManufactorerLink = Actindo_Connector_Model_Data::getFieldValue('products_url');
        $fieldProductsKeywords = Actindo_Connector_Model_Data::getFieldValue('products_keywords');
        //set Manufactorer Link and search keywords
        if(!empty($fieldManufactorerLink) && (int)$fieldManufactorerLink!==-1)
        {
            $manufactorerValue = $this->actindo['shop']['desc'][(int)$default]['products_url'];
            $this->product->setData($fieldManufactorerLink,$manufactorerValue);
        }
        if(!empty($fieldProductsKeywords) && (int)$fieldProductsKeywords!==-1)
        {
            $productSearch = $this->actindo['shop']['desc'][(int)$default]['products_keywords'];
            $this->product->setData($fieldProductsKeywords,$productSearch);
        }
    }

    /**
     * returns the value of a field
     * @param $attribute
     * @return mixed
     */
    protected function getValueFromAttribute($attribute,$field)
    {
        $result=false;
        if($field==='manufacturerid')
        {
            $mapperEntry = Mage::getModel('connector/acmapper')->getCollection()
                ->addFieldToFilter('type','manufacturers')
                ->addFieldToFilter('id',$this->actindo['shop']['art']['manufacturers_id'])
                ->addFieldToFilter('site',1)
                ->getFirstItem();
			if($mapperEntry->getId() > 0){
				$result = $mapperEntry->getValue();
			}else
				if(isset($this->actindo['shop']['art']['manufacturers_id']))
			{
				$result = $this->actindo['shop']['art']['manufacturers_id'];
			}
        }
        else if($field==='weight')
        {
            if(isset($this->actindo['shop']['art']['products_weight']))
            {
                $result = $this->actindo['shop']['art']['products_weight'];
            }
        }
        elseif(isset($this->actindoMapping[$attribute]))
        {
            if(isset($this->actindo[$this->actindoMapping[$attribute]]))
            {
                $result = $this->actindo[$this->actindoMapping[$attribute]];
            }
        }
        else
        {
            if(isset($this->actindo['shop']['art'][$attribute]))
            {
                $result = $this->actindo['shop']['art'][$attribute];
            }
        }
        if($result!==false)
        {
            return $result;
        }
    }
    /**
     * Update Article Attributes
     */
    protected function updateAttributes(&$product)
    {
        foreach($this->attributeCollection as $attribute)
        {
            $field = Actindo_Connector_Model_Data::getFieldValue($attribute);
            if(!empty($field) && (int)$field!==-1)
            {
                $product->setData($field,$this->getValueFromAttribute($attribute,$attribute));
            }
        }
        $field = Actindo_Connector_Model_Data::getFieldValue('uvp');
        if(
            !empty($field)
            &&
            (int)$field!==-1
            &&
            isset($this->actindo['shop']['products_pseudoprices'][-1])
            &&
            (float)$this->actindo['shop']['products_pseudoprices'][-1] > (float)0)
        {
            $pseudo = $this->actindo['shop']['products_pseudoprices'][-1];
            $product->setData($field,$pseudo);
        }
        if(isset($this->actindo['mwst']))
        {
            $product->setTaxClassId($this->taxmap[(int)$this->actindo['mwst']]);
        }
    }
    /**
     * Update Articke Categories
     */
    protected function updateCategories(){
        $categories = array();
        if(!in_array($this->actindo['swg'],$categories)){
            $categories[] = $this->actindo['swg'];
        }
        if(isset($this->actindo['shop']['all_categories'])){
            if(is_array($this->actindo['shop']['all_categories']) && count($this->actindo['shop']['all_categories'])>0){
                foreach($this->actindo['shop']['all_categories'] as $category){
                    if(!in_array($category,$categories)){
                        $categories[] = $category;
                    }
                }
            }else if(!empty($this->actindo['shop']['all_categories'])){
                if(!in_array($this->actindo['shop']['all_categories'], $categories)){
                    $categories[] = $this->actindo['shop']['all_categories'];
                }
            }
        }
        $cat = $this->product->getCategoryIds();
        foreach($cat as $key=>$value){
            if(!in_array($value,$categories)){
                unset($cat[$key]);
            }
        }
        foreach($categories as $value){
            if(!in_array($value,$cat)){
                $cat[] = $value;
            }
        }
        $this->product->setCategoryIds($cat);
    }
    /**
     * Update Content
     * @Todo: needs  to be implemented
     */
    protected function updateContent()
    {

    }
    /**
     * Update Cross Selling
     */
    protected function updateCrosssellings(){
        $crosssellingSet    = array();
        $upsellingSet       = array();
        $relatedSet         = array();
        if(is_array($this->actindo['shop']['xselling']) && count($this->actindo['shop']['xselling'])>0){
            foreach($this->actindo['shop']['xselling'] as $valueSet){
                if(!empty($valueSet['art_nr'])){
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$valueSet['art_nr']);
                    if($product !== false){
                        if((int)$valueSet['group']== 1){
                            $crosssellingSet[$product->getId()] = array('position'=>0);
                        }
                        if((int)$valueSet['group']== 2){
                            $upsellingSet[$product->getId()] = array('position'=>0);
                        }
                        if((int)$valueSet['group']== 3){
                            $relatedSet[$product->getId()] = array('position'=>0);
                        }
                    }
                }
            }
        }
        $this->product->setCrossSellLinkData($crosssellingSet);
        $this->product->setUpSellLinkData($upsellingSet);
        $this->product->setRelatedLinkData($relatedSet);
    }
    /**
     * Currently Black List/White List Customer Groups are not Supported by Magento.
     * There is a plugin but within this connector not supported!
     */
    protected function updateCustomerGroupPermissions(){}
    /**
     * Update Images
     */
    protected function updateImages(){
        //Remove Existing Images to Reupload them
        if(isset($this->imageTranslation) && count($this->imageTranslation)>0)
        {
            foreach($this->imageTranslation['translations'] as $translation){
                $product = Mage::getModel('catalog/product')->setStoreId($translation['language_id'])->load($this->product->getId());
                $product
                    ->setImage(null)
                    ->setSmallImage(null)
                    ->setThumbnail(null);
                $product->save();
            }
        }
        $gallery = $this->product->getData('media_gallery');
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
        if($this->product->getId()>0){
            try{
                $items = $mediaApi->items($this->product->getId());
                foreach($items as $item){
                    $mediaApi->remove($this->product->getId(),$item['file']);
                }
            }catch(Exception $e){
            }
        }
        //Add new images
        if(count($this->actindo['shop']['images'])>0){
            //write images into directory
            //and run Import
            $currentimage = 0;
            $mediaAttribute = array(
                'thumbnail',
                'small_image',
                'image'
            );
            $defaultLanguageIdent = $this->imageTranslation['mapping']['id'][$this->defaultLanguage];
            if(count($this->actindo['shop']['images'])>0){
                foreach($this->actindo['shop']['images'] as $image){
                    $file = $this->importDir.$image['image_name'];
                    file_put_contents($file,$image['image']);
                    if((int)$currentimage===0){
                        $this->product->addImageToMediaGallery($file,$mediaAttribute,false,false);
                    }else{
                        $this->product->addImageToMediaGallery($file,null,false,false);
                    }
                    $currentimage++;
                }
                $images = $this->product->getData('media_gallery');
                foreach($images['images'] as $imageId=>$imageValue){
                    $imageLabel = $this->actindo['shop']['images'][$imageValue['position']]['image_title'][$defaultLanguageIdent];
                    $images['images'][$imageId]['label'] = $imageLabel;
                }
                $this->product->setData('media_gallery',$images);
            }
        }
    }
    protected static $taxSetting=null;
    /**
     * calculates either the netto/brutto price depending on the shop settings
     * @param float $price price
     * @param float $mwst vat
     * @param string $setting either B for Brutton or N for Netto
     * @return float price value
     */
    protected function priceCalculate($price,$mwst,$setting='B'){
        if(self::$taxSetting===null){
            self::$taxSetting = Actindo_Connector_Model_System_Service::priceIncludesTax();
        }
        if(self::$taxSetting){
            if($setting!=='B'&&(int)$setting!==1){
                $tax = ((float)$mwst/(float)100)+(float)1;
                $price = round((float)$price * (float)$tax,4);
            }
        }else if($setting!=='N' && (int)$setting!==0){
            $tax = ((float)$mwst/(float)100)+(float)1;
            if($tax>(float)0){
                $price = (float)$price/(float)$tax;
            }
        }
        $price = round($price,5);
        return $price;
    }
    /**
     * update Prices
     */
    protected function updatePrices(){
        //first unset groups and tears
        $this->product->group_price = array();
        $this->product->tier_price = array();
        $this->product->save();
        //then save them new
        $groups = Mage::getModel('customer/group')->getCollection()->getAllIds();
        if(isset($this->actindo['grundpreis']) && (float)$this->actindo['grundpreis'] > 0.00){
            $this->product->setPrice($this->priceCalculate($this->actindo['grundpreis'],$this->actindo['mwst'],$this->actindo['is_brutto']));
        }
        if(isset($this->actindo['shop']['art']['products_pseudoprices']) && (float)$this->actindo['shop']['art']['products_pseudoprices'] > 0.00){
            $this->product->setMsrp((float)$this->actindo['shop']['art']['products_pseudoprices']);
        }
        //preapre the price data for ranges
        $prepareDatas = array();
        $this->product->setCost($this->actindo['ek']);
        if(count($this->actindo['preisgruppen'])){
            foreach($this->actindo['preisgruppen'] as $group){
                $groupid = (int)$group['gruppe']-250;
                if(in_array($groupid,$this->customgroup)){
                    $dset = array('gruppe'=>0,'gp'=>0,'range'=>array());
                    foreach($group as $key=>$value){
                        if($key ===  'gruppe'){
                            $dset['gruppe']= ((int)$value - (int)250);
                        }else if($key === 'grundpreis'){
                            $dset['gp'] = $this->priceCalculate($value,$this->actindo['mwst'],$group['is_brutto']);
                        }else if(strpos($key,'preis_gruppe')!==false){
                            $ident = (int)str_replace('preis_gruppe','',$key);
                            if(!isset($dset['range'][$ident]) || !is_array($dset['range'][$ident])){
                                $dset['range'][$ident] = array();
                            }
                            $dset['range'][$ident]['price'] = $this->priceCalculate($value,$this->actindo['mwst'],$group['is_brutto']);
                        }else if(strpos($key,'preis_range')!==false){
                            $ident = (int)str_replace('preis_range','',$key);
                            if(!isset($dset['range'][$ident]) || !is_array($dset['range'][$ident])){
                                $dset['range'][$ident] = array();
                            }
                            $dset['range'][$ident]['quantity'] = $value;
                        }
                    }
                }
                if(isset($dset)){
                    $prepareDatas[] = $dset;
                }
            }
        }
        //now process the stuff for each pricegroup to add it to the main article
        $storeid = Mage::app()->getStore()->getWebsiteId();
        $pricegroupdata = array();
        $tiergroupdata = array();
        if(count($prepareDatas)>0){
            foreach($prepareDatas as $prepareData){
                //first set the Grundpreis
                $pricegroupdata[] = array(
                    'website_id'=>0,
                    'cust_group'=>$prepareData['gruppe'],
                    'price'=>(float)$prepareData['gp'],
                    'website_price'=>(float)$prepareData['gp'],
                    'all_Groups'=>0
                );
                //now run through the groups if available
                if(count($prepareData['range'])>0){
                    foreach($prepareData['range'] as $range){
                        if(count($range)>1){
                            $tiergroupdata[] = array(
                                'website_id'=>0,
                                'cust_group'=>$prepareData['gruppe'],
                                'price'=>(float)$range['price'],
                                'website_price'=>(float)$range['price'],
                                'price_qty'=>number_format((float)$range['quantity'], 4, '.',''),
                                'all_Groups'=>0
                            );
                        }
                    }
                }
            }
        }
        if(count($pricegroupdata)>0){
            $this->product->group_price = $pricegroupdata;
        }
        if(count($tiergroupdata)>0){
            $this->product->tier_price = $tiergroupdata;
        }
        #$this->product->save();
    }
    /**
     * Update Translations
     * @param bool/array $translations Per Default = 0 that means it pulls itself the Translation Data, else it is imputed from outside. This is required for Variant Articles!
     */
    protected function updateTranslations($translations = false){
        if(!($this instanceof Actindo_Connector_Model_Service_Product_Import_Child)){
            $artname = $this->actindo['art_name'];
            $desclong = $this->actindo['shop']['desc'][0]['products_description'];
            $descshort = $this->actindo['shop']['desc'][0]['products_short_description'];
        }else{
            $artname = $this->actindo['art_name'];
            $desclong = $this->actindo['langtext'];
            $descshort = $this->actindo['beschreibung'];
        }
        if($translations===false){
            $singleton = Mage::getSingleton('connector/util_util');
            $translations = $singleton->getLanguages();
            $defaultLang = $singleton->getDefaultLanguage();
        }else{
            $defaultLang = 1;
        }
        $this->imageTranslation['translations'] = $translations;
        $this->imageTranslation['defaultLang'] = $defaultLang;
        $permissions = -1;
        //Prepare Properties
        $util = new Actindo_Connector_Model_System_Config_Source_Mapping();
        $allowedfields = $util->getFieldList(Mage::app()->getWebsite()->getId());
        $propertyset = array();
        foreach($this->actindo['shop']['properties'] as $value){
            if(isset($value['language_id'])){
                $langcode = $value['language_id'];
                if(!empty($langcode) && in_array($value['field_id'],$allowedfields)){
                    if(!isset($propertyset[$langcode])){
                        $propertyset[$langcode] = array();
                    }
                    $propertyset[$langcode][] = array(
                        'field'=>$value['field_id'],
                        'value'=>$value['field_value']
                    );
                }
            }elseif(!isset($value['language_id']) && in_array($value['field_id'],$allowedfields)){
                if(!isset($propertyset[0])){
                    $propertyset[0] = array();
                }
                $propertyset[0][] = array(
                    'field'=>$value['field_id'],
                    'value'=>$value['field_value']
                );
            }
        }
        $mapping = array();
        //Mapping According to the System Mapper
        $fieldManufactorerLink = Actindo_Connector_Model_Data::getFieldValue('products_url');
        $fieldProductsKeywords = Actindo_Connector_Model_Data::getFieldValue('products_keywords');
        //shipping STatus Field
        $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
        //prepare Data for Storage
        if(count($translations)>0){
            foreach($translations as $key){
                $mapping['ident'][$key['language_code']][] = $key['language_id'];
                $mapping['id'][$key['language_id']] = $key['language_code'];
            }
            $this->imageTranslation['mapping'] = $mapping;
            $keymaps = Mage::getModel('connector/ackeyvaluemap')
                ->getCollection()
                ->addFieldToFilter('type','languages');
            $existingMap = array();
            if(count($keymaps)>0){
                foreach($keymaps as $keymap){
                    if(!isset($mapping['ident'][$keymap->getValue()])){
                        $mapping['ident'][$keymap->getValue()] = array();
                    }
                }
            }
            $storeid = Actindo_Connector_Model_System_Service::getStoreID();
            foreach($translations as $store){
                $code = self::XMLPATHLANG.'language_'.$store['store_code'];
                $sitelanguage = strtolower(Mage::getStoreConfig($code,$storeid));
                if($sitelanguage!=='default'){
                    $containingContainer = $mapping['id'][$store['language_id']];
                    $unsetkey = array_search($store['language_id'],$mapping['ident'][$store['language_code']]);
                    unset($mapping['ident'][$store['language_code']][$unsetkey]);
                    $mapping['ident'][$sitelanguage][] = $store['language_id'];
                    $mapping['id'][$store['language_id']] = $sitelanguage;
                }
            }
            if(count($this->actindo['shop']['desc'])>0){
                foreach($this->actindo['shop']['desc'] as $currentlanguage=>$currenttr){
                    if(empty($currenttr['language_id'])){
                        $languageCode = $currenttr['language_code'];
                        if(isset($mapping['ident'][$languageCode]) && (int)$mapping['ident'][$languageCode][0]>0){
                            $currenttr['language_id'] = (int)$mapping['ident'][$languageCode][0];
                        }else{
                            continue;
                        }
                    }
                    $currentLanguageIdent = $currenttr['language_id'];
                    //get Store Ids for this translation run
                    //step 1 get current language
                    if(isset($currenttr['language_id']) && isset($mapping['id'][$currentLanguageIdent]) && isset($mapping['ident'][$mapping['id'][$currentLanguageIdent]])){
                        $language = $mapping['id'][$currentLanguageIdent];
                        $shops = $mapping['ident'][$language];
                        if(count($shops)>0){
                            foreach($shops as $shop){
                                $product = Mage::getModel('catalog/product');
                                $id = $product->getIdBySku( $this->artnr );
                                $product->setStoreId($shop)->load( $id );
                                //Core Translation
                                $product->setName((!empty($currenttr['products_name']))?$currenttr['products_name']:$artname);
                                if(isset($this->actindo['shop']['art']['shipping_status'])){
                                    if(!Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                                        $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                                        $product->setData($shippingField,$this->actindo['shop']['art']['shipping_status']);
                                    }
                                }
                                //update properties
                                $this->updateAttributes($product);
                                //Meta Stuff
                                if(!empty($currenttr['products_meta_title']) || !empty($currenttr['products_meta_keywords']) || !empty($currenttr['products_meta_description'])){
                                    $product->setMetaAutogenerate(0);

                                    if(!empty($currenttr['products_meta_title'])){
                                        $product->setMetaTitle($currenttr['products_meta_title']);
                                    }elseif(!empty($currenttr['products_name'])){
                                        $product->setMetaTitle($currenttr['products_name']);
                                    }elseif(!empty($artname)){
                                        $product->setMetaTitle($artname);
                                    }else{
                                        $product->setMetaTitle(false);
                                    }
                                    if(!empty($currenttr['products_meta_description'])){
                                        $product->setMetaDescription($currenttr['products_meta_description']);
                                    }elseif(!empty($currenttr['products_description'])){
                                        $product->setMetaDescription($currenttr['products_short_description']);
                                    }else{
                                        $product->setMetaDescription(false);
                                    }
                                    if(!empty($currenttr['products_meta_keywords'])){
                                        $product->setMetaKeyword($currenttr['products_meta_keywords']);
                                    }else{
                                        $product->setMetaKEyword(false);
                                    }
                                }else{
                                    $product->setMetaTitle(false);
                                    $product->setMetaDescription(false);
                                    $product->setMetaKeyword(false);
                                    $product->setMetaAutogenerate(1);
                                }
                                $short = '';
                                if(!empty($currenttr['products_short_description'])){
                                    $short = $currenttr['products_short_description'];
                                }else{
                                    $short = false;
                                }
                                $product->setShortDescription($short);
                                $long = '';
                                if(!empty($currenttr['products_description'])){
                                    $long = $currenttr['products_description'];
                                }else{
                                    $long = false;
                                }
                                $product->setDescription($long);
                                $propertyRun = array();
                                //Build Property Set if both current language and general are set
                                if(isset($propertyset[$currentLanguageIdent]) && isset($propertyset[0])){
                                    $propertyRun = array_merge($propertyset[$currentLanguageIdent],$propertyset[0]);
                                //atleast the current Language exists
                                }elseif(isset($propertyset[$currentLanguageIdent])){
                                    $propertyRun = $propertyset[$currentLanguageIdent];
                                //at least General Properties exist
                                }elseif(isset($propertyset[0])){
                                    $propertyRun = $propertyset[0];
                                }
                                //if at least one is defined, run
                                if(count($propertyRun)>0){
                                    foreach($propertyRun as $value){
                                        //add Value to Product
                                        if(empty($value['value']))
                                        {
                                            $newValue = false;
                                        }
                                        else
                                        {
                                            $newValue = $value['value'];
                                        }
                                        $product->setData($value['field'],$newValue);
                                    }
                                }
                                //shop visibility
                                if(!is_array($permissions)){
                                    if(isset($this->shoparticle['products_status']) && (bool)$this->shoparticle['products_status']){
                                        $status = 1;
                                    }else{
                                        $status = 2;
                                    }
                                }else{
                                    if(isset($permissions[$shop])){
                                        $status = 1;
                                    }else{
                                        $status = 2;
                                    }
                                }
                                $product->setStatus($status);
                                //set Manufactorer Link and search keywords
                                if(!empty($fieldManufactorerLink) && (int)$fieldManufactorerLink!==-1){
                                    $product->setData($fieldManufactorerLink,$currenttr['products_url']);
                                }
                                if(!empty($fieldProductsKeywords) && (int)$fieldProductsKeywords!==-1){
                                    $product->setData($fieldProductsKeywords,$currenttr['products_keywords']);
                                }
                                //set use default images
                                $product->setImage(false);
                                $product->setSmallImage(false);
                                $product->setThumbnail(false);
                                if(!Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                                    $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                                    $product->setData($shippingField,$this->actindo['shop']['art']['shipping_status']);
                                }
                                //delivery Time Update
                                if(isset($this->actindo['shop']['art']['shipping_status'])){
                                    $shippingstatus = $this->actindo['shop']['art']['shipping_status'];
                                    //update delivery time
                                    if(Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                                        if((int)$shippingstatus>0){
                                            $ship = Mage::getModel('connector/acmapper')->load($shippingstatus);
                                            if($ship->getId()>0){
                                                $product->setData($shippingField,$ship->getValue());
                                            }
                                        }
                                    }
                                }
                                //save
                                $product->save();
                            }
                        }
                    }
                }
            }
        }
        //now Set the Properties for the Default View
        //get current View/actindo id
        $language = $mapping['id'][$defaultLang];
        $views = $mapping['ident'][$language];
        $currentLangugageId = 1;
        foreach($views as $ident){
            if(isset($this->actindo['shop']['desc'][$ident])){
                $currentLangugageId = $ident;
                break;
            }
        }
        $propertyRun = array();
        //Build Property Set if both current language and general are set
        if(isset($propertyset[$currentLangugageId]) && isset($propertyset[0])){
            $propertyRun = array_merge($propertyset[$currentLangugageId],$propertyset[0]);
            //atleast the current Language exists
        }elseif(isset($propertyset[$currentLangugageId])){
            $propertyRun = $propertyset[$currentLangugageId];
            //at least General Properties exist
        }elseif(isset($propertyset[0])){
            $propertyRun = $propertyset[0];
        }
        //if at least one is defined, run
        if(count($propertyRun)>0){
            foreach($propertyRun as $value){
                //add Value to Product
                $this->product->setData($value['field'],$value['value']);
            }
        }
    }
    /**
     * Returns the default Attribute set for simple products (the default Attribute Set Id)
     * @return mixed
     */
    private function getDefaultAttributeSetId(){
        return $this->helper->getDefaultAttributeSetId();
    }
    /**
     * Update Stock
     * @param integer $stock Numberic Value of current stock
     * @param bool $parent If true this is a parent article and has special handling for it
     */
    protected function doUpdateStock($stock,$parent=false){
        $productid = $this->product->getId();
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);
        $stockItemData = $stockItem->getData();
        if (empty($stockItemData)) {
            // Create the initial stock item object
            $stockItem->setData('manage_stock',0);
            $stockItem->setData('is_in_stock',0);
            $stockItem->setData('use_config_manage_stock', 0);
            $stockItem->setData('stock_id',1);
            $stockItem->setData('product_id',$productid);
            $stockItem->setData('qty',0);
            $stockItem->save();
        }
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);
        if($parent===true){
            $stockItem->setUseConfigManageStock(0);
            $stockItem->setIsInStock(1);
            $stockItem->setIsSalable(1);
            $stockItem->setManageStock(1);
        }elseif($stock!==null && $stock!==false){
            $stockItem->setQty($stock);
            $stockItem->setManageStock(1);
            $stockItem->setUseConfigManageStock(0);
            if((float)$stock>(float)0){
                $stockItem->setIsInStock(1);
            }else{
                $stockItem->setIsInStock(0);
            }

            if(!empty($this->actindo['l_minbestand'])){
                $stockItem->setNotifyStockQty($this->actindo['l_minbestand']);
                $stockItem->setUseConfigNotifyStockQty(0);
            }else{
                $stockItem->setUseConfigNotifyStockQty(1);
            }

        }else{
            $stockItem->setIsInStock(1);
            $stockItem->setManageStock(0);
            $stockItem->setUseConfigManageStock(0);
        }
        if($parent===true){
            $catalog = Mage::getModel('cataloginventory/stock_status');
            $catalog->updateStatus($productid);
        }
        $stockItem->save();
    }
    /**
     * update Image Translation label
     */
    protected function updateImageTranslation(){
        $mediaModel = Mage::getModel("catalog/product_attribute_backend_media");
        foreach($this->imageTranslation['translations'] as $translation){
            $product = Mage::getModel('catalog/product')->setStoreId($translation['language_id'])->load($this->product->getId());
            $productArray = $product->toArray();
            $mediaGallery = $productArray['media_gallery'];
            $defaultImage = '';
            foreach($mediaGallery['images'] as $galleryId=>$galleryImage){
                if((int)$galleryImage['position']===1){
                    $defaultImage = $galleryImage['file'];
                }
                $imageLabel = $this->actindo['shop']['images'][$galleryImage['position']]['image_title'][$translation['language_code']];
                if(!empty($imageLabel)){
                    $mediaGallery['images'][$galleryId]['label'] = $imageLabel;
                }else{
                    $mediaGallery['images'][$galleryId]['label'] = '';
                }
            }
            $product->setData('media_gallery',$mediaGallery);
            $product->setImage($defaultImage);
            $product->setSmallImage($defaultImage);
            $product->setThumbnail($defaultImage);
            $product->save();
        }

    }
}
