<?php
/**
 * Actindo Faktura/WWS Connector
 * import for Configurable Product
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 */
class Actindo_Connector_Model_Service_Product_Import_Configurable extends Actindo_Connector_Model_Service_Product_Import_Simple{
    /**
     * @var Actindo_Connector_Model_Variants_Helper Variant Helper Object
     */
    protected $variant;
    /**
     * Price Object Container
     * @var Actindo_Connector_Model_Util_Price
     */
    protected $priceObject;
    /**
     * used types
     * @var
     */
    protected $usedtypes;
    /**
     * translations
     * @var
     */
    protected $translations;
    /**
     * stores variant Attributes available for processing in magento
     * @var array
     */
    protected $variantAttributes = array();
    /**
     * if true, an error will be reported to Actindo
     * @var
     */
    protected $errorAppeared;
    /**
     * attribute to label mapping
     * @var array
     */
    protected $mapping = array();
    /**
     * contains the default Language Id
     * @var integer
     */
    protected $defaultLanguage;
    /**
     * Array that contains data that is prepared in the update translation method
     * @var array
     */
    protected $imageTranslation=array();
    /**
     * Children (befor first save)
     * @var array
     */
    protected $children = array();
    /**
     * construction of class and preparing create data
     */
    public function __construct(){
        parent::__construct();
    }
    /**
     * do Product update for variant articles
     * @param array $product containing the actindo data array
     * @return array tell actindo if the product was created successfull
     */
    public function doUpdate($product) {
        $singleton = Mage::getSingleton('connector/util_util');
        $this->translations = $singleton->getLanguages();
        $this->actindo = $product;
        $this->prepareVariantsAndChecks();
        //cleanup
        unset($check);
        $singleton = Mage::getSingleton('connector/util_util');
        $siteid = Actindo_Connector_Model_System_Service::getStoreID();
        $group = $singleton->getCustomerGroups($siteid);
        foreach($group as $key){
            $this->customgroup[] = $key['customers_status_id'];
        }
        $this->priceObject = new Actindo_Connector_Model_Util_Price($this->actindo,$this->customgroup);
        $this->shoparticle = $product['shop']['art'];
        $this->artnr = $this->actindo['art_nr'];
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(0));
        $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        //check if different type
        if($this->product!==false && $this->product->getTypeId()!==Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
            $entryid = $this->product->getId();
            if($this->product->getTypeId()===Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
                $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this->product);
                if(count($childProducts)>0){
                    foreach($childProducts as $product){
                        Actindo_Connector_Model_System_Service::addChildId($product->getId());
                        $product->delete();
                    }
                }
            }
            $this->product->delete();
            $this->product = false;
        }
        if($this->product!==false){
            //get child articles count
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($this->product);
            $col = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
            //here is a buggy article. We need to remove it bevor continueing!
            if(count($col)<1){
                $this->product->delete();
                $this->product = false;
            }
            $productAttributes = $this->product->getTypeInstance(true)->getConfigurableAttributesAsArray($this->product);
            $countAttributes = count($this->actindo['shop']['attributes']['names']);
            foreach($productAttributes as $attribute){
                foreach($this->actindo['shop']['attributes']['names'] as $nameId=>$nameArray){
                    foreach($nameArray as $translationIdent=>$translationValue){
                        if($attribute['label']==$translationValue || $attribute['attribute_code']==$translationValue || $attribute['frontend_label']==$translationValue || $attribute['store_label']==$translationValue){
                            $countAttributes--;
                            break(2);
                        }
                    }
                }
            }
            if($countAttributes>0){
                throw new Actindo_Connector_Model_Exception_Error('The Article can\'t be uploaded because you have added an additional Variant Dimension. Please delete the Article in the shop and upload it!',502);
            }
            //variants
            //Assign All Children Id's to a variable
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this->product);
            if(count($childProducts)>0){
                foreach($childProducts as $product){
                    $this->children[$product->getId()] = true;
                }
            }
        }
        $this->updateVariants();
        if($this->product===false){
            $this->product = Mage::getModel('catalog/product');
            $this->product->setSku($this->actindo['art_nr']);
            $this->newProduct = true;
            $this->product->setAttributeSetId($this->actindo['shop']['art']['filtergroup_id']);
            $this->product->setStoreId(Mage::app()->getStore()->getStoreId());
            $this->product->setCreatedAt(strtotime('now'));
            $this->product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);
            $this->product->save();
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
            $this->updateVariantsInProduct();
            $this->product->save();
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        }else{
            try{
                $this->updateVariantsInProduct();
                $this->product->save();
            }catch(Exception $e){

            }
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        }
        $this->product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $this->setWebSiteId();
        //Execute the core stuff
        $this->updateCoreData();
        $this->updateTranslations($this->translations);
        $this->updateAttributes($this->product);
        #$this->updateCategories();
        $this->updateContent();
        $this->updateCrosssellings();
        $this->updateCustomerGroupPermissions();
        $this->doUpdateStock(0,true);
        //Add Images
        $this->updateImages();
        //Add/Update Prices
        $this->updatePrices();
        //First do Initial Store
        $this->product->save();
        //reload parent product to correctly set Image,Thumbnail,SmallImage
        $objprod=Mage::getModel('catalog/product')->load($this->product->getId());
        $galleryImages = $objprod->getData('media_gallery');
        if(
            !empty($galleryImages) //gallery can not be empty
            &&
            is_array($galleryImages) //gallery must be an array
            &&
            isset($galleryImages['images']) //images must exist
            &&
            is_array($galleryImages['images']) //and it must be an array
            &&
            count($galleryImages['images'])>0 //image count must be greater then 0
        ){
            $newimage = $galleryImages['images'][0]['file']; //The galleryImages['images'] is an array of Images. The Image 0 is the first Image, which should be the master Image
            $objprod->setSmallImage($newimage);
            $objprod->setImage($newimage);
            $objprod->setThumbnail($newimage);
            $objprod->save();
        }
        /**
         * scan the image directory again, to validate that the images exist (that are in the database)
         * If not delete them
         * Had to be implemented as the api has a bug in simply doubling each stored image!
         */
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
        try{
            $items = $mediaApi->items($this->product->getId());
            foreach($items as $item){
                $imgCheck = Mage::getBaseDir('media').DS.'catalog'.DS.'product'.$item['file'];
                if(!file_exists($imgCheck)){
                    $mediaApi->remove($this->product->getId(),$item['file']);
                }
            }
        }catch(Exception $e){

        }
        if($this->errorAppeared){
            throw new Actindo_Connector_Model_Exception_Error('Some of the Attribute Properties did not exist in Magento and were not created because of this!', 500);
        }
        /**
         * Cleanup remaining child Products
         */
        if(count($this->children)>0){
            foreach($this->children as $child=>$notused){
                $product = Mage::getModel('catalog/product')->load($child);
                if($product->getId()>0){
                    $product->delete();
                }
            }
        }
        //reload parent product to correctly set Image,Thumbnail,SmallImage
        $objprod=Mage::getModel('catalog/product')->load($this->product->getId());
        $galleryImages = $objprod->getData('media_gallery');
        if(
            !empty($galleryImages) //gallery can not be empty
            &&
            is_array($galleryImages) //gallery must be an array
            &&
            isset($galleryImages['images']) //images must exist
            &&
            is_array($galleryImages['images']) //and it must be an array
            &&
            count($galleryImages['images'])>0 //image count must be greater then 0
        ){
            $newimage = $galleryImages['images'][0]['file']; //The galleryImages['images'] is an array of Images. The Image 0 is the first Image, which should be the master Image
            $objprod->setSmallImage($newimage);
            $objprod->setImage($newimage);
            $objprod->setThumbnail($newimage);
            $objprod->save();
        }
        $this->updateImageTranslation();
        return array('ok' => true, 'success' => 1);
    }
    /**
     * checks if all requested Variants really exist in magento
     * @todo implement create/assign of attribute to attribute set
     * @throws Actindo_Connector_Model_Exception_Error
     */
    protected function prepareVariantsAndChecks(){
        $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection');
        foreach($productAttributes->getItems() as $attribute){
            if($attribute->getIsConfigurable()){
                $label = $attribute->getFrontendLabel();
                $this->variantAttributes[$label] = true;
                $this->mapping[$attribute->getFrontendLabel()] = $attribute->getAttributeCode();
            }
        }
        $names = $this->actindo['shop']['attributes']['names'];
        if(count($names)>0){
            foreach($names as $key=>$value){
                $exists  = false;
                $ident = '';
                foreach($value as $entryId=>$entryValue){
                    if(!empty($entryValue) && isset($this->variantAttributes[$entryValue])){
                        $exists = true;
                    }
                }
                if($exists == false){
                    throw new Actindo_Connector_Model_Exception_Error('Attribute Combination does not exist! Please refresh shop connection to Actindo!', 501);
                }
            }
        }
    }
    /**
     * function for Creating/updating child Variants
     * This does not add them to the parent product!
     * @return void
     */
    protected function updateVariants(){
        //create child stuff / aka process them
        $variantDefaults = array(
            'art_name'=>$this->actindo['art_name'],
            'products_description'=>$this->actindo['shop']['desc'][0]['products_description'],
            'products_shortdescription'=>$this->actindo['shop']['desc'][0]['products_short_description'],
            'price'=>$this->actindo['grundpreis'],
            'parentArticle'=>$this->actindo['shop']['desc'],
        );
        $this->variant = array();
        $filterGroupId = $this->actindo['shop']['art']['filtergroup_id'];
        $attributeMap = new Actindo_Connector_Model_Variants_AttributeMap($this->actindo);
        $simple = $this->actindo['shop']['attributes']['combination_simple'];
        $mwst = $this->taxmap[$this->actindo['mwst']];
        $this->usedtypes = array();
        $containedArticles = array();
        foreach($this->actindo['shop']['attributes']['combination_advanced'] as $sku=>$attribute){
            $containedArticles[$sku] = true;
            $child = new Actindo_Connector_Model_Service_Product_Import_Child(
                $attribute,
                $sku,
                $filterGroupId,
                $attributeMap,
                $simple,
                $mwst,
                $this->translations,$variantDefaults,$this->actindo['shop']['desc'],
                $this->actindo['shop']['properties'],
                $this->defaultLanguage
            );
            if($child->getCreated()){
                $result = $child->getResultSet();
                array_push(
                    $this->variant,
                    $result
                );
                $childUsedTypes = $child->getUsedTypes();
                $this->usedtypes = array_merge($this->usedtypes,$childUsedTypes);
            }else{
                $this->errorAppeared = true;
            }
            $childProductID = $child->getProductID();
            if(isset($this->children[$childProductID])){
                unset($this->children[$childProductID]);
            }
        }
        $this->usedtypes = array_unique($this->usedtypes);
        if($this->product!==false && $this->product->getId()>0){
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this->product);
            //delete unused variants
            if(count($childProducts)>0){
                foreach($childProducts as $childProduct){
                    if(!isset($containedArticles[$childProduct->getSku()])){
                        $childProduct->delete();
                    }
                }
            }
        }
    }
    /**
     * update Variants of the product.
     * This update call creates the parents child link
     */
    protected function updateVariantsInProduct(){
        if($this->isNewProduct()){
            //set product to be able to create configurable attrutes
            $this->product->setCanSaveConfigurableAttributes(true);
            $this->product->setCanSaveCustomOptions(true);
            $productTypeInstance = $this->product->getTypeInstance();
            $productTypeInstance->setUsedProductAttributeIds($this->usedtypes);
            $attributesArray = $productTypeInstance->getConfigurableAttributesAsArray();
            foreach($attributesArray as $key=>$attributeArray){
                $attributesArray[$key]['use_default']   = 1;
                $attributesArray[$key]['position']      = 0;
                if(isset($attributeArray['frontend_label'])){
                    $attributesArray[$key]['label']     = $attributeArray['frontend_label'];
                }else{
                    $attributesArray[$key]['label']     = $attributeArray['attribute_code'];
                }
            }
            $this->product->setConfigurableAttributesData($attributesArray);
        }
        $resultArray = array();
        $superAttributeArray = array();
        foreach ($this->variant as $variantsArray) {
            foreach($variantsArray as $variantArray){
                $entryArray =
                    array(
                        "attribute_id" => $variantArray['attr_id'],
                        "label" => $variantArray['label'],
                        "is_percent" => false,
                        "pricing_value" => (float)$variantArray['price'],
                        "value_index"=>$variantArray['super']['value_index'],
                    );
                $resultArray[$variantArray['id']] = $entryArray;
                $superAttributeArray[] = $variantArray['super'];
            }
        }
        $this->product->setConfigurableProductsData($resultArray);
        if($this->isNewProduct()){
            $typeinstance = $this->product->getTypeInstance(true);
            $productAttributeOptions = $typeinstance->getConfigurableAttributesAsArray($this->product);
            $superid = $productAttributeOptions[0]['id'];
            foreach($superAttributeArray as $key=>$superAttribute){
                $superAttributeArray[$key]['product_super_attribute_id']=$superid;
            }
            $productAttributeOptions[0]['values'] = $superAttributeArray;
            $this->product->setConfigurableAttributesData($productAttributeOptions);
        }
        $this->doUpdateStock(null,true);
    }
}