<?php
/**
 * Actindo Faktura/WWS Connector
 * Object for micro managing child products
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 */
class Actindo_Connector_Model_Service_Product_Import_Child extends Actindo_Connector_Model_Service_Product_Import_Simple{
    /**
     * Product Id
     */
    protected $productId;
    /**
     * Actindo Data Container
     * @var array
     */
    protected $actindo;
    /**
     * Magento PRoduct Data Object
     * @var Mage_Catalog_Model_Product
     */
    protected $product;
    /**
     * Id of the corrent Attributes Set
     * @var int
     */
    protected $attributesetid;
    /**
     * Array for Return Data
     * @var array
     */
    protected $returnSet = array();
    /**
     * Shop Article Data from the actindo array
     * @var array
     */
    protected $shoparticle;
    /**
     * Object of Attribute Map that Manages the data given by actindo
     * @var Actindo_Connector_Model_Variants_AttributeMap
     */
    protected $attributemap;
    /**
     * Simple Data from the actindo array
     * @var array
     */
    protected $simple;
    /**
     * Array containing the used attribute properties
     * @var array
     */
    protected $usedtypes =array();
    /**
     * Tax Class Id
     * @var int
     */
    protected $taxClassIdent;
    /**
     * Translation array from the singleton object
     * @var array
     */
    protected $translations;
    /**
     * Super Product Attribute Set
     * @var array $superAttribute
     */
    protected $superAttribute = array();
    /**
     * Basic Price of Master ARticle
     * @var float
     */
    protected $price = 0.00;
    /**
     * list of available attributes
     * @var array
     */
    protected $avaialableAttributes;
    /**
     * check if article has been created.
     * stores false if the attribute parameter does not exist!
     * @var bool
     */
    protected $created = false;
    /**
     * contains the default Language Id
     * @var integer
     */
    protected $defaultLanguage;
    /**
     * Array that contains data that is prepared in the update translation method
     * @var array
     */
    protected $imageTranslation=array();
    /**
     * Constructor to initialize the object
     * @param array $actindo Actindo Object
     * @param mixed $sku String containing the article id
     * @param integer $setid ID of the used attribute set
     * @param $attributemap Actindo_Connector_Model_Variants_AttributeMap
     * @param  $simple Actindo Simple Product Data array
     * @param integer $taxClassIdent ID  of the used Tax Class
     * @param array $translations Available Translations from the singleton object
     * @param array $mainArticleDescription default parameters
     * @param array $parentArticleProperties contains the parent articles properties
     * @param integer defaultLanguage id of the default Language
     */
    public function __construct($actindo,$sku,$setid,Actindo_Connector_Model_Variants_AttributeMap $attributemap,$simple,$taxClassIdent,$translations,$defaultValues,$mainArticleDescription,$parentArticleProperties,$defaultLanguage){
        $this->actindo = $actindo;
        $this->filehandler = new Varien_Io_File();
        $this->defaultLanguage = $defaultLanguage;
        /**
         * File Handler Init
         */
        $this->importDir = Mage::getBaseDir('media') . DS . 'actindoimport' . DS;
        if($this->filehandler->fileExists($this->importDir,false)){
            $this->filehandler->rmdirRecursive($this->importDir);
        }
        $importReadyDirResult = $this->filehandler->mkdir($this->importDir);
        if(!$importReadyDirResult){
            throw new Actindo_Connector_Model_Exception_Error('Directory '.$this->importDir.' could not be created!', 600);
        }
        /**
         * Set Basic Article Data
         */
        $this->artnr = $sku;
        $this->attributesetid = $setid;
        /**
         * Prepare Shop Data
         */
        $this->shoparticle = $actindo['data'];
        $this->attributemap = $attributemap;
        /**
         * Assign Simple Data
         */
        $this->simple = $simple;
        /**
         * Assign Price Data
         */
        $this->price = $defaultValues['price'];
        /**
         * Merge parent Article Properties with the child Article Properties
         */
        $this->actindo['shop']['properties'] = array_merge($parentArticleProperties,$this->actindo['shop']['properties']);
        //check Variants existing
        $result = $this->variantCheck();
        if($result){
            //now build the translation data, if not available
            if(count($this->actindo['shop']['desc'])>0){
                foreach($this->actindo['shop']['desc'] as $key=>$value){
                    if(is_array($value) && count($value)>0){
                        if(isset($value['products_name']) && !empty($value['products_name'])){
                            $this->actindo['shop']['desc'][$key]['products_name'] = $value['products_name'];
                        }
                        if(isset($value['products_description']) && !empty($value['products_description'])){
                            $this->actindo['shop']['desc'][$key]['products_description'] = $value['products_description'];
                        }
                        if(isset($value['products_short_description']) && !empty($value['products_short_description'])){
                            $this->actindo['shop']['desc'][$key]['products_short_description'] = $value['products_short_description'];
                        }
                    }else{
                        if(isset($mainArticleDescription[$key])){
                            $this->actindo['shop']['desc'][$key] = $mainArticleDescription[$key];
                        }
                    }
                }
            }else{
                $this->actindo['shop']['desc'] = $mainArticleDescription;
            }
            //set default Values
            if(empty($this->actindo['shop']['desc'][0]['products_name'])){
                $this->actindo['art_name'] = $defaultValues['art_name'];
            }else{
                $this->actindo['art_name'] = $this->actindo['shop']['desc'][0]['products_name'];
            }
            if(empty($this->actindo['shop']['desc'][0]['products_description'])){
                $this->actindo['langtext'] = $defaultValues['products_description'];
            }else{
                $this->actindo['langtext'] = $this->actindo['shop']['desc'][0]['products_description'];
            }
            if(empty($this->actindo['shop']['desc'][0]['products_short_description'])){
                $this->actindo['beschreibung'] = $defaultValues['products_description'];
            }else{
                $this->actindo['beschreibung'] = $this->actindo['shop']['desc'][0]['products_short_description'];
            }
            $this->actindo['shop']['products_status'] = $this->actindo['data']['products_status'];
            //set other data
            $this->taxClassIdent = $taxClassIdent;
            $this->translations = $translations;
            $desc = $defaultValues['parentArticle'];
            foreach($this->actindo['shop']['desc'] as $key=>$values){
                foreach($values as $ident=>$value){
                    if(!empty($value)){
                        $desc[$key][$ident] = $value;
                    }
                }
            }
            $this->actindo['shop']['desc'] = $desc;
            $this->process();
            $this->created = true;
        }
        else
        {
            throw new Actindo_Connector_Model_Exception_Error(
                    'Some of the Attribute Properties did not exist in'.
                    ' Magento and were not created because of this!'.
                    ' Please Syncronise the connector again');
        }
        $this->productId = $this->product->getId();
    }
    public function getProductID(){
        return $this->productId;
    }
    /**
     * Method to create the product inside of magento
     * @protected
     * @param void
     * @return void
     */
    protected function process(){
        $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        if($this->product === false){
            $this->product = Mage::getModel('catalog/product');
            //get id, if no more exist, returns false and should not be set
            $childId = Actindo_Connector_Model_System_Service::getChildId();
            if($childId!=false && (int)$childId>0){
                $this->product->setId($childId);
            }
            //create child Product
            $this->product->setSku($this->artnr);
            $this->product->setAttributeSetId($this->attributesetid);
            $this->product->setStoreId(Mage::app()->getStore()->getStoreId());
            $sites = $this->product->getWebsiteIds();
            if(!in_array(Actindo_Connector_Model_System_Service::getStoreID(),$sites)){
                $sites[] = Actindo_Connector_Model_System_Service::getStoreID();
            }
            $this->product->setCreatedAt(strtotime('now'));
            $this->product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
            $this->product->save();
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        }
        $this->setWebSiteId();
        //Execute the core stuff
        $this->updateCoreData();
        $this->updateAttributes($this->product);
        $this->updateContent();
        $this->updateCustomerGroupPermissions();
        /**
         * Set Price
         */
        if((float)$this->actindo['preisgruppen'][0]['grundpreis']>(float)0){
            $this->product->setPrice((float)$this->actindo['preisgruppen'][0]['grundpreis']);
        }else {
            $this->product->setPrice((float)$this->price);
        }
        $this->product->setTaxClassId($this->taxClassIdent);
        // Do Stock
        $this->doUpdateStock($this->actindo['l_bestand']);
        //Add Images and do Initial Store of Image
        $this->updateImages();
        /**
         * Build Return Set with all Variant Relevant Information
         */
        $this->returnSet = array();
        if(count($this->actindo['attribute_name_id'])>0){
            $turns = count($this->actindo['attribute_name_id']);
            for($i=0;$i<$turns;$i++){
                $attributeId = $this->actindo['attribute_name_id'][$i];
                $attributeValueId = $this->actindo['attribute_value_id'][$i];
                //get Price for this simple element
                $price = (float)$this->simple[$attributeId][$attributeValueId]['options_values_price'];
                $mapping = $this->attributemap->mapping['mapping'][$attributeValueId];
                $label = $this->attributemap->mapping[$mapping]['values'][$attributeValueId];
                $label = array_shift($label);
                foreach($label as $key=>$value){
                    $label = $value;
                    break;
                }
                $attributeDataSet = $this->attributemap->getAttributeData($attributeId);
                $optionid = $this->attributemap->getOptionId($attributeId,$label);
                $result = array(
                    'id'=>$this->product->getId(),
                    'price'=>$price,
                    'attr_code'=> $attributeDataSet['code'],
                    'attr_id'=>$attributeDataSet['id'],
                    'value'=>$optionid,
                    'label'=>$label,
                    'super'=>array(
                        'value_index'=>$optionid,
                        'label'=>$label,
                        'default_label'=>$label,
                        'store_label'=>$label,
                        'is_percent'=>0,
                        'pricing_value'=>$price,
                        'use_default_value'=>1,
                    )
                );
                $this->usedtypes[] = $attributeDataSet['id'];
                $this->returnSet[] = $result;
                unset($attributeValueId,$attributeId,$result,$label,$mapping,$price);
            }
        }
        $this->updatePrices();
        /**
         * Language to ID mapping
         */
        $trMapping = array();
        foreach($this->translations as $entry){
            if(!isset($map[$entry['language_code']])){
                $trMapping[$entry['language_code']] = $entry['language_id'];
            }
        }
        //Edit Artikel for each Language and save it again!
        if(count($this->actindo['shop']['desc'])>0){
            /**
             * check if language id is set, if not set it
             */
            foreach($this->actindo['shop']['desc'] as $key=>$entry){
                if(!isset($entry['language_id'])){
                    $this->actindo['shop']['desc'][$key]['language_id'] = $trMapping[$entry['language_code']];
                }
            }
            /**
             * Now Run the Translation System of the simple product
             */
            $this->updateTranslations($this->translations);
        }else{
            //update Translation
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
            $this->product->setName($this->actindo['art_name']);
            $this->product->setDescription($this->actindo['langtext']);
            $this->product->setShortDescription($this->actindo['beschreibung']);
        }
        $this->updateAttributeData();
        $this->product->save();
    }
    /**
     * returns true if the article was created, false if not
     * @return bool
     */
    public function getCreated(){
        return $this->created;
    }
    /**
     * get the super product
     * @return array
     */
    public function getSuperAttribute(){
        return $this->superAttribute;
    }
    /**
     * Result Set of Created Product
     * specialy prepared for linking this product ot another
     * @return array
     */
    public function getResultSet(){
        return $this->returnSet;
    }
    /**
     * Returns the used Types for futher processing of the parent object
     * @return array
     */
    public function getUsedTypes(){
        return $this->usedtypes;
    }
    /**
     * Method used to update the Attributes that are predefined by the Attribute Type
     */
    public function updateAttributeData(){
        $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku',$this->artnr);
        foreach($this->returnSet as $key=>$returnSetArray){
            $this->product->setData($returnSetArray['attr_code'],$returnSetArray['value']);
        }
        $this->product->save();
    }
    /**
     * checks if the variant exists or not
     * @return bool
     */
    protected function variantCheck(){
        $attributeIds = $this->actindo['attribute_name_id'];
        $attributeValues = $this->actindo['attribute_value_id'];
        $attributeCount = count($attributeValues);
        foreach($attributeValues as $ident=>$value){
            $ActindoSet = $this->attributemap->mapping[$this->attributemap->mapping['mapping'][$value]]['values'][$value];
            $magentoSet = $this->attributemap->getAttributeData($attributeIds[$ident]);
            $magentoSets = $magentoSet['values'];
            $exists = false;
            foreach($magentoSets as $set){
                foreach($ActindoSet as $aSet){
                    foreach($aSet as $entry){
                        if($set['label']===$entry){
                            $exists = true;
                            break;
                        }
                    }
                    if($exists===true){
                        break;
                    }
                }
                if($exists===true){
                    break;
                }
            }
            if($exists===true){
                $attributeCount--;
            }
        }
        if($attributeCount>0){
            return false;
        }else{
            return true;
        }
    }
}
