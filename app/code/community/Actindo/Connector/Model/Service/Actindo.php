<?php
    /**
     * Actindo Faktura/WWS Connector
     * General Prupous class
     * This class creates the general information flow during connection creation
     * It submits connector relevant informations, shop versions and server capabilities to support Actindo Support
     * It returns the server time,
     * Connector Capabilities
     * Server Ping
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Actindo{
        /**
         * Identification of Connector against actindo
         */
        const IDENTITY = 'magento-1.x';
        /**
         * returns the connector Version, shop version and hardware capabilities
         * @return array containing several information blocks
         */
        public function get_connector_version() {
            $versions = Mage::getConfig()->getModuleConfig("Actindo_Connector")->version;
            list($version, $revision) = explode('.', (string)$versions[0]);
            $arr = array(
                'revision'     => $revision,
                'protocol_version' => (string)$versions[0],
                'shop_type'    => self::IDENTITY,
                'shop_version' => (string)Mage::getVersion(),
                'capabilities' => $this->_getShopCapabilities(),
                'cpuinfo'      => @file_get_contents('/proc/cpuinfo'),
                'meminfo'      => @file_get_contents('/proc/meminfo'),
                'extensions'   => array(),
            );
            foreach(get_loaded_extensions() as $extension) {
                $arr['extensions'][$extension] = phpversion($extension);
            }
            if(is_callable('phpinfo')) {
                ob_start();
                phpinfo();
                $c = ob_get_contents();
                ob_end_clean();
                $arr['phpinfo'] = new Zend_XmlRpc_Value_Base64($c);
            }
            return $arr;
        }
        /**
         * returns the server time based on different time frames
         * @return array
         */
        public function get_time() {
            $arr = array(
                'time_server'     => date('Y-m-d H:i:s'),
                'gmtime_server'   => gmdate('Y-m-d H:i:s'),
                'time_database'   => date('Y-m-d H:i:s'),
                'gmtime_database' => gmdate('Y-m-d H:i:s'),
            );

            if(!empty($arr['gmtime_database'])) {
                $diff = strtotime($arr['time_database']) - strtotime($arr['gmtime_database']);
            }
            else {
                $diff = strtotime($arr['time_server']) - strtotime($arr['gmtime_server']);
            }
            $arr['diff_seconds'] = $diff;
            $diff_neg = $diff < 0;
            $diff = abs($diff);
            $arr['diff'] = ($diff_neg ? '-' : '') . sprintf('%02d:%02d:%02d', floor($diff / 3600), floor(($diff % 3600) / 60), $diff % 60);

            return $arr;
        }
        /**
         * method used for pinging the server
         * @return array
         */
        public function ping() {
            return array(
                'ok'   => true,
                'pong' => 'pong',
            );
        }
        /**
         * returns shop capabilites as an array
         * @return array associative array explaining the shops capabilities
         */
        private function _getShopCapabilities() {
            $liveupdate = (int)Actindo_Connector_Model_System_Service::getMethodActive('stockliveupdate');
            return array(
                'artikel_vpe' => 1,                 // Verpackungseinheiten
                'artikel_shippingtime' => 1,        // Produkt Lieferzeit als fest definierte werte
                'artikel_shippingtime_days' => 1,   // Produkt Lieferzeit als int für n Tage
                'artikel_properties' => 1,
                'artikel_property_sets' => 1,
                'artikel_contents' => 1,
                'artikel_attributsartikel' => 1,    // Attributs-Kombinationen werden tatsächlich eigene Artikel
                'wg_sync' => 1,
                'artikel_list_filters' => 1,
                'multi_livelager' => $liveupdate,
                'config_push'=>1,
            );
        }
    }