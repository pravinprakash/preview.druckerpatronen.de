<?php
    /**
     * Actindo Faktura/WWS Connector
     * Returns Server Settings
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Actindo_Connector_Model_Util_Util
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Settings{
        /**
         * Method to build shop settings
		 * @param struct $vars an associative array with information from actindo that may be relevant for the connector
         * @return array
         */
        public function get($vars) {
            $singleton = Mage::getSingleton('connector/util_util');
            $siteid = Mage::app()->getStore()->getWebsiteId();
            $return = array(
                'languages'=>$singleton->getLanguages(),
                'manufacturers'=>$singleton->getManufacturer($siteid),
                'customers_status'=>$singleton->getCustomerGroups(),
                'shipping'=>$singleton->getShippingTime($siteid),
                'xsell_groups'=>$singleton->getCrosselingGroups(),
                'installed_shipping_modules'=>$singleton->getShippingVendors($siteid),
                'orders_status'=>$singleton->getOrderStatus(),
                'installed_payment_modules'=>$singleton->getPaymentProvider(),
                'artikel_properties'=>$singleton->getAttributesSet($siteid),
                'artikel_property_sets'=>$singleton->getConfigurableSets(),
                'multistores'=>$singleton->getMultiStores($siteid),
                'vpe'=>$singleton->getVpe($siteid),
            );
            $return = Actindo_Connector_Model_Util_Service::ScanForNullAndCorrect($return);
            return array(
                'ok' => true,
                'settings' => $return
            );
        }
        /**
         * store settings to magento
         * @param struct $settings
         * @return array
         */
        public function set($settings){
            $mapping = Actindo_Connector_Model_System_Service::getStoreMapping();
            if(isset($settings['languages'])){
                $languages = $settings['languages'];
                if(count($languages)>0){
                    $keymaps = Mage::getModel('connector/ackeyvaluemap')
                        ->getCollection()
                        ->addFieldToFilter('type','actindoLang');
                    $existingMap = array();
                    if(count($keymaps)>0){
                        foreach($keymaps as $keymap){
                            $existingMap[$keymap->getValue()] = true;
                        }
                    }
                    foreach($languages as $language){
                        $iso = strtoupper($language['iso']);
                        $name = $language['name'];
                        if(!isset($existingMap[$iso])){
                            $entrymap = Mage::getModel('connector/ackeyvaluemap')
                                ->setType('actindoLang')
                                ->setIdent($name)
                                ->setValue($iso)
                                ->save();
                        }
                    }
                }
            }
            if(isset($settings['actindoLanguages']) && count($settings['actindoLanguages'])>0){
                $keymaps = Mage::getModel('connector/ackeyvaluemap')
                    ->getCollection()
                    ->addFieldToFilter('type','languages');
                $changes = false;
                //check if changes accured
                if(count($keymaps)>0){
                    foreach($keymaps as $keymap){
                        if($settings['actindoLanguages'][$keymap->getIdent()]!==$keymap->getValue()){
                            $changes = true;
                            break;
                        }
                        $existingMap[$keymap->getValue()] = true;
                    }
                }
                if($changes){
                    foreach($keymaps as $keymap){
                        $keymap->delete();
                    }
                }
                foreach($settings['actindoLanguages'] as $ident=>$value){
                    if(!isset($existingMap[$value])){
                        try{
                            $entrymap = Mage::getModel('connector/ackeyvaluemap')
                                ->setType('languages')
                                ->setIdent($ident)
                                ->setValue($value)
                                ->save();
                        }catch(Exception $ex){
                            #Mage::log($ex->gtMessage());die;
                        }
                    }
                }
            }
            if(count($mapping['id'])>0){
                if(isset($settings['variants'])){
                    $variants = $settings['variants'];
                    if(count($variants)>0){
                        $variants = new Actindo_Connector_Model_Variants_Settings($variants);
                        $variants->run();
                    }
                }
            }
            if(Actindo_Connector_Model_System_Service::getMethodActive('attributesSync'))
            {

                if(count($settings['properties']) > 0)
                {
                    //first prepare a list
                    $variants = array();
                    if(count($settings['variants']) > 0)
                    {
                        foreach($settings['variants'] as $id=>$variant)
                        {
                            foreach($variant as $language=>$variantData)
                            {
                                $variants[$variantData['name']] = $id;
                            }
                        }
                    }
                    $properties = new Actindo_Connector_Model_Util_Properties($settings['properties'],$variants,$mapping);
                    $properties->run();
                }
            }
            return array(
                'ok'=>true
            );
        }
    }
