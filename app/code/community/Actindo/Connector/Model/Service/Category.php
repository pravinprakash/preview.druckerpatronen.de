<?php

    /**
     * Actindo Faktura/WWS Connector
     * class for managing categories
     * supported features
     * move (there is still a to do in this connecotr
     * delete
     * create
     * edit
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Category{
        /**
         * Service class
         * @var
         */
        protected $service;
        public function __construct(){
            $this->singleton = Mage::getSingleton('connector/util_util');
        }
        public function get() {
            return array('ok'=>1,'categories'=>array());
            #Load Tree Modul
            $tree = Mage::getResourceSingleton('catalog/category_tree')->load();
            //load all available group members
            $website = Mage::app()->getWebsite();
            $groups = $website->getGroups();
            $returnResult = array();
            $singleton = Mage::getSingleton('connector/util_util');
            if(count($groups)>0){
                foreach($groups as $group){
                    $store = Mage::app()->getStore($group->getDefaultStoreId());
                    $defaultCategory = $group->getRootCategoryId();
                    $rootCategory = Mage::getModel('catalog/category')->load($defaultCategory);
                    $entry = array(
                        'categories_id'=>$rootCategory->getEntityId(),
                        'parent_id'=>0,
                        'categories_name'=>new Zend_XmlRpc_Value_Base64($store->getName()),
                        'children'=>$this->getTreeByStoreId($tree,$store,$singleton,$group)
                    );
                    $returnResult[$rootCategory->getEntityId()] = $entry;
                }
            }
            $languages = $singleton->getLanguages();
            $return = array();
            foreach($languages as $languageId=>$languageValue){
                $return[$languageId] = &$returnResult;
            }
            return array('ok' => '1', 'categories' => $return);
        }
        protected function getTreeByStoreId($tree,$store,$singleton,$group){
            $parentId = $store->getRootCategoryId();
            #Load Root Node
            $root = $tree->getNodeById($parentId);
            #Load Collection
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->setStoreId($store->getId())
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('is_active');
            #Assign Collection to Tree
            $tree->addCollectionData($collection, true);
            #Build Tree Nodes
            $treenodes = $this->nodeToArray($root);
            $treenodes = $treenodes['children'];
            return $treenodes;
        }
        /**
         * performs operations on single categories or the category tree
         * technical info about param $data: the type should be just "struct", but when $action is "delete" actindo spuriously passes the type "array"
         * @param string $action known actions are: add, delete, textchange (rename category), append (move category), above (move category), below (move category)
         * @param int $categoryID the category id to perform operations on
         * @param int $parentID the parent id of the category to perform operations on
         * @param int $referenceID this does something aswell
         * @param struct|array $data data required to perform the called action
         * @return struct
         * @throws Actindo_Components_Exception
         */
        public function action($action, $categoryID, $parentID, $referenceID, $data) {
            if(!Actindo_Connector_Model_System_Service::getMethodActive('catmanage')){
                throw new Actindo_Connector_Model_Exception_Error('Categorie Managment has been disabled! Enable it in the Administration of youre shop Magento Shop!',200);
            }
            switch(strtolower($action)){
                default:
                    throw new Actindo_Connector_Model_Exception_Error(sprintf('Unknown category action given: %s', $action),201);
                break;
                case 'add':
                    return $this->add($this->getParentCategoryId($parentID),$data);
                break;
                case 'delete':
                    return $this->delete($categoryID);
                break;
                case 'textchange':
                    return $this->update($categoryID,$data);
                break;
                case 'above':
                case 'below':
                case 'append':
                    $parentIdent = $this->getParentCategoryId($parentID);
                    if(strtolower($action)!=='append'){
                        $parent = $referenceID;
                    }else{
                        $parent = $parentIdent;
                    }
                    return $this->move($categoryID,$parent,$parentIdent,$action);
                break;
            }
            return array('ok'=>false);
        }

        /**
         * Add new Element
         * @param $parentId
         * @param $data
         * @throws Actindo_Connector_Model_Exception_Error
         */
        protected function add($parentId,$data){
            $parent = Mage::getModel('catalog/category')->load((int)$parentId);
            $siteid = Mage::app()->getStore()->getWebsiteId();
            if($parent->hasData()){
                $model = Mage::getModel('catalog/category');
                $singleton = Mage::getSingleton('connector/util_util');
                $defaultLanguageId = $singleton->getDefaultLanguage();
                $model->setName($data['description'][$defaultLanguageId]['name'])
                      ->setPath($parent->getPath())
                      ->setStoreId($siteid)
                      ->setIsActive(1)
                      ->setIncludeInNavigation(true);
                $model->save();
                return array('ok' => true, 'id' => $model->getId());
            }else{
                throw new Actindo_Connector_Model_Exception_Error('Parent does not exist!',202);
            }
        }

        /**
         * Update Element
         * @param $categoryId
         * @param $data
         * @throws Actindo_Connector_Model_Exception_Error
         */
        protected function update($categoryId,$data){
            Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $model = Mage::getModel('catalog/category')->setStoreId(0)->load((int)$categoryId);
            $_FILES = array();
            $_FILES['image']['tmp_name'] = '';
            $_FILES['thumbnail']['tmp_name'] = '';
            if($model->hasData()){
                $singleton = Mage::getSingleton('connector/util_util');
                $defaultLanguageId = $singleton->getDefaultLanguage();
                $model->setName($data['description'][$defaultLanguageId]['name']);
                $model->setIsActive(1);
                $model->save();
                return array('ok' => true);
            }else{
                throw new Actindo_Connector_Model_Exception_Error('Element not found!',203);
            }
        }
        /**
         * Move Element
         * @param $categoryId
         * @param $referenceId
         * @param $parentId
         * @param $action
         * @throws Actindo_Connector_Model_Exception_Error
         */
        protected function move($categoryId,$referenceId,$parentId,$action){
            try{
                if(strtolower($action)==='append'){
                    $tree = Mage::getResourceSingleton('catalog/category_tree')->load();
                    $root = $tree->getNodeById($parentId);
                    $nodes = $this->nodeToArray($root,false);
                    $current = null;
                    foreach($nodes['children'] as $key=>$value){
                        $current = $key;
                    }
                    $category = Mage::getModel('catalog/category')->load($categoryId);
                    $category->move($parentId,$current);
                }else if($action==='above'){
                    $category = Mage::getModel('catalog/category')->load($categoryId);
                    $category->move($parentId,$referenceId);
                }else{
                    $tree = Mage::getResourceSingleton('catalog/category_tree')->load();
                    $root = $tree->getNodeById($parentId);
                    $nodes = $this->nodeToArray($root,false);
                    $previous = null;
                    $current = null;
                    foreach($nodes['children'] as $key=>$value){
                        $current = $key;
                        if($key===$referenceId){
                            break;
                        }else{
                            $previous = $current;
                        }
                    }
                    $category = Mage::getModel('catalog/category')->load($categoryId);
                    $category->move($parentId,$current);
                }
            }catch(Exception $eX){
                throw new Actindo_Connector_Model_Exception_Error($eX->getMessage());
            }
            return array('ok' => true);
        }
        /**
         * Delete Element
         * @param $categoryID
         * @throws Actindo_Connector_Model_Exception_Error
         */
        protected function delete($categoryID){
            #For Delete Operation
            Mage::register('isSecureArea', 1);
            $element = Mage::getModel('catalog/category')->load($categoryID);
            if($element->hasData()){
                $element->delete();
                return array('ok' => true);
            }else{
                throw new Actindo_Connector_Model_Exception_Error('Element not found!',203);
            }
        }

        /**
         * Build Tree Node
         * @param Varien_Data_Tree_Node $node
         * @return array
         */
        protected function nodeToArray(Varien_Data_Tree_Node $node,$base64=true){
            $result = array();
            $result['categories_id'] = $node->getId();
            $result['parent_id'] = $node->getParentId();
            $result['categories_name'] = (($base64)?new Zend_XmlRpc_Value_Base64($node->getName()):$node->getName());
            $result['children'] = array();
            if($node->hasChildren()){
                foreach ($node->getChildren() as $child) {
                    $result['children'][$child->getId()] = $this->nodeToArray($child);
                }
            }
            return $result;
        }

        /**
         * returns the parents category id
         * @param int $parentid
         * @return int
         */
        protected function getParentCategoryId($parentid){
            if($parentid>0)
                return $parentid;
            else
                return Mage::app()->getStore()->getRootCategoryId();;
        }
    }