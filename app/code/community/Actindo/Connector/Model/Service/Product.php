<?php
    /**
     * Actindo Faktura/WWS Connector
     * Class to Export and Import Products
     * It uses two sub classes to achieve this
     * This class provides also capabilities to
     * count articles
     * delete article
     * and do live stock updates
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @uses Actindo_Connector_Model_Service_Product_Import
     * @uses Actindo_Connector_Model_Service_Product_Export
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Product extends Actindo_Connector_Model_Service_Product_Export {
        /**
         * count the number of products
         * @param null $categoryID
         * @param null $ordernumber
         * @return array
         */
        public function count($categoryID=null, $ordernumber=null) {
            //load all available group members
            $website = Mage::app()->getWebsite();
            $groups = $website->getGroups();
            if($categoryID!==null){
                $category = Mage::getModel('catalog/category')->load((int)$categoryID);
                $productCollection = Mage::getModel('catalog/product')->getCollection()->addCategoryFilter($category);
            }else{
                $productCollection = Mage::getModel('catalog/product')->getCollection();
            }
            if(count($groups)>0){
                foreach($groups as $group){
                    $productCollection->addStoreFilter($group->getDefaultStoreId());
                }
            }
            if($categoryID!==null){
                $categoryCol = array($categoryID=>$productCollection->count());
            }else{
                $categoryCol = array(-1=>$productCollection->count());
            }
            return array('ok' => true, 'count' => $categoryCol);
        }
        /**
         * Method used for creating the update
         * @param struct $product Actindo Array
         * @return mixed
         * @throws Actindo_Connector_Model_Exception_Error
         */
        public function create_update($product) {
            $product = Actindo_Connector_Model_Util_Util::preProcessInput($product);
            $regex = "[a-zA-Z0-9.\-_]";
            $result = preg_match($regex,$product['art_nr']);
            if($result===false){
                throw new Actindo_Connector_Model_Exception_Error('Only 0-9, A-Z, a-z, . or -_ are allowed as article number!',300);
            }
            $helper = Mage::helper('connector');
            if((int)$product['shop']['art']['filtergroup_id']>0){
                //if((int)$product['shop']['art']['filtergroup_id']!==(int)$this->getDefaultAttributeSetId() ){
                if(isset($product['shop']['attributes']['combination_advanced']) && count($product['shop']['attributes']['combination_advanced'])>0){
                    if(!Actindo_Connector_Model_System_Service::getMethodActive('catmanage')){
                        throw new Actindo_Connector_Model_Exception_Error('Configurable Product Export has been disabled! Enable it in the Administration of youre shop Magento Shop!',301);
                    }
                    $update = Mage::getModel('connector/service_product_import_configurable');
                }else{
                    if(!Actindo_Connector_Model_System_Service::getMethodActive('articleuploadsimple')){
                        throw new Actindo_Connector_Model_Exception_Error('Simple Product Export has been disabled! Enable it in the Administration of youre shop Magento Shop!',302);
                    }
                    $update = Mage::getModel('connector/service_product_import_simple');
                }
                $returnValue = $update->doUpdate($product);
                #Actindo_Connector_Model_Index_Observer::setIndexFile();
                Mage::getModel('connector/index_observer')->setIndexFile();
                return $returnValue;
            }else{
                throw new Actindo_Connector_Model_Exception_Error('Please select an Fieldgroup first! Without this no Articles can be Uploaded to Magento!',303);
            }
        }
        /**
         * delete a product by it's ordernumber
         * will return true if product is found and deleted
         * will return false if product is not found
         * @param string $ordernumber
         */
        public function delete($ordernumber) {
            if(!Actindo_Connector_Model_System_Service::getMethodActive('articledelete')){
                throw new Actindo_Connector_Model_Exception_Error('Product delete has been disabled! Enable it in the Administration of youre shop Magento Shop!', 304);
            }
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$ordernumber);
            $sites = $product->getWebsiteIds();
            $candelete = false;
            if(in_array(Actindo_Connector_Model_System_Service::getStoreID(),$sites) && count($sites)>1){
                foreach($sites as $key=>$site){
                    if((int)$site === (int)Actindo_Connector_Model_System_Service::getStoreID()){
                        unset($sites[$key]);
                        break;
                    }
                }
                $product->save();
            }else{
                $candelete = true;
            }
            if($product->getId()>0){
                if($candelete){
                    if($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
                        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                        if(count($childProducts)>0){
                            foreach($childProducts as $child){
                                $child->delete();
                            }
                        }
                    }
                    $product->delete();
                }else{
                    $siteId = (int)Actindo_Connector_Model_System_Service::getStoreID();
                    if($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
                        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                        if(count($childProducts)>0){
                            foreach($childProducts as $child){
                                $sites = $child->getWebsiteIds();
                                $key = array_search($siteId,$sites);
                                unset($sites[$key]);
                                $child->setWebsiteIds($sites);
                                $child->save();
                            }
                        }
                    }
                    $sites = $product->getWebsiteIds();
                    $key = array_search($siteId,$sites);
                    unset($sites[$key]);
                    $product->setWebsiteIds($sites);
                    $product->save();
                }
                return array('ok' => true);
            }
            Mage::getModel('connector/index_observer')->setIndexFile();
            return array('ok' => false);
        }
        /**
         * update product stock
         * method is used for live stock update by actindo
         * returns ok=true on success (on multiple products, each successfull or not
         * successful article is also added to the output result
         * @param array $product
         * @return array
         */
        public function update_stock($product) {
            if(!Actindo_Connector_Model_System_Service::getMethodActive('stockupdate')){
                throw new Actindo_Connector_Model_Exception_Error('Stock Update has been disabled! Enable it in the Administration of youre shop Magento Shop!', 305);
            }
            $response = array();
            if(!isset($product['art_nr']) && count($product)) {
                $response = array(
                    'ok'      => true,
                    'success' => array(),
                    'failed'  => array(),
                );
                foreach($product AS $key => $item) {
                    $result = $this->doStockUpdateProduct($item);
                    if(!$result){
                        $response['failed'][$key] = array('ok');
                    }else{
                        $response['success'][$key] = array('ok');
                    }
                }
            }else{
                $result = $this->doStockUpdateProduct($product);
                if(!$result){
                    $response['ok'] = false;
                }else{
                    $response['ok'] = true;
                }
            }
            return $response;
        }

        /**
         * Method to do the update product for the method update_stock per article
         * returns true if successfull
         * returns false if an exception is thrown
         * @param array $product
         * @return bool
         */
        protected function doStockUpdateProduct($product){
            try{
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                //first update variant articles
                if(isset($product['attributes']) && isset($product['attributes']['combination_advanced']) && !empty($product['attributes']['combination_advanced'])){
                    foreach($product['attributes']['combination_advanced'] AS $ordernumber => $variant) {
                        $productObject = Mage::getModel('catalog/product')->loadByAttribute('sku',$ordernumber);
                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productObject->getId());
                        if((int)$stockItem->getManageStock()<1 && (int)$product['l_bestand']>0){
                            $stockItem->setManageStock(1);
                        }
                        $stockItem->setQty($variant['l_bestand']);
                        if($variant['l_bestand']>0){
                            $stockItem->setIsInStock(1);
                        }else{
                            $stockItem->setIsInStock(0);
                        }
                        $stockItem->save();
                        if(isset($variant['data']['products_status']) && (bool)$variant['data']['products_status'])
                        {
                            $productObject->setStatus(1);
                        }
                        else
                        {
                            $productObject->setStatus(2);
                        }
                        if(!empty($variant['data']['shipping_status']))
                        {
                            $shippingstatus = $variant['data']['shipping_status'];
                            $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                            if(!Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                                $productObject->setData($shippingField,$shippingstatus);
                                //delivery Time Update
                            }else{
                                //update delivery time
                                if((int)$shippingstatus>0){
                                    $ship = Mage::getModel('connector/acmapper')->load($shippingstatus);
                                    if($ship->getId()>0){
                                        $productObject->setData($shippingField,$ship->getValue());
                                    }
                                }
                            }
                        }
                        $productObject->save();
                    }
                }else{
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                    //update main article
                    $productObject = Mage::getModel('catalog/product')->loadByAttribute('sku',$product['art_nr']);
                    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productObject->getId());
                    if((int)$stockItem->getManageStock()<1 && (int)$product['l_bestand']>0){
                        $stockItem->setManageStock(1);
                    }
                    $stockItem->setQty($product['l_bestand']);
                    if($product['l_bestand']>0){
                        $stockItem->setIsInStock(1);
                    }else{
                        $stockItem->setIsInStock(0);
                    }
                    $stockItem->save();
                    if(isset($product['products_status']) && (bool)$product['products_status'])
                    {
                        $productObject->setStatus(1);
                    }
                    else
                    {
                        $productObject->setStatus(2);
                    }
                    if(!empty($product['shipping_status']))
                    {
                        $shippingstatus = $product['shipping_status'];
                        $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                        if(!Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                            $productObject->setData($shippingField,$shippingstatus);
                            //delivery Time Update
                        }else{
                            //update delivery time
                            if((int)$shippingstatus>0){
                                $ship = Mage::getModel('connector/acmapper')->load($shippingstatus);
                                if($ship->getId()>0){
                                    $productObject->setData($shippingField,$ship->getValue());
                                }
                            }
                        }
                    }
                    $productObject->save();
                }
            }catch(Exception $e){
                return false;
            }
            return true;
        }
        /**
         * Returns default Attribute  Set (default)
         * @return mixed
         */
        private function getDefaultAttributeSetId(){
            $helper = Mage::helper('connector');
            return $helper->getDefaultAttributeSetId();
        }
    }
