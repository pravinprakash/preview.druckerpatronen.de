<?php
    /**
     * Actindo Faktura/WWS Connector
     * class to handle customers
     * it supports counting customers,
     * getting list of customers
     * exporting customers
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Service_Customer{
        /**
         * Currency Value
         * @var string
         */
        protected $currency;
        /**
         * Constructor
         */
        public function __construct(){
            $curcode = Mage::app()->getStore()->getCurrentCurrencyCode();
            $this->currency = $curcode;
        }
        /**
         * count the number of customers and returns the highest shop and actindo deb kred id
         * @return array
         */
        public function count() {
            $count = Mage::getModel('customer/customer')->getCollection()->count();
            /**
             * get highest customer id
             */
            $highestid = Mage::getModel('customer/customer')->getCollection()->getLastItem()->getData();
            /**
             * get highest debkredid
             */
            $debkred = Mage::getModel('connector/accustmap')->getCollection()->getLastItem()->getData();
            $result = array(
                'ok'=>true,
                'counts'=>null
            );
            $result['counts'] = array(
                'count'=>$count,
                'max_customer_id'=>$highestid['entity_id'],
                'max_deb_kred_id'=>$debkred['deb_kred_id'],
            );
            return $result;
        }

        /**
         * sets the customernumber of a customer
         * @param int $userID userid whos customernumber should be set
         * @param int $customernumber the customernumber to set
         * @return struct
         */
        public function set_deb_kred_id($userID, $customernumber) {
            $mapperEntry = Mage::getModel('connector/accustmap')->getCollection()
                ->addFieldToFilter('id',$userID)
                ->getFirstItem();
            try{
                if((int)$mapperEntry->getId()!==(int)$userID){
                    $mapperEntry->setId((int)$userID);
                }
                $mapperEntry->setDebKredId((int)$customernumber);
                $mapperEntry->save();
                return array('ok'=>true);
            }catch(Exception $e){
                return array('ok'=>false);
            }
        }

        /**
         * get Column List
         * @return array
         */
        protected final function getColumenList(){
            return array(
                '_customers_id'=>'entity_id',
                'deb_kred_id'=>'id',
                'vorname'=>'firstname',
                'name'=>'lastname',
                'firma'=>'company',
                'land'=>'country_id',
                'email'=>'email'
            );
        }
        /**
         * Returns the Customers deb_kred_id
         * @param int $id
         * @return int|bool
         */
        protected function getCustomerKredId($id){
            $model = Mage::getModel('connector/accustmap')
                ->getCollection()
                ->addFieldToFilter('id',(int)$id)
                ->getFirstItem();
            if((int)$model->getId()>0){
                return $model->getDebKredId();
            }else{
                return false;
            }
        }
        /**
         * This is where customers.list is handled (despite the different method name).
         * Exports the customer list or a customers details.
         * @param boolean $list if true, a customerlist is returned. if false, a single customers details are returned
         * @param struct $filters Search Filters
         * @return struct customers list
         */
        public function getList($list, $filters) {
            $helper = Mage::helper('connector');
            $collection = Mage::getModel('customer/customer')->getCollection();
            $query = $helper->buildSearchQuery($filters,$this->getColumenList());
            if(count($query['where'])>0){
                foreach($query['where'] as $where){
                    if($where['field']=='entity_id'){
                        $result = explode(',',$where['value']);
                        $queryValue = array();
                        foreach($result as $key){
                            $queryValue[] = $key;
                        }
                        $collection->addFieldToFilter($where['field'],array('in'=>array($queryValue)));
                    }else{
                        $collection->addFieldToFilter($where['field'],$where['value']);
                    }
                }
            }
            $collection->addAttributeToSort($query['order']['field'],strtoupper($query['order']['direction']));
            $collection->setPageSize($query['limit']);
            $page = ($query['offset'] / $query['limit']) + 1;
            $collection->setCurPage($page);
            $results = $collection->getData();
            $customers = array();
            if((boolean)$list === true && count($results)>0){
                foreach($results as $result){
                    $res = new Actindo_Connector_Model_Customer_Exporter();
                    $res->setSimpleObject();
                    $res->setCustomerId($result['entity_id']);
                    $res->setCurrency($this->currency);
                    if($res->populate()){
                        $customers[] = $res->toArray();
                    }
                }
            }elseif((boolean)$list===false){
                foreach($results as $result){
                    $res = new Actindo_Connector_Model_Customer_Exporter();
                    $res->setCompleteObject();
                    $res->setCustomerId($result['entity_id']);
                    $res->setCurrency($this->currency);
                    if($res->populate()){
                        $customers[] = $res->toArray();
                    }
                }
            }
            $result = array(
                'ok' => true,
                'count' => count($customers),
                'customers' => $customers,
            );
            $result = Actindo_Connector_Model_Util_Util::getOutput($result,true,true);
            return $result;
        }
    }