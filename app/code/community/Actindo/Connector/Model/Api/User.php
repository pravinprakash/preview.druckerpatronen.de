<?php

    /**
     * Actindo Faktura/WWS Connector
     * Model for storing an api key in the api user data base
     * the api key is used to authenticate actindo against magento
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Api_User extends Mage_Api_Model_User{
        /**
         * Method to accept key
         * @return $this|Mage_Api_Model_User|Mage_Core_Model_Abstract
         */
        public function save(){
            $this->_beforeSave();
            $data = array(
                'firstname'     => $this->getFirstname(),
                'lastname'      => $this->getLastname(),
                'email'         => $this->getEmail(),
                'modified'      => Mage::getSingleton('core/date')->gmtDate(),
                'actindo_key'   => md5($this->getNewApiKey()),
            );
            if ($this->getId() > 0) {
                $data['user_id']   = $this->getId();
            }

            if ( $this->getUsername() ) {
                $data['username']   = $this->getUsername();
            }

            if ($this->getApiKey()) {
                $data['api_key']   = $this->_getEncodedApiKey($this->getApiKey());
            }

            if ($this->getNewApiKey()) {
                $data['api_key']   = $this->_getEncodedApiKey($this->getNewApiKey());
            }

            if ( !is_null($this->getIsActive()) ) {
                $data['is_active']  = intval($this->getIsActive());
            }

            $this->setData($data);
            $this->_getResource()->save($this);
            $this->_afterSave();
            return $this;
        }
    }