<?php
    /**
     * Actindo Faktura/WWS Connector
     * class for accessing the key value mapping
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Ackeyvaluemap extends Mage_Core_Model_Abstract{
        /**
         * Method for initializing the connector
         */
        public function _construct(){
            parent::_construct();
            $this->_init('connector/ackeyvaluemap');
        }
    }