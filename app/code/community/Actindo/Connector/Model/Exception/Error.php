<?php
    /**
     * Actindo Faktura/WWS Connector
     * Main Exception that is thrown
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Exception_Error extends Exception{

    }