<?php
    /**
     * Actindo Faktura/WWS Connector
     * class to authenticate against api user
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Auth_Util{
        /**
         * extract auth from request params (first param), validate it and write back the request parms (without the auth)
         * @param Actindo_Components_XmlRpc_Request $request
         * @return Actindo_Components_XmlRpc_Request the same request that was put in but the first request param is stripped
         * @throws Actindo_Components_Exception if authenticitation/authorisation fails for some reason
         */
        public static function checkAuth(Actindo_Connector_Model_Components_XmlRpc_Request $request) {
            $params = $request->getParams();
            $auth = array_shift($params);

            list($hash, $username) = explode('|||', $auth, 2);
            $result = Mage::getModel('api/user')->getCollection()->addFieldToFilter('username',$username)->getFirstItem();
            if((int)$result->getId()<1){
                throw new Actindo_Connector_Model_Exception_Error('Wrong Username or Password',100);
            }
            if(!$result->getIsActive()){
                $msg = sprintf('User `%s` is not active.', $username, $username);
                throw new Actindo_Connector_Model_Exception_Error($msg,102);
            }
            if($result->getActindoKey()!==$hash){
                throw new Actindo_Connector_Model_Exception_Error('Wrong Username or Password',101);
            }
            $request->setParams($params);
            return $request;
        }

    }