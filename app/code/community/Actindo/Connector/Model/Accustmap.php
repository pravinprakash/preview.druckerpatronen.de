<?php
    /**
     * Actindo Faktura/WWS Connector
     * class for access the customers map
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Accustmap extends Mage_Core_Model_Abstract{
        /**
         * consturctor
         * Initializing the opject
         */
        public function _construct(){
            parent::_construct();
            $this->_init('connector/accustmap');
        }
    }