<?php
/**
 * Actindo Faktura/WWS Connector
 * Helper for managing prices
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 */
    class Actindo_Connector_Model_Util_Price{
        /**
         * @var Pricegorup
         */
        protected $pricegroup;
        /**
         * @var tierprices
         */
        protected $tierprices;
        /**
         * @var grundprice
         */
        protected $grundpreis;
        /**
         * @var Actindo Return array
         */
        protected $actindo;
        /**
         * @var pseudo price
         */
        protected $msrp;
        /**
         * Variant Children
         */
        protected $variants=array();
        /**
         * Mapping
         */
        protected $map=array(
            'group'=>array(),
            'range'=>array()
        );
        /**
         * allowed Customer Groups
         * @var array
         */
        protected $customgroup=array();
        protected static $taxSetting;
        /**
         * calculates either the netto/brutto price depending on the shop settings
         * @param float $price price
         * @param float $mwst vat
         * @param string $setting either B for Brutton or N for Netto
         * @return float price value
         */
        protected function priceCalculate($price,$mwst,$setting='B'){
            if(self::$taxSetting===null){
                self::$taxSetting = Actindo_Connector_Model_System_Service::priceIncludesTax();
            }
            if(self::$taxSetting){
                if($setting!=='B'&&(int)$setting!==1){
                    $tax = ((float)$mwst/(float)100)+(float)1;
                    $price = round((float)$price * (float)$tax,4);
                }
            }else if($setting!=='N' && (int)$setting!==0){
                $tax = ((float)$mwst/(float)100)+(float)1;
                if($tax>(float)0){
                    $price = (float)$price/(float)$tax;
                }
            }
            $price = round($price,5);
            return $price;
        }
        /**
         * creates the data object either in direction to magento (data needs to be an array)
         * or in direction to actindo (data needs to be an Instance of Mage_Catalog_Model_Product)
         * @param mixed $data Dataobject given to create the object
         */
        public function __construct($data,$pricegroup){
            $this->customgroup = $pricegroup;
            if($data instanceof Mage_Catalog_Model_Product){
                $this->parseMagentoObject($data);
            }elseif(is_array($data) && count($data)>0){
                $this->parseActindoObject($data);
            }else{
                throw new Actindo_Connector_Model_Exception_Error('No Compatible Dataset given to get the Price Structure of the Article!',710);
            }
        }
        /**
         * Method Parses an Actindo Data Array to an object
         * This also Preparses the Complete Price Tree for all Variants as the Information needs to be read
         * into a pre defined
         * @param array $recordSet Actindo Record Set
         * @return void
         */
        protected function parseActindoObject($recordSet){
            $groups = Mage::getModel('customer/group')->getCollection()->getAllIds();
            if((float)$recordSet['grundpreis'] > (float)0){
                $this->grundpreis = $this->priceCalculate($recordSet['grundpreis'],$recordSet['mwst'],$recordSet['is_brutto']);
            }
            if((float)$recordSet['shop']['art']['products_pseudoprices'] > 0.00){
                $this->msrp = (float)$recordSet['shop']['art']['products_pseudoprices'];
            }
            //preapre the price data for ranges
            $prepareDatas = array();
            if(count($recordSet['preisgruppen'])){
                foreach($recordSet['preisgruppen'] as $group){
                    $groupid = (int)$group['gruppe']-250;
                    if(in_array($groupid,$this->customgroup)){
                        $dset = array('gruppe'=>0,'gp'=>0,'range'=>array());
                        foreach($group as $key=>$value){
                            if($key ===  'gruppe'){
                                $dset['gruppe']= ((int)$value - (int)250);
                            }else if($key === 'grundpreis'){
                                $dset['gp'] = $this->priceCalculate($value,$recordSet['mwst'],$group['is_brutto']);
                            }else if(strpos($key,'preis_gruppe')!==false){
                                $ident = (int)str_replace('preis_gruppe','',$key);
                                if(!isset($dset['range'][$ident]) || !is_array($dset['range'][$ident])){
                                    $dset['range'][$ident] = array();
                                }
                                $dset['range'][$ident]['price'] = $this->priceCalculate($value,$recordSet['mwst'],$group['is_brutto']);
                            }else if(strpos($key,'preis_range')!==false){
                                $ident = (int)str_replace('preis_range','',$key);
                                if(!isset($dset['range'][$ident]) || !is_array($dset['range'][$ident])){
                                    $dset['range'][$ident] = array();
                                }
                                $dset['range'][$ident]['quantity'] = $value;
                            }
                        }
                    }
                    $prepareDatas[$dset['gruppe']] = $dset;
                }
            }
            //now process the stuff for each pricegroup to add it to the main article
            $storeid = Mage::app()->getStore()->getWebsiteId();
            $this->pricegroup = array();
            $this->tierprices = array();
            foreach($prepareDatas as $prepareData){
                //first set the Grundpreis
                $groupident = count($this->pricegroup);
                $this->pricegroup[$groupident] = array(
                    'website_id'=>0,
                    'cust_group'=>$prepareData['gruppe'],
                    'price'=>(float)$prepareData['gp'],
                    'website_price'=>(float)$prepareData['gp'],
                    'all_Groups'=>0
                );
                $this->map['group'][$prepareData['gruppe']] = $groupident;
                //now run through the groups if available
                if(count($prepareData['range'])>0){
                    foreach($prepareData['range'] as $range){
                        if(count($range)>1){
                            $tierid = count($this->tierprices);
                            $this->tierprices[$tierid] = array(
                                'website_id'=>0,
                                'cust_group'=>$prepareData['gruppe'],
                                'price'=>(float)$range['price'],
                                'website_price'=>(float)$range['price'],
                                'price_qty'=>number_format((float)$range['quantity'], 4, '.',''),
                                'all_Groups'=>0
                            );
                            if(!isset($this->map['range'][$prepareData['gruppe']])){
                                $this->map['range'][$prepareData['gruppe']] = array();
                            }
                            $this->map['range'][$prepareData['gruppe']][$range['quantity']] = $tierid;
                        }
                    }
                }
            }
            if(isset($recordSet['shop']['attributes']['combination_advanced']) && is_array($recordSet['shop']['attributes']['combination_advanced']) && count($recordSet['shop']['attributes']['combination_advanced'])>0){
                foreach($recordSet['shop']['attributes']['combination_simple'] as $attribute=>$value){
                    $this->variants[$attribute] = array();
                    foreach($value as $property=>$valueSet){
                        $this->variants[$attribute][$property] = (float)$valueSet['options_values_price'];
                    }
                }
            }
        }
        /**
         * Method for converting an Magento Object to an Actindo Array Collection
         * @param Mage_Catalog_Model_Product $recordSet Product Record Set
         * @return void
         */
        protected function parseMagentoObject(Mage_Catalog_Model_Product $recordSet){
            $this->actindo = array();
            $pricegroup = $recordSet->getData('group_price');
            $tierprice = $recordSet->getData('tier_price');
            $taxHelper = Mage::getSingleton('tax/calculation');
            //Pseudo Preis
            $this->actindo['products_pseudoprices'] = array(
                0=>(float)$recordSet->getMsrp()
            );
            // get Grundpreis
            $this->actindo['grundpreis'] = (float)$recordSet->getPrice();
            //EK
            $this->actindo['ek'] = (float)$recordSet->getCost();
            $defaultTax = $this->helper->getTaxRate($this->product,$taxHelper->getDefaultCustomerTaxClass($this->storeid));
            $this->actindo['is_brutto']=(int)((int)$defaultTax>0);
            //now get the group prices
            if(count($tierprice)>0){
                //pre sorting
                $tierPriceByGroup = array();
                foreach($tierprice as $tier){
                    if((int)$tier['all_groups']>0){
                        $tierPriceByGroup[-1][] = $tier;
                    }else{
                        $tierPriceByGroup[$tier['cust_group']][] = $tier;
                    }
                }
                $tierprices = array();
                //create basic fields for complete set
                foreach($this->customerGroups as $group){
                    $tierprices[$group] = array();
                }
                //if price is valid for all groups
                if(isset($tierPriceByGroup[-1])){
                    foreach($tierPriceByGroup[-1] as $tier){
                        foreach($this->customerGroups as $group){
                            $tierprices[$group][(int)$tier['price_qty']] = $tier['price'];
                        }
                    }
                    //unset all groups tier price
                    unset($tierPriceByGroup[-1]);
                }
                //do rest of the group prices
                if(count($tierPriceByGroup)>0){
                    foreach($tierPriceByGroup as $ident=>$tierByGroup){
                        foreach($tierByGroup as $tier){
                            $tierprices[$ident][(int)$tier['price_qty']] = $tier['price'];
                        }
                    }
                }
                //now fill the main data array
                if(count($tierprices)>0){
                    foreach($tierprices as $groupid=>$priceranges){
                        //sort array
                        ksort($priceranges);
                        $defaultTax = $this->helper->getTaxRate($this->product,$groupid);
                        $this->actindo['preisgruppen'][$groupid] = array(
                            'is_brutto'=>(int)((int)$defaultTax>0)
                        );
                        //check grundpreis, if not product price is used
                        if(!isset($priceranges[1])){
                            $this->actindo['preisgruppen'][$groupid]['grundpreis']=$recordSet->getPrice();
                        }else{
                            $this->actindo['preisgruppen'][$groupid]['grundpreis']=$priceranges[1];
                            unset($priceranges[1]);
                        }
                        //run throuh remaining groups
                        $i = 1;
                        foreach($priceranges as $range=>$price){
                            $this->actindo['preisgruppen'][$groupid]['preis_gruppe'.$i] = (float)$price;
                            $this->actindo['preisgruppen'][$groupid]['preis_range'.$i] = (int)$range;
                            $i++;
                        }
                    }
                }
            }elseif(count($pricegroup)>0){
                foreach($pricegroup as $tier){
                    if(isset($this->actindo['preisgruppen'][0]))
                        $this->actindo['preisgruppen'][0] = array();
                    $id = 0;
                    $defaultTax = $this->helper->getTaxRate($this->product,$tier);
                    $this->actindo['preisgruppen'][$id]['is_brutto'] = (int)((int)$defaultTax>0);
                    $this->actindo['preisgruppen']['grundpreis']=$tier['price'];
                }
            }
        }
        /**
         * returns the tier prices
         * (only valid for actindo>magento)
         * @return tierprices
         */
        public function getTierPrice(){
            return $this->tierprices;
        }
        /**
         * returns the price group data
         * (only valid for actindo>magento)
         * @return Pricegorup
         */
        public function getGroupPrice(){
            return $this->pricegroup;
        }
        /**
         * return the msrp
         * (only valid for actindo>magento)
         * @return float pseudo
         */
        public function getMsrp(){
            return (float)$this->msrp;
        }
        /**
         * returns grundprice
         * (only valid for actindo>magento)
         * @return grundprice
         */
        public function getGrundPrice(){
            return (float)$this->grundpreis;
        }
        /**
         * returns an actindo data set, compatible with actindo
         * @return Actindo
         */
        public function getActindoArray(){
            return $this->actindo;
        }
        /**
         * Set Pseudo Price
         * @param \pseudo $msrp
         */
        public function setMsrp($msrp){
            $this->msrp = $msrp;
        }
        /**
         * set Price Groups
         * @param \Pricegorup $pricegroup
         */
        public function setPricegroup($pricegroup){
            $this->pricegroup = $pricegroup;
        }
        /**
         * set Tier Prices
         * @param \tierprices $tierprices
         */
        public function setTierprices($tierprices){
            $this->tierprices = $tierprices;
        }
        /**
         * set Grund Price
         * @param \grundprice $grundpreis
         */
        public function setGrundpreis($grundpreis){
            $this->grundpreis = $grundpreis;
        }
        /**
         * Returns the Price Map as Object
         * @return object
         */
        public function getMap(){
            return (object)$this->map;
        }
        /**
         * get variants array (+/- Prices for Variants)
         * @return array
         */
        public function getVariant(){
            return $this->variants;
        }
    }