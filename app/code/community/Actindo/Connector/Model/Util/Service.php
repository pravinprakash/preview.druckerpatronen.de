<?php
/**
 * Actindo Faktura/WWS Connector
 * Utility Helper
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Service_Product_Export
 * @version 2.309
 * @abstract
 */
    class Actindo_Connector_Model_Util_Service{
        /**
         * Scans the given valueSet for NULL and converts it to string
         * used to prevent NIL errors on XMLRPC processor
         * @param $valueSet
         * @return array|string
         */
        public static function ScanForNullAndCorrect($valueSet){
            if(is_array($valueSet)){
                foreach($valueSet as $key=>$value){
                    if(is_array($value)){
                        $valueSet[$key] = self::ScanForNullAndCorrect($value);
                    }else{
                        if(empty($value) && $value!==0){
                            $valueSet[$key] = (string)'';
                        }
                    }
                }
                return $valueSet;
            }else{
                if(empty($valueSet) && $valueSet!==0){
                    $valueSet = (string)'';
                }
                return $valueSet;
            }
        }

        /**
         * Converts given structure on the Base64 or empty string if null
         * @param $set
         * @return array|string|Zend_XmlRpc_Value_Base64
         */
        public static function encodeBase64($set){
            if(is_array($set)){
                foreach($set as $key=>$value){
                    if(is_array($value)){
                        $set[$key] = self::encodeBase64($value);
                    }else{
                        if(!empty($value) && !is_object($value)){
                            $set[$key] = new Zend_XmlRpc_Value_Base64($value);
                        }
                    }
                }
                return $set;
            }else{
                if(!empty($value) && !is_object($value)){
                    return new Zend_XmlRpc_Value_Base64($set);
                }else{
                    return (string)'';
                }
            }
        }
    }