<?php
/**
 * Actindo Faktura/WWS Connector
 * utility class
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 */
class Actindo_Connector_Model_Util_Util{
    /**
     * Settings for Encoding/transfer
     * @var array
     */
    protected static $uploadSettings = array();
    const XML_PATH = 'actindo/storemapping/';
    /**
     * return map types
     * @param $type
     *
     * @return string
     */
    public final function getMapTypes($type){
        $list = array(
            'text'=>'textfield',
            'text'=>'textfield',
            'hidden'=>'textfield',
            'price'=>'numberfield',
            'textarea'=>'textarea',
            'multiselect'=>'multiselect',
            'select'=>'combobox',
            'boolean'=>'boolean',
            'date'=>'textfield'
        );
        return (isset($list[$type]))?$list[$type]:'textfield';
    }
    protected $runonce=false;
    /**
     * Constructor
     */
    public function __construct(){
        if(!$this->runonce){
            $this->languagecache = null;
            $this->runonce = true;
        }
    }
    protected $languagecache;
    protected $defaultlanguage;
    /**
     * Get Language
     * @return array|null
     */
    public function getLanguages(){
        if($this->languagecache===null){
            $websiteId = Actindo_Connector_Model_System_Service::getCurrentWebsiteId();
            $stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('website_id',$websiteId);
            $options = array();
            $defaultcfg = Mage::getStoreConfig('actindo/general/defaultlang',Actindo_Connector_Model_System_Service::getStoreID());
            $defaultlng = strtolower($defaultcfg[0].$defaultcfg[1]);
            $defaultlang = 0;
            foreach($stores as $store){
				$currentStoreId = $store->getStoreId();
                $path = self::XML_PATH.'language_'.$store->getCode();
                $lang = Mage::getStoreConfig($path,$currentStoreId);
                if($lang === 'default'){
                    $lang = Mage::getStoreConfig('general/locale/code',$currentStoreId);
                    $default = 0;
                }else{
                    $default = 1;
                }
                $lang = strtoupper($lang);
                if($lang==$defaultlng){
                }
                $lang = $lang[0].$lang[1];
                $lang_code_arr = explode('_', (string)$lang );
                $lang_code_iso2 = array_shift( $lang_code_arr );
                $defaultlang = $currentStoreId;
                $options[$currentStoreId] = array(
                    'language_id'=>$currentStoreId,
                    'language_name'=>$store->getName(),
                    'store_code'=>$store->getCode(),
                    'language_code'=> strtolower($lang_code_iso2),
                    'is_default'=>$default,
                    'url'=>Mage::getStoreConfig('web/secure/base_url',$currentStoreId),
                    'active'=>$store->getIsActive(),
                );
            }
            $this->languagecache = $options;
            $this->defaultlanguage = $defaultlang;
            return $options;
        }
        return $this->languagecache;
    }

    /**
     * get default language
     * @return mixed
     */
    public function getDefaultLanguage(){
        if($this->languagecache===null)
            $this->getLanguages();
        return $this->defaultlanguage;
    }
    protected $manufacturer;

    /**
     * get Manufacturer
     * @param $website
     *
     * @return array|null
     */
    public function getManufacturer($website){
        if($this->manufacturer===null){
            $mappingfield = Mage::getStoreConfig('actindo/mappingfields/manufacturerid',Mage::app()->getStore());
            $attribute = Mage::getModel('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $mappingfield);
            if(!empty($attribute) && $attribute->getFrontendInput()=='select'){
                $return = array();
                foreach ( $attribute->getSource()->getAllOptions(true, true) as $option){
                    if(strlen($option['value']) || strlen($option['label'])){
                        $return[] = array(
                            'manufacturers_id'   => $option['value'],
                            'manufacturers_name' => $option['label']
                        );
                    }
                }
                $this->manufacturer = $return;
            }else{
                //
                //
                $siteviews = Mage::getModel('core/store')
                    ->getCollection()
                    ->addFieldToFilter('website_id',$website);
                $results = Mage::getModel('connector/acmapper')
                    ->getCollection()
                    ->addFieldToFilter('type','manufacturers')
                    ->addFieldToFilter('site',array('in'=>$siteviews->getAllIds()));
                $results->getSelect()
                    ->group('value');
                $return = array();
                foreach($results->getData() as $resultset){
                    $return[] = array(
                        'manufacturers_id'=>$resultset['id'],
                        'manufacturers_name'=>$resultset['value'],
                    );
                }
                $this->manufacturer = $return;
            }
        }
        return $this->manufacturer;
    }
    protected $customerGroups;
    /**
     * retreive Customer Groups
     * @return array
     */
    public function getCustomerGroups(){
        if($this->customerGroups===null){
            $return = array();
            $customerGroups  = Mage::getModel('customer/group')->getCollection();
            foreach($customerGroups as $customerGroup){
                $languages = $this->getLanguages();
                $name = array();
                $custname = $customerGroup->getCustomerGroupCode();
                foreach($languages as $key=>$language){
                    $name[$key] = $custname;
                }
                $return[$customerGroup->getCustomerGroupId()] = array(
                    'customers_status_id' => $customerGroup->getCustomerGroupId(),
                    'customers_status_name' => $custname,//$name,
                    'customers_status_min_order' => (float)0,
                    'customers_status_discount' => (float)0,
                    'customers_status_show_price_tax' => 1,
                );
            }
            $this->customerGroups = $return;
        }
        return $this->customerGroups;
    }
    protected $shippingtime;
    /**
     * retreive Shipping times
     * @param $website
     *
     * @return array
     */
    public function getShippingTime($website){
        if($this->shippingtime === null){
            if(Actindo_Connector_Model_System_Service::isShippingObserverEnabled()){
                $this->shippingtime = array();
                $siteviews = Mage::getModel('core/store')
                    ->getCollection()
                    ->addFieldToFilter('website_id',$website);
                $results = Mage::getModel('connector/acmapper')
                    ->getCollection()
                    ->addFieldToFilter('type','shipping')
                    ->addFieldToFilter('site',array('in'=>$siteviews->getAllIds()));
                $results->getSelect()
                    ->group('value');
                $return = array();
                foreach($results->getData() as $resultset){
                    $return = array(
                        'id'=>$resultset['id'],
                        'text'=>$resultset['value'],
                    );
                    $this->shippingtime[] = $return;
                }
            }else{
                $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',Actindo_Connector_Model_System_Service::getStoreID());
                $attributeModel = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setCodeFilter($shippingField)
                    ->getFirstItem();
                $attributeValueSets = $attributeModel->getSource()->getAllOptions();
                $this->shippingtime = array();
                if(count($attributeValueSets)>0){
                    foreach($attributeValueSets as $valueSets){
                        $key = $valueSets['value'];
                        $value = $valueSets['label'];
                        if(!empty($key) && !empty($value)){
                            $this->shippingtime[] = array(
                                'id'=>$key,
                                'text'=>$value
                            );
                        }
                    }
                }
            }
        }
        return $this->shippingtime;
    }
    protected $xsellinggroups;
    /**
     * retreive Crossselling groups
     * @return array
     */
    public function getCrosselingGroups(){
        if($this->xsellinggroups === null){
            // Crosselling
            $return = array(
                1 => array(
                    'products_xsell_grp_name_id' => 1,
                    'xsell_sort_order' => 0,
                    'groupname' => 'Cross-Selling'
                ),
                2 => array(
                    'products_xsell_grp_name_id' => 2,
                    'xsell_sort_order' => 1,
                    'groupname' => 'Up-Selling',
                ),
                3 => array(
                    'products_xsell_grp_name_id' => 3,
                    'xsell_sort_order' => 2,
                    'groupname' => 'Related Products',
                ),
                4 => array(
                    'products_xsell_grp_name_id' => 4,
                    'xsell_sort_order' => 3,
                    'groupname' => 'Grouped'
                )
            );
            $this->xsellinggroups = $return;
        }
        return $this->xsellinggroups;
    }
    protected $shippingvendors;
    /**
     * get shipping vendors
     * @param $site
     *
     * @return array
     */
    public function getShippingVendors($site){
        if($this->shippingvendors === null){
            $results = Mage::getModel('shipping/config')->getAllCarriers(Actindo_Connector_Model_System_Service::getStoreID());
            $return = array();
            foreach($results as $resultset){
                $return[] = array(
                    'id'=>$resultset->getId(),
                    'name'=>$resultset->getId(),
                    'active'=>(int)$resultset->isActive(),
                    'code'=>$resultset->getId()
                );
            }
            $this->shippingvendors = $return;
        }
        return $this->shippingvendors;
    }
    protected $orderstatus;
    /**
     * get Order Status
     * @return array
     */
    public function getOrderStatus(){
        $helper = Mage::helper('connector');
        if($this->orderstatus===null){
            $orderstatus = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
            $return = array();
            foreach($orderstatus as $orderstatusset){
                $status = $helper->getOrderStatusId($orderstatusset['status']);
                if(!$status){
                    $result = Mage::getModel('connector/ackeyvaluemap');
                    $result->setType('orderstatus')
                        ->setIdent($orderstatusset['status'])
                        ->save();
                    $status = $result->getId();
                }
                $return[$status][$this->getDefaultLanguage()] = $orderstatusset['label'];
            }
            $this->orderstatus = $return;
        }
        return $this->orderstatus;
    }
    protected $paymentprovider;
    /**
     * get Payment Providers
     * @return array
     */
    public function getPaymentProvider(){
        if($this->paymentprovider===null){
            $return = array();
            $payment = Mage::getModel('payment/config');
            foreach($payment->getAllMethods() as $method){
                if($method!==null && $method->isAvailable())
                {
                    $code = $method->getCode();
                    if($method instanceof Payone_Core_Model_Payment_Method_Abstract)
                    {
                        $name = $method->getId();
                        $activeFlag = true;
                    }else{
                        $methodInstance = Mage::helper('payment')->getMethodInstance($code);
                        $name = $methodInstance->getTitle();
                        $activeFlag = Mage::getStoreConfigFlag('payment/'.$code.'/active', null);
                    }
                    $return[] = array(
                        'id'    => $code,
                        'code' => $code,
                        'name' => $name,
                        'active' => $activeFlag,
                    );
                }

            }
            $this->paymentprovider = $return;
        }
        return $this->paymentprovider;
    }
    protected $attributeBlackList;
    /**
     * get Attribute Black List
     * @obsolete
     */
    public function getAttributeBlackList(){

    }
    protected $attributesset;
    /**
     * get Attribute set
     * @param $siteid
     *
     * @return array
     */
    public function getAttributesSet($siteid){
        if($this->attributesset === null){
            $languages = $this->getLanguages();
            $attributes = new Actindo_Connector_Model_System_Config_Source_Mapping();
            #Prepare Attributes
            $this->attributesset = array();
            $alist = $attributes->getConnectorValueList(Actindo_Connector_Model_System_Service::getStoreID());
            foreach($alist as $key){
                $i18n = 0;
                //global Values don't have i18n
                if((int)$key['global']===0){
                    $i18n = 1;
                }
                $data =array(
                    'field_id'      => $key['value'],
                    'field_name'    => new Zend_XmlRpc_Value_Base64($key['label']),
                    'field_i18n'    => $i18n, #on magento always translatable
                    'field_set'     => 'Magento',
                    'field_set_ids' => array(0),
                    'field_help'    => '',
                    'field_noempty' => $key['required'],
                    'field_type'    => $this->getMapTypes($key['type']),
                    'variantable'   => 0
                );
                if($key['valueset']!==false && is_array($key['valueset'])){
                    $data['field_values'] = $key['valueset'];
                }
                $this->attributesset[] = $data;
            }
            $this->attributesset[] = array(
                'field_id' => 'product_type',
                'field_name' => new Zend_XmlRpc_Value_Base64('Produkt Typ (Magento)'),
                'field_i18n' => 0,
                'field_set'  => 'Magento System',
                'field_set_ids' => array(0),
                'field_help' => '',
                'field_noempty'=>0,
                'field_type'=>$this->getMapTypes('select'),
                'variantale'=>0,
                'field_values'=>array(
                    array(
                        'value'=>'simple',
                        'text'=>'Simple Produkt'
                    ),
                    array(
                        'value'=>'configurable',
                        'text'=>'Configurable Produkt'
                    ),
                    array(
                        'value'=>'grouped',
                        'text'=>'Grouped Produkt'
                    ),
                ),
            );
            $this->attributesset[] = array(
                'field_id' => 'product_visibility',
                'field_name' => new Zend_XmlRpc_Value_Base64('Produkt Sichtbarkeit (Magento)'),
                'field_i18n' => 0,
                'field_set'  => 'Magento System',
                'field_set_ids' => array(0),
                'field_help' => '',
                'field_noempty'=>0,
                'field_type'=>$this->getMapTypes('select'),
                'variantale'=>0,
                'field_values'=>array(
                    array(
                        'value'=>'1',
                        'text'=>'Einzeln nicht sichtbar'
                    ),
                    array(
                        'value'=>'2',
                        'text'=>'Katalog'
                    ),
                    array(
                        'value'=>'3',
                        'text'=>'Suche'
                    ),
                    array(
                        'value'=>'4',
                        'text'=>'Katalog und Suche'
                    )
                ),
            );
        }
        return $this->attributesset;
    }

    /**
     * @todo
     * @param $siteid
     * @return array
     */
    public function getAttributeFilters($siteid){
        return array();
    }
    protected $multistores;

    /**
     * get Multi Shop
     * @param $storeid
     *
     * @return array
     */
    public function getMultiStores($storeid){
        if($this->multistores===null){
            $this->multistores = array();
            $stores = $this->getLanguages();
            if(count($stores)>0){
                foreach($stores as $store){
                    $data = array(
                        'id'=>$store['language_id'],
                        'name'=>$store['language_name'],
                        'url_http'=>$store['url'],
                        'active'=>(int)$store['active'],
                        'locale'=>$store['language_code'],
                        'language_id'=>$store['language_id'],
                        'language_name'=>$store['language_name'],
                    );
                    $this->multistores[] = $data;
                }
            }
        }
        return $this->multistores;
    }
    protected $vpe;

    /**
     * get VPE
     * @param $website
     *
     * @return array
     */
    public function getVpe($website){
        if($this->vpe===null){
            $mappingfield = Mage::getStoreConfig('actindo/mappingfields/products_vpe',Mage::app()->getStore());
            $attribute = Mage::getModel('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $mappingfield);
            if(!empty($attribute) && $attribute->getFrontendInput()=='select'){
                $return = array(0=>array());
                $vpes = $attribute->getSource()->getAllOptions(true, true);
                foreach ( $vpes as $option){
                    if(strlen($option['value']) || strlen($option['label'])){
                        $return[0][] = array(
                            'products_vpe'   => $option['value'],
                            'vpe_name' => $option['label']
                        );
                    }
                }
                $this->vpe = $return;
            }else{
                $siteviews = Mage::getModel('core/store')
                    ->getCollection()
                    ->addFieldToFilter('website_id',$website);
                $results = Mage::getModel('connector/acmapper')
                    ->getCollection()
                    ->addFieldToFilter('type','vpe')
                    ->addFieldToFilter('site',array('in'=>$siteviews->getAllIds()));
                $results->getSelect()
                    ->group('value');
                $return = array();
                foreach($results->getData() as $resultset){
                    $return[] = array(
                        'id'=>$resultset['id'],
                        'text'=>$resultset['value'],
                    );
                }
                $this->vpe = $return;
            }
        }
        return $this->vpe;
    }
    /**
     * Method Returns the Available Configurable Sets
     * @return array
     */
    public function getConfigurableSets(){
        $resultSet = array();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection') ->load();
        if(count($attributeSetCollection)>0){
            foreach ($attributeSetCollection as $id=>$attributeSet){
                if(!isset($resultSet[$attributeSet->getAttributeSetName()])){
                    $resultSet[$attributeSet->getAttributeSetName()] = $attributeSet->getAttributeSetId();
                }
            }
        }
        $tmpSet = $resultSet;
        $helper = Mage::helper('connector');
        $resultSet = array(
            array(
                'id'=> $helper->getDefaultAttributeSetId(),
                'name'=>'Standart Artikel (enthaelt alle Felder - Standart fuer Simple Products)',
                'default'=>true,
            )
        );
        if(count($tmpSet)>0){
            foreach($tmpSet as $key=>$value){
                if((int)$value>1){
                    $resultSet[] = array(
                        'id'=>$value,
                        'name'=>new Zend_XmlRpc_Value_Base64($key)
                    );
                }
            }
        }
        return $resultSet;
    }

    /**
     * get Encoding Settings
     * @param $setting setting to search for
     * @return boolean
     */
    public static function getEncodingSettings($setting)
    {
        if(count(self::$uploadSettings)<1)
        {
            $features = Mage::getStoreConfig('actindo/encoding',Mage::app()->getStore()->getWebsiteId());
            foreach($features as $key=>$value){
                self::$uploadSettings[$key] = (bool)$value;
            }
        }
        return self::$uploadSettings[$setting];
    }
    /**
     * Rekursive Funktion to Post Process Array Data and set correct encoding/base64 encoding or not null values
     * @param $output data to be processed
     * @param bool $base64 flag to enable base64 encoding
     * @param bool $removeNull to remove null values
     * @return array|string|Zend_XmlRpc_Value_Base64
     */
    public static function getOutput($output,$base64=true,$removeNull=true)
    {
        if(self::getEncodingSettings('base64encode') && $base64!==false)
        {
            $base64 = false;
        }
        //check if output is an array or not
        if(is_array($output))
        {
            //run through array
            foreach($output as $valueId=>$valueData)
            {
                //if the output is array, run rekursive
                if(is_array($valueData))
                {
                    $output[$valueId] = self::getOutput($valueData,$base64,$removeNull);
                }
                else
                {
                    $tmpData = $valueData;
                    if(self::getEncodingSettings('exportdecode') && !is_object($tmpData))
                    {
                        $tmpData = utf8_decode($tmpData);
                    }
                    //should null values  be removed?
                    if(empty($valueData) && $valueData !== 0 && $removeNull)
                    {
                        $tmpData = (string)$tmpData;
                    }
                    //should the values be base64 encoded
                    if($base64)
                    {
                        $tmpData = new Zend_XmlRpc_Value_Base64($tmpData);
                    }
                    $output[$valueId] = $tmpData;
                }
            }
            return $output;
        }
        else
        {
            //according to Setting of encoding/exportdecode utf8 conversion of data
            if(self::getEncodingSettings('exportdecode') && !is_object($output))
            {
                $output = utf8_decode($output);
            }
            //remove null values?
            if(empty($output) && $output !== 0 && $removeNull)
            {
                $output = (string)'';
            }
            //encode base64?
            if($base64)
            {
                $output = new Zend_XmlRpc_Value_Base64($output);
            }
            return $output;
        }
    }

    /**
     * Pre Process Input Encoding
     * @param $input mixedl containing data
     */
    public static function preProcessInput($input)
    {

        //check if input is an array or not
        if(is_array($input))
        {
            //run through array
            foreach($input as $valueId=>$valueData)
            {
                //if the output is array, run rekursive
                if(is_array($valueData))
                {
                    $input[$valueId] = self::preProcessInput($valueData);
                }
                else
                {
                    //according to Setting of encoding/exportdecode utf8 conversion of data
                    if(self::getEncodingSettings('importencode') && !is_object($valueData))
                    {
                        $input[$valueId] = utf8_encode($valueData);
                    }
                }
            }
        }
        else
        {
            //according to Setting of encoding/exportdecode utf8 conversion of data
            if(self::getEncodingSettings('importencode') && !is_object($input))
            {
                $input = utf8_encode($input);
            }
        }
        return $input;
    }
}
