<?php
/**
 * Actindo Faktura/WWS Connector
 * Sets/Unsets Properties
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Util_Util
 * @version 2.309
 */
class Actindo_Connector_Model_Util_Properties{
    /**
     * Set of Properties
     * @var array
     */
    protected $properties;
    /**
     * Variant Data for checks
     * @var array
     */
    protected $variants;
    /**
     * Store Mapping
     * @var array
     */
    protected $mappings;
    /**
     * construct
     * sets initial values
     * @param array $properties array of properties
     * @param array $variants array of variants
     * @param array $mappings store mapping array
     */
    public function __construct($properties,$variants,$mappings){
        $this->properties   = $properties;
        $this->variants     = $variants;
        $this->mappings     = $mappings;
    }
    public function run(){
        //start processing
        foreach($this->properties as $propertyName=>$propertyData){
            if(!isset($this->variants[$propertyData['id']]) && !isset($this->variants[$propertyData['name']]) && $propertyData['type']==='combobox'){
                $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $propertyData['id']);
                //check if it exists and it's an select field
                if($attribute->getId()>0 && $attribute->getFrontendInput()==='select'){
                    //attributes exist, and is select field, do the update
                    $this->updateAttribute($attribute,$propertyData);
                }
            }
        }
    }
    /**
     * method to do the attribute updates/deletions
     * @param Mage_Eav_Model_Entity_Attribute $attribute current attribute
     * @param $propertyData property data
     */
    protected function updateAttribute(Mage_Eav_Model_Entity_Attribute $attribute,$propertyData){
        //get property values
        $actindo = $propertyData['values'];
        //initiate map array
        $map = array();
        // check if property values exist
        if(count($actindo)>0){
            //build mapping
            foreach($actindo as $id=>$valueArray){
                if($valueArray['name']=='leer'){
                    unset($actindo[$id]);
                    continue;
                }
                $map[$valueArray['name']] = $id;
            }
            //get attribute options (from magento)
            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);
            }
            //now we got the options
            if(count($options)>0){
                //unset actindo properties
                foreach($options as $option){
                    $label = $option['label'];
                    if(isset($map[$label])){
                        $id = $map[$label];
                        unset($actindo[$id]);
                    }
                }
            }
            //if properties are left, create them in magento
            if(count($actindo)>0){
                $cnt = count($options);
                foreach($actindo as $entryID=>$entry){
                    $optionValue = $entry['values'];
                    $optionLabel = $entry['name'];
                    $option = array(
                        'value'=>array(
                            'option'=>array(
                                0=>$optionLabel,
                            ),
                        ),
                        'order'=>array(
                            'option'=>$optionValue,
                        ),
                    );
                    $attribute->setData('option',$option);
                    $attribute->save();
                    $cnt++;
                }
            }
        }
        //building data for fast check if option still exists
        $actindo = array();
        foreach($propertyData['values'] as $entry){
            $actindo[$entry['name']] = $entry['values'];
        }
        //get options collection
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($attribute->getId())
            ->setStoreFilter($attribute->getStoreId())
            ->load();
        //run through collection and search for still existing options
        foreach($collection as $option){
            $value = $option->getDefaultValue();
            if(!isset($actindo[$value])){
                $option->delete();
            }
        }
    }
}