<?php
    /**
     * Actindo Faktura/WWS Connector
     * Debug Helper
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Actindo_Connector_Model_Service_Product_Export
     * @version 2.309
     * @abstract
     */
    class Actindo_Connector_Model_Util_Debug{
        /**
         * Method to Vardump Content to a File
         * needed if you need also the type of a variable!
         * @param $dump
         * @param $file
         * @param bool $append
         */
        public static function dump2file($dump,$file,$append=true){
            ob_start();
            var_dump($dump);
            $content = ob_get_contents();
            ob_end_clean();
            file_put_contents($file,$content,($append)?FILE_APPEND:null);
        }
        /**
         * Monitoring Timer
         * @var
         */
        protected static $time;
        /**
         * Set Start time
         */
        public static function startTime(){
            self::$time = (float)microtime(true);
        }
        /**
         * Print Out Current Stamp and what was done
         * @param $text
         */
        public static function measure($text){
            $ms = (float)microtime(true) - (float)self::$time;
            Mage::log($text.' Ran in '.$ms.' ms');
        }
        public static function dump($dump){
            ob_start();
            var_dump($dump);
            $var_dump = ob_get_contents();
            ob_end_clean();
            $c_type = '<font%%%%%%%%%%color="#FF8000">';
            $c_phpnamespace = '<font%%%%%%%%%%color="#007700">';
            $c_string = '<font%%%%%%%%%%color="#DD0000">';
            $c_integer = '<font%%%%%%%%%%color="#0000BB">';
            $c_cursor = '<font%%%%%%%%%%color="#0000FF">';
            $c_close = '</font>';
            $var_dump = str_replace("\n", "<br>\n", $var_dump);
            $var_dump = preg_replace('/(.*?)\((.*?)\) \"(.*?)\"/ms', "\\1(\\2) ".$c_string.'"\\3"'.$c_close, $var_dump);
            $var_dump = preg_replace('/([a-zA-z]*)(\([a-zA-Z0-9-]*\))/', $c_type."\\1".$c_close."\\2", $var_dump);
            $var_dump = preg_replace("/\(([0-9-]*)\)/", "(".$c_integer."\\1".$c_close.")", $var_dump);
            $var_dump = preg_replace("/\(([a-zA-Z]*)\)/", "(".$c_string."\\1".$c_close.")", $var_dump);
            $var_dump = preg_replace('/\[\"([_a-zA-Z]*)\"\]/', '['.$c_phpnamespace.'"\\1"'.$c_close.']', $var_dump);
            $var_dump = preg_replace('/(=>)/', $c_cursor.'\\1'.$c_close, $var_dump);
            $var_dump = str_replace(" ", "&nbsp;", $var_dump);
            $var_dump = '<br><font size="2" face="Courier New, Fixedsys">'.$var_dump.'</font><br>';
            $var_dump = str_replace("%%%%%%%%%%", " ", $var_dump);
            print $var_dump;
        }
    }