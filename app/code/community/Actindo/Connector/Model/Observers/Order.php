<?php
    /**
     * Actindo Faktura/WWS Connector
     * observer listens to store event of orders and sets either netto or brutto
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Observers_Order{
        /**
         * Event Observer Setting ORder
         * @param Varien_Event_Observer $observer
         */
        public function setSetting(Varien_Event_Observer $observer){
            $order = $observer->getEvent()->getOrder();
            $orderid = $order->getId();
            $shippingTax = (int)Actindo_Connector_Model_System_Service::ShippingIncludesTax();
            $tax = (int)Actindo_Connector_Model_System_Service::priceIncludesTax();
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $model = Mage::getModel('connector/actaxordermap');
            $model->setOrderid($orderid);
            $model->setTax($tax);
            $model->setShipping($shippingTax);
            try{
                $model->save();
            }catch(Exception $e){
            }
        }
    }