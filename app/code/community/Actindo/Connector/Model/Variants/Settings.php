<?php
/**
 * Actindo Faktura/WWS Connector
 * Returns Server Settings
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Actindo_Connector_Model_Util_Util
 * @version 2.309
 */

/**
 * Class Actindo_Connector_Model_Variants_Settings
 * Class for Handling Variant Settings
 */
class Actindo_Connector_Model_Variants_Settings{
    /**
     * Apply new Attribute to what types:
     */
    const APPLIEDTO = 'simple,configurable,grouped,virtual,bundle,downloadable, giftcard';
    /**
     * Variants
     * @var array
     */
    protected $variant=array();
    /**
     * default language ident
     * @var string
     */
    protected $defaultLanguage='';
    /**
     * set of languages
     * @var array|null
     */
    protected $languages=array();
    /**
     * Attributes
     * @var array
     */
    protected $attributes = array();
    /**
     * Attribute Codes identified by their entity id
     * @var array
     */
    protected $attributeCodes = array();
    /**
     * Constructor
     * Collect Basic Data
     * @param $variants
     */
    public function __construct($variants){
        //assign Variants
        $this->variant = $variants;
        //get Util Class
        $utilities = new Actindo_Connector_Model_Util_Util();
        //assign available Languages
        $this->languages = $utilities->getLanguages();
        //assign default Language
        $this->defaultLanguage = $utilities->getDefaultLanguage();
        //get Mapping
        $this->mapping = array();
        foreach($this->languages as $key){
            $this->mapping['byCode'][$key['language_code']][] = $key['language_id'];
            $this->mapping['byId'][$key['language_id']] = $key['language_code'];
        }
        //get Default Language Mapping
        $sets = Actindo_Connector_Model_System_Service::getStoreMapping();
        $this->mapping['defaultId'] = $sets['id'];
        $this->mapping['defaultCode'] = $sets['code'];
        //done
    }
    /**
     * runner method
     * Main Method that does the processing
     */
    public function run(){
        $this->updateAttributes();
        $this->updateAssignment();
    }
    /**
     * create Attribute Group
     * @param integer $setid id of Attribute set
     * @return integer id of new group
     */
    protected function createGroup($setid){
        $group = Mage::getModel('eav/entity_attribute_group')
            ->setAttributeGroupName('Actindo')
            ->setAttributeSetId($setid)
            ->setSortOrder(10000)
            ->save();
        return $group->getId();
    }
    /**
     * Method for Updating Property Assignment to each Attribute Set
     */
    protected function updateAssignment(){
        $attributeSetCollection = Mage::getModel('eav/entity_attribute_set') ->getCollection();
        $processed=array();
        $categoryModel = Mage::getModel("catalog/product");
        $defaultSet = $categoryModel->getDefaultAttributeSetId();
        $defaultAttribute = Mage::getModel('eav/entity_attribute_set')->load($defaultSet);
        foreach($attributeSetCollection as $attributeSet){
            $attributeSetId = $attributeSet->getId();
            if($attributeSet->getAttributeSetName()===$defaultAttribute->getAttributeSetName() && $attributeSetId!==$defaultAttribute->getId()){
                continue;
            }
            $groups = Mage::getModel('eav/entity_attribute_group')
                ->getResourceCollection()
                ->setAttributeSetFilter($attributeSetId)
                ->load()->toArray();
            $create = true;
            $group = null;
            //check if group exist
            foreach($groups['items'] as $entry){
                if($entry['attribute_group_name']==='Actindo'){
                    $create = falsE;
                    $groupId = $entry['attribute_group_id'];
                }
            }
            //if attribute group does not exist, create it
            if($create===true){
                $groupId = $this->createGroup($attributeSetId);
            }
            //load attribute items
            $attributes = Mage::getModel('catalog/product_attribute_api')->items($attributeSetId);
            $set = array();
            foreach($attributes as $attribute){
                $set[] = $attribute['code'];
            }
            //get list of attributes
            foreach($this->attributeCodes as $attributeId=>$attributeCode){
                if(!in_array($attributeCode,$set)){
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
                    //check if new attribute exists in list, if not create it
                    if($attribute!==false && $attribute->getId()>0){
                        $attribute->setAttributeSetId($attributeSetId);
                        $attribute->setAttributeGroupId($groupId);
                        $attribute->save();
                    }
                }
            }
        }
    }
    /**
     * Method for update/create Variants
     */
    protected function updateAttributes(){
        $counter=1;
        foreach($this->variant as $variantId=>$variantArray){
            $this->AttributeHandler($variantId,$variantArray);
        }
    }
    protected function AttributeHandler($variantId,$variantArray)
    {
        $keyMap = Mage::getModel('connector/ackeyvaluemap')
            ->getCollection()
            ->addFieldToFilter('type','attrmap')
            ->addFieldToFilter('ident',$variantId)
            ->getFirstItem();
        $attributeCode = 'act_attr_'.$variantId;
        $doUpdate = false;
        if($keyMap->getId()>0){
            $variantObject = Mage::getModel('eav/entity_attribute')->load($keyMap->getValue());
            if($variantObject->getId()>0){
                $doUpdate = true;
            }else{
                $keyMap->delete();
            }
        }
        if($doUpdate){
            $variantObject = Mage::getModel('eav/entity_attribute')->load($keyMap->getValue());
            //do Update
            $this->updateAttribute($variantId,$variantObject);
        }else{
            $likefilter = array();
            foreach($variantArray as $arrayId=>$arrayArray){
                $likefilter[] = $arrayArray['name'];
                $likefilter[] = strtolower($arrayArray['name']);
                $likefilter[] = str_replace(' ','_',strtolower($arrayArray['name']));
            }
            $variantObject = Mage::getModel('eav/entity_attribute')
                ->getCollection()
                ->addFieldToFilter(
                    'attribute_code',
                    array(
                        'in'=>$likefilter
                    )
                )
                ->addFieldToFilter('entity_type_id',Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId())
                ->getFirstItem();
            if($variantObject===false||$variantObject->getId()<1){
                $variantObject = Mage::getModel('eav/entity_attribute')
                    ->getCollection()
                    ->addFieldToFilter(
                        'frontend_label',
                        array(
                            'in'=>$likefilter
                        )
                    )
                    ->addFieldToFilter('entity_type_id',Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId())
                    ->getFirstItem();
            }
            if($variantObject!==false && $variantObject->getId()>0){
                $keyMap = Mage::getModel('connector/ackeyvaluemap');
                $keyMap->setType('attrmap')
                    ->setIdent($variantId)
                    ->setValue($variantObject->getId())
                    ->save();
                $this->updateAttribute($variantId,$variantObject);
            }else{
                $this->createAttribute($variantId,$attributeCode);
            }
        }
    }
    /**
     * Prepares the Variant data (attribute name + attribute options name)
     * @param integer $id id of attribute
     * @return array collection of attribute data
     */
    protected function prepareAttributeUpdate($id){
        if(is_numeric($this->defaultLanguage)){
            $this->defaultLanguage = $this->mapping['byId'][$this->defaultLanguage];
        }
        $defaultId = $this->mapping['defaultCode'][$this->defaultLanguage];
        $defaultCode = $this->mapping['byId'][$defaultId];
        $attribute = array(
            $this->variant[$id][$defaultCode]['name'],
        );
        $options = array();
        foreach($this->variant[$id] as $languageCode=>$translation){
            if(isset($this->mapping['byCode'][$languageCode])){
                foreach($this->mapping['byCode'][$languageCode] as $shop){
                    $attribute[$shop] = $translation['name'];
                    if(count($translation['values'])>0){
                        foreach($translation['values'] as $optionId=>$optionValue){
                            if(!isset($options[$optionId]) || !is_array($options[$optionId])){
                                $options[$optionId] = array(
                                    $optionValue
                                );
                            }
                            $options[$optionId][$shop] = $optionValue;
                        }
                    }
                }
            }
        }
        return array($attribute,$options);
    }

    /**
     * checks if ident is in arrayData field
     * If so, return integer, else boolean false
     * @param mixed $ident ident to check
     * @param array $arrayData to check
     * @return bool|int
     */
    protected function check($ident,$arrayData){
        foreach($arrayData as $arrayId=>$arrayValue){
            if(in_array($ident,$arrayValue)){
                return $arrayId;
            }
        }
        return false;
    }
    /**
     * Update a Attribute
     * @param integer $id ID of attribute
     * @param Mage_Core_Model_Abstract $attribute Attribute Object
     */
    protected function updateAttribute($id,$attribute){
        //get attribute code
        $attributeCode = $attribute->getAttributeCode();
        //reload full object
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attributeCode);
        //get options and labels
        $result = $this->prepareAttributeUpdate($id);
        $attributeLabels = $result[0];
        $optionLabels = $result[1];
        //reset user session
        $session = Mage::getSingleton('adminhtml/session');
        Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
        $session->setAttributeData(false);
        //get attributedata
        $attributeData = $attribute->toArray();
        $attributeData['frontend_label'] = $attributeLabels;
        $attribute->addData($attributeData);
        //update labels
        $attribute->save();
        //set session cache
        $session->setAttributeData($attributeData);
        //check labels
        $optionLabelsCheck = $optionLabels;
        //do only if attribute type is select
        if($attribute->getFrontendInput()==='select'){
            //pre set variables
            $optionSets = array();
            $newOptionSets = array();
            //get all options
            $options = $attribute->getSource()->getAllOptions();
            if(count($options)>0){
                $subOptions = array();
                foreach($options as $valueSet){
                    if(!empty($valueSet['value'])){
                        $subOptions[$valueSet['value']]=true;
                    }
                }
                foreach($options as $optionId=>$optionArray){
                    $keyMap = Mage::getModel('connector/ackeyvaluemap')->getCollection()->addFieldToFilter('type','optionmap')->addFieldToFilter('value',$optionArray['value'])->getFirstItem();
                    if($keyMap===false||$keyMap->getId()<1){
                        $id = $this->check($optionArray['label'],$optionLabels);
                        if($id!==false){
                            $optionSets[$optionArray['value']] = $optionLabels[$id];
                            unset($optionLabelsCheck[$id]);
                            $keyMap = Mage::getModel('connector/ackeyvaluemap')
                                ->getCollection()
                                ->addFieldToFilter('type','optionmap')
                                ->addFieldToFilter('value',$optionArray['value'])
                                ->getFirstItem();
                            if(!($keyMap->getId()>0)){
                                $keyMap->setValue($optionArray['value']);
                            }else{
                                $keyMap->setType('optionmap')
                                    ->setIdent($id)
                                    ->setValue($optionArray['value'])
                                    ->save();
                            }
                        }
                    }else{
                        $ident = $keyMap->getIdent();
                        $valueId = $keyMap->getValue();
                        if(isset($subOptions[$valueId]) && isset($optionLabels[$ident])){
                            $optionSets[$valueId] = $optionLabels[$ident];
                            unset($optionLabelsCheck[$ident]);
                        }
                    }
                }
            }
            //fill none existing sets
            foreach($optionLabelsCheck as $entryId=>$entryArray){
                $newOptionSets[$entryId] = $entryArray;
            }
            //set options
            $attribute->addData(array('option'=>array('value'=>$optionSets)));
            $attribute->save();
            //reload options for next sort order id
            $options = $attribute->getSource()->getAllOptions();
            $counter = count($options);
            $createData = array();
            //create new entries
            foreach($newOptionSets as $valueId=>$valueValue){
                $value = $order = array();
                $value['option'] = $valueValue;
                $order['option'] = $counter;
                $resSet = array(
                    'value'=>$value,
                    'order'=>$order,
                );
                $attribute->setData('option',$resSet);
                $attribute->save();
                $createData[$valueId] = $value['option'][0];
                $counter++;
            }
            unset($checkData,$key,$value);
            $options = $attribute->getSource()->getAllOptions();
            foreach($options as $value){
                if (false !== $key = array_search($value['label'], $createData)) {
                    $keyMap = Mage::getModel('connector/ackeyvaluemap')->getCollection()->addFieldToFilter('type','optionmap')->addFieldToFilter('value',$value['value'])->getFirstItem();
                    if($keyMap->getId()>0){
                        $keyMap->setValue($value['value'])
                            ->save();
                    }else{
                        $keyMap->setType('optionmap')
                            ->setIdent($key)
                            ->setValue($value['value'])
                            ->save();
                    }
                }
            }
        }
        //fill internal data
        $this->attributes[$id] = $attribute->getId();
        $this->attributeCodes[$this->attributes[$id]] = $attributeCode;
    }

    /**
     * create a new Variant
     * @param integer $id Attribute ID
     * @param string $attributeCode Attrbute code (form: act_attr_X)
     * @throws Actindo_Connector_Model_Exception_Error
     */
    protected function createAttribute($id,$attributeCode){
        //get Attrobute label and options
        $result = $this->prepareAttributeUpdate($id);
        $attribute = $result[0];
        $options = $result[1];
        //basic data for attribute
        $data = array(
            'is_global'                     => '1',
            'frontend_input'                => 'select',
            'default_value_text'            => '',
            'default_value_yesno'           => '0',
            'default_value_date'            => '',
            'default_value_textarea'        => '',
            'is_unique'                     => '0',
            'is_required'                   => '0',
            'frontend_class'                => '',
            'is_searchable'                 => '0',
            'is_visible_in_advanced_search' => '0',
            'is_comparable'                 => '0',
            'is_used_for_promo_rules'       => '0',
            'is_html_allowed_on_front'      => '1',
            'is_visible_on_front'           => '0',
            'used_in_product_listing'       => '0',
            'used_for_sort_by'              => '0',
            'is_configurable'               => '1',
            'is_filterable'                 => '0',
            'is_filterable_in_search'       => '0',
            'backend_type'                  => 'varchar',
            'default_value'                 => '',
            'apply_to'                      => self::APPLIEDTO,
            'attribute_code'                => $attributeCode,
            'frontend_label'                => $attribute,
            'value'                         => array()
        );
        //create Model
        $model = Mage::getModel('catalog/resource_eav_attribute');
        //add Data
        $model->addData($data);
        //set entity type id
        $entityTypeID = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $model->setEntityTypeId($entityTypeID);
        //set user defined
        $model->setIsUserDefined(1);
        //save
        try{
            $model->save();
        }catch(Exception $e){
            throw new Actindo_Connector_Model_Exception_Error('An Error appeared during creation. This is the Message: '.$e->getMessage(),700);
        }
        //now assign values to each attribute
        $attributeProperties = array();
        $counter = 0;
        if(count($options)>0){
            foreach($options as $optionArray){
                $value = array();
                $value['option'] = $optionArray;
                $result = array(
                    'value'=>$value,
                    'order'=>$counter
                );
                $model->setData('option',$result);
                $model->save();
                $counter++;
            }
        }
        //assign internal attribute data
        $this->attributes[$id] = $model->getId();
        $this->attributeCodes[$this->attributes[$id]] = $attributeCode;
        //create mapping entry
        $keyMap = Mage::getModel('connector/ackeyvaluemap');
        $keyMap->setType('attrmap')
            ->setIdent($id)
            ->setValue($model->getId())
            ->save();
        //cleanup
        unset($id,$value,$optionSet);
        //reload options for next sort order id
        $model = Mage::getModel('catalog/resource_eav_attribute')->load($model->getId());
        $magentoOptions = $model->getSource()->getAllOptions();
        //resort Actindo Options
        foreach($magentoOptions as $optionSet){
            foreach($options as $id=>$value){
                foreach($value as $label){
                    if($optionSet['label']===$label){
                        $keyMap = MAge::getModel('connector/ackeyvaluemap')->load($id);
                        if($keyMap->getId()>0){
                            $keyMap->delete();
                        }
                        $keyMap = MAge::getModel('connector/ackeyvaluemap');
                        $keyMap->setType('optionmap')
                            ->setIdent($id)
                            ->setValue($optionSet['value'])
                            ->save();
                        break(2);
                    }
                }
            }
        }
    }
}