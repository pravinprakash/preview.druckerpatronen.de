<?php
    /**
     * Actindo Faktura/WWS Connector
     * Variant Attribute Mapper
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Actindo_Connector_Model_Service_Product_Export
     * @version 2.309
     * @abstract
     */
    class Actindo_Connector_Model_Variants_AttributeMap{
        /**
         * Mapper
         * @var array
         */
        public  $mapping = array();
        /**
         * Creates the object and processes the data in the actindo array
         * @param array $actindo
         */
        public function __construct($actindo){
            //preperation for Finale Children Import
            $attributeNames = &$actindo['shop']['attributes']['names'];
            $attributeValues = &$actindo['shop']['attributes']['values'];
            $attributes = array(
                'mapping'=>array()
            );
            //Run through data
            if(count($attributeNames)>0){
                foreach($attributeNames as $attributeKey=>$attributeValue){
                    $attributes[$attributeKey] = array(
                        'id'=>$attributeKey,
                        'translations'=>array(),
                        'values'=>array()
                    );
                    foreach($attributeValue as $valueKey=>$value){
                        $attributes[$attributeKey]['translations'][] = $value;
                    }
                }
                unset($attributeKey,$attributeValue,$valueKey,$value);
                foreach($attributeValues as $attributeKey=>$attributeValue){
                    foreach($attributeValue as $valueKey=>$value){
                        $attributes[$attributeKey]['values'][$valueKey] = array();
                        foreach($value as $k=>$v){
                            $attributes[$attributeKey]['values'][$valueKey][] = $value;
                        }
                        $attributes['mapping'][$valueKey]=$attributeKey;
                    }
                }
                $this->mapping = $attributes;
            }else{
                throw new Actindo_Connector_Model_Exception_Error('Artikel for Upload has no Attributes defined!',720);
            }
        }
        /**
         * Get Complete Information Set about an Attribute
         * @param mixed $ident Identification of Attribzte
         * @return mixed
         * @throws Actindo_Connector_Model_Exception_Error
         */
        public function getAttributeData($ident){
            if(!isset($this->attributeIds[$ident])){
                foreach($this->mapping[$ident]['translations'] as $translation){
                    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$translation);
                    if($attribute->getId()>1){
                        break;
                    }else{
                        $attribute = Mage::getModel('eav/entity_attribute')
                            ->getCollection()
                            ->addFieldToFilter('frontend_label',$translation)
                            ->getFirstItem();
                        if($attribute->getId()>1){
                            break;
                        }
                    }
                }
                if($attribute->getId()<1){
                    throw new Actindo_Connector_Model_Exception_Error('Attribute '.$ident.' not found!',721);
                }
                $storeid = Mage::app()->getStore()->getStoreId();
                $options = Mage::getResourceModel('eav/entity_attribute_option_collection');
                $values = $options
                    ->setAttributeFilter($attribute->getId())
                    ->setStoreFilter($storeid)
                    ->toOptionArray();
                $attr = $attribute->toArray();
                $this->attributeIds[$ident] = array(
                    'id'=>$attr['attribute_id'],
                    'label'=>$attr['frontend_label'],
                    'code'=>$attr['attribute_code'],
                    'flabel'=>$attr['frontend_label'],
                    'values'=>$values,
                );
            }
            return $this->attributeIds[$ident];
        }
        /**
         * Array for Buffering results to increase Performance
         * Option to Value Mapping
         * @var array
         */
        protected $optionmapping = array();
        /**
         * Returns the Id of an given option and value
         * @param mixed $ident
         * @param mixed $option
         * @return bool/mixed ID of the option or false if not found!
         */
        public function getOptionId($ident,$option){
            if(isset($this->optionmapping[$ident]) && isset($this->optionmapping[$ident][$option])){
                return $this->optionmapping[$ident][$option];
            }else{
                $optionData = $this->getAttributeData($ident);
                $optionsArray = $optionData['values'];
                foreach($optionsArray as $key=>$optionArray){
                    if($optionArray['label'] === $option){
                        if(!isset($this->optionmapping[$ident])){
                            $this->optionmapping[$ident] = array();
                        }
                        $this->optionmapping[$ident][$option] = $optionArray['value'];
                        return $optionArray['value'];
                    }
                }
            }
            return false;
        }
    }