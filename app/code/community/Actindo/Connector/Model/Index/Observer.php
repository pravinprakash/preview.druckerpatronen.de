<?php
    /**
     * Actindo Faktura/WWS Connector
     * CronJob
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @uses Mage_Core_Controller_Front_Action
     * @version 2.309
     */
    class Actindo_Connector_Model_Index_Observer{
        /**
         * indexes to be rebuilt
         * @var array
         */
        protected $reindex = array(
            1,  //Articles Attribute
            2,  //Articles Price
            3,  //catalog URL Rewrites
            4,  //Articles Flat Files
            6,  //Articles Kategory
            7,  //katalog search index
            9,  //Schlagwort Gruppierung
        );
        /**
         * Time for Checking if Index has to be rebuilt or not
         * @default 300s = 5Minutes
         */
        const CHECKTIME = 300;
        /**
         * get the file path to filecheck
         * @return string
         * @private
         */
        private function getTestFile(){
            $path = Mage::getBaseDir('var').'/tmp/connectorAtestFile';
            return $path;
        }
        /**
         * Rebuild Index and Caches
         * @public
         * @return void
         */
        public function rebuildIndex(){
            //check if file exists
            if(file_exists($this->getTestFile())){
                //get atime
                $atime = (int)file_get_contents($this->getTestFile());
                //check if atime is bigger then 0 and older then self::CHECKTIME seconds
                if($atime>0 && $atime<(time()-self::CHECKTIME)){
                    //unlink the file
                    @unlink($this->getTestFile());
                    //start indexer run if atleast one reindex is set
                    if(count($this->reindex)>0){
                        foreach($this->reindex as $index){
                            //do rebuild
                            Mage::getModel('index/process')->load($index)->reindexAll();
                        }
                    }
                    //clean Cache
                    Mage::app()->cleanCache();
                }
            }
        }
        /**
         * sets the Index File
         * @public
         * @return void
         */
        public function setIndexFile(){
            @unlink($this->getTestFile());
            file_put_contents($this->getTestFile(),time());
        }
    }