<?php
    /**
     * Actindo Faktura/WWS Connector
     * model to access the mapper table
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Actaxordermap extends Mage_Core_Model_Abstract{
        /**
         * constructor
         * Initializing the model
         */
        public function _construct(){
            parent::_construct();
            $this->_init('connector/actaxordermap');
        }
    }