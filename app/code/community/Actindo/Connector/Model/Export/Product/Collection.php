<?php
    /**
     * Actindo Faktura/WWS Connector
     * product collection export class to get only parent products
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Export_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection{
        /**
         * handle the after load events
         * @return Mage_Catalog_Model_Resource_Product_Collection|void
         */
        public function _afterLoad(){
            $this->filterForParentObjects();
            parent::_afterLoad();
        }
        /**
         * filter used to filter out only variant products
         */
        public function filterForParentObjects(){
            $childProductId = array();
            foreach($this as $product){
                if($product->getTypeId()=='configurable'){
                    $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
                    $product->setChildren($childProducts);
                    foreach($childProducts as $childProduct){
                        $childProductId[] = $childProduct->getId();
                        $this->removeItemByKey($childProduct->getId());
                    }
                }
            }
        }
    }