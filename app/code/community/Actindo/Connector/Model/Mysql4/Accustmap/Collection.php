<?php
    /**
     * Actindo Faktura/WWS Connector
     * data model access classes for collections
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Mysql4_Accustmap_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract{
        /**
         * Constructor
         */
        public function _construct(){
            $this->_init('connector/accustmap');
        }
    }