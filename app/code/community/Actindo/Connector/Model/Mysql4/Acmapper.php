<?php
    /**
     * Actindo Faktura/WWS Connector
     * data access model
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_Mysql4_Acmapper extends Mage_Core_Model_Mysql4_Abstract{
        /**
         * Constructor
         */
        protected function _construct(){
            $this->_init('connector/acmapper','id');
        }
    }