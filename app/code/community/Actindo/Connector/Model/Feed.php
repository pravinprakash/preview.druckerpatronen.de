<?php

    class Actindo_Connector_Model_Feed extends  Mage_AdminNotification_Model_Feed{
        const XML_FEEDS_PATH = 'actindo/rss_feeds/connector';
        public function checkUpdate(){
            $feeds = $this->getAllFeeds();
            if (!is_array($feeds)) {
                Mage::log('Actindo Connector: No feeds found.');
                return $this;
            }
            #if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            #    return $this;
            #}
            /* @var $inbox Mage_AdminNotification_Model_Inbox */
            $inbox = Mage::getModel('adminnotification/inbox');
            foreach ($feeds as $feed) {
                // IMPORTANT
                // in getFeedData $this->getFeedUrl() is called,
                // which returns $this->_feedUrl
                // to not overwrite this method, I set this attribute.
                $this->_feedUrl = $feed;
                $feedData = array();
                $feedXml = $this->getFeedData($feed);
                if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
                    foreach ($feedXml->channel->item as $item) {
                        $feedData[] = array(
                            'severity'    => (int)isset($item->severity) ? $item->severity : Mage_AdminNotification_Model_Inbox::SEVERITY_NOTICE,
                            'date_added'  => $this->getDate((string)$item->pubDate),
                            'title'       => (string)$item->title,
                            'description' => (string)$item->description,
                            'url'         => ( $item->link ? (string)$item->link : null ),
                        );
                    }
                    if ($feedData) {
                        $inbox->parse(array_reverse($feedData));
                    }
                }
                $this->setLastUpdate($feed);
            }
            return $this;
        }
        /**
         * @return array
         */
        protected function getAllFeeds(){
            $feeds = Mage::getStoreConfig(self::XML_FEEDS_PATH);
            Mage::log('data');
            Mage::log($feeds);
            return $feeds;
        }
        /**
         * Retrieve Last update time
         *
         * @return int
         */
        public function getLastUpdate(){
            return Mage::app()->loadCache('actindo_connector_admin_notifications');
        }
        /**
         * Set last update time (now)
         *
         * @return Mage_AdminNotification_Model_Feed
         */
        public function setLastUpdate(){
            Mage::app()->saveCache(time(), 'actindo_connector_admin_notifications');
            return $this;
        }
    }