<?php
    /**
     * Actindo Faktura/WWS Connector
     * Class for Managing Active Features
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     * @abstract
     */
    abstract class Actindo_Connector_Model_System_Service{
        const XMLPATHLANG = 'actindo/storemapping/sites';
        /**
         * checks if class is already loaded
         * @var bool
         * @static
         */
        protected static $init=false;
        /**
         * Services enabled/disabled
         * @var array
         * @static
         */
        protected static $services = array();
        /**
         * Required Fields Map
         * @var array
         * @static
         */
        protected static $requiredFields = array();
        /**
         * Initializes the class data
         * @static
         */
        protected static function doInit(){
            if(!self::$init){
                $storeid = Mage::app()->getStore()->getStoreId();
                $features = Mage::getStoreConfig('actindo/features',$storeid);
                foreach($features as $key=>$value){
                    self::$services[$key] = (bool)$value;
                }
                $required = Mage::getStoreConfig('actindo/requiredFields',$storeid);
                foreach($required as $key=>$value){
                    self::$requiredFields[$key] = (bool)$value;
                }
                self::$init = true;
            }
        }
        /**
         * Check if Method is active or not
         * @param string $method Name of $valuethe object
         * @return bool
         * @static
         */
        public static function getMethodActive($method){
            self::doInit();
            return (isset(self::$services[$method]) && self::$services[$method]===true)?true:false;
        }
        /**
         * Check if Method is active or not
         * @param string $method Name of $valuethe object
         * @return bool
         * @static
         */
        public static function getRequiredField($method){
            self::doInit();
            return (isset(self::$requiredFields[$method]) && self::$requiredFields[$method]===true)?true:false;
        }
        /**
         * Method for reindexing data
         * @depricated
         * @param $product
         * @static
         */
        public static function reindexData($product){
            Mage::getSingleton('core/cache')->flush();
            Mage::getModel('catalog/product_flat_indexer')->updateProduct($product['art_nr'], null);
        }
        /**
         * determins if the prices contain brutto or not
         * @return bool
         * @static
         */
        public static function priceIncludesTax(){
            $websiteid = self::getStoreID();
            $value = Mage::getStoreConfig('tax/calculation/price_includes_tax',$websiteid);
            if((int)$value>0){
                return true;
            }else{
                return false;
            }
        }
        /**
         * This method determens if the shipping is a brutto or netto value
         * @return bool
         * @static
         */
        public static function ShippingIncludesTax(){
            $websiteid = Mage::app()->getStore()->getWebsiteId();
            $value = Mage::getStoreConfig('tax/calculation/shipping_includes_tax',$websiteid);
            if((int)$value>0){
                return true;
            }else{
                return false;
            }
        }
        /**
         * gets the current Website Id
         * @return integer
         * @static
         */
        public static function getCurrentWebsiteId(){
            return Mage::app()->getWebsite()->getId();
        }
        /**
         * store id of current request
         * @var
         * @static
         */
        protected static $siteId;
        /**
         * sets the current store id
         * @param $id
         * @static
         */
        public static function setStoreId($id){
            self::$siteId = $id;
        }
        /**
         * gets the current store id
         * @return mixed
         * @static
         */
        public static function getStoreID(){
            return self::$siteId;
        }
        /**
         * array of existing child id's
         * @var array
         * @static
         */
        protected static $childIds = array();
        /**
         * adds a child id to the storage
         * @used Actindo_Connector_Model_Service_Product_Import_Configurable::doUpdate
         * @param $id
         * @static
         */
        public static function addChildId($id){
            self::$childIds[] = $id;
        }
        /**
         * retrieves an child id from the child id storage, if no more exists return false
         * @used Actindo_Connector_Model_Service_Product_Import_Child::process
         * @return bool|mixed
         * @static
         */
        public static function getChildId(){
            if(count(self::$childIds)>0){
                return array_pop(self::$childIds);
            }else{
                return false;
            }
        }
        /**
         * Store to Language Mapping
         * @var array
         * @static
         */
        protected static $storeMapping;
        /**
         * returns the store to language mapp
         * structure
         *      |-id (id to iso code mapping)
         *      |-code (iso code to id mapping)
         * @return array
         * @static
         */
        public static function getStoreMapping(){
            if(self::$storeMapping === null){
                self::$storeMapping = array(
                    'id'=>array(),
                    'code'=>array()
                );
                //
                $XML_PATH = 'actindo/storemapping/language_';
                $config = Mage::getConfig();
                $store = Mage::app()->getStore(self::getStoreID());
                $website = Mage::app()->getWebsite($store->getWebsiteId());
                $result = array();
                if($website->getId()>0){
                    foreach($website->getGroups() as $group){
                        $stores = $group->getStores();
                        foreach($stores as $store){
                            $languageCode = Mage::getStoreConfig($XML_PATH.$store->getCode());
                            $languageCode = strtolower($languageCode);
                            if($languageCode !== 'default'){
                                self::$storeMapping['id'][$store->getStoreId()] = $languageCode;
                                self::$storeMapping['code'][$languageCode] = $store->getStoreId();
                            }
                        }
                    }
                }
            }
            return self::$storeMapping;
        }
        /**
         * is set to true if the shipping observer is running or not
         * @var null
         * @static
         */
        protected static $shippingObserverEnabled=null;
        /**
         * checks if the shipping observer is running or not
         * @return boolean
         * @static
         * @public
         */
        public static function isShippingObserverEnabled(){
            if(self::$shippingObserverEnabled===null){
                $shippingField = Mage::getStoreConfig('actindo/mappingfields/shippingstatus',self::getStoreID());
                $attributeModel = Mage::getResourceModel('eav/entity_attribute_collection')
                                        ->setCodeFilter($shippingField)
                                        ->getFirstItem();
                if($attributeModel->getFrontendInput()==='select'){
                    self::$shippingObserverEnabled = false;
                }else{
                    self::$shippingObserverEnabled = true;
                }
            }
            return self::$shippingObserverEnabled;
        }
    }