<?php


    class Actindo_Connector_Model_System_Config_Source_Site extends Mage_Adminhtml_Block_System_Config_Form_Field{
        public function toOptionArray(){
            $returnSet = array();
            $websiteId = $this->getRequest()->getParam('website');
            $groups = Mage::app()->getWebsite($websiteId)->getGroups();
            foreach($groups as $group){
                $views = $group->getStores();
                $currentSite = array(
                    'label'=>$group->getName(),
                    'value'=>array(),
                );
                foreach($views as $view){
                    $storeView = $view->getData();
                    $storeId = $storeView['store_id'];
                    $storeName = $storeView['name'];
                    $storeLanguage = substr(Mage::getStoreConfig('general/locale/code',$storeId),0,2);
                    $entry = array(
                        'value'=>$storeId,
                        'label'=>$storeName.' ('.$storeLanguage.')',
                    );
                    $currentSite['value'][] = $entry;
                }
                $returnSet[] = $currentSite;
            }
            return $returnSet;
        }
    }