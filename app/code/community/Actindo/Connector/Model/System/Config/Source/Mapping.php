<?php
    /**
     * Actindo Faktura/WWS Connector
     * class for mapping attributes to actindo attributes
     * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
     * @author Daniel Haimerl
     * @author Patrick Prasse
     * @author Christopher Westerfield
     * @version 2.309
     */
    class Actindo_Connector_Model_System_Config_Source_Mapping{
        /**
         * array of attributes to ignore
         * @var array
         */
        private $_ignoreAttributes = array(
            'sku',
            'category_ids',
            'has_options',
            'created_at',
            'cost',
            'custom_design',
            'custom_design_from',
            'custom_design_to',
            'custom_layout_update',
            'description',
            'gallery',
            'gift_message_available',
            'group_price',
            'image',
            'image_label',
            'is_recurring',
            'links_exist',
            'links_purchased_separately',
            'links_title',
            'media_gallery',
            'meta_autogenerate',
            'msrp_display_actual_price_type',
            'msrp_enabled',
            'old_id',
            'options_container',
            'page_layout',
            'price_type',
            'price_view',
            'recurring_profile',
            'required_options',
            'shipment_type',
            'sku_type',
            'small_image',
            'small_image_label',
            'thumbnail',
            'thumbnail_label',
            'updated_at',
            'url_key',
            'weight_type',
            'tier_price',
            'price',
            'image',
            'small_image',
            'thumbnail',
            'gallery',
            'meta_title',
            'meta_keyword',
            'meta_description',
            'name',
            'description',
            'short_description',
            'status',
            'tax_class_id',
            'visibility',
            'samples_title'
        );
        protected $requiredfields = array(
            'weight',
            'manufacturer',
            'delivery_time',
            'msrp',
        );
        /**
         * get all attributes as array that aren't special (in the list above)
         * @return array
         */
        public function toOptionArray(){
            $optionsArray = array();
            $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection');
            $optionsArray[] = array('value'=>-1,'label'=>'Nicht gemapped');
            foreach ($productAttributes as $productAttribute) {
                /** $productAttr Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!in_array($productAttribute->getAttributeCode(), $this->_ignoreAttributes) && strlen($productAttribute->getFrontendLabel()) > 0 && ((int)$productAttribute->getData('is_configurable')===0 || in_array($productAttribute->getAttributeCode(),$this->requiredfields))){
                    $optionsArray[] = array(
                        'value' => $productAttribute->getAttributeCode(),
                        'label' => $productAttribute->getFrontendLabel()
                    );
                }
            }
            return $optionsArray;
        }
        /**
         * method to get the ignored attributes list
         * @return array
         */
        public function getRestrictionList(){
            return $this->_ignoreAttributes;
        }
        /**
         * list of attributes
         * @var
         */
        protected $fieldlist;
        /**
         * list of attributes values
         * @var
         */
        protected $fieldlistvalues;
        public function getFieldList($site){
            $this->fieldlist = array();
            $this->fieldlistvalues = array();
            $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection');
            $filter = $this->_ignoreAttributes;
            $mappedlist = Mage::getStoreConfig('actindo/mappingfields',$site);
            $subfilter = array();
            if(count($mappedlist)>0){
                foreach($mappedlist as $value){
                    if((int)$value!==-1){
                        $subfilter[]=$value;
                    }
                }
            }
            $subfilter = array_values(array_filter(array_unique($subfilter)));
            $filter = array_merge($filter,$subfilter);
            $optionsArray = array();
            foreach ($productAttributes as $productAttribute) {
                /** $productAttr Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!in_array($productAttribute->getAttributeCode(), $filter) && strlen($productAttribute->getFrontendLabel()) > 0){
                    $optionsArray[] = $productAttribute->getAttributeCode();
                }
            }
            return $optionsArray;
        }
        /**
         * get the attributes for the connector
         * @param int $site store id / site id
         * @return array
         */
        public function getConnectorFieldList($site){
            if($this->fieldlist===null){
                $this->fieldlist = array();
                $this->fieldlistvalues = array();
                $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection');
                $filter = $this->_ignoreAttributes;
                $mappedlist = Mage::getStoreConfig('actindo/mappingfields',$site);
                $subfilter = array();
                foreach($mappedlist as $value){
                    if((int)$value!==-1){
                        $subfilter[]=$value;
                    }
                }
                $subfilter = array_values(array_filter(array_unique($subfilter)));
                $filter = array_merge($filter,$subfilter);
                $optionsArray = array();
                foreach ($productAttributes as $productAttribute) {
                    /** $productAttr Mage_Catalog_Model_Resource_Eav_Attribute */
                    if(!in_array($productAttribute->getAttributeCode(), $filter) && strlen($productAttribute->getFrontendLabel()) > 0){
                        $ident = count($optionsArray);
                        if($productAttribute->getIsVisible() || $productAttribute->getIsRequired()){
                            $optionsArray[$ident] = array(
                                'value' => $productAttribute->getAttributeCode(),
                                'label' => $productAttribute->getFrontendLabel(),
                                'type'  => $productAttribute->getFrontendInput(),
                                'required'=>$productAttribute->getIsRequired(),
                                'valueset'=>false,
                                'global'=>$productAttribute->getIsGlobal(),
                            );
                            if($productAttribute->getFrontendInput() === 'select'  || $productAttribute->getFrontendInput() === 'multiselect'){
                                $entityType = 'catalog_product';
                                $attrCode = $productAttribute->getAttributeCode();
                                $attribute = Mage::getModel('eav/config')->getAttribute($entityType,$attrCode);
                                $attributeValueSets = $attribute->getSource()->getAllOptions();
                                $fieldValues = array(
                                    array(
                                        'value'=>'',
                                        'text'=>'leer'
                                    )
                                );
                                if(count($attributeValueSets)>0){
                                    foreach($attributeValueSets as $valueSets){
                                        $key = $valueSets['value'];
                                        $value = $valueSets['label'];
                                        if(!empty($key) && !empty($value)){
                                            $fieldValues[] = array(
                                                'value'=> new Zend_XmlRpc_Value_Base64($key),
                                                'text' => new Zend_XmlRpc_Value_Base64($value),
                                            );
                                        }
                                    }
                                }
                                $optionsArray[$ident]['valueset'] = $fieldValues;
                            }
                        }
                    }
                }
            }
            $this->fieldlist = $optionsArray;
            return $this->fieldlist;
        }
        /**
         * get's attribute Value list
         * @param int $site store id / site id
         * @return mixed
         */
        public function getConnectorValueList($site){
            if($this->fieldlistvalues === null)
                $this->getConnectorFieldList($site);
            return $this->fieldlist;
        }
    }
