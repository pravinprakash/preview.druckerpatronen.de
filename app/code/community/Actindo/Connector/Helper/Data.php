<?php
/**
 * Actindo Faktura/WWS Connector
 * Helper class to handle certain tasks
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @version 2.309
 */

class Actindo_Connector_Helper_Data extends Mage_Core_Helper_Abstract{
    /**
     * Mapping Path in config file
     */
    const XML_PATH_MAPPED = 'actindo/mappingfields';
    /**
     * fields
     * @var null
     */
    protected $fields=null;
    /**
     * lables
     * @var
     */
    protected $labels;
    /**
     * types
     * @var
     */
    protected $types;
    /**
     * id's
     * @var
     */
    protected $ids;
    /**
     * get Mapped Field Name
     * @param $name
     * @return bool
     */
    public function getMappedFieldName($name){
        if($this->fields === null){
            $this->init();
        }
        return (isset($this->fields[$name]) && ($this->fields[$name]!==-1)?$this->fields[$name]:false);
    }
    /**
     * Init Functions
     */
    protected function init(){
        $this->fields = Mage::getStoreConfig(self::XML_PATH_MAPPED,Mage::app()->getWebsite()->getId());
        $this->labels = array();
        $this->types = array();
        if(count($this->fields)>0){
            foreach($this->fields as $key){
                if($key!=="-1"){
                    $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$key);
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
                    $attributeOptions = $attribute ->getSource()->getAllOptions();
                    $frontendtype = $attribute->getData('frontend_input');
                    $this->types[$key] = $frontendtype;
                    $this->ids[$key] = $attributeId;
                    if($this->types[$key]==='select'){
                        foreach($attributeOptions as $k){
                            $this->labels[$key][$k['value']] = $k['label'];
                        }
                    }
                }
            }
        }
    }
    public function getDefaultAttributeSetId(){
        if (!isset($this->defaultAttributeSetId)){
            $categoryModel = Mage::getModel("catalog/product");
            $this->defaultAttributeSetId = $categoryModel->getDefaultAttributeSetId();
        }
        return $this->defaultAttributeSetId;
    }
    /**
     * get All Mapped Fields
     * @return null
     */
    public function getAllMappedFields(){
        return $this->fields;
    }
    /**
     * gets an corresponding field by it's id
     * @param $field
     *
     * @return bool
     */
    public function getIdByField($field){
        $field = $this->getMappedFieldName($field);
        if(!isset($this->types[$field])) return false;
        return $this->ids[$field];
    }
    /**
     * get field Type
     * @param $field
     * @return mixed
     */
    public function getFieldType($field){
        $field = $this->getMappedFieldName($field);
        if(!isset($this->types[$field])) return false;
        return $this->types[$field];
    }
    /**
     * get Select Map Value
     * @param $field
     * @param $value
     * @return mixed
     */
    public function getSelectValue($field,$value){
        $field = $this->getMappedFieldName($field);
        if(!isset($this->labels[$field][$value])) return false;
        return $this->labels[$field][$value];
    }
    /**
     * Builds Query Data Array
     * @param $filter filters given by action
     * @param $columnMap internal column map
     * @return array search array containing fields limit, offset order array and where array
     * @throws Actindo_Connector_Model_Exception_Error
     */
    public function buildSearchQuery($filters,$columnMap,$listmap=null){
        $query = array(
            'limit'=>0,
            'offset'=>0,
            'order'=>array(
                'field'=>'id',
                'direction'=>'ASC'
            ),
            'where'=>array()
        );
        if(isset($filters[0]['field'])) {
            $whereFilters = $filters;
        }else{
            $whereFilters = $filters['filter'];

            if(!empty($filters['sortColName'])) {
                if(isset($columnMap[$filters['sortColName']])) {
                    $query['order'] = array(
                        'field'=>$columnMap[$filters['sortColName']],
                        'direction'=> ((!empty($filters['sortOrder']))?$filters['sortOrder']:'ASC')
                    );
                }
            }
            if(!empty($filters['start'])) {
                $query['offset'] = (int) $filters['start'];
            }
            if(!empty($filters['limit'])) {
                $query['limit'] = (int) $filters['limit'];
            }
        }
        if(is_array($whereFilters)) {
            foreach($whereFilters AS $filter) {
                if(!isset($columnMap[$filter['field']])) {
                    throw new Actindo_Connector_Model_Exception_Error('Unknown filter field found while building query: ' . $filter['field'],901);
                }else{
                    if($filter['data']['type'] == 'list') {
                        $valueList = array();
                        $list = explode(',',$filter['data']['value']);
                        if(!isset($listmap[$filter['field']])){
                            $valueList = $filter['data']['value'];
                        }else{
                            foreach($list as $key){
                                $valueList[] = $listmap[$filter['field']][$key];
                            }
                        }
                        $query['where'][] = array(
                            'field'=>$columnMap[$filter['field']],
                            'value'=>$valueList
                        );
                        //Filter currently not yet supported
                    }else{
                        $query['where'][] = array(
                            'field'=>$columnMap[$filter['field']],
                            'value'=>$filter['data']['value'],
                        );
                    }
                }
            }
        }
        return $query;
    }
    /**
     * tax groups
     * @var array
     */
    protected $taxgroups = null;
    /**
     * returns available tax groups
     * @param $groups
     * @return array
     */
    public function getTaxGroup($group){
        if($this->taxgroups===null){
            $custerGroups = Mage::getModel('customer/group')->getCollection()->getItems();
            foreach($custerGroups as $customerGroup){
                $this->taxgroups[$customerGroup->getId()] = $customerGroup->getTaxClassId();
            }
        }
        return (isset($this->taxgroups[(int)$group]))?$this->taxgroups[(int)$group]:false;
    }

    /**
     * get tax rate of product based on customer group
     * @param $product
     * @param $customerGroupTaxId
     *
     * @return int
     */
    public function getTaxRate($product,$customerGroup){
        $customerGroupTaxId = $this->getTaxGroup($customerGroup);
        $taxHelper = Mage::getSingleton('tax/calculation');
        $taxRequest = $taxHelper->getRateOriginRequest();
        $taxRequest->setProductClassId($product->getTaxClassId());
        $taxRequest->setCustomerClassId($customerGroupTaxId);
        $taxHelper = Mage::getSingleton('tax/calculation');
        $taxRate = $taxHelper->getRate($taxRequest);
        return (int)$taxRate;
    }
    /**
     * map of all status
     * @var array
     */
    protected $statusmap=array();

    /**
     * returns the order status id
     * @param $status
     * @return bool|int order id of order status
     */
    public function getOrderStatusId($status){
        if(!isset($this->statusmap[$status])){
            $mapperEntry = Mage::getModel('connector/ackeyvaluemap')
                ->getCollection()
                ->addFieldToFilter('type','orderstatus')
                ->addFieldToFilter('ident',$status)
                ->getFirstItem();
            $this->statusmap[$status] = $mapperEntry->getId();
        }
        if(isset($this->statusmap[$status])){
            return $this->statusmap[$status];
        }else{
            return false;
        }
    }
}
