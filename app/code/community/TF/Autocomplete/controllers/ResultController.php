<?php
class TF_Autocomplete_ResultController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
	{
		$limit = Mage::getStoreConfig('catalog/autocomplete/limit');
		$show_manufacturer = Mage::getStoreConfig('catalog/autocomplete/manufacturer');
		$show_price = Mage::getStoreConfig('catalog/autocomplete/price');
		
        $query = Mage::helper('catalogSearch')->getQuery();
        $query->setStoreId(Mage::app()->getStore()->getId());
        if ($query->getQueryText()) {
            $query->prepare();
			
			$collection = Mage::getSingleton('catalogsearch/layer')->getProductCollection()
			->addAttributeToFilter('level', 3)
      #->addAttributeToSelect('manufacturer')->addAttributeToSelect('image_ext_url')
      ;
			if($limit>0){
				#$collection->getSelect()->limit($limit); # limit abgestellt, wird unten abgefragt
			}
			#echo "<pre>"; print_r($collection); echo "</pre>";
			/*$i = 0;
			foreach($collection as $p) {
        if($i == 0) {
          $cat_ids = $p->getCategoryIds();
        } else {
          $cat_ids = array_intersect($cat_ids, $p->getCategoryIds());
        }
        $i++;
			}*/
			
			#$cats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($cat_ids); <- zu langsam
			
			$i = 0;
			foreach($collection as $cat) {
			
			  if($i == $limit) break;
			
				$products = Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($cat)->addAttributeToFilter('status', 1);
        $product_id = $products->getAllIds(1);
        if(! isset($product_id[0])) continue;
			
        $cat_id = $cat->getId();
        $cat = Mage::getModel('catalog/category')->load($cat_id);
        $i++;
        if($i % 2 == 0) {
            $class = "ac_even";
        } else {
            $class = "ac_odd";
        }
        #$cat = Mage::getModel('catalog/category')->load($cat_id);
        $parent_name = $cat->getPrinterManufacturer();
			  $cat_name = $cat->getName();
			  $url = $cat->getUrl()."/";
			  
			  echo '<div class="ac_product_row">'.$parent_name.' '.$cat_name.'</div>';
			  echo '|'.$parent_name.' '.$cat_name;
			  echo "|".$url;
				echo "\n ";
      }
      
			/*foreach($cat_ids as $cat_id) {
        $cat = Mage::getModel('catalog/category')->load($cat_id);
        $parent_id = $cat->getParentId();
        $parent = Mage::getModel('catalog/category')->load($parent_id);
        $parent_name = $parent->getName();
			  $cat_name = $cat->getName();
			  $cat_url = $cat->getUrl();
			  echo '<div class="ac_product_row">'.$parent_name.' '.$cat_name.'</div>';
			  echo '|'.$parent_name.' '.$cat_name;
			  echo "|".$cat_url;
				echo "\n ";
      }*/
			
			#foreach($collection as $p) {
				# drop down list html
				#echo '<div class="ac_product_row">'.count($cat_ids);
				
				#$img = Mage::helper('catalog/image')->init($p, 'small_image')->resize(50, 50);
        #$exturl = $p->getImageExtUrl();
	      #if(strpos($img, "/placeholder/") && $exturl) $img = $exturl;
	      
				/*
        echo '<img src="'.$exturl.'" width="50" height="50" class="ac_product_image"/>';
				echo '<div class="ac_product_title">'.$p->getName().'</div>';
				if($show_manufacturer){
					echo '<div class="ac_product_manufacturer">'.$p->getManufacturer().'</div>';
				}
				*/
				
				/*
        echo '<div class="ac_product_price">'.count($cat_ids);
        foreach($cat_ids as $id) {
            echo $id.";";
        }
        echo '</div>';
        */
        
				/*
        if($show_price){
					$price = Mage::helper('core')->currency($p->getPrice());
					echo '<div class="ac_product_price">'.$price.'</div>';
				}
        */

				#echo '</div>';
				
				# display the product name when selected
				#echo '|'.$p->getName();
				
				/*begin
        if(isset($cat_ids[0])) {
            $cat_url = Mage::getModel('catalog/category')->load($cat_ids[0])->getUrl();
            $url = $cat_url."/".$p->getUrlKey().".html";
        } else {
            $url = $p->getProductUrl();
        }
        end*/
				
				# product url to redirct to
				#echo "|".$url;
				#echo "\n ";
			#}
        }
        else {
            $this->_redirectReferer();
        }
    }
}