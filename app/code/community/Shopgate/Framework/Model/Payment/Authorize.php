<?php
/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */

/**
 * native implementation of authorize.net payment
 *
 * @package     Shopgate_Framework_Model_Payment_Authorize
 * @author      Peter Liebig <p.liebig@me.com, peter.liebig@magcorp.de>
 */
class Shopgate_Framework_Model_Payment_Authorize extends Shopgate_Framework_Model_Payment_Abstract
{
    /**
     * const for transaction types of shopgate
     */
    const SHOPGATE_PAYMENT_STATUS_AUTH_ONLY    = 'auth_only';
    const SHOPGATE_PAYMENT_STATUS_AUTH_CAPTURE = 'auth_capture';

    /**
     * @param $order            Mage_Sales_Model_Order
     * @param $shopgateOrder    ShopgateOrder
     *
     * @return Mage_Sales_Model_Order
     */
    public function manipulateOrderWithPaymentData($order, $shopgateOrder)
    {
        $paymentInfos     = $shopgateOrder->getPaymentInfos();
        $paymentAuthorize = Mage::getModel('paygate/authorizenet');
        $order->getPayment()->setMethod($paymentAuthorize->getCode());
        $paymentAuthorize->setInfoInstance($order->getPayment());
        $order->getPayment()->setMethodInstance($paymentAuthorize);
        $order->save();

        $lastFour = substr($paymentInfos['credit_card']['masked_number'], -4);
        $order->getPayment()->setCcTransId($paymentInfos['transaction_id']);
        $order->getPayment()->setCcApproval($paymentInfos['authorization_number']);
        $order->getPayment()->setLastTransId($paymentInfos['reference_number']);
        $cardStorage = $order->getPayment()->getMethodInstance()->getCardsStorage();
        $card        = $cardStorage->registerCard();
        $card->setRequestedAmount("")
             ->setBalanceOnCard("")
             ->setLastTransId($paymentInfos['transaction_id'])
             ->setProcessedAmount($shopgateOrder->getAmountComplete())
             ->setCcType($this->_getCcTypeName($paymentInfos['credit_card']['type']))
             ->setCcOwner($paymentInfos['credit_card']['holder'])
             ->setCcLast4($lastFour)
             ->setCcExpMonth("")
             ->setCcExpYear("")
             ->setCcSsIssue("")
             ->setCcSsStartMonth("")
             ->setCcSsStartYear("");
        
        $paymentStatus = $paymentInfos['transaction_type'];
        try {
            switch ($paymentStatus) {
                case self::SHOPGATE_PAYMENT_STATUS_AUTH_CAPTURE:
                    $this->_createTransaction(
                         $order->getPayment(),
                         $card,
                         Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE
                    );
                    $invoice         = $this->_getPaymentHelper()->createOrderInvoice($order);
                    $amountToCapture = $order->getBaseCurrency()->formatTxt($invoice->getBaseGrandTotal());
                    $order->getPayment()->setAmountAuthorized($invoice->getGrandTotal());
                    $order->getPayment()->setBaseAmountAuthorized($invoice->getBaseGrandTotal());
                    $order->getPayment()->setBaseAmountPaidOnline($invoice->getBaseGrandTotal());
                    $card->setCapturedAmount($card->getProcessedAmount());
                    $cardStorage->updateCard($card);
                    $message = Mage::helper('sales')->__('Captured amount of %s online.', $amountToCapture);
                    $invoice->setIsPaid(true);
                    $invoice->pay();
                    $invoice->setTransactionId(1);
                    $invoice->save();
                    $order->addRelatedObject($invoice);
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, $message);
                    break;
                case self::SHOPGATE_PAYMENT_STATUS_AUTH_ONLY:
                    $this->_createTransaction(
                         $order->getPayment(),
                         $card,
                         Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH
                    );
                    $formattedPrice = $order->getBaseCurrency()->formatTxt($order->getTotalDue());
                    $order->getPayment()->setAmountAuthorized($order->getGrandTotal());
                    $order->getPayment()->setBaseAmountAuthorized($order->getBaseGrandTotal());
                    $order->getPayment()->setIsTransactionPending(true);
                    $cardStorage->updateCard($card);
                    $message = Mage::helper('paypal')->__('Authorized amount of %s.', $formattedPrice);

                    $order->setState(Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW, true, $message);
                    break;
                default:
                    throw new Exception("Cannot handle payment status '{$paymentStatus}'.");
            }
        } catch (Exception $x) {
            $order->addStatusHistoryComment(Mage::helper('sales')->__('Note: %s', $x->getMessage()));
            Mage::logException($x);
        }
        return $order;
    }

    /**
     * Retrieve credit card type by mapping
     *
     * @param  $ccType string
     * @return string
     */
    protected function _getCcTypeName($ccType)
    {
        switch ($ccType) {
            case 'visa':
                $ccType = 'VI';
                break;
            case 'mastercard':
                $ccType = 'MC';
                break;
            case 'american_express':
                $ccType = 'AE';
                break;
            case 'discover':
                $ccType = 'DI';
                break;
            case 'jcb':
                $ccType = 'JCB';
                break;
            case 'maestro':
                $ccType = 'SM';
                break;
            default:
                $ccType = 'OT';
                break;
        }
        return $ccType;
    }
    
    /**
     * @param $orderPayment
     * @param $card
     * @param $type
     */
    protected function _createTransaction($orderPayment, $card, $type)
    {
        $transaction = Mage::getModel('sales/order_payment_transaction');
        $transaction->setOrderPaymentObject($orderPayment);
        $transaction->setTxnId($card->getLastTransId());
        $transaction->setIsClosed(false);
        $transaction->setTxnType($type);
        $transaction->setData('is_transaciton_closed', '0');
        $transaction->setAdditionalInformation('real_transaction_id', $card->getLastTransId());
        $transaction->save();
    }
}