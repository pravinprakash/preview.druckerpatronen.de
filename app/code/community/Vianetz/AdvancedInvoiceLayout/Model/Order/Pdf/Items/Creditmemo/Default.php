<?php
/**
 * AdvancedInvoiceLayout Order Creditmemo Pdf default items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Creditmemo_Default extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $shift  = array(0, 10, 0);
        $leftBound  =  78;
        $rightBound = 545;
        
        $this->loadConfig($order->getStore());

        $this->_setFontRegular();
        $x = $leftBound;
        
        // draw SKU
        foreach (Mage::helper('core/string')->str_split($this->getSku($item), 10) as $key => $part) {
            $page->drawText($part, $x, $pdf->y - $shift[2], self::PDF_CHARSET);
                $shift[2] += 10;
        }
        $x += 70;
        
        // draw name
        foreach (Mage::helper('core/string')->str_split($item->getName(), $this->fontsize_regular*4, true, true) as $key => $part) {
            $page->drawText($part, $x, $pdf->y - $shift[0], self::PDF_CHARSET);
            $shift[0] += 10;
        }
        // draw options
        if ($options = $this->getItemOptions()) {
            foreach ($options as $option) {
                // draw options label
                $this->_setFontItalic();
                foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), $x, false, true) as $_option) {
                    $page->drawText($_option, $x, $pdf->y - $shift[0], self::PDF_CHARSET);
                    $shift[0] += 10;
                }
                // draw options value
                $this->_setFontRegular();
                $_printValue = isset($option['print_value']) ? $option['print_value'] :
strip_tags($option['value']);
                foreach (Mage::helper('core/string')->str_split($_printValue, $x, true,
true) as $_value) {
                    $page->drawText($_value, $x + 5, $pdf->y - $shift[0], self::PDF_CHARSET);
                    $shift[0] += 10;
                }
            }
        }

        if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
            $shift[0] += 4;
            $this->_setFontRegular($this->fontsize_regular-1);
            foreach
(Mage::helper('core/string')->str_split(strip_tags($this->_parseShortDescription()),
60-$this->fontsize_regular*2,
true, true) as $description) {
                $page->drawText(strip_tags($description), $x, $pdf->y-$shift[0], self::PDF_CHARSET);
                $shift[0] += 10;
            }
            $this->_setFontRegular();
        }

        // draw product description
        foreach ($this->_parseDescription() as $description){
            $page->drawText(strip_tags($description), $x + 5, $pdf->y - $shift[1], self::PDF_CHARSET);
            $shift[1] += 10;
        }
        $x += 190;


        $font = $this->_setFontBold();
        
        // draw QTY
        $text = $item->getQty() * 1;
        $page->drawText($text, $pdf->getAlignCenter($text, $x, 30, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);
        $x += 50;

        // draw Total(ex)
        $text = $order->formatPriceTxt($item->getRowTotal());
        //$page->drawText($text, $pdf->getAlignRight($text, $x, 50, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);
        $x += 0;

        // draw Discount
        $text = $order->formatPriceTxt(-$item->getDiscountAmount());
        #$page->drawText($text, $pdf->getAlignRight($text, $x, 50, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);
        $page->drawText($text, 435-$pdf->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular), $pdf->y, self::PDF_CHARSET);

        $x += 90;

        // draw Tax
        $text = $order->formatPriceTxt($item->getTaxAmount());
        //$page->drawText($text, $pdf->getAlignRight($text, $x, 45, $font, 9, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);
        $x += 0;

        // draw Total(inc)
        $text = $order->formatPriceTxt($item->getRowTotal() + $item->getTaxAmount() - $item->getDiscountAmount());
        #$page->drawText($text, $pdf->getAlignRight($text, $x, $rightBound - $x, $font, $this->fontsize_bold, 0), $pdf->y, self::PDF_CHARSET);
        $page->drawText($text, 545-$pdf->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular), $pdf->y, self::PDF_CHARSET);


        $pdf->y -= max($shift) + 10;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
