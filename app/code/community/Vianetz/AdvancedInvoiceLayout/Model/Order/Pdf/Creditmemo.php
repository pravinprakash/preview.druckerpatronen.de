<?php
/**
 * AdvancedInvoiceLayout Order Creditmemo PDF model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Creditmemo extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract
{
    public function getPdf($creditmemos = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('creditmemo');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($creditmemos as $creditmemo) {
            if ($creditmemo->getStoreId()) {
                Mage::app()->getLocale()->emulate($creditmemo->getStoreId());
            }
            $this->page = $this->pdf->newPage(self::PDF_PAGE_FORMAT);
            $this->pdf->pages[] = $this->page;

            $order = $creditmemo->getOrder();

            $this->loadConfig($creditmemo->getStore());

            // Add image
            $this->insertLogo($this->page, $creditmemo->getStore());

            // Add address
            $this->insertAddress($this->page, $creditmemo->getStore());
            
            $this->insertShippingAndBillingAddresses($this->page, $order);
            
            $this->y -=50; 
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($this->page, $this->fontsize_regular+4);
            $this->page->drawText(Mage::helper('sales')->__('Credit Memo # ') . $creditmemo->getIncrementId(), 76, $this->y, self::PDF_CHARSET);
            $this->_setFontRegular($this->page);
            $this->y -=10;
                        
            // Add Head
            $this->insertHeader($this->page, $order);

            // Add Order
            $this->insertOrder($this->page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_CREDITMEMO_PUT_ORDER_ID, $order->getStoreId()));    

            // Add table head
            $this->_drawHeader($this->page);
            $this->y -=30;

            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            // Add body
            foreach ($creditmemo->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                
                // Draw item
                $this->_drawItem($item, $this->page, $order);

                $shift = array();
                if ($this->y < $this->footer_y+140) {
					
                    // Add new table head
                    $this->page = $this->_newPage($this->pdf, $this->page, $item->getStore());
                    
                    $this->_drawHeader($this->page);
                    $this->y -=20;
                }
            }
           
            $this->y -= 40;
            if ($this->y < $this->footer_y+60) {
				$this->page = $this->_newPage($this->pdf, $this->page,
$creditmemo->getStoreId());
				$font = $this->_setFontRegular($this->page, 9);
				$this->y -=40;
            }

            // Add totals
            $this->insertTotals($this->page, $creditmemo);

            $this->y -=20;
            // Insert free text
            $text = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_FREETEXT,
$creditmemo->getStoreId());
            $text = preg_replace('/{{invoice_date(\+\d)?}}/e',
"Mage::helper('core')->formatDate(\$invoice->getCreatedAt().('\\1'!=''?'\\1'.' days':''))"
    , $text);
            $this->insertFreeText($this->page, $this->pdf, $creditmemo->getStoreId(), $text);


            $this->insertFooter($this->page);
        }
        
        $this->_afterGetPdf();

        if ($creditmemo->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->pdf;
    }

    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        $this->_setFontRegular($page, $this->fontsize_regular-1);
        $font = $page->getFont();
        $size = $page->getFontSize();

        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(70, $this->y, 550, $this->y-15);
        $this->y -=10;
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));

        $page->drawText(Mage::helper('sales')->__('SKU'), $x = 78, $this->y, self::PDF_CHARSET);
        $x += 70;

        $page->drawText(Mage::helper('sales')->__('Products'), $x, $this->y, self::PDF_CHARSET);
        $x += 190;
        
        $text = Mage::helper('sales')->__('QTY');
        $page->drawText($text, $this->getAlignCenter($text, $x, 30, $font, $size), $this->y, self::PDF_CHARSET);
        $x += 50;

        $text = Mage::helper('sales')->__('Total(ex)');
        //$page->drawText($text, $this->getAlignRight($text, $x, 50, $font, $size), $this->y, self::PDF_CHARSET);
        $x += 0;

        $text = Mage::helper('sales')->__('Discount');
        $page->drawText($text, 435-$this->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular-1), $this->y, self::PDF_CHARSET);
        $x += 90;

        $text = Mage::helper('sales')->__('Tax');
        //$page->drawText($text, $this->getAlignRight($text, $x, 45, $font, $size, $this->fontsize_bold), $this->y, self::PDF_CHARSET);
        $x += 0;

        $text = Mage::helper('sales')->__('Total(inc)');
        $page->drawText($text,
545-$this->widthForStringUsingFontSize($text, $font, $this->fontsize_regular-1), $this->y, self::PDF_CHARSET);
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
