<?php
/**
 * AdvancedInvoiceLayout Order Shipment PDF model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Shipment extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract
{
    public function getPdf($shipments = array())
    {   
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                Mage::app()->getLocale()->emulate($shipment->getStoreId());
            }
            $this->page = $this->pdf->newPage(self::PDF_PAGE_FORMAT);
            $this->pdf->pages[] = $this->page;

            $order = $shipment->getOrder();
            
            $this->loadConfig($shipment->getStore());

            // Add image
            $this->insertLogo($this->page, $shipment->getStore());

            // Add address
            $this->insertAddress($this->page, $shipment->getStore());
            
            $this->insertShippingAndBillingAddresses($this->page, $order, true);
            
            $this->y -=50; 
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($this->page, $this->fontsize_regular+4);
            $this->page->drawText(Mage::helper('sales')->__('Packingslip # ') . $shipment->getIncrementId(), 76, $this->y, self::PDF_CHARSET);
            $this->_setFontRegular($this->page);
            $this->y -=10;
                        
            // Add Head
            $this->insertHeader($this->page, $order);

            // Add Order
            $this->insertOrder($this->page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID, $order->getStoreId()));


            // Add table
            $this->_drawHeader($this->page);

            $this->y -=30;

            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            // Add body
            foreach ($shipment->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                $shift = 10;
                $shift = array();
                if ($this->y < $this->footer_y+50) {
					
                    // Add new table head
                    $this->page = $this->_newPage($this->pdf, $this->page, $item->getStore());

                    $this->_drawHeader($this->page);

                    $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                    $this->y -=20;
                }

                // Draw item
                $this->_drawItem($item, $this->page, $order);
            }

		    if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_SHOW_GIFTMSG,
$shipment->getStore())) {
                // Insert Gift Message
                $this->insertGiftMessage($this->page, $this->pdf, $shipment);
            }

            $this->y -= 40;

            // Insert free text
            $text = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_FREETEXT,
$shipment->getStore());
            $text = preg_replace('/{{invoice_date(\+\d)?}}/e',
"Mage::helper('core')->formatDate(\$invoice->getCreatedAt().('\\1'!=''?'\\1'.' days':''))"
    , $text);
            $this->insertFreeText($this->page, $this->pdf, $shipment->getStore(), $text);

        
            $this->insertFooter($this->page);
        }


        $this->_afterGetPdf();

        if ($shipment->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->pdf;
    }

    protected function _drawHeader(&$page) {
        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(70, $this->y, 550, $this->y-15);
        $this->y -=10;
        $this->_setFontRegular($page, $this->fontsize_regular-1);
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
        $page->drawText(Mage::helper('sales')->__('SKU'), 78, $this->y, self::PDF_CHARSET);
        $page->drawText(Mage::helper('sales')->__('Products'), 178, $this->y, self::PDF_CHARSET);
        $page->drawText(Mage::helper('sales')->__('QTY'), 450, $this->y, self::PDF_CHARSET);
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
