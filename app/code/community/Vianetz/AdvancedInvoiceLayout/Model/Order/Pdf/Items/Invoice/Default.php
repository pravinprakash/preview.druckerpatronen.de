<?php
/**
 * AdvancedInvoiceLayout Order Invoice Pdf default items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Invoice_Default extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $shift  = array(0, 10, 0);

        $this->loadConfig($order->getStore());
        
        $this->_setFontRegular();

        $page->drawText($item->getQty()*1, 400, $pdf->y, self::PDF_CHARSET);

        // in case Product name is longer than 80 chars - it is written in a few lines 
        foreach (Mage::helper('core/string')->str_split($item->getName(), 60-$this->fontsize_regular*2, true, true) as $key => $part) {
            $page->drawText($part, 178, $pdf->y-$shift[0], self::PDF_CHARSET);
            $shift[0] += 10;
        }

        $options = $this->getItemOptions();
        if (isset($options)) {
            foreach ($options as $option) {
                // draw options label
                $this->_setFontItalic();
                foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), 60, false, true) as $_option) {
                    $page->drawText($_option, 178, $pdf->y-$shift[0], self::PDF_CHARSET);
                    $shift[0] += 10;
                }
                // draw options value
                $this->_setFontRegular();
                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value']
: strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        foreach (Mage::helper('core/string')->str_split($value, 60,true,true) as $_value) {
                            $page->drawText($_value, 180, $pdf->y-$shift[0], self::PDF_CHARSET);
                            $shift[0] += 10;
                        }
                    }
                }
            }
        }

        if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract::XML_PATH_SALES_PDF_INVOICE_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
            $shift[0] += 4;
            $this->_setFontRegular($this->fontsize_regular-1);
            foreach
(Mage::helper('core/string')->str_split(strip_tags($this->_parseShortDescription()),
60-$this->fontsize_regular*2,
true, true) as $description) {
                $page->drawText(strip_tags($description), 178, $pdf->y-$shift[0], self::PDF_CHARSET);
                $shift[0] += 10;
            }
            $this->_setFontRegular();
        }

        foreach ($this->_parseDescription() as $description){
            $page->drawText(strip_tags($description), 185, $pdf->y-$shift[1], self::PDF_CHARSET);
            $shift[1] += 10;
        }

        /* in case Product SKU is longer than 15 chars - it is written in a few lines */
        foreach (Mage::helper('core/string')->str_split($this->getSku($item), 15) as $key => $part) {
            if ($key > 0) {
                $shift[2] += 10;
            }
            $page->drawText($part, 78, $pdf->y-$shift[2], self::PDF_CHARSET);
        }

        $font = $this->_setFontBold();
        
        $price = $item->getPrice()+(round(($item->getBaseTaxAmount())/($item->getQty()), 2));

        $row_total = $order->formatPriceTxt(round($price*$item->getQty(), 2));
        $page->drawText($row_total, 545-$pdf->widthForStringUsingFontSize($row_total, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);

        $price = $order->formatPriceTxt($price);
        $page->drawText($price, 460-$pdf->widthForStringUsingFontSize($price, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);

        $tax = $order->formatPriceTxt($item->getTaxAmount());
        //$page->drawText($tax, 495-$pdf->widthForStringUsingFontSize($tax, $font, $this->fontsize_bold), $pdf->y, self::PDF_CHARSET);

        $pdf->y -= max($shift)+10;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
