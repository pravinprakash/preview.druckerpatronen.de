<?php
/**
 * AdvancedInvoiceLayout Order PDF abstract model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
abstract class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract extends Mage_Sales_Model_Order_Pdf_Abstract
{
	/**
     * x coordinate of current position in document
     * @var int
     */
    public $y;

	/**
     * y coordinate of current position in document
     * @var int
     */
	public $x;

    /**
     * Current Pdf
     * @var Zend_Pdf
     */
    public $pdf;

    /**
     * Current Page
     * @var Zend_Pdf_Page
     */
    public $page;

	
	/**
     * Padding For Footer
     * @var int
     */
	public $footer_y = 0;
	
	/**
     * Telephone prefix (according to app/code/core/Mage/Customer/etc/config.xml)
     * @var string
     */
	public $prefix_telephone = "T: ";

	/**
     * Telefax prefix (according to app/code/core/Mage/Customer/etc/config.xml)
     * @var string
     */
	public $prefix_fax = "F: ";
	
	/**
     * Should telephone and fax number be skipped
     * @var bool
     */
	public $isCommunicationSkipped = true;
	
	/**
     * Default Fontsize For Regular Font
     * @var int
     */
	public $fontsize_regular = 10;

	/**
     * Default Fontsize For Bold Font
     * @var int
     */
	public $fontsize_bold = 10;

	/**
     * Default Fontsize For Italic Font
     * @var int
     */
	public $fontsize_italic = 10;
	
	/**
     * Padding scaling factor
     * @var float
     */
	const pad_sf = 1.2;
	
	/**
     * Top Padding of Page
     * @var float
     */
	const page_top = 0;

    /**
     * PDF Page Format
     * Possible values are SIZE_LETTER, SIZE_LETTER_LANDSCAPE, SIZE_A4, SIZE_A4_LANDSCAPE
     * @var Zend_Pdf_Page
     */
    const PDF_PAGE_FORMAT = Zend_Pdf_Page::SIZE_A4; 

    /**
     * PDF Character Setting
     * @var string
     */
    const PDF_CHARSET = 'UTF-8';
	
	/**
     * XML Admin Path For Corner X
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_LOGO_CORNER_X = 'sales_pdf/advancedinvoicelayout/logo_corner_x';

	/**
     * XML Admin Path For Corner Y
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_LOGO_CORNER_Y = 'sales_pdf/advancedinvoicelayout/logo_corner_y';

	/**
     * XML Admin Path For Footer Text First Column
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_1COLUMN = 'sales_pdf/advancedinvoicelayout/footertext_1column';

	/**
     * XML Admin Path For Footer Text Second Column
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_2COLUMN = 'sales_pdf/advancedinvoicelayout/footertext_2column';

	/**
     * XML Admin Path For Footer Text Third Column
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_3COLUMN = 'sales_pdf/advancedinvoicelayout/footertext_3column';

	/**
     * XML Admin Path For Split Taxes Option
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_SPLIT_TAXES = 'sales_pdf/advancedinvoicelayout/split_taxes';

	/**
     * XML Admin Path For Invoice Free Text
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_FREETEXT = 'sales_pdf/invoice/freetext';

	/**
     * XML Admin Path For Shipment Free Text
     * @var string
     */
	const XML_PATH_SALES_PDF_SHIPMENT_FREETEXT = 'sales_pdf/shipment/freetext';

	/**
     * XML Admin Path For Creditmemo Free Text
     * @var string
     */
	const XML_PATH_SALES_PDF_CREDITMEMO_FREETEXT = 'sales_pdf/creditmemo/freetext';

	/**
     * XML Admin Path For Show Tax VAT Option
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_SHOW_TAXVAT = 'sales_pdf/invoice/show_taxvat';

	/**
     * XML Admin Path For Default Font Size
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE = 'sales_pdf/advancedinvoicelayout/default_fontsize';

	/**
     * XML Admin Path For Show Gift Message Option On Invoices
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_SHOW_GIFTMSG = 'sales_pdf/invoice/show_giftmsg';

	/**
     * XML Admin Path For Show Gift Message Option On Shipments
     * @var string
     */
	const XML_PATH_SALES_PDF_SHIPMENT_SHOW_GIFTMSG = 'sales_pdf/shipment/show_giftmsg';

	/**
     * XML Admin Path For Margin Top
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP =
'sales_pdf/advancedinvoicelayout/margin_top';

	/**
     * XML Admin Path For Margin Bottom
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_BOTTOM =
'sales_pdf/advancedinvoicelayout/margin_bottom';

	/**
     * XML Admin Path For Margin Left
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_LEFT =
'sales_pdf/advancedinvoicelayout/margin_left';

	/**
     * XML Admin Path For Margin Right
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_RIGHT =
'sales_pdf/advancedinvoicelayout/margin_right';

	/**
     * XML Admin Path For Switch Addresses on Invoices
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_SWITCH_ADDRESSES =
'sales_pdf/invoice/switch_addresses';

	/**
     * XML Admin Path For Show Tax Rate Name
     * @var string
     */
	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_SHOW_TAXRATE_NAME =
'sales_pdf/advancedinvoicelayout/show_taxrate_name';

	/**
     * XML Admin Path For Show Customer Email Address
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_SHOW_CUSTOMER_EMAIL =
'sales_pdf/invoice/show_customer_email';

	/**
	 * Function loadConfig
	 *
	 * Read Admin Configuration Values
     * @param string $store Store Id
	 */
	public function loadConfig( $store ) {
        $fontsize = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE, $store);
        if ( $fontsize != null ) {
			$this->fontsize_regular = $fontsize;

			$this->fontsize_bold = $fontsize;
			
			$this->fontsize_italic = $fontsize;
		}

        // Calculate footer padding
        $this->footer_y =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_BOTTOM,
$store);
                    
    	$this->footer_y +=
20+max(substr_count(Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_1COLUMN,
$store), "\n"),
substr_count(Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_2COLUMN,
$store), "\n"),
substr_count(Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_3COLUMN,
$store), "\n"))*10;
    }


	/**
	 * Insert logo in document with given position in admin.
     * @param Zend_Pdf_Page $page 
     * @param string $store Store Id
	 */
    protected function insertLogo(&$page, $store = null)
    {
        $image = Mage::getStoreConfig('sales/identity/logo', $store);
        if ($image) {
            $image = Mage::getStoreConfig('system/filesystem/media', $store) . '/sales/store/logo/' . $image;
            if (is_file($image)) {
				// Get x and y coordinates from config
				$xCorner = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_LOGO_CORNER_X, $store);
				$yCorner = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_LOGO_CORNER_Y, $store);
				
				// Default values for x and y coordinates if admin values are empty
				if ( $xCorner == "" )
					$xCorner = 535;
				if ( $yCorner == "" )
					$yCorner = 780;
				
				// Calculate dimensions of image (width and height)
                $image = Zend_Pdf_Image::imageWithPath($image);
                /* Logo von halber auf 3/4-groesse geandert */
                $page->drawImage($image, $xCorner-$image->getPixelWidth()*0.75, $yCorner-$image->getPixelHeight()*0.75, $xCorner, $yCorner);

                //$this->y -= $yCorner - $image->getPixelHeight()/2;
            }
        }
    }

	/**
	 * Insert address of shop company on top.
     * @param Zend_Pdf_Page $page 
     * @param string $store Store Id
	 */
    protected function insertAddress(&$page, $store = null)
    {
        // $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.5));
        $this->_setFontRegular($page, $this->fontsize_regular-3);
        
        // Read address from admin
        $_y = substr_count(Mage::getStoreConfig('sales/identity/address', $store), "\n")*10;

        if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP) == "" )
            $padding = 0;
        else
            $padding =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP);

        $maxlen = 0;
		$len = strlen(Mage::getStoreConfig('sales/identity/address', $store));
		if ( $len > 0 ) {
			$this->y = 711;
        } else {
			$this->y = 701;
		}
		
		// Draw address
        $page->setLineWidth(1);
        /*foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
            $maxlen = max($maxlen, strlen($value));
            if ($value!=='') {
				$this->y -=10;
                $page->drawText(trim(strip_tags($value)), 76, $this->y, self::PDF_CHARSET);
            }
        }*/
        $value = Mage::getStoreConfig('sales/identity/address', $store);
        #$value = str_replace("\n", chr(183)." ", $value);
        #$value = str_replace("\n", "� ", $value);
        $value = str_replace("\n", "- ", $value);
        $value = str_replace("- Deutschland", "", $value);
        $maxlen = strlen($value);
        $page->drawText(trim(strip_tags($value)), 76, $this->y, self::PDF_CHARSET);
        
		// Draw line under address
		if ( $len > 0 ) {
			$this->y -=5;
			$page->setLineWidth(0.5);
			$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
			$page->drawLine(76, $this->y,
min($maxlen*(0.9+0.53*$this->fontsize_regular-1), 300), $this->y);
        } 
    }
    
    /**
     * Retrieve Customer if available
     * @param Mage_Order $order 
     */
    public function getCustomer($order)
    {
        if (!$this->_customer instanceof Mage_Customer_Model_Customer) {
            $this->_customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        }
        return $this->_customer;
    }
    
    /**
     * Insert header with some numbers and dates.
     * @param Zend_Pdf_Page $page
     * @param Mage_Order $order
     * @param bool $putCustomerId
     * @param bool $putOrderId
     */
    protected function insertHeader(&$page, $order, $putCustomerId = true, $putOrderId = true)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(5.5));

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);

		$this->y -=$this->fontsize_regular*self::pad_sf;
        if ($putOrderId) {
            $page->drawText(Mage::helper('sales')->__('Order #: ').
$order->getRealOrderId(), 76, $this->y, self::PDF_CHARSET);
			$this->y -=$this->fontsize_regular*self::pad_sf;
        }
        $page->drawText(Mage::helper('sales')->__('Order Date: ') .
Mage::helper('core')->formatDate($order->getCreatedAt(), 'medium', false), 76, $this->y,
self::PDF_CHARSET);
        $this->y -=$this->fontsize_regular*self::pad_sf;
        if ( $putCustomerId && $this->getCustomer($order)->getIncrementId() != "" ) {
			$page->drawText(Mage::helper('sales')->__('Customer #: ') .
$this->getCustomer($order)->getIncrementId(), 76, $this->y, self::PDF_CHARSET);
			$this->y -=$this->fontsize_regular*self::pad_sf;
        }

        $page->setFillColor(new Zend_Pdf_Color_Rgb(1, 1, 1));
        $page->setLineWidth(0);
    }
    
    /** 
     * Insert Shipping and/or Billing Address.
     * @param Zend_Pdf_Page $page
     * @param Mage_Order $order
     * @param bool isOnlyShipping Display only shipping address
     * @param bool isAddressSwitched Switch order of billing and shipping address
     */
    protected function insertShippingAndBillingAddresses(&$page, $order, $isOnlyShipping =
false, $isAddressSwitched = false)
    {
		// x coordinate of addresses
        $_billing_x = 76;
        $_shipping_x = 325;

        if ( !$order->getIsVirtual && $isAddressSwitched) {
            $_billing_x = 325;
            $_shipping_x = 76;
        }

        // Billing Address
        $billingAddress = $this->_formatAddress($order->getBillingAddress()->format('pdf'));

        // Shipping Address and Method
        if (!$order->getIsVirtual()) {
            // Shipping Address
            $shippingAddress = $this->_formatAddress($order->getShippingAddress()->format('pdf'));

        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);
        //$page->drawText(Mage::helper('sales')->__('SOLD TO:'), 35, $this->y, 'UTF-8');

        if (!$order->getIsVirtual() && ! $isOnlyShipping && ! $isAddressSwitched) {
            $page->drawText(Mage::helper('sales')->__('SHIP TO:'), $_shipping_x, $this->y ,
self::PDF_CHARSET);
        }

        if (!$order->getIsVirtual()) {
            $y = 730 - (max(count($billingAddress), count($shippingAddress)) * 10 + 5);
        }
        else {
            $y = 730 - (count($billingAddress) * 10 + 5);
        }
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        if ( ! $isAddressSwitched )
            $this->_setFontBold($page, $this->fontsize_bold+2);
        else
            $this->_setFontRegular($page, $this->fontsize_regular+2);
        $this->y -= 15;
        $_y = $this->y;

		if ( ! $isOnlyShipping ) {
			foreach ($billingAddress as $value){
				if ($value!=='' && ( ! $this->isCommunicationSkipped || 
				( substr($value, 0, strlen($this->prefix_telephone)) != $this->prefix_telephone && substr($value, 0, strlen($this->prefix_fax)) != $this->prefix_fax) )) {

                     foreach
(Mage::helper('core/string')->str_split(strip_tags(ltrim($value)), 35, true) as
$key => $part) {

					    $page->drawText($part, $_billing_x, $this->y, self::PDF_CHARSET);
					    $this->y -=($this->fontsize_bold+2)*self::pad_sf;
                    }
				}
			}

            if ( $isAddressSwitched )
                $this->_setFontBold($page, $this->fontsize_bold+2);
            else
                $this->_setFontRegular($page, $this->fontsize_regular+2);

			if (!$order->getIsVirtual()) {
				$this->y = $_y;
				foreach ($shippingAddress as $value){
				if ($value!=='' && ( ! $this->isCommunicationSkipped || 
				( substr($value, 0, strlen($this->prefix_telephone)) != $this->prefix_telephone && substr($value, 0, strlen($this->prefix_fax)) != $this->prefix_fax) )) {
                     foreach
(Mage::helper('core/string')->str_split(strip_tags(ltrim($value)), 35, true) as
$key => $part) {
						    $page->drawText($part, $_shipping_x, $this->y,
self::PDF_CHARSET);
						    $this->y -=($this->fontsize_regular+2)*self::pad_sf;
                    }  
					}
				}
			}
        } else {
            if (!$order->getIsVirtual()) {
				// Shipping address is added as primary address
				$this->_setFontBold($page,$this->fontsize_bold+2);       
				foreach ($shippingAddress as $value){
				    if ($value!=='' && ( ! $this->isCommunicationSkipped || 
				    ( substr($value, 0, strlen($this->prefix_telephone)) != $this->prefix_telephone && substr($value, 0, strlen($this->prefix_fax)) != $this->prefix_fax) )) {
						$page->drawText(strip_tags(ltrim($value)), $_billing_x, $this->y,
self::PDF_CHARSET);
						$this->y -=($this->fontsize_bold+2)*self::pad_sf;
					}
				}
			}
		}
    }

	/**
	 * Insert order items.
     * @param Zend_Pdf_Page $page
     * @param Mage_Order $order
     * @param bool $putOrderId
	 */
    protected function insertOrder(&$page, $order, $putOrderId = true)
    {
        /* Payment */
        $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
            ->setIsSecureMode(true)
            ->toPdf();
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key=>$value){
            if (strip_tags(trim($value))==''){
                unset($payment[$key]);
            }
        }
        reset($payment);

        $this->y -=20;
        
        if (!$order->getIsVirtual()) {
            $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $page->setLineWidth(0.5);

            $this->_setFontBold($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page->drawText(Mage::helper('sales')->__('Payment Method'), 76, $this->y,
self::PDF_CHARSET);
            $page->drawText(Mage::helper('sales')->__('Shipping Method:'), 285, $this->y ,
self::PDF_CHARSET);

            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            $yPayments   = $this->y - 5;
        } else {
            $this->_setFontBold($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page->drawText(Mage::helper('sales')->__('Payment Method'), 76, $this->y,
self::PDF_CHARSET);
            
            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            
            $yPayments   = $this->y - 5;
        }

        foreach ($payment as $value){
            if (trim($value)!=='') {
                $shift = 0;
                foreach (Mage::helper('core/string')->str_split(strip_tags(trim($value)),
40, true) as
$key => $part) {
                    $page->drawText(trim($part), 76, $yPayments - $shift,
self::PDF_CHARSET);
                    $shift += 10;
               }

                $yPayments =$yPayments - $shift - 10;
            }
        }

        if (!$order->getIsVirtual()) {
            $this->y -=5;

            $shippingMethod  = $order->getShippingDescription();

            $shift = 0;
            foreach (Mage::helper('core/string')->str_split($shippingMethod, 60) as
$key => $part) {
                $page->drawText(trim($part), 285, $this->y - $shift, self::PDF_CHARSET);
                $shift += 10;
            }

            $yShipments = $this->y;

            $totalShippingChargesText = "(" . Mage::helper('sales')->__('Total Shipping Charges') . " " . $order->formatPriceTxt($order->getBaseShippingAmount()) . ")";

            //$page->drawText($totalShippingChargesText, 285, $yShipments-12, 'UTF-8');
            $yShipments -=12;
            $tracks = $order->getTracksCollection();
            if (count($tracks)) {
                $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                $page->setLineWidth(0.5);

                $this->_setFontRegular($page);
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                //$page->drawText(Mage::helper('sales')->__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Title'), 290, $yShipments - 12,
self::PDF_CHARSET);
                $page->drawText(Mage::helper('sales')->__('Number'), 385, $yShipments -
12, self::PDF_CHARSET);

				// Print tracking information
                $yShipments -=23;
                $this->_setFontRegular($page, 7);
                foreach ($order->getTracksCollection() as $track) {

                    $CarrierCode = $track->getCarrierCode();
                    if ($CarrierCode!='custom')
                    {
                        $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
                        $carrierTitle = $carrier->getConfigData('title');
                    }
                    else
                    {
                        $carrierTitle = Mage::helper('sales')->__('Custom Value');
                    }

                    $truncatedCarrierTitle = substr($carrierTitle, 0, 35) . (strlen($carrierTitle) > 35 ? '...' : '');
                    $truncatedTitle = substr($track->getTitle(), 0, 45) . (strlen($track->getTitle()) > 45 ? '...' : '');
                    //$page->drawText($truncatedCarrierTitle, 285, $yShipments , 'UTF-8');
                    $page->drawText($truncatedTitle, 300, $yShipments , self::PDF_CHARSET);
                    $page->drawText($track->getNumber(), 395, $yShipments , self::PDF_CHARSET);
                    $yShipments -=8;
                }
            } else {
                $yShipments -= 8;
            }

            $currentY = min($yPayments, $yShipments);

            $this->y = $currentY;
            $this->y -= 15;
        } else {
			$this->y = $yPayments-30;
		}
    }
    
    protected function _drawItem(Varien_Object $item, Zend_Pdf_Page $page, Mage_Sales_Model_Order $order)
    {
        $type = $item->getOrderItem()->getProductType();
        $renderer = $this->_getRenderer($type);
        $renderer->setOrder($order);
        $renderer->setItem($item);
        $renderer->setPdf($this);
        $renderer->setPage($page);
        $renderer->setRenderedModel($this);

        $renderer->draw();
    }
    
    /**
     * Insert Footer with company legal and payment informations on bottom of document.
     * @param Zend_Pdf_Page $page
     * @param string $store Store Id
     */
    protected function insertFooter(&$page, $store = null) {

            $_column1 =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_1COLUMN,
$store);
            $_column2 =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_2COLUMN,
$store);
            $_column3 =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_3COLUMN,
$store);

			// Draw line
			$page->setLineWidth(0.5);
			$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
			if ( trim($_column1) != "" || trim($_column2) != "" || trim($_column3) != "" )
				$page->drawLine(66, $this->footer_y+10, 550, $this->footer_y+10);
			
			// Draw first column from admin
			$this->y = $this->footer_y;
			$this->x = 76;   
			$font = $this->_setFontRegular($page, 6);
			foreach (explode("\n", $_column1) as $value ) {
				$page->drawText(trim(strip_tags($value)), $this->x, $this->y, self::PDF_CHARSET); // 45
				$this->y -=10;
			}
			
			// Draw second column from admin
            if ( trim($_column2) != "" ) {
			    $this->y = $this->footer_y;
                $this->x +=170;
			    foreach (explode("\n", $_column2) as $value){
				    $page->drawText(trim(strip_tags($value)), $this->x, $this->y,
self::PDF_CHARSET); // 45
				    $this->y -=10;
			    }
                $this->x +=180;
            } else {
                $this->x +=260;
            }

			// Draw third column from admin
			$this->y = $this->footer_y;
			$font = $this->_setFontRegular($page, 6);
			foreach (explode("\n", $_column3) as $value ) {
				$page->drawText(trim(strip_tags($value)), $this->x, $this->y, self::PDF_CHARSET); // 45
				$this->y -=10;
			}
    }

	/**
	 * Count and insert totals in document.
     * @param Zend_Pdf_Page $page
     * @param string $source
	 */
    protected function insertTotals($page, $source){
        $order = $source->getOrder();
        $font = $this->_setFontBold($page);

        $order_subtotal = Mage::helper('sales')->__('Order Subtotal:');
        #$order_subtotal = Mage::helper('sales')->__('Zwischensumme:');
        $page->drawText($order_subtotal,
475-$this->widthForStringUsingFontSize($order_subtotal, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);

        $order_subtotal = $order->formatPriceTxt($source->getSubtotal());
        $page->drawText($order_subtotal,
545-$this->widthForStringUsingFontSize($order_subtotal, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);
        $this->y -=15;

        if ((float)$source->getDiscountAmount()){
            $discount = Mage::helper('sales')->__('Discount :');
            $page->drawText($discount, 475-$this->widthForStringUsingFontSize($discount,
$font, $this->fontsize_bold), $this->y, self::PDF_CHARSET);

            $discount = $order->formatPriceTxt(0.00 - $source->getDiscountAmount());
            $page->drawText($discount, 545-$this->widthForStringUsingFontSize($discount,
$font, $this->fontsize_bold), $this->y, self::PDF_CHARSET);
            $this->y -=15;
        }

        if ((float)$source->getShippingAmount()){
            $order_shipping = Mage::helper('sales')->__('Shipping & Handling:');
            $page->drawText($order_shipping,
475-$this->widthForStringUsingFontSize($order_shipping, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);

            $order_shipping = $order->formatPriceTxt($source->getShippingAmount());
            $page->drawText($order_shipping,
545-$this->widthForStringUsingFontSize($order_shipping, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);
            $this->y -=15;
        }

        if ($source->getAdjustmentPositive()){
            $adjustment_refund = Mage::helper('sales')->__('Adjustment Refund:');
            $page ->drawText($adjustment_refund,
475-$this->widthForStringUsingFontSize($adjustment_refund, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);

            $adjustment_refund = $order->formatPriceTxt($source->getAdjustmentPositive());
            $page ->drawText($adjustment_refund,
545-$this->widthForStringUsingFontSize($adjustment_refund, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);
            $this->y -=15;
        }

        if ((float) $source->getAdjustmentNegative()){
            $adjustment_fee = Mage::helper('sales')->__('Adjustment Fee:');
            $page ->drawText($adjustment_fee,
475-$this->widthForStringUsingFontSize($adjustment_fee, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);

            $adjustment_fee=$order->formatPriceTxt($source->getAdjustmentNegative());
            $page ->drawText($adjustment_fee,
545-$this->widthForStringUsingFontSize($adjustment_fee, $font, $this->fontsize_bold),
$this->y, self::PDF_CHARSET);
            $this->y -=15;
        }
        
        if ((float)$source->getTaxAmount()){
			$fullInfo = $order->getFullTaxInfo();
			
			if ( $fullInfo && Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_SPLIT_TAXES, $source->getStore()) ) {
				foreach ($fullInfo as $info) {
					$percent = $info['percent'];
					$amount = $info['amount']; 
					$rates = array_unique($info['rates']);
					foreach ($rates as $rate) {
						if ( $rate['percent'] ) {
                            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_SHOW_TAXRATE_NAME,
$source->getStore())) 
							    $order_tax = "Zzgl. ".$rate['title'] . ":";
                            else
                                $order_tax = "Zzgl. ".$rate['percent'] . "% " .
Mage::helper('sales')->__('Tax') . ":";

							$page->drawText($order_tax,
475-$this->widthForStringUsingFontSize($order_tax, $font, $this->fontsize_bold), $this->y,
self::PDF_CHARSET);
						}
						$order_tax = $order->formatPriceTxt($amount);
						$page->drawText($order_tax,
545-$this->widthForStringUsingFontSize($order_tax, $font, $this->fontsize_bold), $this->y,
self::PDF_CHARSET);
						$this->y -=15;
					}
				}
			} else {
				$order_tax = Mage::helper('sales')->__('Tax :');
				$page->drawText($order_tax,
475-$this->widthForStringUsingFontSize($order_tax, $font, $this->fontsize_bold), $this->y,
self::PDF_CHARSET);
				$order_tax = $order->formatPriceTxt($source->getTaxAmount());
				$page->drawText($order_tax,
545-$this->widthForStringUsingFontSize($order_tax, $font, $this->fontsize_bold), $this->y,
self::PDF_CHARSET);
			    $this->y -=15;
            }
            
			$this->y -=15;
        }


        $page->setFont($font, $this->fontsize_bold+2);

        #$order_grandtotal = Mage::helper('sales')->__('Grand Total:');
        $order_grandtotal = Mage::helper('sales')->__('Gesamtsumme inkl. MwSt.:');
        $page ->drawText($order_grandtotal,
475-$this->widthForStringUsingFontSize($order_grandtotal, $font, $this->fontsize_bold+2),
$this->y, self::PDF_CHARSET);

        $order_grandtotal = $order->formatPriceTxt($source->getGrandTotal());
        $page ->drawText($order_grandtotal,
545-$this->widthForStringUsingFontSize($order_grandtotal, $font, $this->fontsize_bold+2),
$this->y, self::PDF_CHARSET);
        $this->y -=15;
    }
    
    /** 
     * Insert Free Text from admin in invoice document
     * @param Zend_Pdf_Page $page
     * @param Zend_Pdf $pdf
     * @param string $store
     */
    protected function insertFreeText(&$page, $pdf, $store = null, $text = null) {
		if ( $text ) {
			foreach (explode("\n", $text ) as $value ) {
				if ( $this->y < $this->footer_y+20 ) {
					$page = $this->_newPage($pdf, $page, $store);
				}
				$this->_setFontRegular($page);

                foreach
(Mage::helper('core/string')->str_split(strip_tags(ltrim($value)), 95, true) as
$key => $part) {
			        $page->drawText(trim(strip_tags($part)), 76, $this->y, self::PDF_CHARSET);
                    $this->y -=$this->fontsize_regular*self::pad_sf;
                }
			}
			$this->y -=15;
		}
    }

    /**
     * Create new PDF Page
     * @access private
     * @param Zend_Pdf $pdf
     * @param Zend_Pdf_Page $page
     * @param string $store
     */
	protected function _newPage(&$pdf, $page, $store) {
		$this->insertFooter($page,$store);
        if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP) == "" )
            $padding = 0;
        else
            $padding =
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP);

		$page = $pdf->newPage(self::PDF_PAGE_FORMAT);
		$pdf->pages[] = $page;
		$this->y = 800-$padding;
		
		return $page;
	}

    /**
     * Insert Gift Message From Customer
     * @param Zend_Pdf_Page $page
     * @param Zend_Pdf $pdf
     * @param Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract $source
     */
    protected function insertGiftMessage(&$page, $pdf, $source) {
        $msg = Mage::getModel('giftmessage/message');

        if ( $this->y < $this->footer_y+50 ) {
            $page = $this->_newPage($pdf, $page, $source->getStore());
        }
        
        $msg_id = $source->getOrder()->getGiftMessageId();
        $this->_setFontRegular($page);
        if ( !is_null($msg_id) ) {
            $msg->load((int)$msg_id);
            $gift_sender = $msg->getData('sender');
            $gift_recipient = $msg->getData('recipient');
            $gift_message = $msg->getData('message');
            $page->drawText(Mage::helper('sales')->__('Message from') . ': ' .
Mage::helper('sales')->__($gift_sender), 76, $this->y, self::PDF_CHARSET);
            $this->y -=$this->fontsize_regular*self::pad_sf;
            $page->drawText(Mage::helper('sales')->__('Message to') . ': ' .
Mage::helper('sales')->__($gift_recipient), 76, $this->y, self::PDF_CHARSET);

            $this->y -=$this->fontsize_regular*self::pad_sf*1.5;
            $page->drawText($gift_message, 76, $this->y, self::PDF_CHARSET);
        } 
    }


	/**
	 * Set Font To Regular
     * @access private
	 */
    protected function _setFontRegular($object, $size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $object->setFont($font, $size == null ? $this->fontsize_regular : $size);
        return $font;
    }

	/**
	 * Set Font To Bold
     * @access private
	 */
    protected function _setFontBold($object, $size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $object->setFont($font, $size == null ? $this->fontsize_bold : $size);
        return $font;
    }

	/**
	 * Set Font To Italic
     * @access private
	 */
    protected function _setFontItalic($object, $size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        $object->setFont($font, $size == null ? $this->fontsize_italic : $size);
        return $font;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
