<?php
/**
 * AdvancedInvoiceLayout Order Pdf Items renderer Abstract
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
abstract class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract extends Mage_Sales_Model_Order_Pdf_Items_Abstract
{
	/**
     * Default fontsizes
     */
	public $fontsize_regular = 10;
	public $fontsize_bold = 10;
	public $fontsize_italic = 10;
	
	/**
     * Padding scaling factor
     */
	const pad_sf = 1.2;

    /**
     * PDF Character Setting
     * @var string
     */
    const PDF_CHARSET = 'UTF-8';

	const XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE = 'sales_pdf/advancedinvoicelayout/default_fontsize';

	/**
     * XML Admin Path For Show Product Short Description
     * @var string
     */
	const XML_PATH_SALES_PDF_INVOICE_SHOW_PRODUCT_SHORTDESCRIPTION =
'sales_pdf/invoice/show_product_shortdescription';

	/**
     * XML Admin Path For Show Product Short Description
     * @var string
     */
	const XML_PATH_SALES_PDF_SHIPMENT_SHOW_PRODUCT_SHORTDESCRIPTION =
'sales_pdf/shipment/show_product_shortdescription';

	/**
     * XML Admin Path For Show Product Short Description
     * @var string
     */
	const XML_PATH_SALES_PDF_CREDITMEMO_SHOW_PRODUCT_SHORTDESCRIPTION =
'sales_pdf/creditmemo/show_product_shortdescription';
	
	/**
	 * Function loadConfig
	 *
	 * Read admin values
	 */
	public function loadConfig( $store ) {
        $fontsize = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE, $store);
        if ( $fontsize != null ) {
			$this->fontsize_regular = $fontsize;

			$this->fontsize_bold = $fontsize;

			$this->fontsize_italic = $fontsize;
		}
    }
	
    protected function _formatOptionValue($value)
    {
        $order = $this->getOrder();

        $resultValue = '';
        if (is_array($value)) {
            if (isset($value['qty'])) {
                $resultValue .= sprintf('%d', $value['qty']) . ' x ';
            }

            $resultValue .= $value['title'];

            if (isset($value['price'])) {
                $resultValue .= " " . $order->formatPrice($value['price']);
            }
            return  $resultValue;
        } else {
            return $value;
        }
    }

    protected function _parseDescription()
    {
        $description = $this->getItem()->getDescription();
        if (preg_match_all('/<li.*?>(.*?)<\/li>/i', $description, $matches)) {
            return $matches[1];
        }

        return array($description);
    }

    protected function _parseShortDescription()
    {
        $product = Mage::getModel('catalog/product')->load($this->getItem()->getProductId());
        return $product->getShortDescription(); 
    }

    public function getItemOptions() {
        $result = array();
        if ($options = $this->getItem()->getOrderItem()->getProductOptions()) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }

    protected function _setFontRegular($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_regular : $size);
        return $font;
    }

    protected function _setFontBold($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_bold : $size);
        return $font;
    }

    protected function _setFontItalic($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_italic : $size);
        return $font;
    }

}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
