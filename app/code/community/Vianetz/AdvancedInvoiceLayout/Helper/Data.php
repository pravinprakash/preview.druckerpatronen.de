<?php
/**
 * AdvancedInvoiceLayout Helper Class
 * 
 * @category Vianetz
 * @package AdvancedInvoiceLayout
 * @author Christoph Massmann <C.Massmann@vianetz.com>
 * @license http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Helper_Data extends Mage_Core_Helper_Abstract
{  
    /**
     * Dump a variable to the logfile
     *
     * @param mixed $var
     * @param string $file
     */
    public function log($var, $file = null)
    {
        $file = isset($file) ? $file : 'advancedinvoicelayout.log';

        $var = print_r($var, 1);
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') $var = str_replace("\n", "\r\n", $var);
            Mage::log($var, null, $file);
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
