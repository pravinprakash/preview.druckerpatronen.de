<?php

/**
 * Academic Free License ("AFL") v. 3.0
 *
 * This Academic Free License (the "License") applies to any original work
 * of authorship (the "Original Work") whose owner (the "Licensor") has
 * placed the following licensing notice adjacent to the copyright notice
 * for the Original Work:
 *
 * Licensed under the Academic Free License version 3.0
 *
 * 1) Grant of Copyright License. Licensor grants You a worldwide,
 * royalty-free, non-exclusive, sublicensable license, for the duration of
 * the copyright, to do the following:
 *
 *     a) to reproduce the Original Work in copies, either alone or as
 *        part of a collective work;
 *
 *     b) to translate, adapt, alter, transform, modify, or arrange the
 *        Original Work, thereby creating derivative works ("Derivative
 *        Works") based upon the Original Work;
 *
 *     c) to distribute or communicate copies of the Original Work and
 *        Derivative Works to the public, UNDER ANY LICENSE OF YOUR
 *        CHOICE THAT DOES NOT CONTRADICT THE TERMS AND CONDITIONS,
 *        INCLUDING LICENSOR'S RESERVED RIGHTS AND REMEDIES, IN THIS
 *        ACADEMIC FREE LICENSE;
 *
 *     d) to perform the Original Work publicly; and
 *
 *     e) to display the Original Work publicly.
 *
 * 2) Grant of Patent License. Licensor grants You a worldwide,
 * royalty-free, non-exclusive, sublicensable license, under patent claims
 * owned or controlled by the Licensor that are embodied in the Original
 * Work as furnished by the Licensor, for the duration of the patents, to
 * make, use, sell, offer for sale, have made, and import the Original Work
 * and Derivative Works.
 *
 * 3) Grant of Source Code License. The term "Source Code" means the
 * preferred form of the Original Work for making modifications to it and
 * all available documentation describing how to modify the Original Work.
 * Licensor agrees to provide a machine-readable copy of the Source Code of
 * the Original Work along with each copy of the Original Work that
 * Licensor distributes. Licensor reserves the right to satisfy this
 * obligation by placing a machine-readable copy of the Source Code in an
 * information repository reasonably calculated to permit inexpensive and
 * convenient access by You for as long as Licensor continues to distribute
 * the Original Work.
 *
 * 4) Exclusions From License Grant. Neither the names of Licensor, nor the
 * names of any contributors to the Original Work, nor any of their
 * trademarks or service marks, may be used to endorse or promote products
 * derived from this Original Work without express prior permission of the
 * Licensor. Except as expressly stated herein, nothing in this License
 * grants any license to Licensor's trademarks, copyrights, patents, trade
 * secrets or any other intellectual property. No patent license is granted
 * to make, use, sell, offer for sale, have made, or import embodiments of
 * any patent claims other than the licensed claims defined in Section 2.
 * No license is granted to the trademarks of Licensor even if such marks
 * are included in the Original Work. Nothing in this License shall be
 * interpreted to prohibit Licensor from licensing under terms different
 * from this License any Original Work that Licensor otherwise would have a
 * right to license.
 *
 * 5) External Deployment. The term "External Deployment" means the use,
 * distribution, or communication of the Original Work or Derivative Works
 * in any way such that the Original Work or Derivative Works may be used
 * by anyone other than You, whether those works are distributed or
 * communicated to those persons or made available as an application
 * intended for use over a network. As an express condition for the grants
 * of license hereunder, You must treat any External Deployment by You of
 * the Original Work or a Derivative Work as a distribution under section
 * 1(c).
 *
 * 6) Attribution Rights. You must retain, in the Source Code of any
 * Derivative Works that You create, all copyright, patent, or trademark
 * notices from the Source Code of the Original Work, as well as any
 * notices of licensing and any descriptive text identified therein as an
 * "Attribution Notice." You must cause the Source Code for any Derivative
 * Works that You create to carry a prominent Attribution Notice reasonably
 * calculated to inform recipients that You have modified the Original
 * Work.
 *
 * 7) Warranty of Provenance and Disclaimer of Warranty. Licensor warrants
 * that the copyright in and to the Original Work and the patent rights
 * granted herein by Licensor are owned by the Licensor or are sublicensed
 * to You under the terms of this License with the permission of the
 * contributor(s) of those copyrights and patent rights. Except as
 * expressly stated in the immediately preceding sentence, the Original
 * Work is provided under this License on an "AS IS" BASIS and WITHOUT
 * WARRANTY, either express or implied, including, without limitation, the
 * warranties of non-infringement, merchantability or fitness for a
 * particular purpose. THE ENTIRE RISK AS TO THE QUALITY OF THE ORIGINAL
 * WORK IS WITH YOU. This DISCLAIMER OF WARRANTY constitutes an essential
 * part of this License. No license to the Original Work is granted by this
 * License except under this disclaimer.
 *
 * 8) Limitation of Liability. Under no circumstances and under no legal
 * theory, whether in tort (including negligence), contract, or otherwise,
 * shall the Licensor be liable to anyone for any indirect, special,
 * incidental, or consequential damages of any character arising as a
 * result of this License or the use of the Original Work including,
 * without limitation, damages for loss of goodwill, work stoppage,
 * computer failure or malfunction, or any and all other commercial damages
 * or losses. This limitation of liability shall not apply to the extent
 * applicable law prohibits such limitation.
 *
 * 9) Acceptance and Termination. If, at any time, You expressly assented
 * to this License, that assent indicates your clear and irrevocable
 * acceptance of this License and all of its terms and conditions. If You
 * distribute or communicate copies of the Original Work or a Derivative
 * Work, You must make a reasonable effort under the circumstances to
 * obtain the express assent of recipients to the terms of this License.
 * This License conditions your rights to undertake the activities listed
 * in Section 1, including your right to create Derivative Works based upon
 * the Original Work, and doing so without honoring these terms and
 * conditions is prohibited by copyright law and international treaty.
 * Nothing in this License is intended to affect copyright exceptions and
 * limitations (including "fair use" or "fair dealing"). This License shall
 * terminate immediately and You may no longer exercise any of the rights
 * granted to You by this License upon your failure to honor the conditions
 * in Section 1(c).
 *
 * 10) Termination for Patent Action. This License shall terminate
 * automatically and You may no longer exercise any of the rights granted
 * to You by this License as of the date You commence an action, including
 * a cross-claim or counterclaim, against Licensor or any licensee alleging
 * that the Original Work infringes a patent. This termination provision
 * shall not apply for an action alleging patent infringement by
 * combinations of the Original Work with other software or hardware.
 *
 * 11) Jurisdiction, Venue and Governing Law. Any action or suit relating
 * to this License may be brought only in the courts of a jurisdiction
 * wherein the Licensor resides or in which Licensor conducts its primary
 * business, and under the laws of that jurisdiction excluding its
 * conflict-of-law provisions. The application of the United Nations
 * Convention on Contracts for the International Sale of Goods is expressly
 * excluded. Any use of the Original Work outside the scope of this License
 * or after its termination shall be subject to the requirements and
 * penalties of copyright or patent law in the appropriate jurisdiction.
 * This section shall survive the termination of this License.
 *
 * 12) Attorneys' Fees. In any action to enforce the terms of this License
 * or seeking damages relating thereto, the prevailing party shall be
 * entitled to recover its costs and expenses, including, without
 * limitation, reasonable attorneys' fees and costs incurred in connection
 * with such action, including any appeal of such action. This section
 * shall survive the termination of this License.
 *
 * 13) Miscellaneous. If any provision of this License is held to be
 * unenforceable, such provision shall be reformed only to the extent
 * necessary to make it enforceable.
 *
 * 14) Definition of "You" in This License. "You" throughout this License,
 * whether in upper or lower case, means an individual or a legal entity
 * exercising rights under, and complying with all of the terms of, this
 * License. For legal entities, "You" includes any entity that controls, is
 * controlled by, or is under common control with you. For purposes of this
 * definition, "control" means (i) the power, direct or indirect, to cause
 * the direction or management of such entity, whether by contract or
 * otherwise, or (ii) ownership of fifty percent (50%) or more of the
 * outstanding shares, or (iii) beneficial ownership of such entity.
 *
 * 15) Right to Use. You may use the Original Work in all ways not
 * otherwise restricted or conditioned by this License or by law, and
 * Licensor promises not to interfere with or be responsible for such uses
 * by You.
 *
 * 16) Modification of This License. This License is Copyright (c) 2005
 * Lawrence Rosen. Permission is granted to copy, distribute, or
 * communicate this License without modification. Nothing in this License
 * permits You to modify this License as applied to the Original Work or to
 * Derivative Works. However, You may modify the text of this License and
 * copy, distribute or communicate your modified version (the "Modified
 * License") and apply it to other original works of authorship subject to
 * the following conditions: (i) You may not indicate in any way that your
 * Modified License is the "Academic Free License" or "AFL" and you may not
 * use those names in the name of your Modified License; (ii) You must
 * replace the notice specified in the first paragraph above with the
 * notice "Licensed under <insert your license name here>" or with a notice
 * of your own that is not confusingly similar to the notice in this
 * License; and (iii) You may not claim that your original works are open
 * source software unless your Modified License has been approved by Open
 * Source Initiative (OSI) and You comply with its license review and
 * certification process.
 *
 * @package PSMext
 */

/**
 * Helper for the administrative backend.
 *
 * @author Michael Leiss <m.leiss@techdivision.com>
 * @copyright TechDivision GmbH
 * @package Helper
 */
class Techdivision_Psmext_Helper_Data extends Mage_Core_Helper_Abstract
{

	public  function getProductUrl($product,$null) {
		return $product->getProductUrl();
	}
	
	public  function getCategoryProductUrl($product,$category) {
	  $url = $category->getUrl()."/".$product->getUrlPath();
		return str_replace("/index.php", "", $url);
	}

	public  function getProductUrlNoIndex($product,$null) {
		return str_replace("/index.php", "", $product->getProductUrl());
	}

	public function getProductPriceComma($product,$null){
		return str_replace(".",",",round ($product->getPrice(),2));
	}
	
	public function getState($product,$null){
		return "neu";
	}
	
	public function getImgUrl($product,$null){
	  if($path = $product->getImgMedium()) {
		  $url = "http://www.druckerpatronen.de/".$path;
		} else {
		  # $this->getSkinUrl nicht verwendbar!
      $url = "http://www.druckerpatronen.de/skin/frontend/default/blank/images/media/platzhalter_klein.png";
    }
    return $url;
	}
	
	public function getProductDescription($product,$null){
	
	/*
	
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,$product->getSku().":");
fclose($fp);

    $type = $product->getType2();
    $type = str_replace("extra", "zubehör", $type);
    $color = $product->getColor();
    if($color) $color = " ".$color;
    $cat_ids = $product->getCategoryIds();
    $description = ucfirst($type).$color;
    
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,"1");
fclose($fp);
    
    switch (count($cat_ids)) {
      case 0: # funktioniert die abfrage ueberhaupt oder kommt fehler? anscheinend count(NULL) = 0)
        $description.= " zu keinem Drucker kompatibel";
        return $description;
      case 1:
        $description.= " passend für den Drucker ";
        break;
      default:
        $description.= " passend für die Drucker ";
    }
    
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,"2");
fclose($fp);
    
    if($cat_ids) { # ueberpruefung eigtl nicht notwendig
    
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,"3");
fclose($fp);

      $hlpr = Mage::getModel('catalog/category');
      
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,"4");
fclose($fp);
      
      $parents = array();
      $count = 0;
      $comma = "";
      $alledrucker = "";
      foreach($cat_ids as $cat_id) {
        $count++;
        if($count == 2) $comma = ", ";
        $cat = $hlpr->load($cat_id);
        $cat_name = $cat->getName();
        $parent_id = $cat->parent_id;
        $parent = $hlpr->load($parent_id); # getParentId() oder getParentCategory() liefert hier falsches Ergebnis! deshalb parent_id!
        $parent_name = $parent->getName();
        if(! in_array($parent_id, $parents)) {
          array_push($parents, $parent_id);
          $alledrucker.= $comma.$parent_name." ".$cat_name;
        } else {
          $alledrucker.= $comma.$cat_name;
        }
      }
      $description.= $alledrucker;
    }
    
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/test.txt","a+");
fputs($fp,"5\n");
fclose($fp);

		return $description;*/
		return "irgendeine Beschreibung";
	}
	
	public function getProductPriceCommaBrutto($product,$null){
	# funktioniert auf der produktseite, hier in der function aber komischerweise nicht
	  /*$block = new Mage_Catalog_Block_Product();
    $block->setData('product', $product);
    $pricehtml = $block->getPriceHtml($product);
    $strpos1 = strpos($pricehtml, "<span class=\"price\">");
    $pricehtml = substr($pricehtml, $strpos1 + 20);
    $strpos2 = strpos($pricehtml, "</span>");
    $price = substr($pricehtml, 0, $strpos2 - 4);*/
    
  # stattdessen hardcoded:
    return str_replace(".",",",round (($product->getPrice() * 1.19),2));
	}

	public function getProductPriceDot($product,$null){
		return round ($product->getPrice(),2);
	}

	public function getFinalProductPriceComma($product,$null){
		return str_replace(".",",",round ($product->getFinalPrice(),2));
	}

	public function getFinalProductPriceDot($product,$null){
		return round ($product->getFinalPrice(),2);
	}


	public  function getImageUrl($product,$null) {
		if ($product->getImage() != 'no_selection' && $product->getImage()) {
			return Mage::helper('catalog/image')->init($product, 'image');
		}else  {
			$ids= $product->loadParentProductIds()->getParentProductIds();
			if(!empty($ids)){
				foreach ($ids as $_id){
					$product = Mage::getModel('catalog/product')->load($_id);
					if ($product->getImage() != 'no_selection' && $product->getImage()){
						return Mage::helper('catalog/image')->init($product, 'image');
					}
				}
			}else return "Kein Bild vorhanden!";
		}
	}

	public  function getCategoryName($product,$null)
	{
		$id = $product->getData('category_ids');
		if (is_array($id)){
			$categoryId=$id[0];
		}else { $categoryId = $id;}
		$cat = Mage::getModel('catalog/category')->load($categoryId);
		return $cat->getName();
	}

	public function getCategoryPath($product, $null){
		$id = $product->getData('category_ids');
		if (is_array($id)){
			$categoryId=$id[0];
		}else { $categoryId = $id;}
		$category = Mage::getModel('catalog/category');
		$cat_model = $category->load($id);

		$path = $cat_model->getData('path');
		$arr = explode("/", $path);
		$level = $cat_model->getData('level');
		//Pfad zusammenbauen
		$category_path_string="";
		for ($i=0;$i<=$level;$i++){
			$actual_model = Mage::getModel('catalog/category')->load($arr[$i]);
			$name = $actual_model->getData('name');
			if ($name != null && $name != "Root Catalog"){
				$category_path_string= $category_path_string.$actual_model->getData('name')." > ";
			}
		}
		if ($category_path_string ==""){
			return "Keiner Kategorie zugeordnet ";
		}else return substr($category_path_string, 0, -3);
	}

	public  function getManufacturer($product,$null)
	{
		return $product->getResource()->getAttribute('manufacturer')
		->getFrontend()->getValue($product);
	}

	//THX TO D. FUHR FOR CODE REWORK
	public function getAttribute($product, $attributeCode)
	{
		// workaround to return availability string
		if($attributeCode == 'availabe_in' && $product->getData('is_in_stock') == 1)
		{
			return "Auf Lager";
		}

		// fetch attribute value from product
		$attributeValue = $product->getData($attributeCode);

		// init the appropriate attribute model
		$attribute = Mage::getModel('eav/entity_attribute')
		->loadByCode('catalog_product', $attributeCode);
		/* @var $attribute Mage_Eav_Model_Entity_Attribute */

		// extra handling for 'select' and 'multiselect'
		if(in_array($attribute->getFrontendInput(), array('select', 'multiselect')))
		{
			if($attribute->getFrontendInput() == 'select')
			{
				// wrap the value in an array
				$attributeValue = array($attributeValue);
			}
			else
			{
				// explode the attribute value to an array
				$attributeValue = explode(',', $attributeValue);
			}

			// fetch the option collection with the frontend labels
			$optionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
			->setIdFilter($attributeValue)
			->setPositionOrder('asc', true);
				
			/* @var $optionCollection Mage_Eav_Model_Mysql4_Entity_Attribute_Option_Collection */
			$optionCollection->getSelect()
			->joinLeft(
			array(
						'store_value' => Mage::getSingleton('core/resource')
			->getTableName('eav/attribute_option_value')
			),
					'store_value.option_id = main_table.option_id AND '
					. $optionCollection->getConnection()
					->quoteInto('store_value.store_id=?', $product->getStoreId()),
					array(
						'store_value' => 'value'
						)
						)
						->where(
						$optionCollection->getConnection()
						->quoteInto('store_value.store_id = ?', $product->getStoreId())
						);
						$optionCollection->load();
							
						// reset the attribute value
						$attributeValue = array();
							
						foreach($optionCollection as $option)
						{
							/* @var $option Mage_Eav_Model_Entity_Attribute_Option */
							$attributeValue[] = $option->getStoreValue();
						}
							
						// concatenate the frontend labels
						$attributeValue = implode(', ', $attributeValue);
		}

		return $attributeValue;
	}

	
//    public function getAttribute($product, $attribute)
//    {
//    	if($attribute == 'availabe_in' && $product->getData('is_in_stock') == 1){
//        	 return "Auf Lager";	
//        } 
//		else return $product->getData($attribute);
//	}

	
//    public function getAttribute($product, $attribute)
//    {
//        $text = $product->getAttributeText($attribute);
//
//        if ($text==null | empty($text)){
//            $_attribute="get".$attribute;
//            return $product->$_attribute();
//        }
//        else {
//            return $text;
//        }
//    }
	
	
	public function getProductShipperPriceGoogle($product, $shipper){
		$price = $this->getProductShipperPrice($product, $shipper);
		if ($price != "Keine Versandart deckt das Produkt ab!"){
			$price = "DE"."::".$shipper.":".$price;
			return $price;
		}else return $price;
	}


	public function getProductShipperPrice($product,$shipper){

		//Maße und Gewicht des Produkts holen
		$weight = $product->getWeight();

		//Dimension in Hoehe, Breite und Tiefe  "4.2 x 2 x 0.6 inches"
		$prod_dimension = $product->getDimension();
		$price = $product->getPrice();
		$categories = $product->getData('category_ids');

		//Dimensions Check

		//Shipper Daten holen
		$data = Mage::getModel('psmext/shipping')->getCollection()->addFilter('shipping_name',$shipper)->getItems();
		$filtered_data = array();

		//Alle Datensätze  die  auf das Produkt zutreffen filtern und nach $filtered_data pushen

		$filtered_category_data=array();
		foreach ($data as $_data){

			$a = $this->productInCategory($product->getData('category_ids'),$_data->getCat());


			//1. Gewicht bezogen
			if($weight <  $_data->getMweight()   || $_data->getMweight() ==0	){
				//2. Kategorie bezogen
				if($a == true || $_data->getCat() ==0){
					//3. Max. Preis bezogen
					if ($price > $_data->getMprice()   || $_data->getMprice() ==0){
						//4. Dimensionsbezogen
						if ( ($this->checkSizeFits($prod_dimension, $_data->getMheight(), $_data->getMwidth(),$_data->getMlength()))
						|| $_data->getMheight() ==0 || $_data->getMwidth() == 0 || $_data->getMlength() ==0){
							array_push($filtered_data, $_data);
							//War es ein Produkt das in die Kategorie passt, dann merke alle Shipperdatensätze hierzu
							if ($a==true){
								array_push($filtered_category_data, $_data);
							}
						}
					}
				}
			}

		}


		if (count($filtered_category_data) == 0){
			//Günstigsten Preis ermitteln
			$prices = array();
			foreach ($filtered_data as $_data){
				if (!empty($_data)){
					$price = $_data->getPrice();
					array_push($prices, $price);
				}
			}
		}

		//Wenn Kategoriebezogene Datensätze vorhanden sind
		if (count($filtered_category_data) > 0){
			$prices=array();
			foreach ($filtered_category_data as $_data){
				if (!empty($_data)){
					$price = $_data->getPrice();
					array_push($prices, $price);
				}

			}
		}

		if (count($prices)>0){
			return min($prices);
		}
		else return "Keine Versandart deckt das Produkt ab!";

	}


	public function productInCategory($prod_category_ids_string,$shipper_category){
		$prod_category_ids = explode(',',$prod_category_ids_string);
		if (is_array($prod_category_ids))	{

			foreach ($prod_category_ids as $_prod_category_id){
				if ($_prod_category_id == $shipper_category){return true;}
			}
			return false;
		}
		else {
			if ($prod_category_ids == $shipper_category){return true;}
			else {
				return false;}
		}

	}

	public function  checkSizeFits($prod_dimension, $mheight, $mwidth,$mlength){

		//Produkt Dimensions Array aufbauen
		$dimArray=explode("x",$prod_dimension);
		$productDimArray = array();

		if (count($dimArray)<3){
			return "0";
		}

		$productDimArray[0] =1 * $dimArray[0];
		$productDimArray[1] =1 * $dimArray[1];
		$productDimArray[2] =1 * $dimArray[2];
		array_multisort($productDimArray);

		//Shipper Dimensions Array aufbauen
		$shipperDimArray = array();
		$shipperDimArray[0]=$mheight;
		$shipperDimArray[1]=$mwidth;
		$shipperDimArray[2]=$mlength;
		array_multisort($shipperDimArray);

		//Checken ob Kleinster Wert Produkt   < kleinster Wert Versender
		$sig=0;
		for ($j=0;$j<3;$j++){
			if ($shipperDimArray[$j] < $productDimArray[$j]){
				$sig=1;
			}
		}

		if ($sig ==1) {return false;} else{ return true;};
	}

	public function getExistingEngines(){

		$data = Mage::getModel('psmext/psmext')->getCollection()->getData();
		$i = 0;
		$set = array();
		foreach ($data as $_data)
		{
			$set[$i] = $_data['psm_set'];
			$i++;
		}

		return  array_unique($set);
	}

	public function checkShipperExists($title){
		$data = Mage::getModel('psmext/shipping')->getCollection()->getData();
		foreach ( $data as $_shipper){
			if ($_shipper['shipping_name'] == $title){
				return true;
			}
		}
		return false;
	}


	public function getCategoryOptions(){
    # edit 22.04.2010 felix lades: bug gefixt bei zu vielen kategorien

		/*$category = Mage::getModel('catalog/category');
		$tree = $category->getTreeModel();
		$tree->load();
		$ids = $tree->getCollection()->getAllIds();*/
		
		$cats = Mage::getResourceModel('catalog/category_collection')->addOrderField('name');
		
		$arr2 = array();

		#if ($ids){
			#foreach ($ids as $id){
			foreach ($cats as $cat){
				#$cat = Mage::getModel('catalog/category');
				#$cat->load($id);
				if ($cat->getName() !="" && !is_numeric($cat->getName()) ){
					$arr = array();
					#$arr['key']=$cat->getEntityId();
					$arr['key']=$cat->getId();
					$arr['value']= $cat->getName();
					//{"key":"35","value":"Zweitshop"}
					array_push($arr2, Zend_Json::encode($arr) );
				}
			}
		#}

		return "{ records:[". implode(",",$arr2) ."]}";

	}

	public function getCategoryNameById($id){

		$cat = Mage::getModel('catalog/category')->load($id);
		return $cat;
	}

	public function getShopOptions(){

		
		//MULTI WEBSITE
		//		$optionArray = array();
		//		foreach ( Mage::app()->getWebsites() as $_site){
		//			$store_id = $_site->getDefaultGroup()->getDefaultStoreId();
		//			$data = $_site->getData();
		//			$name = $data['name'];
		//			array_push($optionArray, array($store_id,$name));
		//		}
		//		Mage::log($optionArray);
	     	
		//MULTI STORE
		$groups = Mage::app()->getGroups();
        $optionArray = array();
        foreach ($groups as $_group){    	
        array_push($optionArray, array($_group->getId(), $_group->getName()));
        }

	return $optionArray;
	}

	public function getRandomShopValue(){
		foreach ($this->getShopOptions() as $_option){
			return $_option[0];
		};

	}

	public function getShippers(){

		$data = Mage::getModel('psmext/shipping')->getCollection()->getData();
		$i = 0;
		$set = array();
		foreach ($data as $_data)
		{
			$set[$i] = $_data['shipping_name'];
			$i++;
		}
		$unique = array_unique($set);

		$numArray = array();
		$i= 0;
		foreach ($unique as $_value){
			$numArray[$i]=$_value;
			$i++;
		}

		return  Zend_Json::encode($numArray);
	}

	public function getShippingValues($shipper){
		$data = Mage::getModel('psmext/shipping')->getCollection()->addFilter('shipping_name',$shipper)->getData();
		return "{ \"root\": ".Zend_Json::encode($data)."}";
	}

	public  function getDirListing($storeId){


		$path = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/psmext/psmext/'.$storeId.'/';
		if (!is_dir( $path)){
			mkdir($path,0776,TRUE);
		}

		$dir = dir($path);
		$filelist = array();
		$i=0;
		while (($file = $dir->read()) !== false)
		{
			if ( $file != ".." && $file!=".") {
				$fullurl =   "<a href=". Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'psmext/psmext/'.$storeId.'/'.$file." target=_blank>Link</a>";
				$time = filectime  ($path.$file);
				$timestamp = date("d. M. Y H:i", $time);
				$filelist[$i]= "[\"".$file ."\",\"" .$fullurl."\",\"".$timestamp."\"]" ;
				$i++;
			}
		}
		$json = "[".   implode( ",",$filelist)  ."]";
		$dir->close();
		return $json;
	}



}
