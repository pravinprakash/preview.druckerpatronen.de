<?php
$installer = $this;
$installer->startSetup();
$installer->run("


DROP TABLE IF EXISTS {$this->getTable('psmext_psmext')};
CREATE TABLE IF NOT EXISTS {$this->getTable('psmext_psmext')} (
  `psmext_id` int(5) NOT NULL auto_increment,
  `psm_set` varchar(255) NOT NULL,
  `position_nr` int(11) NOT NULL,
  `function_nr` int(5) NOT NULL,
  PRIMARY KEY  (`psmext_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=226 ;

DROP TABLE IF EXISTS {$this->getTable('psmext_functions')};
CREATE TABLE IF NOT EXISTS {$this->getTable('psmext_functions')} (
  `function_id` int(11) NOT NULL auto_increment,
  `function_name` varchar(32) NOT NULL,
  `function_param1` varchar(32) NOT NULL,
  `function_description` varchar(32) NOT NULL,
  PRIMARY KEY  (`function_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;


INSERT INTO {$this->getTable('psmext_functions')} (`function_id`, `function_name`, `function_param1`, `function_description`) VALUES
(3, 'getAttribute', 'name', 'Produkt Name'),
(2, 'getImageUrl', '', 'Image Link'),
(1, 'getProductUrl', '', 'Produkt Link'),
(4, 'getAttribute', 'status', 'Zustand'),
(7, 'getAttribute', 'description', 'Beschreibung'),
(8, 'getAttribute', 'sku', 'Artikelnummer'),
(9, 'getAttribute', 'short_description', 'Kurzbeschreibung'),
(11, 'getManufacturer', '', 'Hersteller'),
(12, 'getCategoryName', '', 'Kategorie'),
(13, 'getProductUrlNoIndex', '', 'Produkt Link (ohne index.php)'),
(14, 'getProductPriceComma', '', 'Preis (Komma)'),
(15, 'getProductPriceDot', '', 'Preis (Punkt)'),
(16, 'getCategoryPath', '', 'Kategorie Pfad'),
(17, 'getFinalProductPriceComma', '', 'Preis Final (Komma)'),
(18, 'getFinalProductPriceDot', '', 'Preis Final (Punkt)');


DROP TABLE IF EXISTS {$this->getTable('psmext_separators')};
CREATE TABLE IF NOT EXISTS {$this->getTable('psmext_separators')} (
  `psmext_set` varchar(32) NOT NULL,
  `psmext_separator` varchar(1) NOT NULL,
  `psmext_headline` varchar(1024) NOT NULL,
  `psmext_id` int(32) NOT NULL auto_increment,
  PRIMARY KEY  (`psmext_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

DROP TABLE IF EXISTS {$this->getTable('psmext_shipping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('psmext_shipping')} (
  `shipping_id` int(11) NOT NULL auto_increment,
  `shipping_name` varchar(32) character set utf8 NOT NULL,
  `mlength` float NOT NULL,
  `mheight` float NOT NULL,
  `mwidth` float NOT NULL,
  `mweight` float NOT NULL,
  `mprice` float NOT NULL,
  `cat` int(11) NOT NULL,
  `price` float NOT NULL,
  UNIQUE KEY `shipping_id` (`shipping_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=229 ;

DROP TABLE IF EXISTS {$this->getTable('psmext_key')};
CREATE TABLE IF NOT EXISTS {$this->getTable('psmext_key')} (
  `psmext_key` varchar(64) character set utf8 NOT NULL,
  `psmext_id` int(11) NOT NULL,
  PRIMARY KEY  (`psmext_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

");


$installer->endSetup();