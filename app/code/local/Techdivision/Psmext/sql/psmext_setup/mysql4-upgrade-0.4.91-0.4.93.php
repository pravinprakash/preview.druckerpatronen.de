<?php
$installer = $this;
$installer->startSetup();
$installer->run("
INSERT IGNORE INTO {$this->getTable('psmext_functions')}  (`function_name`, `function_param1`, `function_description`) VALUES
('getAttribute', 'available_in', 'Verfuegbarkeit');
");
$installer->endSetup();