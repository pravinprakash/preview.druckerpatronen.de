<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('psmext_functions')} ADD UNIQUE (
`function_description`
);
INSERT IGNORE INTO {$this->getTable('psmext_functions')}  (`function_name`, `function_description`) VALUES
('getProductPriceComma', 'Preis (Komma)'),
('getProductPriceDot', 'Preis (Punkt)'),
('getCategoryPath', 'Kategorie Pfad'),
('getFinalProductPriceComma', 'Preis Final (Komma)'),
('getFinalProductPriceDot', 'Preis Final (Punkt)');");
$installer->endSetup();

