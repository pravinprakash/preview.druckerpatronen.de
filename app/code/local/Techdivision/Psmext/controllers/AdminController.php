<?php
/**
 * Academic Free License ("AFL") v. 3.0
 *
 * This Academic Free License (the "License") applies to any original work
 * of authorship (the "Original Work") whose owner (the "Licensor") has
 * placed the following licensing notice adjacent to the copyright notice
 * for the Original Work:
 *
 * Licensed under the Academic Free License version 3.0
 *
 * 1) Grant of Copyright License. Licensor grants You a worldwide,
 * royalty-free, non-exclusive, sublicensable license, for the duration of
 * the copyright, to do the following:
 *
 *     a) to reproduce the Original Work in copies, either alone or as
 *        part of a collective work;
 *
 *     b) to translate, adapt, alter, transform, modify, or arrange the
 *        Original Work, thereby creating derivative works ("Derivative
 *        Works") based upon the Original Work;
 *
 *     c) to distribute or communicate copies of the Original Work and
 *        Derivative Works to the public, UNDER ANY LICENSE OF YOUR
 *        CHOICE THAT DOES NOT CONTRADICT THE TERMS AND CONDITIONS,
 *        INCLUDING LICENSOR'S RESERVED RIGHTS AND REMEDIES, IN THIS
 *        ACADEMIC FREE LICENSE;
 *
 *     d) to perform the Original Work publicly; and
 *
 *     e) to display the Original Work publicly.
 *
 * 2) Grant of Patent License. Licensor grants You a worldwide,
 * royalty-free, non-exclusive, sublicensable license, under patent claims
 * owned or controlled by the Licensor that are embodied in the Original
 * Work as furnished by the Licensor, for the duration of the patents, to
 * make, use, sell, offer for sale, have made, and import the Original Work
 * and Derivative Works.
 *
 * 3) Grant of Source Code License. The term "Source Code" means the
 * preferred form of the Original Work for making modifications to it and
 * all available documentation describing how to modify the Original Work.
 * Licensor agrees to provide a machine-readable copy of the Source Code of
 * the Original Work along with each copy of the Original Work that
 * Licensor distributes. Licensor reserves the right to satisfy this
 * obligation by placing a machine-readable copy of the Source Code in an
 * information repository reasonably calculated to permit inexpensive and
 * convenient access by You for as long as Licensor continues to distribute
 * the Original Work.
 *
 * 4) Exclusions From License Grant. Neither the names of Licensor, nor the
 * names of any contributors to the Original Work, nor any of their
 * trademarks or service marks, may be used to endorse or promote products
 * derived from this Original Work without express prior permission of the
 * Licensor. Except as expressly stated herein, nothing in this License
 * grants any license to Licensor's trademarks, copyrights, patents, trade
 * secrets or any other intellectual property. No patent license is granted
 * to make, use, sell, offer for sale, have made, or import embodiments of
 * any patent claims other than the licensed claims defined in Section 2.
 * No license is granted to the trademarks of Licensor even if such marks
 * are included in the Original Work. Nothing in this License shall be
 * interpreted to prohibit Licensor from licensing under terms different
 * from this License any Original Work that Licensor otherwise would have a
 * right to license.
 *
 * 5) External Deployment. The term "External Deployment" means the use,
 * distribution, or communication of the Original Work or Derivative Works
 * in any way such that the Original Work or Derivative Works may be used
 * by anyone other than You, whether those works are distributed or
 * communicated to those persons or made available as an application
 * intended for use over a network. As an express condition for the grants
 * of license hereunder, You must treat any External Deployment by You of
 * the Original Work or a Derivative Work as a distribution under section
 * 1(c).
 *
 * 6) Attribution Rights. You must retain, in the Source Code of any
 * Derivative Works that You create, all copyright, patent, or trademark
 * notices from the Source Code of the Original Work, as well as any
 * notices of licensing and any descriptive text identified therein as an
 * "Attribution Notice." You must cause the Source Code for any Derivative
 * Works that You create to carry a prominent Attribution Notice reasonably
 * calculated to inform recipients that You have modified the Original
 * Work.
 *
 * 7) Warranty of Provenance and Disclaimer of Warranty. Licensor warrants
 * that the copyright in and to the Original Work and the patent rights
 * granted herein by Licensor are owned by the Licensor or are sublicensed
 * to You under the terms of this License with the permission of the
 * contributor(s) of those copyrights and patent rights. Except as
 * expressly stated in the immediately preceding sentence, the Original
 * Work is provided under this License on an "AS IS" BASIS and WITHOUT
 * WARRANTY, either express or implied, including, without limitation, the
 * warranties of non-infringement, merchantability or fitness for a
 * particular purpose. THE ENTIRE RISK AS TO THE QUALITY OF THE ORIGINAL
 * WORK IS WITH YOU. This DISCLAIMER OF WARRANTY constitutes an essential
 * part of this License. No license to the Original Work is granted by this
 * License except under this disclaimer.
 *
 * 8) Limitation of Liability. Under no circumstances and under no legal
 * theory, whether in tort (including negligence), contract, or otherwise,
 * shall the Licensor be liable to anyone for any indirect, special,
 * incidental, or consequential damages of any character arising as a
 * result of this License or the use of the Original Work including,
 * without limitation, damages for loss of goodwill, work stoppage,
 * computer failure or malfunction, or any and all other commercial damages
 * or losses. This limitation of liability shall not apply to the extent
 * applicable law prohibits such limitation.
 *
 * 9) Acceptance and Termination. If, at any time, You expressly assented
 * to this License, that assent indicates your clear and irrevocable
 * acceptance of this License and all of its terms and conditions. If You
 * distribute or communicate copies of the Original Work or a Derivative
 * Work, You must make a reasonable effort under the circumstances to
 * obtain the express assent of recipients to the terms of this License.
 * This License conditions your rights to undertake the activities listed
 * in Section 1, including your right to create Derivative Works based upon
 * the Original Work, and doing so without honoring these terms and
 * conditions is prohibited by copyright law and international treaty.
 * Nothing in this License is intended to affect copyright exceptions and
 * limitations (including "fair use" or "fair dealing"). This License shall
 * terminate immediately and You may no longer exercise any of the rights
 * granted to You by this License upon your failure to honor the conditions
 * in Section 1(c).
 *
 * 10) Termination for Patent Action. This License shall terminate
 * automatically and You may no longer exercise any of the rights granted
 * to You by this License as of the date You commence an action, including
 * a cross-claim or counterclaim, against Licensor or any licensee alleging
 * that the Original Work infringes a patent. This termination provision
 * shall not apply for an action alleging patent infringement by
 * combinations of the Original Work with other software or hardware.
 *
 * 11) Jurisdiction, Venue and Governing Law. Any action or suit relating
 * to this License may be brought only in the courts of a jurisdiction
 * wherein the Licensor resides or in which Licensor conducts its primary
 * business, and under the laws of that jurisdiction excluding its
 * conflict-of-law provisions. The application of the United Nations
 * Convention on Contracts for the International Sale of Goods is expressly
 * excluded. Any use of the Original Work outside the scope of this License
 * or after its termination shall be subject to the requirements and
 * penalties of copyright or patent law in the appropriate jurisdiction.
 * This section shall survive the termination of this License.
 *
 * 12) Attorneys' Fees. In any action to enforce the terms of this License
 * or seeking damages relating thereto, the prevailing party shall be
 * entitled to recover its costs and expenses, including, without
 * limitation, reasonable attorneys' fees and costs incurred in connection
 * with such action, including any appeal of such action. This section
 * shall survive the termination of this License.
 *
 * 13) Miscellaneous. If any provision of this License is held to be
 * unenforceable, such provision shall be reformed only to the extent
 * necessary to make it enforceable.
 *
 * 14) Definition of "You" in This License. "You" throughout this License,
 * whether in upper or lower case, means an individual or a legal entity
 * exercising rights under, and complying with all of the terms of, this
 * License. For legal entities, "You" includes any entity that controls, is
 * controlled by, or is under common control with you. For purposes of this
 * definition, "control" means (i) the power, direct or indirect, to cause
 * the direction or management of such entity, whether by contract or
 * otherwise, or (ii) ownership of fifty percent (50%) or more of the
 * outstanding shares, or (iii) beneficial ownership of such entity.
 *
 * 15) Right to Use. You may use the Original Work in all ways not
 * otherwise restricted or conditioned by this License or by law, and
 * Licensor promises not to interfere with or be responsible for such uses
 * by You.
 *
 * 16) Modification of This License. This License is Copyright (c) 2005
 * Lawrence Rosen. Permission is granted to copy, distribute, or
 * communicate this License without modification. Nothing in this License
 * permits You to modify this License as applied to the Original Work or to
 * Derivative Works. However, You may modify the text of this License and
 * copy, distribute or communicate your modified version (the "Modified
 * License") and apply it to other original works of authorship subject to
 * the following conditions: (i) You may not indicate in any way that your
 * Modified License is the "Academic Free License" or "AFL" and you may not
 * use those names in the name of your Modified License; (ii) You must
 * replace the notice specified in the first paragraph above with the
 * notice "Licensed under <insert your license name here>" or with a notice
 * of your own that is not confusingly similar to the notice in this
 * License; and (iii) You may not claim that your original works are open
 * source software unless your Modified License has been approved by Open
 * Source Initiative (OSI) and You comply with its license review and
 * certification process.
 *
 * @package PSMext
 */
/**
 * Controller for the administrative backend.
 *
 * @author Michael Leiss <m.leiss@techdivision.com>
 * @copyright TechDivision GmbH
 * @package controllers
 */
class Techdivision_Psmext_AdminController extends Mage_Adminhtml_Controller_Action
{
	protected $_logicModel = null;
	protected function getLogic ()
	{
		if ($this->_logicModel == null) {
			$this->_logicModel = Mage::getModel("psmext/logic");
		}
		return $this->_logicModel;
	}
	public function indexAction ()
	{
		$this->loadLayout();
		$this->_setActiveMenu('psmext');
		$this->renderLayout();
	}
	public function overviewAction ()
	{
		$this->loadLayout();
		$this->_setActiveMenu('psmext');
		$this->renderLayout();
	}
	public function functionAction ()
	{
		$this->loadLayout();
		$this->_setActiveMenu('psmext');
		$this->renderLayout();
	}
	public function helpAction ()
	{
		$this->loadLayout();
		$this->_setActiveMenu('psmext');
		$this->renderLayout();
	}
	public function resultAction ()
	{
		$_SESSION['_storeId']=$this->getRequest()->getParam('storeId');
		$this->loadLayout();
		$this->_setActiveMenu('psmext');

		$this->renderLayout();
	}
	public function shippingAction ()
	{
		$this->loadLayout();
		$this->_setActiveMenu('psmext');
		$this->renderLayout();
	}
	public function getshipperAction ()
	{
		echo Mage::helper('psmext')->getShippers();
	}
	public function getshippingvaluesAction ()
	{
		$param = $this->getRequest()->getParam('param');
		echo Mage::helper('psmext')->getShippingValues($param);
	}
	public function delshipperAction ()
	{
		$title = $this->getRequest()->getParam('title');
		$data = Mage::getModel('psmext/shipping')->getCollection()->addFilter('shipping_name', $title);
		//Kontrolle ob Shipper überhaupt existiert
		if (! Mage::helper('psmext')->checkShipperExists($title)) {
			$this->_redirect('psmext/admin/shipping');
			return true;
		}
		foreach ($data as $_model) {
			$_model->delete();
		}

		//array von function ids erstellen
		$function_ids=array();
		$data = Mage::getModel('psmext/functions')->getCollection()->addFilter('function_param1', $title);
		foreach ($data as $_model) {
			array_push($function_ids, $_model->getFunction_id());
			$_model->delete();
		}
		$remove = array();

		$psmext_data = Mage::getModel('psmext/psmext')->getCollection()
		->addFieldToFilter('function_nr', array('in' => $function_ids));


		foreach ($psmext_data as $_data) {
			array_push($remove, $_data->getPsm_set());
		}

		foreach ($remove as $_remove) {
			$collection = Mage::getModel('psmext/psmext')->getCollection()->addFilter('psm_set', $_remove);
			foreach ($collection as $_psmext) {
				$_psmext->delete();
			}
			$collection = Mage::getModel('psmext/separators')->getCollection()->addFilter('psmext_set', $_remove);
			foreach ($collection as $_psmext) {
				$_psmext->delete();
			}
		}
		$this->_redirect('psmext/admin/shipping');
	}
	public function shippingwriteAction ()
	{
		$field_params = $this->getRequest()->getParam('param');
		$title = $this->getRequest()->getParam('title');
		$array = Zend_Json::decode($field_params, Zend_Json::TYPE_OBJECT);
		$bool = Mage::helper('psmext')->checkShipperExists($title);
		if ($bool == true) {
			$data = Mage::getModel('psmext/shipping')->getCollection()->addFilter('shipping_name', $title);
			foreach ($data as $_model) {
				$_model->delete();
			}
		}
		$bool_google = Mage::helper('psmext')->checkShipperExists( $title."-Google");
		if ($bool_google == true) {
			$data_google = Mage::getModel('psmext/shipping')->getCollection()->addFilter('shipping_name', $title);
			foreach ($data_google as $_model) {
				$_model->delete();
			}
		}
		foreach ($array as $_shipping) {
			$shipping = Mage::getModel('psmext/shipping');
			$shipping->setShipping_name($title);
			if (! empty($_shipping->mlength)) {
				$shipping->setMlength($_shipping->mlength);
			}
			if (! empty($_shipping->mwidth)) {
				$shipping->setMwidth($_shipping->mwidth);
			}
			if (! empty($_shipping->mheight)) {
				$shipping->setMheight($_shipping->mheight);
			}
			if (! empty($_shipping->mweight)) {
				$shipping->setMweight($_shipping->mweight);
			}
			if (! empty($_shipping->mprice)) {
				$shipping->setMprice($_shipping->mprice);
			}
			if (! empty($_shipping->cat)) {
				$shipping->setCat($_shipping->cat);
			}
			if (! empty($_shipping->price)) {
				$shipping->setPrice($_shipping->price);
			}
			$shipping->save();
		}

		$data = Mage::getModel('psmext/functions')->getCollection()->addFilter('function_param1', $title);
		$k = 0;
		foreach ($data as $_model) {
			++ $k;
		}
		$create=0;
		if ($k == 0) {
			//General Shipping Function
			$function_model = Mage::getModel('psmext/functions');
			$function_model->setData('function_description', $title);
			$function_model->setData('function_name', 'getProductShipperPrice');
			$function_model->setData('function_param1', $title);
			$function_model->save();
			$create = 1;
		}
		if ($create == 1){
			//Google Base Function
			$function_model = Mage::getModel('psmext/functions');
			$function_model->setData('function_description', $title."-Google");
			$function_model->setData('function_name', 'getProductShipperPriceGoogle');
			$function_model->setData('function_param1', $title);
			$function_model->save();
		}

		$this->_redirect('psmext/admin/shipping');
	}
	public function receiveAction ()
	{
		$field_params = $this->getRequest()->getParam('param');
		$psmset = $this->getRequest()->getParam('psmset');
		$fields = explode(',', $field_params);
		$separator = $this->getRequest()->getParam('psmseparator');
		if ($separator == "TAB") {
			$separator = "\t";
		}
		$headline = $this->getRequest()->getParam('headline');
		if (strpos($headline, "TAB")) {
			$headline = str_replace(" TAB ", "\t", $headline);
		}
		$psmext_separator = Mage::getModel('psmext/separators');
		$psmext_separator->setData('psmext_set', $psmset);
		$psmext_separator->setData('psmext_separator', $separator);
		$psmext_separator->setData('psmext_headline', $headline);
		$psmext_separator->save();
		$i = 0;
		foreach ($fields as $_field) {
			$psmext = Mage::getModel('psmext/psmext');
			$psmext->setData('psm_set', $psmset);
			$psmext->setData('function_nr', $_field);
			$psmext->setData('position_nr', $i);
			$psmext->save();
			$i ++;
		}
		$this->_redirect('psmext/admin/overview');
	}
	public function removeAction ()
	{
		$searchengine = $this->getRequest()->getParam('param');
		$collection = Mage::getModel('psmext/psmext')->getCollection()->addFilter('psm_set', $searchengine)->getItems();
		foreach ($collection as $_psmext) {
			$_psmext->delete();
		}
		$collection = Mage::getModel('psmext/separators')->getCollection()->addFilter('psmext_set', $searchengine)->getItems();
		foreach ($collection as $_psmext) {
			$_psmext->delete();
		}
		$this->_redirect('psmext/admin/overview');
	}
	public function delfileAction ()
	{
		$file = $this->getRequest()->getParam('param');
		$storeId = $this->getRequest()->getParam('storeId');
		unlink(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext/'.$storeId.'/'. $file);
		$this->_redirect('psmext/admin/result', array('storeId'=>$storeId));
	}
	public function saveAction ()
	{
		$params = $this->getRequest()->getParams();
		if ($params['id'] == '') {
			$functions = Mage::getModel('psmext/functions');
		} else {
			$functions = Mage::getModel('psmext/functions')->load($params['id']);
		}
		$functions->setData('function_description', $params['name']);
		$functions->setData('function_name', $params['func']);
		$functions->setData('function_param1', $params['attr']);
		$functions->save();
		$this->_redirect('psmext/admin/function');
	}
	/**
	 * Deletes the function.
	 *
	 * @return string The path to redirect to
	 */
	public function deleteAction ()
	{
		// load the request parameters
		$params = $this->getRequest()->getParams();
		// check if the recommended id exists
		if (! array_key_exists("id", $params)) {
			// if not throw an Exception
			throw new Exception('Recommended id does not exist in request');
		} else {
			$id = $params["id"];
		}
		// load the model and delete the function
		$model = Mage::getModel('psmext/functions');
		$model->deleteFunction($id);
		// return to path to the function overview
		$this->_redirect('psmext/admin/function');
	}
	public function writeAction ()
	{
		$searchengine = $this->getRequest()->getParam('param');
		$counterstat = $this->getRequest()->getParam('counterstat');
		$store_id=$this->getRequest()->getParam('storeId');
		Mage::getModel('psmext/logic')->write($searchengine, $counterstat, $store_id);
	}
	public function writeFreewareAction ()
	{
		$searchengine = $this->getRequest()->getParam('param');
		$counterstat = $this->getRequest()->getParam('counterstat');
		$store_id=$this->getRequest()->getParam('storeId');
		Mage::getModel('psmext/logic')->write($searchengine, $counterstat, $store_id);
		$this->_redirect('psmext/admin/result');
	}

	public function getdirlistingAction(){
		$store = $this->getRequest()->getParam('storeId');
		$return =  Mage::helper('psmext')->getDirListing($store);
		echo $return;
	}

	public function copyAction()
	{
		$searchengine = $this->getRequest()->getParam('param');
		$storeId = $this->getRequest()->getParam('storeId');

		copy(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext_temp/'.$storeId.'/'. $searchengine, Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext/'.$storeId.'/'.$searchengine);
		unlink(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext_temp/' .$storeId.'/'. $searchengine);
		$this->_redirect('psmext/admin/result', array('storeId'=>$storeId));
	}
	public function deltempfileAction ()
	{
		$searchengine = $this->getRequest()->getParam('param');
		$storeId = $this->getRequest()->getParam('storeId');
		if (file_exists(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext_temp/'.$storeId.'/'.$searchengine)) {
			unlink(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/psmext/psmext_temp/'.$storeId.'/'.$searchengine);
		}
	}
}