<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */

error_reporting(E_ALL);
 
require_once 'Mage/Contacts/controllers/IndexController.php';

require_once(Mage::getBaseDir('lib') . '/PHPMailer/class.phpmailer.php');

class LBI_Contacts_IndexController extends Mage_Contacts_IndexController
{
    public function postAction()
    { 
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
				
				$recipient_email = Mage::getStoreConfig('contacts/email/recipient_email');
				$smtp_user       = Mage::getStoreConfig('contacts/smtp_email/smtp_user');
				$smtp_pass       = Mage::getStoreConfig('contacts/smtp_email/smtp_pass');
				
				if (!isset($smtp_user) || empty($smtp_user) || !isset($smtp_pass) || empty($smtp_pass)) {
					$mailTemplate = Mage::getModel('core/email_template');
					$mailTemplate->setDesignConfig(array('area' => 'frontend'))
						->setReplyTo($post['email'])
						->sendTransactional(
							Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
							Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
							Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
							null,
							array('data' => $postObject)
						);

					if (!$mailTemplate->getSentSuccess()) {
						throw new Exception();
					}
				} else {
					$mail = new PHPMailer;
					$mail->IsSMTP();                                      
					$mail->Host = 'smtp.gmail.com';  
					$mail->Port = 465;  
					$mail->SMTPAuth = true;                               
					$mail->Username = $smtp_user;                            
					$mail->Password = $smtp_pass;                           
					$mail->SMTPSecure = 'ssl'; 
					$mail->From = $post['email'];
					$mail->FromName = $post['name'];
					$mail->AddAddress($recipient_email);
					$mail->IsHTML(true);                                 
					$mail->Subject = 'druckerpatronen.de Contact Form Message';
					$mail->Body    = "From Name: " . $post['name'] . "<br /><br />" . "From Email: " . $post['email'] . "<br /><br />" . "Comment: " . $post['comment'];
					if(!$mail->Send()) {
					   throw new Exception();
					}
				}
				
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }

}
