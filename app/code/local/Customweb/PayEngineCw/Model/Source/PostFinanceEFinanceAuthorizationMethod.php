<?php
class Customweb_PayEngineCw_Model_Source_PostFinanceEFinanceAuthorizationMethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'PaymentPage', 'label'=>Mage::helper('adminhtml')->__("Payment Page"))
		);
		return $options;
	}
}
