<?php
class Customweb_PayEngineCw_Model_Source_Capturing{
	public function toOptionArray(){
		$options = array(
			array('value'=>'direct', 'label'=>Mage::helper('adminhtml')->__("Directly after order")),
			array('value'=>'deferred', 'label'=>Mage::helper('adminhtml')->__("Deferred"))
		);
		return $options;
	}
}
