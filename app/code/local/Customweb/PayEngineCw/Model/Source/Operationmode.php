<?php
class Customweb_PayEngineCw_Model_Source_Operationmode{
	public function toOptionArray(){
		$options = array(
			array('value'=>'test', 'label'=>Mage::helper('adminhtml')->__("Test Mode")),
			array('value'=>'live', 'label'=>Mage::helper('adminhtml')->__("Live Mode"))
		);
		return $options;
	}
}
