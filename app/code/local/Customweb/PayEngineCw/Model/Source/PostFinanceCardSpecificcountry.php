<?php
class Customweb_PayEngineCw_Model_Source_PostFinanceCardSpecificcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'CH', 'label'=>Mage::helper('adminhtml')->__("Switzerland"))
		);
		return $options;
	}
}
