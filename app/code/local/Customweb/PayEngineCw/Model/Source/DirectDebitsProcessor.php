<?php
class Customweb_PayEngineCw_Model_Source_DirectDebitsProcessor{
	public function toOptionArray(){
		$options = array(
			array('value'=>'telego', 'label'=>Mage::helper('adminhtml')->__("Telego (Direct Debits DE)")),
			array('value'=>'equens', 'label'=>Mage::helper('adminhtml')->__("Equens (Direct Debits NL)"))
		);
		return $options;
	}
}
