<?php
class Customweb_PayEngineCw_Model_Source_Threedsecurebehavior{
	public function toOptionArray(){
		$options = array(
			array('value'=>'always', 'label'=>Mage::helper('adminhtml')->__("Mark all transactions as uncertain, except
							those from countries listed below.
						")),
			array('value'=>'never', 'label'=>Mage::helper('adminhtml')->__("Do never mark the transaction as uncertain,
							except those from countries listed below.
						"))
		);
		return $options;
	}
}
