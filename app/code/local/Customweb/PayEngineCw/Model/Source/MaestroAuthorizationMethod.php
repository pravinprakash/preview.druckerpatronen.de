<?php
class Customweb_PayEngineCw_Model_Source_MaestroAuthorizationMethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'PaymentPage', 'label'=>Mage::helper('adminhtml')->__("Payment Page")),
			array('value'=>'HiddenAuthorization', 'label'=>Mage::helper('adminhtml')->__("Hidden Authorization (Alias Gateway)"))
		);
		return $options;
	}
}
