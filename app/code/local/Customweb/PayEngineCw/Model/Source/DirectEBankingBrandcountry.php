<?php
class Customweb_PayEngineCw_Model_Source_DirectEBankingBrandcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'at', 'label'=>Mage::helper('adminhtml')->__("Austria (AT)")),
			array('value'=>'be', 'label'=>Mage::helper('adminhtml')->__("Belgium (BE)")),
			array('value'=>'ch', 'label'=>Mage::helper('adminhtml')->__("Switzerland (CH)")),
			array('value'=>'no_code', 'label'=>Mage::helper('adminhtml')->__("Germany (no code)")),
			array('value'=>'de', 'label'=>Mage::helper('adminhtml')->__("Germany (DE)")),
			array('value'=>'fr', 'label'=>Mage::helper('adminhtml')->__("France (FR)")),
			array('value'=>'gb', 'label'=>Mage::helper('adminhtml')->__("Great Britain (GB)")),
			array('value'=>'it', 'label'=>Mage::helper('adminhtml')->__("Italy (IT)")),
			array('value'=>'nl', 'label'=>Mage::helper('adminhtml')->__("Netherlands (NL)"))
		);
		return $options;
	}
}
