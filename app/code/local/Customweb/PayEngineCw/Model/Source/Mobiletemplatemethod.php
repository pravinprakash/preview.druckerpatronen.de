<?php
class Customweb_PayEngineCw_Model_Source_Mobiletemplatemethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'default', 'label'=>Mage::helper('adminhtml')->__("Use standard template (as selected above)
				")),
			array('value'=>'custom', 'label'=>Mage::helper('adminhtml')->__("Use own template")),
			array('value'=>'customweb', 'label'=>Mage::helper('adminhtml')->__("Use customweb hosted template"))
		);
		return $options;
	}
}
