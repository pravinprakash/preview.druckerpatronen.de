<?php
class Customweb_PayEngineCw_Model_Source_Template{
	public function toOptionArray(){
		$options = array(
			array('value'=>'default', 'label'=>Mage::helper('adminhtml')->__("Use shop template")),
			array('value'=>'custom', 'label'=>Mage::helper('adminhtml')->__("Use own template")),
			array('value'=>'none', 'label'=>Mage::helper('adminhtml')->__("Don't change the layout of the payment page
				"))
		);
		return $options;
	}
}
