<?php
class Customweb_PayEngineCw_Model_Source_OpenInvoiceSpecificcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'AT', 'label'=>Mage::helper('adminhtml')->__("Austria")),
			array('value'=>'CH', 'label'=>Mage::helper('adminhtml')->__("Switzerland")),
			array('value'=>'DE', 'label'=>Mage::helper('adminhtml')->__("Germany")),
			array('value'=>'FI', 'label'=>Mage::helper('adminhtml')->__("Finland")),
			array('value'=>'NL', 'label'=>Mage::helper('adminhtml')->__("Netherlands")),
			array('value'=>'NO', 'label'=>Mage::helper('adminhtml')->__("Norway")),
			array('value'=>'SE', 'label'=>Mage::helper('adminhtml')->__("Sweden"))
		);
		return $options;
	}
}
