<?php
class Customweb_PayEngineCw_Model_Source_OpenInvoiceBrandcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'at', 'label'=>Mage::helper('adminhtml')->__("Austria (AT)")),
			array('value'=>'ch', 'label'=>Mage::helper('adminhtml')->__("Switzerland (CH)")),
			array('value'=>'de', 'label'=>Mage::helper('adminhtml')->__("Germany (DE)")),
			array('value'=>'dk', 'label'=>Mage::helper('adminhtml')->__("Denmark (DK)")),
			array('value'=>'fi', 'label'=>Mage::helper('adminhtml')->__("Finland (FI)")),
			array('value'=>'nl', 'label'=>Mage::helper('adminhtml')->__("Netherlands (NL)")),
			array('value'=>'no', 'label'=>Mage::helper('adminhtml')->__("Norway (NO)")),
			array('value'=>'se', 'label'=>Mage::helper('adminhtml')->__("Sweden (SE)"))
		);
		return $options;
	}
}
