<?php
class Customweb_PayEngineCw_Model_Source_PostFinanceEFinanceSpecificcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'CH', 'label'=>Mage::helper('adminhtml')->__("Switzerland"))
		);
		return $options;
	}
}
