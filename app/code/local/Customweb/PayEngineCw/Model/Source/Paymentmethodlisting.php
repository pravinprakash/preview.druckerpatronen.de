<?php
class Customweb_PayEngineCw_Model_Source_Paymentmethodlisting{
	public function toOptionArray(){
		$options = array(
			array('value'=>'0', 'label'=>Mage::helper('adminhtml')->__("Horizontally grouped logos with the name on
							the left side
						")),
			array('value'=>'1', 'label'=>Mage::helper('adminhtml')->__("Horizontally grouped logos with no group
							names
						")),
			array('value'=>'2', 'label'=>Mage::helper('adminhtml')->__("Vertical list of logos with the name on the
							left side
						"))
		);
		return $options;
	}
}
