<?php
class Customweb_PayEngineCw_Model_Source_OpenInvoiceCurrency{
	public function toOptionArray(){
		$options = array(
			array('value'=>'EUR', 'label'=>Mage::helper('adminhtml')->__("Euro (EUR)")),
			array('value'=>'CHF', 'label'=>Mage::helper('adminhtml')->__("Swiss franc (CHF)"))
		);
		return $options;
	}
}
