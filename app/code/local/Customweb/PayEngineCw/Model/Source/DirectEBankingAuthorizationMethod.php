<?php
class Customweb_PayEngineCw_Model_Source_DirectEBankingAuthorizationMethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'PaymentPage', 'label'=>Mage::helper('adminhtml')->__("Payment Page"))
		);
		return $options;
	}
}
