<?php
class Customweb_PayEngineCw_Model_Source_OpenInvoiceProcessor{
	public function toOptionArray(){
		$options = array(
			array('value'=>'afterpay', 'label'=>Mage::helper('adminhtml')->__("AfterPay")),
			array('value'=>'billpay', 'label'=>Mage::helper('adminhtml')->__("Billpay")),
			array('value'=>'PostFinanceFIS', 'label'=>Mage::helper('adminhtml')->__("PostFinance FIS")),
			array('value'=>'klarna', 'label'=>Mage::helper('adminhtml')->__("Klarna")),
			array('value'=>'ratepay', 'label'=>Mage::helper('adminhtml')->__("RatePay"))
		);
		return $options;
	}
}
