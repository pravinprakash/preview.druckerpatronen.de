<?php
class Customweb_PayEngineCw_Model_Source_Countrycheck{
	public function toOptionArray(){
		$options = array(
			array('value'=>'inactive', 'label'=>Mage::helper('adminhtml')->__("Inactive")),
			array('value'=>'all', 'label'=>Mage::helper('adminhtml')->__("All country codes must match.")),
			array('value'=>'ip_country_code_issuer_code', 'label'=>Mage::helper('adminhtml')->__("IP country code and issuer country code must
							match.
						")),
			array('value'=>'ip_country_code_billing_code', 'label'=>Mage::helper('adminhtml')->__("IP country and billing country code must
							match.
						")),
			array('value'=>'issuer_code_billing_code', 'label'=>Mage::helper('adminhtml')->__("Issuer country code and billing country code.
						"))
		);
		return $options;
	}
}
