<?php
class Customweb_PayEngineCw_Model_Source_Creditcardbrands{
	public function toOptionArray(){
		$options = array(
			array('value'=>'visa', 'label'=>Mage::helper('adminhtml')->__("VISA")),
			array('value'=>'mastercard', 'label'=>Mage::helper('adminhtml')->__("MasterCard")),
			array('value'=>'americanexpress', 'label'=>Mage::helper('adminhtml')->__("American Express")),
			array('value'=>'jcb', 'label'=>Mage::helper('adminhtml')->__("JCB")),
			array('value'=>'maestro', 'label'=>Mage::helper('adminhtml')->__("Maestro")),
			array('value'=>'cartebleu', 'label'=>Mage::helper('adminhtml')->__("Carte Bleue")),
			array('value'=>'solo', 'label'=>Mage::helper('adminhtml')->__("Solo Card"))
		);
		return $options;
	}
}
