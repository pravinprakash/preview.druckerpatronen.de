<?php
class Customweb_PayEngineCw_Model_Source_DirectEBankingCurrency{
	public function toOptionArray(){
		$options = array(
			array('value'=>'EUR', 'label'=>Mage::helper('adminhtml')->__("Euro (EUR)"))
		);
		return $options;
	}
}
