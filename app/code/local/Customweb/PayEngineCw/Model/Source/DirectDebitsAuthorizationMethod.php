<?php
class Customweb_PayEngineCw_Model_Source_DirectDebitsAuthorizationMethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'PaymentPage', 'label'=>Mage::helper('adminhtml')->__("Payment Page")),
			array('value'=>'ServerAuthorization', 'label'=>Mage::helper('adminhtml')->__("Server Authorization"))
		);
		return $options;
	}
}
