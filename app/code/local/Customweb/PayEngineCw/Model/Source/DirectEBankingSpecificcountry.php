<?php
class Customweb_PayEngineCw_Model_Source_DirectEBankingSpecificcountry{
	public function toOptionArray(){
		$options = array(
			array('value'=>'AT', 'label'=>Mage::helper('adminhtml')->__("Austria")),
			array('value'=>'BE', 'label'=>Mage::helper('adminhtml')->__("Belgium")),
			array('value'=>'CH', 'label'=>Mage::helper('adminhtml')->__("Switzerland")),
			array('value'=>'DE', 'label'=>Mage::helper('adminhtml')->__("Germany")),
			array('value'=>'FR', 'label'=>Mage::helper('adminhtml')->__("France")),
			array('value'=>'GB', 'label'=>Mage::helper('adminhtml')->__("United Kingdom")),
			array('value'=>'IT', 'label'=>Mage::helper('adminhtml')->__("Italy")),
			array('value'=>'NL', 'label'=>Mage::helper('adminhtml')->__("Netherlands"))
		);
		return $options;
	}
}
