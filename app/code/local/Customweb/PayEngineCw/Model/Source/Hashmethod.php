<?php
class Customweb_PayEngineCw_Model_Source_Hashmethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'sha1', 'label'=>Mage::helper('adminhtml')->__("SHA-1")),
			array('value'=>'sha256', 'label'=>Mage::helper('adminhtml')->__("SHA-256")),
			array('value'=>'sha512', 'label'=>Mage::helper('adminhtml')->__("SHA-512"))
		);
		return $options;
	}
}
