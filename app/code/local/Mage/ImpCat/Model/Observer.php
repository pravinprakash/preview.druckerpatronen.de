<?php
class Mage_ImpCat_Model_Observer extends Mage_Core_Model_Abstract {

        /*public function __construct () {
                $this->_init('extension/observer');
                [...]
                parent::__construct();
        }*/

        public function import($schedule) {

                // So wird die in der Admin gesetzte Einstellung eingelesen
                #$importpath = Mage::getStoreConfig('mage_impcat/setup/path_to_import_files', $this->_storeId);
                $importpath = "var/import";
                #[...]
                if ($handle = opendir($importpath)) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == "Categories.csv") {
                            if ($this->importFile($file) == true) {
                                    rename($importpath.'/'.$file, $importpath.'/processed/'.$file);
                            }
                        }
                    }
                    closedir($handle);
                }
                // rewrites und index sollten aufgefrischt werden, da dies beim import nicht automatisch geschieht
                #Mage::getSingleton('catalog/url')->refreshRewrites();
                #Mage::getResourceSingleton('catalog/category')->refreshProductIndex();
                #[...]
                return $this;
        }

        function importFile ($filename) {

                // So wird die in der Admin gesetzte Einstellung eingelesen
                #$profileId = Mage::getStoreConfig('mage_impcat/setup/profile_id', $this->_storeId);
                $profileId = 8;

                // über diesen Umweg wird dem CSV-Parser mitgeteilt, welche Datei er einlesen soll
                Mage::app()->getRequest()->setParam('files', $filename);
                $profile = Mage::getModel('dataflow/profile');
                $userModel = Mage::getModel('admin/user');
                $userModel->setUserId(0);
                Mage::getSingleton('admin/session')->setUser($userModel);
                $profile->load($profileId);
                Mage::register('current_convert_profile', $profile, true);
                $profile->run();

                // "Insert value list does not match column list"-Fix
                setlocale(LC_ALL, 'en_US');
                $batchModel = Mage::getSingleton('dataflow/batch');
                if ($batchModel->getId()) {
                        if ($batchModel->getAdapter()) {
                                $batchId = $batchModel->getId();
                                $batchImportModel = $batchModel->getBatchImportModel();
                                $importIds = $batchImportModel->getIdCollection();
                                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);
                                $adapter = Mage::getModel($batchModel->getAdapter());
                                $adapter->setBatchParams($batchModel->getParams());

                                foreach ($importIds as $importId) {

                                        $this->recordCount++;
                                        $batchImportModel->load($importId);
                                        if (!$batchImportModel->getId()) {
                                                $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                                                continue;
                                        }
                                        $importData = $batchImportModel->getBatchData();
                                        try {
                                                $adapter->saveRow($importData);
                                        } catch (Exception $e) {
                                                return false;
                                        }
                                }
                                // damit die vorangegangenen Dateien nicht immer wieder mitimportiert werden:
                                $batchImportModel->deleteCollection();
                        }
                }
                return true;
        }
}
?>