<?php class Mage_ImpCat_Model_Convert_Adapter_Category
    extends Mage_Eav_Model_Convert_Adapter_Entity
{
    protected $_categoryCache = array();

    protected $_stores;

    /**
     * Category display modes
     */
    protected $_displayModes = array( 'PRODUCTS', 'PAGE', 'PRODUCTS_AND_PAGE');

    public function parse()
    {
        $batchModel = Mage::getSingleton('dataflow/batch');
        /* @var $batchModel Mage_Dataflow_Model_Batch */

        $batchImportModel = $batchModel->getBatchImportModel();
        $importIds = $batchImportModel->getIdCollection();

        foreach ($importIds as $importId) {
            //print '<pre>'.memory_get_usage().'</pre>';
            $batchImportModel->load($importId);
            $importData = $batchImportModel->getBatchData();

            $this->saveRow($importData);
        }
    }

    /**
     * Save category (import)
     *
     * @param array $importData
     * @throws Mage_Core_Exception
     * @return bool
     */
    public function saveRow(array $importData)
    {
    
        if (empty($importData['store'])) {
            if (!is_null($this->getBatchParams('store'))) {
                $store = $this->getStoreById($this->getBatchParams('store'));
            } else {
                $message = Mage::helper('catalog')->__('Skip import row, required field "%s" not defined', 'store');
                Mage::throwException($message);
            }
        } else {
            $store = $this->getStoreByCode($importData['store']);
        }

        if ($store === false) {
            $message = Mage::helper('catalog')->__('Skip import row, store "%s" field not exists', $importData['store']);
            Mage::throwException($message);
        }
        
        $rootId = $store->getRootCategoryId();
        if (!$rootId) {
            return array();
        }
        $rootPath = '1/'.$rootId;

        $importData['categories'] = preg_replace('#\s*/\s*#', '/', trim($importData['categories']));
        if (!empty($cache[$importData['categories']])) {
            return true;
        }

        $path = $rootPath;
        $namePath = '';

        $i = 1;
        $categories = explode('/', $importData['categories']);
        
        $catId = 0;

        foreach ($categories as $catName) {
            $catName = trim($catName);
            $catName = str_replace("##", "/", $catName);
            if($catName == "") break;
            $coll = Mage::getResourceModel('catalog/category_collection')->addAttributeToFilter('name', $catName);
            if($catId) $coll->addAttributeToFilter('parent_id', $catId);
            $catIds = $coll->getAllIds(1);
            if(isset($catIds[0])) { # already exists
                $catId = $catIds[0];
            } else {
                $dispMode = $this->_displayModes[2];

                $cat = Mage::getModel('catalog/category')
                    ->setStoreId($store->getId())
                    ->setPath($path)
                    ->setName($catName)
                    ->setIsActive(1)
                    ->setIsAnchor(1)
                    ->setDisplayMode($dispMode)
                    ->save();
                $catId = $cat->getId();
            }
            
            $path .= '/'.$catId;
            $i++;
            
        }
        
        if(empty($cat)) $cat = Mage::getModel('catalog/category')->load($catId);
        
        if(isset($importData['meta_title'])) $cat->setMetaTitle($importData['meta_title'])->getResource()->saveAttribute($cat, "meta_title");
        if(isset($importData['meta_description'])) $cat->setMetaDescription($importData['meta_description'])->getResource()->saveAttribute($cat, "meta_description");
        if(isset($importData['meta_keywords'])) $cat->setMetaKeywords($importData['meta_keywords'])->getResource()->saveAttribute($cat, "meta_keywords");
        if(isset($importData['description'])) $cat->setDescription($importData['description'])->getResource()->saveAttribute($cat, "description");
        if(isset($importData['headline'])) $cat->setHeadline($importData['headline'])->getResource()->saveAttribute($cat, "headline");
        if(isset($importData['printer_manufacturer'])) $cat->setPrinterManufacturer($importData['printer_manufacturer'])->getResource()->saveAttribute($cat, "printer_manufacturer");
        if(isset($importData['type'])) $cat->setType($importData['type'])->getResource()->saveAttribute($cat, "type");
        if(isset($importData['search'])) $cat->setSearch($importData['search'])->getResource()->saveAttribute($cat, "search");
        if(isset($importData['serie'])) $cat->setSerie($importData['serie'])->getResource()->saveAttribute($cat, "serie");
        /*if(isset($importData['default_product'])) {
            $cat->setDefaultProduct($importData['default_product'])->save();
            # echo $cat->getName()." saved...";
        }*/

        return true;
    }

    /**
     * Retrieve store object by code
     *
     * @param string $store
     * @return Mage_Core_Model_Store
     */
    public function getStoreByCode($store)
    {
        $this->_initStores();
        if (isset($this->_stores[$store])) {
            return $this->_stores[$store];
        }
        return false;
    }

    /**
     *  Init stores
     *
     *  @param    none
     *  @return      void
     */
    protected function _initStores ()
    {
        if (is_null($this->_stores)) {
            $this->_stores = Mage::app()->getStores(true, true);
            foreach ($this->_stores as $code => $store) {
                $this->_storesIdCode[$store->getId()] = $code;
            }
        }
    }
}

?> 