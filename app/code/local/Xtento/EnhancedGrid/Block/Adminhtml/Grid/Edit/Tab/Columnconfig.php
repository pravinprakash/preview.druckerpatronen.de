<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2013-10-30T17:25:41+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Adminhtml/Grid/Edit/Tab/Columnconfig.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Adminhtml_Grid_Edit_Tab_Columnconfig extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('enhanced_grid_current_grid');
        $configuredColumns = $model->getConfiguredColumns();

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => Mage::helper('xtento_enhancedgrid')->__('Column: Purchased Items'),
        ));

        $fieldset->addField('show_custom_options', 'select', array(
            'label' => Mage::helper('xtento_enhancedgrid')->__('Show custom options'),
            'name' => 'columns[purchased_items][show_custom_options]',
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value' => $this->_getColumnConfigValue($configuredColumns, 'purchased_items', 'show_custom_options')
        ));

        $fieldset->addField('show_thumbnail', 'select', array(
            'label' => Mage::helper('xtento_enhancedgrid')->__('Show product thumbnail image'),
            'name' => 'columns[purchased_items][show_thumbnail]',
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value' => $this->_getColumnConfigValue($configuredColumns, 'purchased_items', 'show_thumbnail')
        ));

        return parent::_prepareForm();
    }

    private function _getColumnConfigValue($columns, $column, $name)
    {
        if (isset($columns[$column]) && array_key_exists($name, $columns[$column])) {
            return $columns[$column][$name];
        } else {
            return false;
        }
    }
}