<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2013-11-05T12:11:57+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/Backordered.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_Backordered extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderItems = $row->getAllVisibleItems();
        $backOrderItemQty = 0;
        foreach ($orderItems as $orderItem) {
            if ($orderItem->getQtyBackordered() > 0) {
                $backOrderItemQty += $orderItem->getQtyBackordered();
            }
        }
        $columnHtml = 'No';
        if ($backOrderItemQty > 0) {
            $columnHtml = '<span style="font-size:14px; color: red; font-weight: bold;">Yes ('.$backOrderItemQty.')</span>';
        }
        return $columnHtml;
    }
}

?>