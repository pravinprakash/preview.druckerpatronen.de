<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2014-01-08T15:50:15+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/Commenthistory.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_Commenthistory extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';

        $statusHistoryEntries = $row->getAllStatusHistory();
        foreach ($statusHistoryEntries as $statusHistoryEntry) {
            $comment = $statusHistoryEntry->getComment();
            if (!empty($comment)) {
                $html .= $comment . "\n";
            }
        }

        if (Mage::helper('xtento_enhancedgrid')->isMageExport()) {
            return strip_tags($html);
        }

        $html = nl2br(Mage::helper('core/string')->truncate($html, 130, '', $_remainder));

        if ($_remainder) {
            $_id = 'id' . uniqid();
            $html .= '... <span id="' . $_id . '">' . nl2br($_remainder) . '</span>';
            $html .= '<script type="text/javascript">
            $(\'' . $_id . '\').hide();
            $(\'' . $_id . '\').up().observe(\'mouseover\', function(){$(\'' . $_id . '\').show();});
            $(\'' . $_id . '\').up().observe(\'mouseout\',  function(){$(\'' . $_id . '\').hide();});
            </script>
            ';
        }

        return $html;
    }

    /*
     * Return dummy filter.
     */
    public function getFilter()
    {
        return false;
    }
}

?>