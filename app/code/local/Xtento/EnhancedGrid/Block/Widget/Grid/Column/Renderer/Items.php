<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2014-01-08T13:48:59+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/Items.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_Items extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderItems = $row->getAllVisibleItems();
        if (Mage::helper('xtento_enhancedgrid')->isMageExport()) {
            // Export reduced version when exporting orders using the built-in CSV/Excel XML export functionality of Magento
            $productInfo = "";
            foreach ($orderItems as $orderItem) {
                $productInfo .= sprintf("%dx %s - ", round($orderItem->getQtyOrdered()), $orderItem->getName());
            }
            $productInfo = substr($productInfo, 0, -3);
        } else {
            $hasCustomOptions = false;
            if ($this->getColumn()->getShowCustomOptions()) {
                foreach ($orderItems as $orderItem) {
                    $productOptions = $orderItem->getProductOptions();
                    if (isset($productOptions['options']) || isset($productOptions['attributes_info'])) {
                        $hasCustomOptions = true;
                        break;
                    }
                }
            }
            $productInfo = '<div><table class="xtento-item-table"><thead><tr class="xtento-item-tr">';
            if ($this->getColumn()->getShowThumbnail()) {
                $productInfo .= '<th>Image</th>';
            }
            $productInfo .= '<th>Name</th>';
            if ($hasCustomOptions) {
                $productInfo .= '<th>Product Options</th>';
            }
            $productInfo .= '<th>SKU</th>';
            $productInfo .= '<th>Total</th>';
            $productInfo .= '</tr></thead><tbody>';
            foreach ($orderItems as $orderItem) {
                $productInfo .= '<tr class="xtento-item-tr">';
                if ($this->getColumn()->getShowThumbnail()) {
                    try {
                        $pictureUrl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($orderItem->getProductId()), 'small_image')->resize(50);
                    } catch (Exception $e) {
                        $pictureUrl = '';
                    }
                    $productInfo .= '<td><img alt="' . $this->__('No Image') . '" src="' . $pictureUrl . '" style="max-height: 50px" /></td>';
                }
                $productInfo .= '<td>' . $orderItem->getName() . '</td>';
                if ($hasCustomOptions) {
                    $customOptionText = '';
                    $customOptions = '';
                    $productOptions = $orderItem->getProductOptions();
                    if (isset($productOptions['options']))
                        $customOptions = $productOptions['options'];
                    else if (isset($productOptions['attributes_info'])) {
                        $customOptions = $productOptions['attributes_info'];
                    }
                    if ($customOptions) {
                        foreach ($customOptions as $customOption) {
                            $customOptionText .= '<b><i>' . $customOption['label'] . ':</i></b><br /> ';
                            $customOptionText .= $customOption['value'] . '<br />';
                        }
                    }
                    $productInfo .= '<td>' . $customOptionText . '</td>';
                }

                $productInfo .= '<td>' . round($orderItem->getQtyOrdered()) . 'x ' . $orderItem->getSku() . '</td>';
                $productInfo .= '<td>' . Mage::helper('core')->currency($orderItem->getRowTotalInclTax(), true, false) . '</td>';
                $productInfo .= '</tr>';

            }
            $productInfo .= '</tbody></table></div>';
        }
        return $productInfo;
    }
}

?>