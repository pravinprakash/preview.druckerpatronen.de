<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2014-01-07T16:08:57+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/Address.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_Address extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $address = false;
        if ($this->getColumn()->getId() == 'full_billing_address') {
            $address = $row->getBillingAddress();
        }
        if ($this->getColumn()->getId() == 'full_shipping_address') {
            $address = $row->getShippingAddress();
        }
        if ($address) {
            if (Mage::helper('xtento_enhancedgrid')->isMageExport()) {
                return str_replace("\n", " - ", $address->format('text'));
            } else {
                return $address->format('html');
            }
        }
        return "";
    }
}

?>