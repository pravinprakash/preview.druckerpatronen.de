<?php

/**
 * Product:       Xtento_EnhancedGrid (1.2.1)
 * ID:            cdn4i9tG8dYJqr3eNlYZwDAe1e6UZQY1aA34oRmZZhs=
 * Packaged:      2014-01-29T17:33:29+00:00
 * Last Modified: 2014-01-20T13:10:25+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Model/Grid/Processor.php
 * Copyright:     Copyright (c) 2014 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Model_Grid_Processor
{
    public function processBlock($block, $reinitMode = false)
    {
        $gridType = Mage::helper('xtento_enhancedgrid')->getGridBlockType($block);
        // Process grid
        if ($gridType) {
            $columnConfiguration = false;
            Mage::getSingleton('xtento_enhancedgrid/columns')->getAndSaveGridColumns($block, $gridType);
            $gridConfigurationCollection = Mage::getModel('xtento_enhancedgrid/grid')->getCollection();
            $gridConfigurationCollection->addFieldToFilter('type', $gridType);
            $gridConfigurationCollection->addFieldToFilter('enabled', 1);
            foreach ($gridConfigurationCollection as $gridConfiguration) {
                if (in_array(implode("", Mage::getSingleton('admin/session')->getUser()->getRoles()), explode(",", $gridConfiguration->getRoleIds()))) {
                    $columnConfiguration = $gridConfigurationCollection->getFirstItem();
                    break 1;
                }
            }
            // Process columns
            if ($columnConfiguration !== false) {
                $blockColumns = $block->getColumns();
                // Pre-process existing columns to avoid filtering problems
                if (!$reinitMode) {
                    foreach ($blockColumns as $column) {
                        if ($column->getIndex()) {
                            $column->setFilterIndex('`main_table`.' . $column->getIndex());
                        }
                    }
                }
                // Add custom columns
                $customColumns = Mage::getSingleton('xtento_enhancedgrid/columns_custom')->getCustomColumns($columnConfiguration->getType());
                $usedCustomColumns = array();
                $configuredColumns = $columnConfiguration->getConfiguredColumns();
                foreach ($configuredColumns as $columnIndex => $columnData) {
                    if (!$columnData['is_visible']) {
                        if (isset($blockColumns[$columnIndex])) {
                            $block->xtRemoveColumn($columnIndex);
                        }
                    } else {

                        if (!isset($blockColumns[$columnIndex])) {
                            if (!$reinitMode) {
                                foreach ($columnData as $key => $value) {
                                    if (preg_match("/\_default$/", $key)) {
                                        unset($columnData[$key]);
                                    }
                                }
                                if (preg_match('/price/i', $columnData['renderer'])) {
                                    if (!array_key_exists('currency', $columnData)) {
                                        $columnData['currency'] = 'base_currency_code';
                                    }
                                }
                                $block->addColumn($columnIndex, $columnData);
                                if (isset($customColumns[$columnIndex])) {
                                    $usedCustomColumns[$columnIndex] = $customColumns[$columnIndex];
                                }
                            }
                        } else {
                            $block->xtUpdateColumn($columnIndex, $columnData);
                        }
                    }
                }

                if (!$reinitMode) {
                    // Apply column sort order
                    $block->xtResetColumnsOrder();
                    $previousIndex = null;
                    foreach ($configuredColumns as $columnIndex => $columnData) {
                        if (isset($columnData['is_visible']) && $columnData['is_visible']) {
                            if (!is_null($previousIndex)) {
                                $block->addColumnsOrder($columnIndex, $previousIndex);
                            }
                            $previousIndex = $columnIndex;
                        }
                    }
                    #var_dump($block->getColumns());
                    $block->sortColumnsByOrder();

                    Mage::register('xtento_enhancedgrid_block_info', new Varien_Object(array('block' => $block, 'custom_columns' => $usedCustomColumns)), true);
                    $block->xtPrepareCollection();
                    if (!Mage::helper('xtento_enhancedgrid')->isMageExport()) {
                        Mage::unregister('xtento_enhancedgrid_block_info');
                    }
                    #$block->xtCountCollection();
                }
            }
        }
        return $this;
    }
}