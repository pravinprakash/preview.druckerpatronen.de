<?php
/**
 * AutomaticInvoice Observer Class
 *
 * @category Vianetz
 * @package AutomaticInvoice
 * @author Christoph Massmann <C.Massmann@vianetz.com>
 * @license http://www.vianetz.com/license
 */

class Vianetz_AutomaticInvoice_Model_Order_Observer
{
    public function __construct()
    {
    }

    /**
     * Generate invoice
     * 
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function generateInvoice($observer)
    {
        $event = $observer->getEvent();
        $order = $event->getOrder();

        if ($order->canInvoice()) {
            $invoice = $order->prepareInvoice();

            $invoice->register();
            Mage::getModel('core/resource_transaction')
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
               ->save();
               
            $addr = $order->getShippingAddressId();
            $invoice->setShippingAddressId($addr)->save();
            
            $order->addRelatedObject($invoice);

            /*$invoice->setEmailSent(true);
            $invoice->sendEmail(true, '');*/
        }
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
