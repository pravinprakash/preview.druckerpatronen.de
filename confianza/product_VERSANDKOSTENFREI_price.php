<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');



$csv = new Varien_File_Csv();

$csvdata = array();

//(toner = 72912, druc= 72914, extra = 73038)
$csvdata[] = array('sno','id','price','price with tax','Versandkostenfrei');


$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_decimal')." cpei where cpei.value > 33 and cpei.attribute_id = 60 and store_id=0";

$file = 'csv/product_VERSANDKOSTENFREI_price.csv';

$categories = $read->fetchAll($query); 

$i = 1;

foreach($categories as $cat){

    $pid = $cat['entity_id'];
    $price = $cat['value'];
	$price_tax = $price*1.19;
	
    $free_shipping = _getAttr($pid,'free_shipping');

	if( $price_tax < 40 || (int)$free_shipping == 1 ){
		
		continue;
		
	}

    $product_data = array(
							$i,
							$pid,
							$price,
							$price_tax,
							$free_shipping,
							);
	
						
	$i++;
	
  	echo "$i\n";
	
	
	_updatePrice($pid);

	echo $pid."<br>";
	
	
	
    $csvdata[] = $product_data;
	
}
 $csv->saveData($file, $csvdata);
 

function _getAttr($eid,$name){
    
    $ad = _getAttrId($name);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $table = "catalog_product_entity_";

    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}

$atts_ids = array();
function _getAttrId($name){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where entity_type_id = 4 and attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

function _updatePrice($pid){
	
	$ad = _getAttrId('free_shipping');
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

 	$table = "catalog_product_entity_";
	
    $resource = Mage::getSingleton('core/resource');
    $write = $resource->getConnection('core_write');
	
	    $query = 'UPDATE '. $resource->getTableName($table.$type)." SET value='1' where attribute_id = '$aid' and entity_id = '$pid' ";
		$num_rows = $write->exec($query);
		if($num_rows == 0){
			$query1 = 'INSERT INTO '. $resource->getTableName($table.$type)." (entity_type_id,attribute_id,store_id,value,entity_id) VALUES ('4','545','1','1','$pid') ";
			
			
			$write->query($query1);
		}
}

