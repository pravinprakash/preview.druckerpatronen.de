<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

if( isset($_GET['post']) ){
	
	extract($_POST);
	
	if( !isset($pid) || $pid == '' || !isset($ele) || !in_array($ele,array(1,2)) ){
		die('Input Missing');
	}
	
    $resource = Mage::getSingleton('core/resource');
    $write = $resource->getConnection('core_write');
	
 	$table = "catalog_product_entity_";
	
	if( $ele == 1 ){

		$status = 2;
		$visibility = 1;

	}else if( $ele == 2 ){

		$status = 1;
		$visibility = 4;

	}

	$ad_vis = _getAttrId('visibility');
	
    $aid = $ad_vis['attribute_id'];
    $type = $ad_vis['backend_type'];
	
    $query = 'UPDATE '. $resource->getTableName($table.$type)." SET value='$visibility' where attribute_id = '$aid' and entity_id = '$pid' ";

	$write->query($query);

	$ad_sta = _getAttrId('status');

    $aid = $ad_sta['attribute_id'];
    $type = $ad_sta['backend_type'];
	
    $query = 'UPDATE '. $resource->getTableName($table.$type)." SET value='$status' where attribute_id = '$aid' and entity_id = '$pid' ";
    
	if($write->query($query)){
    	die('ok');
	}else{
    	die('failed');
	}
	
}

echo '
		<table>
		<tr>
			<td><b>Sno</b></td>
			<td><b>Name</b></td>
			<td><b>Teilenr</b></td>
			<td><b>Repititions</b></td>
			<td><b>Products</b></td>
		</tr>
	';


$query = 'SELECT *,COUNT( * ) AS repetitions FROM  ' . $resource->getTableName('catalog_product_entity_varchar')." WHERE  `attribute_id` =56 AND  `store_id` = 0 and value != '' GROUP BY  `value` HAVING repetitions >1";
$products = $read->fetchAll($query); 

$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_entity_text')." ccet where ccet.value in ('toner','druckerpatrone') and ccet.attribute_id = 531 group by ccet.entity_id ";
$categories = $read->fetchAll($query); 

$base_url = Mage::getBaseUrl(); 

$i = 1;

$all_products = array();

foreach($categories as $cat){

    $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." ccp where ccp.category_id = '".$cat['entity_id']."'";
    $tmp_products = $read->fetchAll($query); 

	foreach($tmp_products as $tps){
			
		$pid = $tps['product_id'];
		$name = _getAttr($pid,'name');
		$bechlem_partnr = _getAttr($pid,'bechlem_partnr');
		
		if($bechlem_partnr == '' || in_array($pid, $all_products[$name][$bechlem_partnr]) ){
			continue;
		}

		$all_products[$name][$bechlem_partnr][] = $pid;

	}
}

//asort($all_products);

//echo "<pre>";
//print_r($all_products[0]);

foreach($all_products as $name => $same_name){

	foreach($same_name as $man_code => $same_bechlem_partnr){
			
		if( count($same_bechlem_partnr) < 2 ){
			continue;
		}
		
	    $main_html = "<tr>
	    		<td>$i</td>
	    		<td>".$name."</td>
	    		<td>".$man_code."</td>
	    		<td>".count($same_bechlem_partnr)."</td>
	    		<td>
			";
	
		$j = 1;
			$sub_html = '
					<table>
					<tr>
						<td><b>sno</b></td>
						<td><b>id</b></td>
						<td><b>sku</b></td>
						<td><b>manufacturer</b></td>
						<td><b>manufacturer code</b></td>
						<td><b>price</b></td>
						<td><b>link</b></td>
					</tr>
				';
		
		foreach($same_bechlem_partnr as $p){
	
			$pid = $p;
			$sku = _getSku($pid);
		    $man_code = _getAttr($pid,'bechlem_artnr');
		    $manufacturer = _getAttr($pid,'manufacturer');
			$price = _getAttr($pid,'price');
			
			$status = _getAttr($pid,'status');
			
	        if( $status != 1 || _getAttr($pid,'visibility') == 1 ){
	            continue;
	        }
			
			$url = $base_url._getAttr($pid,'url_path');
	
		    $sub_html .= "<tr>
		    		<td>".$j."</td>
		    		<td>".$pid."</td>
		    		<td>".$sku."</td>
		    		<td>".$manufacturer."</td>
		    		<td>".$man_code."</td>
		    		<td>".$price."</td>";
		    $sub_html .= '<td><a href="'.$url.'" target="_blank">Go</a></td>
			    		<td width="500">
			    			<span class="addList" id="addList_'.$pid.'">
				    			<a href="javascript:_markDuplicate(\''.$pid.'\',1)" target="_blank" >Mark Duplicate</a>
			    			</span>
			    			<a href="javascript:_markDuplicate(\''.$pid.'\',2)" target="_blank" class="removeList" id="removeList_'.$pid.'" style="display:none">Not Duplicate</a>
			    			&nbsp;&nbsp;<span id="span_load_'.$pid.'" ></span>
		    			</td>
			    	</tr>
					';
	
			$j++;
			
		}
	
		$sub_html .= ' </table>';
		
		if( $j > 2 ){
			echo $main_html;
			echo $sub_html;
		    echo "</td>
				</tr>";
		$i++;
			
		}
				
	
	    //echo $i."\n";
		
		
		if( $i > 10 ){
			//break;
		}
		
	}
}
?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
	function _markDuplicate(pid,ele){
	
		var loading = '';
		if( ele == 1 || ele == 3 ){
			document.getElementById('addList_'+pid).style.display = "none";
			loading = "Adding as duplicate...";
		}else{
			document.getElementById('removeList_'+pid).style.display = "none";
			loading = "Removing as duplicate...";
		}

		$("#span_load_"+pid).html(loading);

		var pass_data = 'pid='+pid+'&ele='+ele;
		
		$.ajax({
			type : "POST",
			url : '<?php echo $base_url; ?>/confianza/duplicate_products.php?post',
			data : pass_data,
			success : function(responseText) {
				if (responseText.trim() == 'ok') {

					$("#span_load_"+pid).html('Done');
					
					if( ele == 1 ){
						document.getElementById('removeList_'+pid).style.display = "block";
					}else{
						document.getElementById('addList_'+pid).style.display = "block";
					}

				}else{

					$("#span_load_"+pid).html(responseText.trim());

					if( ele == 1 ){
						document.getElementById('addList_'+pid).style.display = "block";
					}else{
						document.getElementById('removeList_'+pid).style.display = "block";
					}

				}
			}
		});
		
	}
</script>

<?php
 
function _getSku($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT sku FROM ' . $resource->getTableName('catalog_product_entity')." where entity_id  = '$pid' ";

    return $read->fetchOne($query); 

}

function _getAttr($eid,$name){
    
    $ad = _getAttrId($name);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $table = "catalog_product_entity_";

    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}

$atts_ids = array();
function _getAttrId($name){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where entity_type_id = 4 and attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

