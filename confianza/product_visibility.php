<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_int')." cpei where cpei.attribute_id = 80 and cpei.value = 1 ";
$products = $read->fetchAll($query); 

foreach($products as $pe){

    $pid = $pe['entity_id'];

    $visibility = _getAttr($pid,'visibility');

    if( $visibility == 4 ){
        continue;
    }
 
    $query = "update " . $resource->getTableName('catalog_product_entity_int')." set value = 4 where attribute_id = 85 and entity_id =  $pid";
    $result = $write->query($query); 
    
    echo $pid."\n";
    
    //var_dump($result);
    
}
 
function _getAttrC($pid,$name,$all = false){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    
    if( $all ){
        
        $carray = array();
        
        $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $c_all = $read->fetchAll($query); 
        foreach($c_all as $ca){
            $carray[] = _getAttr($ca['category_id'],$name,'category');
        }
        
        return $carray;
    }else{
        
        $query = 'SELECT category_id FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $cid = $read->fetchOne($query); 
        
        return _getAttr($cid,$name,'category');
        
    }
}
function _getAttr($eid,$name,$type_s='product'){
    
    $ad = _getAttrId($name);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    
    if( $type_s == 'product' ){

        $table = "catalog_product_entity_";

    }else{
        $table = "catalog_category_entity_";
        
    }
    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}
$atts_ids = array();
function _getAttrId($name){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

/*
//Magento does not load all attributes by default
//Add as many as you like
$products->addAttributeToSelect('name');
foreach($products as $product) {
//do something
}*/
