<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$file = '../feeds/printer_feed.csv';
$csv = new Varien_File_Csv();

$csvdata = array();

$csvdata[] = array('id','title','Costum Label','type','description','availability','google_product_category','condition','brand','color','image_link','link','Mpn','EAN','Price','Product_type','Shipping(time)','Shipping(Country:Price)','Adwords_labels','Sku');

$csv->setDelimiter(',');

//$query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product').' cp , ' . $resource->getTableName('catalog_product_entity_varchar')." cpev where cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') and cp.product_id = cpev.entity_id ";
$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_varchar')." cpev, " . $resource->getTableName('catalog_category_product')." cpp where cpp.product_id = cpev.entity_id and cpev.attribute_id = 521 and cpev.value in ('toner','druckerpatrone') ORDER BY value_id desc";
$products = $read->fetchAll($query); 

$pobj = Mage::getSingleton('catalog/product');

$i = 1;

$base_url = Mage::getBaseUrl();
foreach($products as $pe){

    $pid = $pe['entity_id'];
    $category_id = $pe['category_id'];

    $product = Mage::getModel('catalog/product')->load($pid); 
    
    if( $product->getStatus() != 1){
        continue;
    }
    
    $sku = _getSku($pid);

    $user_defined = _getDisplay($sku);
    $defined1 = $user_defined['defined1'];
    $brand = $user_defined['Marke'];
 
    $original = _getAttr($pid,'original');
	$compatible = _getAttr($pid,'compatible');
    $color    = _getAttr($pid,'color');

    $ean    = _getAttr($pid,'ean');
    $mpn    = _getAttr($pid,'bechlem_artnr');

    $img    = _getAttr($pid,'img_big');
    $delivery_time    = _getAttr($pid,'delivery_time');

	$delivery_time = substr_replace($delivery_time ,"",-1);
		
    $printer    = _getAttrC($pid,'name');
    
    $printer_all_array  = array_diff(_getAttrC($pid,'name',true),array($printer) );

    $imps = ", ";
    if( $brand != '' )
    $imps = $imps.$brand." ";
    
    $printer_all = implode($imps,array_unique($printer_all_array) );
    
    if( $printer_all != '' )
    $printer_all = $brand." ".$printer_all;
    
    $serie      = _getAttrC($pid,'serie');

//defined1

//
    $url_path_c = _getAttrC($pid,'url_path');
    $url_path_ca = explode("/",$url_path_c);

    if( in_array($url_path_ca[0], array('druckerpatrone','toner','extra','farbband','thermotransferfolie') ) ){
        continue;
    }
    
    $url_complete = $base_url.$url_path_c."/"._getAttr($pid,'url_key').".html";
    
    $repace_clr = false;
    $pack = "";
    $pack_type = substr($sku, 5, 2);
    
    if( $pack_type == 'dp' ){
        
        $pack = "Doppelpack";
        $repace_clr = true;

        if($pe['value'] == 'toner'){
            $img = $base_url."/products_images/dummies/dummie_medium_toner_schwarz_doppelpack.png";
        }else if($pe['value'] == 'druckerpatrone'){
            $img=$base_url."/products_images/dummies/dummie_medium_druckerpatrone_schwarz_doppelpack.png";
        } 

    }else if( $pack_type == 'sp' ){

        $pack_post = substr($sku, 7, strlen($sku));

        $pack = "sparset".$pack_post;
        $repace_clr = true;
		
		if($pe['value'] == 'toner'){
			$pack = "sparpack".$pack_post;
			$img = "http://www.druckerpatronen.de/products_images/dummies/dummie_medium_toner_schwarz_".$pack.".png";
		}else if($pe['value'] == 'druckerpatrone'){
			$img="http://www.druckerpatronen.de/products_images/dummies/dummie_medium_druckerpatrone_schwarz_".$pack.".png";
		} 

    }
    
    if( $repace_clr ){
        
        $color_array = explode(" ",$color);
        array_pop($color_array);
        $color = implode(" ", $color_array);
        
    }
    
    $original_t = $original_d = "";

   	 if( $original == '1' ){
    	 if($compatible != '0' || (round($product->getPrice() * 0.25, 2) > 10) ){

    		continue;
		}
		
		
        $original_t = $original_d = "Original";
		

        
    }else{

        $original_d = "Alternativ";

    }
    
    $name_type = $desc_type = "";
    
    if( $pe['value'] == 'toner' ){

		$costum_label ="Drucker_Toner";
        $name_type = "Toner";
        $desc_type = "Günstig Toner";

    }else{

		$costum_label = "Drucker_Druckerpatrone";
        $name_type = "Tinte Patronen";
        $desc_type = "Günstig Druckerpatronen";
        
    }
    
    if( $serie == NULL || $serie == 'NULL' ){
        $serie = "";
    }else{
        if( strtolower(substr($printer, 0, strlen($serie))) == strtolower($serie)) {
            $printer = trim(substr($printer, strlen($serie)));
        } 
    }
    
    if( $defined1 == NULL && $defined1 == 'NULL' ){
        $defined1 = "";
    }

    if( strtolower($brand) == 'hp' ){
        $name = array($brand,$serie,$printer,$name_type,$brand." ".$defined1.",",$original_t,$serie,$printer,$color,$pack);
    }else{
        $name = array($brand,$serie,$printer,$name_type,$defined1.", ".$brand,$original_t,$serie,$printer,$color,$pack);
    }

    $name = array_diff( $name, array( '' ) );

    $name_str = implode(" ",$name);
    if(strlen($name_str) > 70 ){
        unset($name[6]);
        $name_str = implode(" ",$name);
    }
    $desc = array($desc_type,$color,$pack,"für",$brand,$serie,$printer.".","Kaufen Sie",$original_d,$brand,"Patronen",$defined1,"für",$brand,$serie,$printer.",",$printer_all);
    $desc = array_diff( $desc, array( '' ) );
    $desc_str = implode(" ",$desc);
    
    $product_data = array(
                            $i,
                            $name_str,
                            $costum_label,
                            $original_d,
                            $desc_str,
                            'In stock',
                            'Elektronik > Drucker-, Kopierer-, Scanner- & Faxzubehör > Druckerzubehör > Drucker-Verbrauchsmaterial > Toner- & Inkjet-Kartuschen',
                            'new',
                            $brand,
                            strtolower($color),
                            $img,
                            $url_complete,
                            $mpn,
                            $ean,
                            round($product->getPrice() * 1.19, 2),
                            $product->getTypeID(),
                            $delivery_time,
                            'DE:3.90 EUR',
                            "Artikel ".ucfirst($pe['value'])."",
                             $sku,
                         );
 
    $csvdata[] = $product_data;
    
    
    /*if( $i > 100 ){
            
            break;
            
    }*/
    
    echo $i."\n";
    
    $i++;
}
 
$csv->saveData($file, $csvdata);

function _getStock($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT qty FROM ' . $resource->getTableName('cataloginventory_stock_item')." where product_id  = '$pid' ";
    return $read->fetchOne($query); 

}
function _getSku($pid){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = 'SELECT sku FROM ' . $resource->getTableName('catalog_product_entity')." where entity_id  = '$pid' ";

    return $read->fetchOne($query); 

}
function _getDisplay($sku){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    $query = "SELECT * FROM user_defined_table where sku = '$sku' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    return $ad[0];

}
function _getAttrC($pid,$name,$all = false){

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    
    if( $all ){
        
        $carray = array();
        
        $query = 'SELECT * FROM ' . $resource->getTableName('catalog_category_product')." where product_id = '$pid' ";
        $c_all = $read->fetchAll($query); 
        foreach($c_all as $ca){
            $carray[] = _getAttr($ca['category_id'],$name,'category');
        }
        
        return $carray;
    }else{
        
        global $category_id;
        $cid = $category_id; 
        
        return _getAttr($cid,$name,'category');
        
    }
}
function _getAttr($eid,$name,$type_s='product'){
    
    $ad = _getAttrId($name,$type_s);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    
    if( $type_s == 'product' ){

        $table = "catalog_product_entity_";

    }else{
        $table = "catalog_category_entity_";
        
    }
    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}
$atts_ids = array();
function _getAttrId($name,$type_s='product'){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');

    if( $type_s == 'product' ){

        $entity_type_id = "4";

    }else{
        $entity_type_id = "3";
        
    }
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where entity_type_id = $entity_type_id and  attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

/*
//Magento does not load all attributes by default
//Add as many as you like
$products->addAttributeToSelect('name');
foreach($products as $product) {
//do something
}*/
