<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
        
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

require '../app/Mage.php';

$app = Mage::app('default');

/**
 * Get the resource model
*/
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');



$csv = new Varien_File_Csv();

$csvdata = array();

//(toner = 72912, druc= 72914, extra = 73038)
$csvdata[] = array('sno','id',$from,'Inhalt/capacity','Seitenanzahl');

if($from == 'original'){
$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_int')." cpei where ccet.value = 1 and ccet.attribute_id = 526 ";
$file = 'csv/products_original.csv';
}else if($from == 'alternative'){
	$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_varchar')." cpei where ccet.value = 'Alternativ' and ccet.attribute_id = 66 ";
	$file = 'csv/products_alternative.csv';
}

$categories = $read->fetchAll($query); 

$i = 1;

foreach($categories as $cat){

    $cid = $cat['entity_id'];
	
    $man_code = _getAttr($cid,'bechlem_artnr');
	
    $Seitenanzahl = _getAttr($cid,'capacity');

	$inhalt = _getAttr($cid,'content');
	    
    $product_data = array(
							$i,
							$cid,
							$man_code,
							$inhalt,
							$Seitenanzahl,
							
							);

/*
	if($i>20){
    	echo $i."\n";
		break;
	}
*/
	$i++;
	
  
	
    $csvdata[] = $product_data;
	
}
 $csv->saveData($file, $csvdata);
 

function _getAttr($eid,$name){
    
    $ad = _getAttrId($name);
    
    if( !$ad ){
        return false;
    }
    
    $aid = $ad['attribute_id'];
    $type = $ad['backend_type'];

    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $table = "catalog_product_entity_";

    $query = 'SELECT value FROM ' . $resource->getTableName($table.$type)." where attribute_id = '$aid' and entity_id = '$eid' ";

    $ad = $read->fetchOne($query); 

    return $ad;
    
}

$atts_ids = array();
function _getAttrId($name){
    
    global $atts_ids;
    
    if( isset($atts_ids[$name]) ){
        return $atts_ids[$name];
    }
    
    $resource = Mage::getSingleton('core/resource');
    $read  = $resource->getConnection('core_read');
    
    $query = 'SELECT * FROM ' . $resource->getTableName('eav_attribute')." where entity_type_id = 4 and attribute_code = '$name' ";
    $ad = $read->fetchAll($query); 
    
    if( !$ad ){
        return false;
    }
    
    $atts_ids[$name] = $ad[0];
    
    return $atts_ids[$name];
    
}

