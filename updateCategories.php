<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
		
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
require 'app/Mage.php';

try {
    $app = Mage::app('default');
	$file = 'druckerpatronen-de-categories-18-08-2014-15-58.csv';
	
	$handle = fopen($file, "r");
    $printers = $models = array();
	$first = true;
	$count = 1;
	$limit = 10;
	//read file
	while (false !== ($line = fgetcsv($handle, 0, ";"))) 
	{ 
		if($first){
			$first = false;
			continue;
		}
		if(empty($line[1])) continue;
		//
		
		$isModel = (empty($line[8])) ? false : true;
		if($isModel) {
			$data = explode('/', $line[1]);
			$printer = $data[0];
			$line[] = str_replace('##', '/', $data[1]);
			$models[$printer][] = $line;
		} else {
			$line[1] = $printer = str_replace('##', ' ', $line[1]);
			$printers[$printer] = $line;
		}
		
	}
	fclose($handle);
	//echo '<pre>';
	//print_r($printers);
	//exit;
	//update printers and models
	foreach ($models as $printerName => $printerModels)
	{
		//if($count > $limit) break;
		//$count++;
		$md = $printerModels;
		$printer = Mage::getModel('catalog/category')->loadByAttribute('name', $printerName);
		if(empty($printer) || !$printer->getId()){
			$printer = Mage::getModel('catalog/category');
		}
		if(isset($printers[$printerName]))
		{
			$data = $printers[$printerName];
			$printer
				->setName($printerName)
				->setIsActive(1)
				->setIsAnchor(1)
				->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
				->setPrinterManufacturer($data[6])
				->setIncludeInMenu(1)
				->setMetaTitle($data[2])
				->setMetaDescription($data[3])
				->setMetaKeywords($data[4])
				->setHeadline($data[5])
				->setSearch($data[7])
				->save();
			//array consisting of all child categories id
			$children = $printer->getResource()->getAllChildren($printer); 
			if(0 < count($children)){
				//echo '-';
				foreach ($children as $childId){
					$child = Mage::getModel('catalog/category')->load($childId);
					foreach($md as $key => $data){
						if(strtolower($data[11]) == strtolower($child->getName())){
							$child
							->setName($data[11])
							->setIsActive(1)
							->setIsAnchor(1)
							->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
							->setPrinterManufacturer($data[6])
							->setIncludeInMenu(1)
							->setMetaTitle($data[2])
							->setMetaDescription($data[3])
							->setMetaKeywords($data[4])
							->setHeadline($data[5])
							->setSearch($data[7])
							->setType($data[9])
							->setSerie($data[10])
							->setDefaultProduct($data[8])
							->save();
							unset($md[$key]);
						}
					}
				}
			}
			foreach($md as $data){
				$child = Mage::getModel('catalog/category');
				$child
					->setName($data[11])
					->setIsActive(1)
					->setIsAnchor(1)
					->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
					->setPrinterManufacturer($data[6])
					->setIncludeInMenu(1)
					->setMetaTitle($data[2])
					->setMetaDescription($data[3])
					->setMetaKeywords($data[4])
					->setHeadline($data[5])
					->setSearch($data[7])
					->setType($data[9])
					->setSerie($data[10])
					->setDefaultProduct($data[8])
					->save();
				Mage::getResourceModel('catalog/category')->changeParent($child, $printer);	
			}			
		}
	}
	
	
	echo 'end';
	//echo '<pre>';
	//print_r($printers);

} catch (Exception $e) {
    Mage::printException($e);
}

exit(1); 
