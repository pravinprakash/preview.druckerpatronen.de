<?php
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(1);
Mage::getSingleton('customer/session', array('name'=>'frontend'));

/*echo "<pre>";
echo var_dump(Mage::getSingleton('sales/order_config')->getStatuses());
echo var_dump(Mage::helper('payment/data')->getStoreMethods());
echo "</pre>";
exit;*/

/*$model = new Mage_Core_Model_Url_Rewrite();
$model->setIdPath("cachingdropdowns/2")
      ->setTargetPath("cachingdropdowns.php")
      ->setOptions("R")
      ->setDescription("")
      ->setRequestPath("cachingdropdowns2")
      ->setIsSystem(0)
      ->setStoreId(1)
      ->save();
exit;*/

/*echo "before";
$request = new Zend_Controller_Request_Http("http://www.druckerpatronen.de/cachingdropdowns/");
$response = new Zend_Controller_Response_Http("http://www.druckerpatronen.de/cachingdropdowns.php");
$url = new Mage_Core_Model_Url_Rewrite();
$url->rewrite($request, $response)->save();
echo "after";
exit;*/


#Mage::log('test'); #funktioniert nicht... warum?
#throw new Mage_Core_Exception('Unable to find writable var_dir');

# phpinfo(); exit;
# voraussetzung fuer aendern der php.ini: safe_mode muss deaktiviert sein!
# wichtig: 256M reichen definitiv nicht aus!
# total: 235.007458925 s nach test
//if(! ini_set("memory_limit","2048M")) echo "changing memory_limit failed<br>\n";
//if(! ini_set("max_execution_time","3000000")) echo "changing max_execution_time failed<br>\n";
$folder = "cache/dropdown/";
if(! file_exists($folder)) mkdir($folder);
$i = 0;

# first dropdown begin

$time_a[$i] = microtime(true);
$cats2 = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getChildrenCategories(); # um einiges schneller als getStoreCategories()
$ids = array();
foreach($cats2 as $cat2) {
    array_push($ids, $cat2->getId());
}
$cats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($ids)->addOrderField('name');

$inhalt = "<select id=\"select1\" onchange=\"if(this.value != '') sendRequest();\" \">\n";

$topcat_ids = array(3, 144, 153, 260, 290, 296, 298, 429, 764, 784, 786, 837, 1066, 2557, 2657, 3196, 3412);

# erst id-filter und dann select: 5 sek.; erst select und dann id-filter: 1 sek.
$topcats = Mage::getResourceModel('catalog/category_collection')->addAttributeToSelect('name')->addIdFilter($topcat_ids)->addOrderField('name');

$inhalt.= "<option value=\"\">Bitte Hersteller w&auml;hlen...</option>\n";
$inhalt.= "<option value=\"\">-----------------</option>\n";

foreach($topcats as $topcat) {
    $inhalt.= "<option value=\"".$topcat->getId()."\">".$topcat->getName()."</option>\n";
}

$inhalt.= "<option value=\"\">-----------------</option>\n";

foreach ($cats as $cat) {
    $cat_id = $cat->getId();
    if($cat->hasChildren() && ! in_array($cat_id, $topcat_ids)) {
        $subcat_ids = $cat->getChildren();
        $subcats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($subcat_ids);
        foreach($subcats as $subcat) {
            $products = Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($subcat)->addAttributeToFilter('status', 1);
            $product_flag = 0;
            $product_id = $products->getAllIds(1);
            if(isset($product_id[0])) {
                $inhalt.= "<option value=\"".$cat_id."\">".$cat->getName()."</option>\n";
                break;
            }
        }
    }
}
$inhalt.= "</select>";
echo count($cats);

$fp = fopen($folder.'/0', 'w+');
fwrite($fp, $inhalt);
fclose($fp);

$time_b[$i] = microtime(true);
$time[$i] = ($time_b[$i] - $time_a[$i]);

echo "cached first dropdown successfully, ".$time[$i]." s needed<br>\n";
$i++;

# first dropdown end

foreach ($cats as $cat) {
    $time_a[$i] = microtime(true);
    $cat_id = $cat->getId();
    $cat_name = $cat->getName();
    $subcat_ids = $cat->getChildren();
    
    $inhalt = "<select id=\"select2\" style=\"border: 3px solid #e2007a;color: #111111; font-size: 18px; height: 38px;padding: 4px 0 0 0; width: 245px; vertical-align: middle; margin: 0;\" onchange=\"if(this.value != '') { document.location.href=this.value; document.getElementById('loadingpage').style.display = 'block'; }\">\n";
    $inhalt.= "<option value=\"\">Bitte Drucker w&auml;hlen...</option>\n";
    $inhalt.= "<option value=\"\">-----------------</option>\n";

    $subcats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($subcat_ids)->addOrderField('name');

    foreach($subcats as $subcat) {
        $products = Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($subcat)->addAttributeToFilter('status', 1);
        $product_flag = 0;
        $product_id = $products->getAllIds(1);
        if(isset($product_id[0])) {
            $inhalt.= "<option value=\"".$subcat->getUrl()."/\">".$subcat->getName()."</option>\n";
        }
    }

    $inhalt.= "</select>";
    
    #chmod($folder, 0777); # nicht nötig, 0777 wird schon automatisch gesetzt
    
    #$verz = opendir($folder);
    #while($file = readdir($verz)) {
    #    echo $file."<br>";
    #    echo getcwd()." | ".$file."<br>\n";
    #}
    #closedir($verz);
    
    $fp = fopen($folder.'/'.$cat_id, 'w+');
    fwrite($fp, $inhalt);
    fclose($fp);
    
    $time_b[$i] = microtime(true);
    $time[$i] = ($time_b[$i] - $time_a[$i]);
    echo "cached category '".$cat_name."' successfully, ".$time[$i]." s needed<br>\n";
    $i++;
}
echo "total: ".array_sum($time)." s";
?>
