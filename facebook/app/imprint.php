<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Druckerpatronen.de Gutschein</title>
<style type="text/css">
body {
	background-color: #ffffff;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	overflow: hidden;
	font-family: Arial, Tahoma, Verdana, sans-serif;
	font-size: 12px;
	line-height: 16px;
	color: #111111;
}

a {
    color: #009ee0;
    text-decoration: none;
}

a:hover {
    color: #009ee0;
    text-decoration: underline;
}

</style>
</head>

<body>

    <strong>Druckerpatronen.de</strong><br />
    <br />
    MenzeMedia.de GmbH<br />
    Am Hofe 15<br />
    58640 Iserlohn<br />
    Telefon: 02371 / 7786060<br />
    Fax: 02371 / 7786061<br />
    Web: <a href="http://www.druckerpatronen.de" target="_blank">www.druckerpatronen.de</a><br />
    E-Mail: <a href="mailto:info@druckerpatronen.de">info@druckerpatronen.de</a><br />
    <br />
    Finanzamt: Iserlohn<br />
    Steuernummer: 328/5813/1036<br />
    USt.Id.: DE265536437<br />
    <br />
    Geschäftsführer: Frank Menze<br />
    Registergericht: Iserlohn<br />
    HRB-Nummer: 6785<br />
    <br />
    Verantwortlich gemäß § 55 RStV: Frank Menze Am Hofe 15 58640 Iserlohn

</body>
</html>