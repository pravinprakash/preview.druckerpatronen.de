<?php
/* -----------------------------------------------------------------------------------------
   PUPPET - Products UP Professional Export Tool (http://productsup.io/)
   Copyright (c) 2013 Products Up
  
   @package data-mining
   @subpackage magento
   -----------------------------------------------------------------------------------------
 */

header('Content-Type: text/html; charset=utf-8');
session_start();
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);

define ('APP_NAME', 'puppet');
define ('FS_ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
define ('FS_APP', FS_ROOT.APP_NAME.'/');
define ('FS_EXPORT', FS_APP.'export/');
define ('WS_ROOT', $_SERVER['SERVER_NAME'].'/');
define ('WS_APP', WS_ROOT.APP_NAME.'/');
define ('WS_EXPORT', WS_APP.'export/');
define ('SYSTEM_INI', FS_APP.'system.ini.'.TEST_SYSTEM.'.php');
define ('PRODUCTS_ENTITY_ID', 4);
define ('CATEGORIES_ENTITY_ID', 3);

//ini_set("memory_limit","0");
//ini_set(' max_execution_time', "0");


class FileGenerator {
    static public $allCols;
    static function writeFile($file, $content, $timestamp="true", $extension=".csv") {
            if ($timestamp) {
                    $_SESSION['filename'] = $filename = URL."_".date("ymdHi")."_".$file.$extension;
            } else {
                    $_SESSION['filename'] = $filename = URL."_".$file.$extension;
            }
            $fh = fopen(FS_EXPORT.$filename, "w");
            fwrite($fh, $content);
            fclose($fh);
            return array(
                "path" => WS_EXPORT,
                "filename" => $filename
            );
    }
    static function prepareProductsFile($products) {
       $products = self::removeEmptyCols($products);
       $products = self::makeArraysTwoCols($products);
       $temp_cols = self::$allCols;
       foreach ($temp_cols as $col => $value) {
           $cols[] = $col;
       }
       foreach ($products as $product) {
           foreach ($cols as $col) {
               $temp_product[$col] = $product[$col];
           }
           $temp_products[] = $temp_product;
       }
       unset($products);
       $products = $temp_products;
       $output = implode(";", $cols)."\n";
       foreach ($products as $product) {
           $output .= implode(";", $product)."\n";
       }
       return $output;
   }
   static function removeEmptyCols($products) {
       foreach ($products as $product) {
           foreach ($product as $column => $value) {
               $colcounter[$column] += ($value != "") ? 1 : 0; 
           }
       }
       foreach ($products as $product) {
           foreach ($colcounter as $col => $value) {
               if ($value <= MIN_ATTR_TRESHHOLD) {
                   unset($product[$col]);
               }
           }
           $products_temp[] = $product;
       }
       return $products_temp;
   }
   static function makeArraysTwoCols($products) {
       foreach ($products as $product) {
           $temp_product = $product;
           foreach ($product as $col => $value) {
               $columns[$col]++;
               if (is_array($value)) {
                   $temp_product[$col] = $value['id'];
                   $temp_product[$col."_value"] = $value['value'];
                   $columns[$col."_value"]++;
               }
           }
           $products_temp[] = $temp_product;
       }
       self::$allCols = $columns;
       return $products_temp;
   }
}

class Magento {
    protected $name = 'Magento';
    protected $db_host;
    protected $db_user;
    protected $db_pass;
    protected $db_name;
    protected $tablenames;
    private $entity_id;
    public function __construct($host, $user, $pass, $name) {
        $this->setDbHost($host);
        $this->setDbUser($user);
        $this->setDbPass($pass);
        $this->setDbName($name);
        $this->dbConnect(DB_SYSTEM);
    }
    public function getName() { return $this->name; }
    public function setDbHost($host) { $this->db_host = $host; }
    public function setDbUser($user) { $this->db_user = $user; }
    public function setDbPass($pass) { $this->db_pass = $pass; }
    public function setDbName($name) { $this->db_name = $name; }
    protected function dbConnect($system = "mysql") {
        switch ($system) {
            case "mysql":
            default:
                $this->connection = mysql_connect($this->db_host, $this->db_user, $this->db_pass);
                mysql_select_db($this->db_name);
                if (!$this->connection) {
                    die('Connect Error (' . mysql_errno() . ') '. mysql_error());
                }
                break;
        }
    }
    public function getProducts() { return $this->getEntity(PRODUCTS_ENTITY_ID); }
    public function getCategories() { return $this->getEntity(CATEGORIES_ENTITY_ID); }
    public function getEntityId() { return $this->entity_id; }
    private function getEntity($entity_id) {
        $this->entity_id = $entity_id;
        switch($entity_id) {
            case "4":
                $this->entity_name = "product";
                break;
            case "3":
                $this->entity_name = "category";
                break;
        }
        if (!isset($this->entity_name) || $this->entity_name == "") {
            $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."Entity Name undefined";
            return false;
        }
        if ($this->getEntityAttributeCodes()) {
            if ($this->getEntityAttributeTypes()) {
                return $this->getAllEntityIdsAndAttributeValues();
            } else {
                $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Entity Attribute Types Found";
                return false;
            }
        } else {
            $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Entity Attribute Codes Found";
            return false;
        }
    }
    private function getTableName($ask_tablename){

            if($this->tablenames[$ask_tablename]){
                return $this->tablenames[$ask_tablename]; // cache..
            }
            // serach for eav_attribute table..
            /*Array
            (
                [26] => mage_catalog_eav_attribute
                [27] => mage_customer_eav_attribute
                [18] => mage_eav_attribute
            )*/

            $this->querystring =  'show tables like "%'.$ask_tablename.'" '; // 
            $this->result = mysql_query($this->querystring);
            $mytables=array();
            while ($table= mysql_fetch_assoc($this->result)){
                foreach ($table as $key => $tablename) {
                     $mytables[(strlen($tablename))]=$tablename;
                }
            }
            krsort($mytables);
            $tablename=array_pop($mytables); // take shortest..
            $this->tablenames[$ask_tablename]=$tablename;

            return $tablename;
    }    
    private function getEntityAttributeCodes() {
        if (!isset($this->entity_id)) {
            $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Entity ID defined. ";
            return false;
        } else {
            //$query_prep = 'SELECT attribute_code FROM eav_attribute WHERE entity_type_id = %s;';      
            $query_prep = 'SELECT attribute_code FROM '.$this->getTableName('eav_attribute').' WHERE entity_type_id = %s;';      

            $this->querystring = sprintf($query_prep, $this->real_escape_string($this->entity_id));
            $this->result = mysql_query($this->querystring);
            while ($grid[] = mysql_fetch_assoc($this->result));
            unset($grid[count($grid)-1]);
            $this->entityAttributeCodes = $grid;
            if (is_array($this->entityAttributeCodes)) {
                return $this->entityAttributeCodes;
            } else {
                $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Attribute Codes";
                return false;
            }
        }
    }
    private function getEntityAttributeTypes() {
        if (!isset($this->entityAttributeCodes)) {
            $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Entity Attribute Codes Found. ";
            return false;
        } else {
            $query_prep = "SELECT attribute_code, backend_type FROM ".$this->getTableName('eav_attribute')." WHERE entity_type_id = %s AND attribute_code = '%s';";
            foreach ($this->entityAttributeCodes as $row) {
                $this->querystring = sprintf($query_prep, $this->real_escape_string($this->entity_id), $this->real_escape_string($row['attribute_code']));
                $this->result = mysql_query($this->querystring);
                $attribute_type_row = mysql_fetch_assoc($this->result);
                $attributes_types[$row['attribute_code']] = $attribute_type_row['backend_type'];
            }
            $this->entityAttributeTypes = $attributes_types;
            return $this->entityAttributeTypes;
        }
    }
    private function getAllEntityIdsAndAttributeValues() {
        if (!isset($this->entityAttributeTypes)) {
            $this->errorstack[] = __FILE__.", Zeile ".__LINE__." - "."No Entity Attribute Types Found";
            return false;
        } else {
            $query_prep = "SELECT e.entity_id AS ".$this->entity_name."_id, var.value AS ".$this->entity_name."_%s
                         FROM ".$this->getTableName("catalog_".$this->entity_name."_entity")." e, ".$this->getTableName('eav_attribute')." eav, %s var 

                            WHERE 
                                e.entity_type_id = eav.entity_type_id 
                                AND eav.attribute_code = '%s'
                                AND eav.attribute_id = var.attribute_id
                                AND var.entity_id = e.entity_id";

            foreach ($this->entityAttributeTypes as $attribute_name => $attribut_type) {
                $attributesByType[$attribut_type][] = $attribute_name;
                //$this->querystring = sprintf($query_prep, $this->real_escape_string($attribute_name), $this->real_escape_string($attribut_type), $this->real_escape_string($attribute_name));
                $this->querystring = sprintf($query_prep, $this->real_escape_string($attribute_name), $this->getTableName("catalog_".$this->entity_name."_entity_$attribut_type"), $this->real_escape_string($attribute_name));
                $this->result = mysql_query($this->querystring); // or die(mysql_error());
                if ($this->result) {
                    while ($row = mysql_fetch_assoc($this->result)) {
                        $entity[$row[$this->entity_name.'_id']][$attribute_name] = $row[$this->entity_name.'_'.$attribute_name];
                        $entity[$row[$this->entity_name.'_id']][$this->entity_name.'_id'] = $row[$this->entity_name.'_id'];
                    }
                }
            }

            $this->AttributesByType['int'] = array_flip($attributesByType['int']);
            $cols = $this->getAllProductAttributes();
            $cols[] = $this->entity_name.'_id';
            foreach ($entity as $row) {
                foreach ($cols as $col) {
                    $temp_row[$col] = $this->$col($row[$col]);
                    $temp_row[$col] = str_replace(array("\r\n", "\r", "\n"), "", $temp_row[$col]); //TODO: Loosing Information?!
                    $temp_row[$col] = str_replace(";", ":", $temp_row[$col]); //TODO: Loosing Information?!
                }
                $temp_entity[] = $temp_row;
                unset($temp_row);
            }
            return $temp_entity;
        }
    }
    public function getAllProductAttributes() {
        foreach ($this->getEntityAttributeCodes(PRODUCTS_ENTITY_ID) as $attribute) {
            $array[] = $attribute['attribute_code'];
        }
        return $array;
    }
    public function __call($method, $arguments) {
        $value = strip_tags(str_replace(";",":",html_entity_decode($arguments[0])));
        setlocale(LC_ALL, LOCALE);        
        switch($method) {
            case "price":
            case "special_price":
            case "cost":
                if ($value != "") {
                    $return = money_format('%i', $value);
                    if (substr($return, 0, 3) == "EUR") {
                        $return = str_replace("EUR", "", $return)." €";
                    }
                    return $return;
                } else {
                    return $value;
                }
                break;
            case "weight":
                if ($value != "") {
                    return number_format($value, 2, ",", ".")." kg";
                } else {
                    return $value;
                }
                break;
            case "image":
            case "small_image":
            case "thumbnail":
                if ($value != "") {
                    return "http://www.".URL."/media/catalog/product".$value;
                } else {
                    return $value;
                }
            case "url_path":
                return "http://www.".URL."/".$value;
            case "meta_keyword":
                return str_replace(array("\r\n", "\r", "\n"), ", ", $value);
                break;
            default:
                if ((preg_match("/^([0-9]+,?)+$/",$value) && strstr($value, ",")) || (isset($_SESSION['is_multifield']) && array_key_exists($method, $_SESSION['is_multifield']))) {
                    $_SESSION['is_multifield'][$method] = "";
                    $multi_values = explode(",",$value);
                    foreach ($multi_values as $multi_value_id) {
                        $multi_value_array[] = $this->getAttributNameById($multi_value_id);
                    }
                    return implode(",", $multi_value_array);
                } else {
                    if (is_array($this->AttributesByType['int']) && array_key_exists($method, $this->AttributesByType['int'])) {
                        $return = $this->getAttributNameById($value);
                        if ($return == "") {
                            return $value;
                        } else {
                            $array['id'] = $value;
                            $array['value'] = $this->getAttributNameById($value);
                            return $array; 
                        }
                    } else {
                        return $value;
                    }
                }
        }
    }
    private function getAttributNameById($id) {
        $query_prep = "SELECT value FROM ".$this->getTableName('eav_attribute_option_value')." WHERE option_id = %s"; // AND store_id = ".STORE_ID;
        $this->querystring = sprintf($query_prep, $this->real_escape_string($id));
        $this->result = mysql_query($this->querystring);
        if (is_object($this->result)) {
            $value = mysql_fetch_assoc($this->result);
            return $value['value'];
        } else {
            return $id;
        }
    }
    public function getProductCategories($product_id) {
        $query_prep = "SELECT
                            group_concat(ccev.value)
                       FROM 
                       ".$this->getTableName('catalog_category_product')."  AS ccp,
                       ".$this->getTableName('catalog_category_entity_varchar')."  AS ccev,
                       WHERE 
                            ccp.product_id = %s AND
                            ccp.category_id = ccev.entity_id AND
                            ccev.store_id = ".STORE_ID." AND
                            ccev.entity_type_id = ".CATEGORIES_ENTITY_ID."
                       GROUP BY 
                            ccp.product_id";
        $this->querystring = sprintf($query_prep, $this->real_escape_string($product_id));
        $this->result = mysql_query($this->querystring);
        if($this->result) {
	        $categories = mysql_fetch_all($this->result);
    	    return $categories[0][0];
    	} else {
    		return "";
    	}
    }
}

define('LOCALE', 'de_DE');
define('STORE_ID', '0');
define('URL', 'http://www.druckerpatronen.de/');
define('MIN_ATTR_TRESHHOLD', 0);
define('PID', 'product_id');
define('DB_HOST', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', '');
define('DB_SYSTEM', 'mysql');

//connecting to Shop DB
$shop = new Magento(DB_HOST, DB_USER, DB_PASS, DB_NAME);

//getting products
$products = $shop->getProducts();
if($shop->errorstack) {
    echo "<pre>"; print_r($shop->errorstack); echo "</pre>";
}

//getting category of product
foreach ($products as $product) {
    $temp_product = $product;
    $temp_product['categories'] = $shop->getProductCategories($product[PID]);
    $temp_products[] = $temp_product;
    unset($temp_product);
}
$products = $temp_products;
unset($temp_products);

echo json_encode($products);

//debug output
//echo "<pre>"; print_r($products); die();

//writing data to output file and echoling link to download
//$content = FileGenerator::prepareProductsFile($products);

//echo utf8_encode($content);

//if ($file_data = FileGenerator::writeFile('products', $content)) {
//    echo "<a href='http://".$file_data['path'].$file_data['filename']."'>Download File <i>".$file_data['filename']."</i></a>";
//}