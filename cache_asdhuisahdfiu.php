<!Doctype HTML>
<html>
<head>
	<title>ClearCache - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>
<body>
<form method="post" action="">

	<h3>Magento Cache Tool</h3>

	<input type="checkbox" name="type[]" value="main_cache" id="main_cache" checked="checked"/>
	<label for="main_cache" title="Löschen bei Änderungen an XML-Dateien!">Main Cache</label><br/>

	<input type="checkbox" name="type[]" value="apc" id="apc" checked="checked"/>
	<label for="sessions" title="Löschen bei Änderungen an XML-Dateien!">Alternative PHP Cache</label><br/>

	<input type="checkbox" name="type[]" value="sessions" id="sessions"/>
	<label for="sessions">Sessions</label><br/>

	<input type="checkbox" name="type[]" value="mediaProductCache" id="mediaProductCache"/>
	<label for="sessions">Product Images Cache</label><br/>

	<br/>
	<input type="submit" value="clear"/>
	<br/><br/>
</form>
</body>
</html>


<?php
define('MAGENTO', realpath(dirname(__FILE__)) . '/');
require_once MAGENTO . '/app/Mage.php';
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$cachePath = "var/cache/";
$sessionsPath = "var/session/";
$mediaProductCachePath = "media/catalog/product/cache/";

if (!empty($_POST['type']))
{
	foreach ($_POST['type'] as $type)
	{
		if ($type == "main_cache")
		{
			echo "<b>Delete Cache:</b><br/>";
			try
			{
				Mage::app()->cleanCache();
				Mage::app()->getCacheInstance()->flush();
				echo 'Successful! <br />';
			}
			catch (Exception $e)
			{
				throw new Exception('ERROR deleting Main Cache: ' . $e->getMessage());
			}


			//deleteFiles($cachePath);
		}
		if ($type == "sessions")
		{
			echo "<b>Delete Sessions:</b><br/>";
			deleteFiles($sessionsPath);
		}
		if ($type == 'apc')
		{
			echo "<b>Alternative PHP Cache:</b><br/>";
			try
			{
				if (function_exists('apc_clear_cache'))
				{
					apc_clear_cache();
					apc_clear_cache('user');
					apc_clear_cache('opcode');
					echo 'Successful!<br />';
				}
				else
				{
					echo 'APC not available on this Server.<br />';
				}
			}
			catch (Exception $e)
			{
				throw new Exception('ERROR deleting Alternative PHP Cache: ' . $e->getMessage());
			}
		}
		if ($type == 'mediaProductCache')
		{
			echo "<b>Produkt Bilder Cache:</b><br/>";
			deleteFiles($mediaProductCachePath);
		}
	}
}

function deleteFiles_old($path)
{
	$files = scandir($path);

	foreach ($files as $file)
	{
		if ($file != '.' && $file != '..')
		{
			if (is_dir($path . $file))
			{
				if (rmdir($path . $file))
				{
					echo "Dir: $path.$file has been deleted<br/>";
				}
				else
				{
					echo "Dir: " . $path . $file . " could not be deleted<br/>";
				}
			}
			else
			{
				if (unlink($path . $file))
				{
					echo "File: " . $path . $file . " has been deleted<br/>";
				}
				else
				{
					echo "File: " . $path . $file . " could not be deleted<br/>";
				}
			}
		}

	}
}

function deleteFiles($path)
{
	chmod($path, 777);
	$handle = opendir($path);
	while (($file = readdir($handle)) !== false)
	{
		if ($file != '.' && $file != '..')
		{
			$filepath = $path ."/". $file;
			if (is_dir($filepath))
			{
				if (rmdir($filepath))
				{
					echo '<span style="color: #0f0;">Dir: ' . $filepath . ' has been deleted</span><br/>';

				}
				else
				{
					echo '<span  style="color: #f00;">Dir: ' . $filepath . ' could not be deleted</span><br/>';
					deleteFiles($filepath);
				}

			}
			else
			{
				if (unlink($filepath))
				{
					echo '<span  style="color: #0f0;">File: ' . $filepath . ' has been deleted</span><br/>';
				}
				else
				{
					echo '<span  style="color: #f00;">File: ' . $filepath . ' could not be deleted</span><br/>';
				}
			}
		}
	}
	closedir($handle);
}

?>
