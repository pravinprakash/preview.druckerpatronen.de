#!/usr/bin/env php
<?php 
/**
 * Actindo Faktura/WWS Connector
 * CronJob Caller
 * @copyright Copyright (c) 2013, Actindo GmbH (http://www.actindo.de)
 * @author Daniel Haimerl
 * @author Patrick Prasse
 * @author Christopher Westerfield
 * @uses Mage_Core_Controller_Front_Action
 * @version 2.309
 */
//Init Mage API
require 'app/Mage.php';
//use session in url disabled
Mage::app('admin')->setUseSessionInUrl(false);
//set to admin context
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//load Observer
$index = Mage::getModel('connector/index_observer');
//rebuild Index
$index->rebuildIndex();
//done
$sql = '
INSERT IGNORE INTO
	dpm_ac_mapper
	
(value,type,site)

SELECT
	pev.value,
	IF(ea.attribute_code = \'delivery_time\',\'shipping\',IF(ea.attribute_code = \'manufacturer\',\'manufacturers\',\'\')) as type,
	1 as site
FROM
	dpm_catalog_product_entity_varchar pev
LEFT JOIN
	dpm_eav_attribute ea
	ON
	ea.attribute_id=pev.attribute_id
WHERE
	ea.entity_type_id=4
	and
	(
		ea.attribute_code=\'delivery_time\'
		or
		ea.attribute_code=\'manufacturer\'
	)
GROUP BY
	pev.value
;
';
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$writeConnection->query($sql);
