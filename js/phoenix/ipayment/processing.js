/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @copyright  Copyright (c) 2009 Phoenix Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 */

/**
 * Add wrapper to the original payment.save method.
 * The payment object will be available after the page has been loaded.
 * @see: opcheckout.js payment prototype
 */
function ipaymentObserver(ev) {
    if (payment.currentMethod == 'ipayment_cc') {
        var ipayment = new ipaymentMultishipping();
        if ($('ipayment_cc_request_status').value != 'SUCCESS' ) {
            ipayment.save();
            Event.stop(ev);
        }
    }
    if (payment.currentMethod == 'ipayment_elv') {
        var ipayment = new ipaymentMultishipping();
        if ($('ipayment_elv_request_status').value != 'SUCCESS' ) {
            ipayment.save();
            Event.stop(ev);
        }
    }
}

Event.observe(window, 'load', function() {
    Validation.creditCartTypes.set('VI', [new RegExp('^4[0-9]{12}([0-9]{3}|[0-9]{6})?$'), new RegExp('^[0-9]{3}$'), true]);
    var form = $('multishipping-billing-form');
    if (form) {
        Event.observe(form, 'submit', ipaymentObserver)
        return;
    }    
	payment.save = payment.save.wrap(function(origSaveMethod){
		if (this.currentMethod && this.currentMethod.substr(0,9) == 'ipayment_') {
			if (checkout.loadWaiting!=false) return;
			var validator = new Validation(this.form);
			if (this.validate() && validator.validate()) {
				checkout.setLoadWaiting('payment');
				$('loadingpage').setStyle({ display: 'block' });
				if (this.currentMethod.substr(9) == 'cc') {
                    var application = new ipaymentApplication();
                    var data = application.buildCcData();
					// Pixelmechanics FL 14.12.2012: Kreditkarte gefixt
					var cc_trxuser_id = 10794;
                    application.sendIpaymentRequest(data, cc_trxuser_id, 'processOnepagecheckoutResponse');
                } else if (this.currentMethod.substr(9) == '3ds') {
                    var application = new ipaymentApplication();
                    var data = application.buildCcData();
                    application.sendIpaymentRequest(data, _3ds_trxuser_id, 'processOnepagecheckoutResponse');
				} else {
                    var application = new ipaymentApplication();
                    var data = application.buildElvData();
                    //alert('test');
                    //alert(elv_trxuser_id);
                    // die elv_trxuser_id ist anscheinend nur vorhanden, wenn der magento user nicht als gast ohne cookie im checkout ist
                    var elv_trxuser_id = 10794;
                    application.sendIpaymentRequest(data, elv_trxuser_id, 'processOnepagecheckoutResponse');
                }
			}
		} else {
			origSaveMethod();
		}
	});
});

function ipaymentSwitchEditFields(method, mode)
{
	if ((typeof mode == 'undefined') || (mode == 'enable')) {
		$(method+'_edit_switch').style.display = 'none';
		mode = false;
	} else {
		$(method+'_edit_switch').style.display = '';
		mode = true;
	}

	var form = $('payment_form_'+payment.currentMethod);
	var elements = form.getElementsByClassName('no-submit');
	for (var i=0; i<elements.length; i++) elements[i].disabled = mode;
}


var ipaymentMultishipping = Class.create();
ipaymentMultishipping.prototype = {
     initialize: function(){
         this.form = $('multishipping-billing-form');
     },
     validate: function() {
        var methods = document.getElementsByName('payment[method]');
        if (methods.length==0) {
            alert(Translator.translate('Your order can not be completed at this time as there is no payment methods available for it.'));
            return false;
        }
        for (var i=0; i<methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert(Translator.translate('Please specify payment method.'));
        return false;
    },
    save: function() {        
        var validator = new Validation(this.form);
        if (this.validate() && validator.validate()) {
            if (payment.currentMethod.substr(9) == 'cc') {
                var application = new ipaymentApplication();
                var data = application.buildCcData();
                application.sendIpaymentRequest(data, cc_trxuser_id, 'processMultishippingCcResponse');
                ipaymentSwitchEditFields(payment.currentMethod, 'disable');
                Element.show('ipayment-cc-please-wait');
            }
            if (payment.currentMethod.substr(9) == 'elv') {
                var application = new ipaymentApplication();
                var data = application.buildElvData();
                application.sendIpaymentRequest(data, elv_trxuser_id, 'processMultishippingElvResponse');
                Element.show('ipayment-elv-please-wait');
            }
        }
    }
}



var ipaymentApplication = Class.create();
ipaymentApplication.prototype = {
     initialize: function(){
     },
     buildCcData: function() {
        var data = {
            trx_paymenttyp:		'cc',
            addr_name:			$(payment.currentMethod+'_cc_owner').value,
            cc_number:			$(payment.currentMethod+'_cc_number').value,
            cc_expdate_month:	$(payment.currentMethod+'_expiration').value,
            cc_expdate_year:	$(payment.currentMethod+'_expiration_yr').value,
            cc_checkcode:		$(payment.currentMethod+'_cc_cid').value,
            error_lang:			document.getElementsByTagName('html')[0].getAttribute('lang')
        }
        return data;
     },
     buildElvData: function() {
        var data = {
            trx_paymenttyp:		'elv',
            addr_name:			$(payment.currentMethod+'_owner').value,
            bank_code:			$(payment.currentMethod+'_bank_code').value,
            bank_name:			$(payment.currentMethod+'_bank_name').value,
            bank_country:		'DE',
            bank_accountnumber:	$(payment.currentMethod+'_account_number').value,
            error_lang:			document.getElementsByTagName('html')[0].getAttribute('lang')
        }
        //alert(data.addr_name + '\n' + data.bank_code + '\n' + data.bank_name + '\n' + data.bank_country + '\n' + data.bank_accountnumber + '\n' + data.error_lang);
        return data;
     },
     sendIpaymentRequest: function (data, trxuser_id, callback ){
        // kommt nicht an!
        // alert('test');
        //alert(trxuser_id + callback);
        var request = new IpaymentRequest(
            data, {
                return_type : 'object',
                callback_function_name : callback
            }, trxuser_id);
        request.checkAndStore();
     }
}

function processOnepagecheckoutResponse(response) {
    // kommt nicht an!

    processResponse (response);

    if (payment.currentMethod.substr(9) == 'cc' || payment.currentMethod.substr(9) == '3ds')
        ipaymentSwitchEditFields(payment.currentMethod, 'disable');

    if(response.get('ret_status') == 'SUCCESS') {
        if (response.get('paydata_bank_name'))
            document.getElementById('ipayment_elv_bank_name').value = response.get('paydata_bank_name');

        var request = new Ajax.Request(
            payment.saveUrl,
            {
                method:'post',
                onComplete: payment.onComplete,
                onSuccess: payment.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(payment.form)
            }
        );
    }else{
	    $('loadingpage').setStyle({ display: 'none' });
    }
}

function processMultishippingCcResponse (response) {
    $('ipayment_cc_request_status').value = response.get('ret_status');
    Element.hide('ipayment-cc-please-wait');
    processResponse (response);
    if(response.get('ret_status') == 'SUCCESS') {
        // Disable form fields before submit.
        var form  = $('multishipping-billing-form');
        var elements = form.getElementsByClassName('no-submit');
        for (var i=0; i<elements.length; i++) elements[i].disabled = 'disable';
        var f = document.getElementById('multishipping-billing-form');
        f.submit();
    }
    if(response.get('ret_status') == 'ERROR') {
        ipaymentSwitchEditFields(payment.currentMethod, 'enable');
    }
}

function processMultishippingElvResponse (response) {
    $('ipayment_elv_request_status').value = response.get('ret_status');
    Element.hide('ipayment-elv-please-wait');
    processResponse (response);
    if(response.get('ret_status') == 'SUCCESS') {
        var f = document.getElementById('multishipping-billing-form');
        f.submit();
    }
}


function processResponse (response) {
    if(response.get('ret_status') == 'SUCCESS') {
        prepareSubmitedFields(response);
    } else if (response.get('ret_status') == 'ERROR') {
        processError(response);
    } else {
        alert('Payment status "'+response.get('ret_status')+'" not implemented yet.');
    }
}


function processError(response) {
    alert(response.get('ret_errormsg'));
    checkout.setLoadWaiting(false);
    switch (response.get('ret_errorcode')) {
        case '5000':
        case '5009':
            // name wrong
            break;
        case '5002':
        case '5007':
            // cc number wrong
            break;
        case '5003':
        case '5004':
            // cc date wrong
            break;
        case '5005':
        case '5006':
            // ccv wrong
                break;
        }
}

function prepareSubmitedFields(response) {
    var form = $('payment_form_'+payment.currentMethod);
    var elements = form.getElementsBySelector('input');
    for (var i=0; i<elements.length; i++) {
        if (elements[i].type == 'hidden') {
            switch (elements[i].name) {
                case 'payment[cc_owner]':
                    elements[i].value = $(payment.currentMethod+'_cc_owner').value;
                    break;
                case 'payment[cc_type]':
                    elements[i].value = $(payment.currentMethod+'_cc_type').value;
                    break;
                case 'payment[cc_number]':
                    elements[i].value = response.get('paydata_cc_number');
                    break;
                case 'payment[cc_exp_month]':
                    elements[i].value = $(payment.currentMethod+'_expiration').value;
                    break;
                case 'payment[cc_exp_year]':
                    elements[i].value = $(payment.currentMethod+'_expiration_yr').value;
                    break;
                case 'payment[additional_data]':
                    elements[i].value = response.get('storage_id');

// Pixelmechanics FL 14.12.2012: Kreditkarte gefixt
if(document.getElementById('payment_additional_data_cc')) {
	$('payment_additional_data_cc').value = response.get('storage_id');
}
if(document.getElementById('payment_additional_data_elv')) {
	$('payment_additional_data_elv').value = response.get('storage_id');
}

                    break;
            }
        }
    }
}
