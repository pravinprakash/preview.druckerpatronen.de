/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Bechlem
 * @package     Karliuka_Bechlem
 * @copyright   Copyright (c) 2014 Karliuka Vitalii (karliuka.vitalii@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
 
var Bechlem = new Class.create();

Bechlem.prototype = 
{
    initialize : function()
	{},

    import: function (url) 
	{	
		var store;
		if ($('store_switcher') && $('store_switcher').value) store = $('store_switcher').value;
		new Ajax.Request(url, {
			onSuccess: function(transport) {
				//location.reload();
				this.process(transport, url);
			}.bind(this),
			method: 'post',
			parameters: {store:store}
		});
        return false;
    },
	process: function(transport, url) 
	{
        if (!transport.responseText.isJSON()) return;
        var json = transport.responseText.evalJSON();
		if (json.error && json.error == '1') return;
		if (json.action) 
		{
			console.log(json);
			if (json.action == 'import') this.import(url);
			if (json.action == 'end') location.reload();
		}
		return;
    },	
}
Bechlem = new Bechlem();