<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
umask(0);

ignore_user_abort(true);
set_time_limit(0);
		
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
require 'app/Mage.php';

try {
    $app = Mage::app('default');
	
	$count = 0;
	$limit = 3;
	/**
     * Get the resource model
    */
    $resource = Mage::getSingleton('core/resource');
	
    $read  = $resource->getConnection('core_read');
    $write = $resource->getConnection('core_write');
	
	/**
     * Read product table
    */	
	$query = 'SELECT * FROM ' . $resource->getTableName('products_export');
    $results = $read->fetchAll($query);	
	$categories = array();
	
	foreach($results as $row){
		$count++;
		//if($count > $limit) break;
		
		$pair = explode(';;', $row['categories']);
		
		foreach($pair as $element)
		{
			$like = $element;
			$element = str_replace('[[[[', '/', $element);
			$data = explode('[[', $element, 2);
			if(empty($data[1])) continue;
			
			$title = $description = '';
			
			$query = 'SELECT `sku`, `price` FROM ' . $resource->getTableName('products_export') . " WHERE `categories` LIKE '%{$like}%' ORDER BY `price` LIMIT 1";
			$res = $read->fetchRow($query);
			
			$price = str_replace('.', ',', round($res['price'] * 1.19, 2));
			$category = str_replace('/', '&#047;', $data[0]) . '/' . str_replace('/', '&#047;', $data[1]);
			
			if($row['type2'] == 'toner') $description = "Toner und Druckerzubehör für {$data[0]} {$data[1]} günstig kaufen im Shop auf Druckerpatronen.de - finden Sie immer die richtigen Toner & Lasertoner";	
			elseif($row['type2'] == 'druckerpatrone') $description = "Druckerpatronen und Tintenpatronen für {$data[0]} {$data[1]} günstig kaufen im Shop auf Druckerpatronen.de - finden Sie immer die richtige Druckerpatrone";	
			elseif($row['type2'] == 'farbband') $description = "Farbbänder für {$data[0]} {$data[1]} günstig kaufen im Shop auf Druckerpatronen.de - finden Sie immer die richtigen Farbbänder";
			
			if($row['type2'] == 'toner') $title = "{$data[0]} {$data[1]} Toner & Druckerzubehör ab {$price} EUR";	
			elseif($row['type2'] == 'druckerpatrone') $title = "{$data[0]} {$data[1]} Druckerpatronen & Tintenpatronen ab {$price} EUR";	
			elseif($row['type2'] == 'farbband') $title = "{$data[0]} {$data[1]} Farbbänder ab {$price} EUR";

			
			
			$query = "REPLACE INTO " . $resource->getTableName('products_categories') . 
				" (`store`, `categories`, `meta_title`, `meta_description`, `headline`, `printer_manufacturer`, `search`, `type`, `default_product`)" . 
				" VALUES ('default', '{$category}', '{$title}', '{$description}', '{$data[0]} {$data[1]}', '{$data[0]}', '" . preg_replace("/\s+/", '', $data[0] . $data[1]) . "', '{$row['type2']}', '{$res['sku']}')";

			$write->query($query);
		}
	}
	
	$models = array();
	
	$query = 'SELECT * FROM ' . $resource->getTableName('products_categories');
    $results = $read->fetchAll($query);	
	
	foreach($results as $row){
		$data = explode('/', $row['categories'], 2);
		foreach($data as $key => $part){
			$data[$key] = str_replace('&#047;', '/', $part);
		}
		list($printer, $model) = $data;
		$models[$printer][$model] = $row;
	}
	
	$import = array();
	
	foreach ($models as $printerName => $printerModels)
	{
		$import = $temp = $printerModels;
		
		$printer = Mage::getModel('catalog/category')->loadByAttribute('name', $printerName);
		if(empty($printer) || !$printer->getId()) continue;
		
		//array consisting of all child categories id
		$children = $printer->getResource()->getAllChildren($printer); 
		if(0 < count($children))
		{
			foreach ($children as $childId){
				$child = Mage::getModel('catalog/category')->load($childId);
				foreach($temp as $name => $data){
					if(strtolower($name) == strtolower($child->getName())){
						$child
						->setName($name)
						->setIsActive(1)
						->setIsAnchor(1)
						->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
						->setPrinterManufacturer($data['printer_manufacturer'])
						->setIncludeInMenu(1)
						->setMetaTitle($data['meta_title'])
						->setMetaDescription($data['meta_description'])
						->setMetaKeywords($data['meta_keywords'])
						->setHeadline($data['headline'])
						->setSearch($data['search'])
						->setType($data['type'])
						->setSerie($data['serie'])
						->setDefaultProduct($data['default_product'])
						->save();
						unset($temp[$name]);
					}
				}
			}
		}
		foreach($temp as $name => $data){
			$child = Mage::getModel('catalog/category');
			$child
				->setName($name)
				->setIsActive(1)
				->setIsAnchor(1)
				->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
				->setPrinterManufacturer($data['printer_manufacturer'])
				->setIncludeInMenu(1)
				->setMetaTitle($data['meta_title'])
				->setMetaDescription($data['meta_description'])
				->setMetaKeywords($data['meta_keywords'])
				->setHeadline($data['headline'])
				->setSearch($data['search'])
				->setType($data['type'])
				->setSerie($data['serie'])
				->setDefaultProduct($data['default_product'])
				->save();
				
			Mage::getResourceModel('catalog/category')->changeParent($child, $printer);	
		}
		break;
	}
	
	echo '<pre>';
	print_r($models);
	echo '</pre>';	
	
	echo 'end';
	//echo '<pre>';
	//print_r($printers);

} catch (Exception $e) {
    Mage::printException($e);
}

exit(1); 
