<?php
require_once 'app/Mage.php';
umask(0);
error_reporting(0);

$baseurl = Mage::app()->setCurrentStore(1)->getStore()->getBaseUrl();
Mage::getSingleton('customer/session', array('name'=>'frontend'));

# phpinfo(); exit;
# voraussetzung fuer aendern der php.ini: safe_mode muss deaktiviert sein!
# wichtig: 256M haben das letzte mal ausgereicht, trotzdem besser 512M verwenden!
# total: 206.635994673 s nach test
//if(! ini_set("memory_limit","2048M")) echo "changing memory_limit failed<br>\n";
//if(! ini_set("max_execution_time","3000000")) echo "changing max_execution_time failed<br>\n";

if($_GET['type'] == "druckerpatronen") {
    $folder = "cache/druckerpatronen/";
    $type = "druckerpatronen";
    $attrToFilter = "druckerpatrone";
    $append = "#druckerpatronen";
} else if($_GET['type'] == "toner") {
    $folder = "cache/toner/";
    $type = "toner";
    $attrToFilter = "toner";
    $append = "#toner";
} else if($_GET['type'] == "zubehoer") {
    $folder = "cache/zubehoer/";
    $type = "zubehoer";
    $attrToFilter = "extra";
    $append = "#extra";
} else if($_GET['type'] == "categories") {
    $folder = "cache/categories/";
    $type = "";
    $attrToFilter = "";
    $append = "";
} else exit;

if(! file_exists($folder)) mkdir($folder);
$i = 0;
$g = 0;

# manufacturer overview begin

$time_a[$i] = microtime(true);
$cats2 = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getChildrenCategories(); # um einiges schneller als getStoreCategories()

$ids2 = array();
foreach($cats2 as $cat2) {
    array_push($ids2, $cat2->getId());
}
$cats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($ids2)->addOrderField('name');

$topcat_ids = array(3, 144, 153, 260, 290, 296, 298, 429, 764, 784, 786, 837, 1066, 2557, 2657, 3196, 3412);

# erst id-filter und dann select: 5 sek.; erst select und dann id-filter: 1 sek.
$topcats = Mage::getResourceModel('catalog/category_collection')->addAttributeToSelect('name')->addIdFilter($topcat_ids)->addOrderField('name');

if(! $type) {
    $anzahl = count($cats);
    $x = ceil($anzahl / 3);
    $k = 0;
    $inhalt = "<ul id=\"manufacturer-list-".$g."\">\n";
    
    foreach($topcats as $topcat) {
        $inhalt.= "<li><a href=\"".$topcat->getUrl()."/".$append."\">".$topcat->getName()."</a></li>\n";
        $k++;
    }
    $inhalt.= "<li>----------------</li>\n";
    $k++;

    foreach ($cats as $cat) {
        $cat_id = $cat->getId();
        if($k % $x == 0 && $k != 0) {
            $g++;
            $inhalt.= "</ul>\n<ul id=\"manufacturer-list-".$g."\">\n";
        }
   
        $cat_url = $cat->getUrl()."/";
        if($cat->hasChildren() && ! in_array($cat_id, $topcat_ids)) {
            #$id = $cat->getId();
            #$url = Mage::getModel('catalog/category')->load($id)->getUrl(); /* sehr umstaendlich aber anscheinend unumgaenglich */
            $inhalt.= "<li><a href=\"".$cat_url.$type."\">".$cat->getName()."</a></li>\n";
            $k++;
        }
    }
    $inhalt.= "</ul>\n<div class=\"clearer\"></div>";

    $fp = fopen($folder."/0", w);
    fwrite($fp, $inhalt);
    fclose($fp);

    $time_b[$i] = microtime(true);
    $time[$i] = ($time_b[$i] - $time_a[$i]);

    echo "cached manufacturer overview successfully, ".$time[$i]." s needed<br>\n";
    $i++;
} else {
$countprinters = array();
$cat_flag = array();
$countmanufacturers = 0;
foreach ($cats as $cat) {
    $cat_id = $cat->getId();
    if($cat->hasChildren()) {
        $subcats2 = $cat->getChildrenCategories();

        $ids = array();
        foreach($subcats2 as $subcat2) {
            array_push($ids, $subcat2->getId());
        }
        $subcats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($ids)->addOrderField('name');

        foreach ($subcats as $subcat) {
            $products = Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($subcat)->addAttributeToFilter('type2', $attrToFilter);
            $product_flag = 0;
            $product_id = $products->getAllIds(1);
#if($subcat->getName() == "Pixma IP 2200" && $attrToFilter == "toner") {
#	echo "test<br><br>".(isset($product_id[0]) == false)."</br></br>test"; exit;
#} 
            if(isset($product_id[0])) {
                $product_flag = 1;
            }
            if($product_flag) { # mind. 1 produkt in subkategorie vorhanden
                $countprinters[$cat_id]++;
                $subcat_id = $subcat->getId();
                if(empty($cat_flag[$cat_id])) {
                    $countmanufacturers++;
                    $cat_flag[$cat_id] = true;
                }
            } else { # kein produkt in subkategorie vorhanden
                continue;
            }
        }
    }
}
}
$countmanufacturers++; # wegen $inhalt2.= "<li>----------------</li>\n";
$noProductsFlag = 0;
if($type) $inhalt2 = "<ul id=\"manufacturer-list-".$g."\">\n";
if($type) {
    $y = ceil($countmanufacturers / 3);
    $k = 0;
}
$cat_flag = array();

foreach($topcats as $topcat) {
    $inhalt2.= "<li><a href=\"".$topcat->getUrl()."/".$append."\">".$topcat->getName()."</a></li>\n";
    $k++;
}
$inhalt2.= "<li>----------------</li>\n";
$k++;

foreach($cats as $cat) {
    $time_a[$i] = microtime(true);
    $cat_id = $cat->getId();
    $cat_name = $cat->getName();
    $cat_url = $cat->getUrl()."/";
    $f = 0;
    if($type) $hasChildren = $countprinters[$cat_id]; else $hasChildren = $cat->hasChildren();
    $inhalt = "";
    
    if($hasChildren) { # falls Unterkategorien vorhanden sind
        $subcats2 = $cat->getChildrenCategories();

        $ids = array();
        foreach($subcats2 as $subcat2) {
            array_push($ids, $subcat2->getId());
        }
        $subcats = Mage::getResourceModel('catalog/category_collection')->addIdFilter($ids)->addOrderField('name');
        if($type) $anzahl = $countprinters[$cat_id]; else $anzahl = count($subcats);
        $x = ceil($anzahl / 4);
        $j = 0;
        $inhalt = "<ul id=\"printer-list-".$f."\">\n";

        foreach ($subcats as $subcat) {
            $subcat_id = $subcat->getId();
            $subcat = Mage::getModel('catalog/category')->load($subcat_id);
            if($j % $x == 0 && $j != 0 && $noProductsFlag != 1) {
                $f++;
                $inhalt.= "</ul>\n<ul id=\"printer-list-".$f."\">\n";
            }
            $noProductsFlag = 0;
            if($j % 2 == 0) $class = "odd"; else $class = "even";
            $subcat_name = $subcat->getName();
            $subcat_url = $subcat->getUrl()."/";
            $subcat_default_product_sku = $subcat->getDefaultProduct();
            # O(n) auf O(1) reduzierbar, bloss wie? <- aber hier nicht so wichtig, da die collections sehr schnell erstellt werden
            $products = Mage::getResourceModel('catalog/product_collection')
            ->addCategoryFilter($subcat)
            ;
            if($attrToFilter) $products = $products->addAttributeToFilter('type2', $attrToFilter);
            $product_flag = 0;
            $product_id = $products->getAllIds(1);
            if(isset($product_id[0])) {
                $product_flag = 1;
            }
#if($subcat->getName() == "Pixma IP 2200" && $attrToFilter == "toner") {
#	echo "test<br><br>".$product_id[0]."</br></br>test"; exit;
#} 
            if($product_flag) { # mind. 1 produkt in subkategorie vorhanden
                $inhalt.= "<li class=\"".$class."\"><a href=\"".$subcat_url."\">".$subcat_name."</a></li>\n";
                if($type && empty($cat_flag[$cat_id]) && ! in_array($cat_id, $topcat_ids)) {
                    if($k % $y == 0 && $k != 0) {
                        $g++;
                        $inhalt2.= "</ul>\n<ul id=\"manufacturer-list-".$g."\">\n";
                    }
                    $inhalt2.= "<li><a href=\"".$cat_url.$append."\">".$cat_name."</a></li>\n";
                    $cat_flag[$cat_id] = true;
                    $k++;
                }
            } else { # kein produkt in subkategorie vorhanden
                if($type) {
                    $noProductsFlag = 1;
                    continue;
                } else {
                    $inhalt.= "<li class=\"".$class."\"><a href=\"".$subcat_url."\">".$subcat_name."</a></li>\n";
                }
            }
            $j++;
        }
        $inhalt.= "</ul>\n<div class=\"clearer\"></div>";
    }
    
    $fp = fopen($folder."/".$cat_id, w);
    fwrite($fp, $inhalt);
    fclose($fp);
    
    $time_b[$i] = microtime(true);
    $time[$i] = ($time_b[$i] - $time_a[$i]);
    echo "cached category '".$cat_name."' successfully, ".$time[$i]." s needed<br>\n";
    $i++;
}
if($type) {
    $inhalt2.= "</ul>\n<div class=\"clearer\"></div>";

    $fp = fopen($folder."/0", w);
    fwrite($fp, $inhalt2);
    fclose($fp);
    echo "cached manufacturer overview for '".$type."' successfully<br>\n";
}
echo "total: ".array_sum($time)." s"
?>
