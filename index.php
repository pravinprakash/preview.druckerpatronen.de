<?php


/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
if(!function_exists("preprint")) {
	function preprint($s) {
		echo "<pre>";
		print_r($s);
		echo "</pre>";
	}
}
//echo $_SERVER['REQUEST_URI'];//exit;
if ($_SERVER['REQUEST_URI'] == '/umschläge/versandtaschen'||$_SERVER['REQUEST_URI'] == '/umschläge/versandtaschen/'||$_SERVER['REQUEST_URI'] == '/umschl%c3%a4ge/versandtaschen/'){
	header("Location: /umschlaege/versandtaschen/");
	exit;
}
if ($_SERVER['REQUEST_URI'] == '/umschläge/fensterbriefumschläge'||$_SERVER['REQUEST_URI'] == '/umschläge/fensterbriefumschläge/'||$_SERVER['REQUEST_URI'] == '/umschl%c3%a4ge/fensterbriefumschl%c3%a4ge/'){
	header("Location: /umschlaege/fensterbriefumschlage-dl/");
	exit;
}
	/**
	 * Wandelt geladene Datensätze so um, dass sie assoziativ über die ID zugreifbar sind.
	 * Welche ID es ist, wird mit dem Parameter $key_field_name festgelegt
	 * $flat = true bewirkt, dass Einträge zu $return[$id] hinzugefügt werden (zB $return[$id][0] und $return[$id][1], usw).
	 *
	 * @param array $rows
	 * @param string $key_field_name
	 * @return array
	 */
	    function create_select_array($rows, $key_field_name, $flat = true, $flat_field_name="") {
	    	$return = array();
	    	foreach ($rows as $row) {
	    		$id = $row[$key_field_name];
	    		
	    		if ($flat) {
					if ($flat_field_name!=="" && isset($row[$flat_field_name])) $return[$id] = $row[$flat_field_name];
					else $return[$id] = $row;
	    		}
	    		else {
	    			if (!isset($return[$id])) $return[$id] = array();
	    			$return[$id][] = $row;
	    		}
	    	}
	    	return $return;
	    }
	    
	    
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.2.0 or newer.
<a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a>
 Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

/**
 * Error reporting
 */
error_reporting(E_ALL | E_STRICT);
// Error Reporting komplett abschalten
//error_reporting(0);
/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile)) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}
require_once $mageFilename;
#Varien_Profiler::enable();
if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}

ini_set('display_errors', 1);

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
Mage::run($mageRunCode, $mageRunType);
 /* hack http end */

if(!(strpos($_SERVER['REQUEST_URI'], ".html")
  || strpos($_SERVER['REQUEST_URI'], "/?")
  || substr($_SERVER['REQUEST_URI'], -1) == "/")) {
    $fp = fopen($_SERVER["DOCUMENT_ROOT"]."/uri-q7djj4.txt","a+");
    fputs($fp,"URI: ".$_SERVER['REQUEST_URI']." (Referer: ".getenv("HTTP_REFERER").")\n");
    fclose($fp);
}

if(strpos($_SERVER['REQUEST_URI'], "?")){
  // 1. Affili
  // 2. Adcell
  // 3. Criteo
  // 4. Interactive
  
	$zeichenkette = $_SERVER['REQUEST_URI'];
    $suchmuster = "i=1";
	$suchmuster2= "i=2";
	$suchmuster3= "i=3";
	$suchmuster4= "i=4";
	
    if ( strpos($zeichenkette,$suchmuster )) { 
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
        $session->setData("network", 1);
       } elseif ( strpos($zeichenkette,$suchmuster2 ))  {
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend")); 
        $session->setData("network", 2);
	    } elseif ( strpos($zeichenkette,$suchmuster3 )) {
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend")); 
        $session->setData("network", 3);
        } elseif ( strpos($zeichenkette,$suchmuster4 )) {
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend")); 
        $session->setData("network", 4);
        } else {
	    //$session->setData("network", 0);
	    }
   
}
